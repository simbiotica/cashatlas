<?php

namespace Simbiotica\CalpBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Simbiotica\CartoDBBundle\CartoDBLink\Mapping as CartoDB;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\Table(name="coordination_mechanism")
 * @Gedmo\Loggable(logEntryClass="Simbiotica\CalpBundle\Entity\Logs\CoordinationMechanismLogs")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 * @Gedmo\TranslationEntity(class="Simbiotica\CalpBundle\Entity\Translations\CoordinationMechanismTranslation")
 */

class CoordinationMechanism
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Gedmo\Versioned
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=128, nullable=true)
     * @Groups({"project", "search"})
     */
    protected $name;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectCoordinationMechanism", mappedBy="coordinationMechanism", cascade={"all"})
     */
    protected $projects;
    
    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="createdCoordinationMechanisms")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    protected $createdBy;
    
    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="updatedCoordinationMechanisms")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    protected $updatedBy;
    
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at" ,type="datetime")
     */
    protected $createdAt;
    
    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at" ,type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;
    
    /**
     * @ORM\OneToMany(targetEntity="Simbiotica\CalpBundle\Entity\Translations\CoordinationMechanismTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function __toString()
    {
    	return empty($this->name)?"":$this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct($name  = null)
    {
        if ($name)
            $this->name = $name;
        $this->projects = new ArrayCollection();
        $this->translations = new ArrayCollection();
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Sector
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Sector
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Sector
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }


    /**
     * Set createdBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $createdBy
     * @return Sector
     */
    public function setCreatedBy(\Simbiotica\CalpBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $updatedBy
     * @return Sector
     */
    public function setUpdatedBy(\Simbiotica\CalpBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;
    
        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
    
    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Add projects
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $projects
     * @return CoordinationMechanism
     */
    public function addProject(\Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $projects)
    {
        $this->projects[] = $projects;
    
        return $this;
    }

    /**
     * Remove projects
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $projects
     */
    public function removeProject(\Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $projects)
    {
        $this->projects->removeElement($projects);
    }

    /**
     * Get projects
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProjects()
    {
        return $this->projects;
    }

    public function getTranslations()
    {
        return $this->translations;
    }

    public function setTranslations(\Doctrine\Common\Collections\ArrayCollection $translations)
    {
        $this->translations = $translations;
        return $this;
    }

    public function addTranslation($translation)
    {
        $translation->setObject($this);
        $this->translations[] = $translation;
        return $this;
    }

    public function removeTranslation($translation)
    {
        $this->translations->removeElement($translation);
    }
}