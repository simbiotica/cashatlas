<?php

namespace Simbiotica\CalpBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Sonata\MediaBundle\Entity\BaseMedia as BaseMedia;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Simbiotica\CalpBundle\Constraints as SimbioticaAssert;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\Table(name="media")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 * @SimbioticaAssert\UniqueFilename
 */
class Media extends BaseMedia
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"dashboard", "project"})
     */
    protected $id;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectMedia", mappedBy="media", cascade={"all"})
     */
    protected $galleryHasMedias;
    
    /**
     * @ORM\OneToMany(targetEntity="Organization", mappedBy="media", cascade={"all"})
     */
    protected $organizations;
    
    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="media", cascade={"persist"})
     * @ORM\JoinTable(name="media_user",
     *      joinColumns={@ORM\JoinColumn(name="media_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    protected $users;
    
    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="createdMedia")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    protected $createdBy;
    
    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="updatedMedia")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    protected $updatedBy;
    
    /**
     * @ORM\Column(name="imported_at", type="datetime", nullable=true)
     */
    protected $importedAt;
    
    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;

    protected $filename;
    
    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->galleryHasMedias = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->enabled = true;
    }
    
    public function __toString()
    {
        return $this->name?:"New Media";
    }
    
    /**
     * Add galleryHasMedias
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectMedia $galleryHasMedias
     * @return Media
     */
    public function addGalleryHasMedia(\Simbiotica\CalpBundle\Entity\ProjectMedia $galleryHasMedias)
    {
        $galleryHasMedias->setMedia($this);
        
        $this->galleryHasMedias[] = $galleryHasMedias;
    
        return $this;
    }

    /**
     * Remove galleryHasMedias
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectMedia $galleryHasMedias
     */
    public function removeGalleryHasMedia(\Simbiotica\CalpBundle\Entity\ProjectMedia $galleryHasMedias)
    {
        $this->galleryHasMedias->removeElement($galleryHasMedias);
    }

    /**
     * Get galleryHasMedias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGalleryHasMedias()
    {
        return $this->galleryHasMedias;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Media
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set createdBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $createdBy
     * @return Media
     */
    public function setCreatedBy(\Simbiotica\CalpBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $updatedBy
     * @return Media
     */
    public function setUpdatedBy(\Simbiotica\CalpBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;
    
        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
    
    public function getFilename()
    {
        return $this->filename;
    }
    
    public function setFilename($name)
    {
        $this->filename = SimbioticaCalpBundle::fileSlugify($name);
    }

    /**
     * Add organizations
     *
     * @param \Simbiotica\CalpBundle\Entity\Organization $organizations
     * @return Media
     */
    public function addOrganization(\Simbiotica\CalpBundle\Entity\Organization $organizations)
    {
        $this->organizations[] = $organizations;
    
        return $this;
    }

    /**
     * Remove organizations
     *
     * @param \Simbiotica\CalpBundle\Entity\Organization $organizations
     */
    public function removeOrganization(\Simbiotica\CalpBundle\Entity\Organization $organizations)
    {
        $this->organizations->removeElement($organizations);
    }

    /**
     * Get organizations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrganizations()
    {
        return $this->organizations;
    }

    /**
     * Set importedAt
     *
     * @param \DateTime $importedAt
     * @return Media
     */
    public function setImportedAt($importedAt)
    {
        $this->importedAt = $importedAt;

        return $this;
    }

    /**
     * Get importedAt
     *
     * @return \DateTime 
     */
    public function getImportedAt()
    {
        return $this->importedAt;
    }

    /**
     * Add users
     *
     * @param \Simbiotica\CalpBundle\Entity\User $users
     * @return Media
     */
    public function addUser(\Simbiotica\CalpBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Simbiotica\CalpBundle\Entity\User $users
     */
    public function removeUser(\Simbiotica\CalpBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }
    
    public function setUsers($users)
    {
        $this->users = new ArrayCollection();
        
        foreach ($users as $user) {
            $this->addUser($user);
        }
    }
}