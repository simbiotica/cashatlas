<?php

namespace Simbiotica\CalpBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Simbiotica\CartoDBBundle\CartoDBLink\Mapping as CartoDB;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Simbiotica\CalpBundle\Repository\RegionRepository")
 * @ORM\Table(name="region")
 * @Gedmo\Loggable(logEntryClass="Simbiotica\CalpBundle\Entity\Logs\RegionLogs")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 * @Gedmo\TranslationEntity(class="Simbiotica\CalpBundle\Entity\Translations\RegionTranslation")
 * @CartoDB\CartoDBLink(connection="cashatlas", table="region_sync", cascade={"persist", "remove"})
 */

class Region
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"country", "region", "project", "search"})
     */
    protected $id;

    /**
     * @Gedmo\Versioned
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=128, nullable=true)
     * @CartoDB\CartoDBColumn(column="name")
     * @Groups({"region", "project", "projectPreview"})
     */
    protected $name;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectLocation", mappedBy="region", cascade={"persist"})
     * @ORM\OrderBy({"scale" = "ASC"})
     */
    protected $projects;
    
    /**
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="regions", cascade={"persist"})
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * @Gedmo\Versioned
     * @CartoDB\CartoDBColumn(column="country_id", index=true)
     * @Groups({"region"})
     */
    protected $country;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="cartodb_id", type="integer", nullable=true)
     * @CartoDB\CartoDBColumn(column="calp_id", index=true)
     * @Groups({"project", "region"})
     */
    protected $cartodbId;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="code", type="string", length=128, nullable=true)
     * @CartoDB\CartoDBColumn(column="code")
     * @Groups({"region"})
     */
    protected $code;
    
    /**
     * @ORM\Column(name="geom", type="text", nullable=true)
     * @CartoDB\CartoDBColumn(column="the_geom", set="ST_GeomFromGeoJSON('%s')", get="ST_AsGeoJSON(%s)")
     */
    protected $geom;
    
    /**
     * @ORM\Column(name="centroid", type="text", nullable=true)
     * @CartoDB\CartoDBColumn(column="centroid")
     * @Groups({"region"})
     */
    protected $centroid;
    
    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="createdRegions")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    protected $createdBy;
    
    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="updatedRegions")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    protected $updatedBy;
    
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at" ,type="datetime")
     */
    protected $createdAt;
    
    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at" ,type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;
    
    /**
     * @ORM\OneToMany(targetEntity="Simbiotica\CalpBundle\Entity\Translations\RegionTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;
    
    /**
     * Used in Ajax controller
     * 
     * @Groups({"region"})
     */
    protected $projectCount;
    
    /**
     * Used in Ajax controller
     * 
     * @Groups({"region"})
     */
    protected $scaleSum;

    /**
     * Used in Ajax controller
     * 
     * @Groups({"continent"})
     */
    protected $budgetAllocated;    

    /**
     * Used in Ajax controller
     * 
     * @Groups({"continent"})
     */
    protected $budgetOverall;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function __toString()
    {
    	return empty($this->name)?"":$this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }
    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Project
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Project
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Project
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set createdBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $createdBy
     * @return Project
     */
    public function setCreatedBy(\Simbiotica\CalpBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $updatedBy
     * @return Project
     */
    public function setUpdatedBy(\Simbiotica\CalpBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;
    
        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set country
     *
     * @param \Simbiotica\CalpBundle\Entity\Country $country
     * @return Region
     */
    public function setCountry(\Simbiotica\CalpBundle\Entity\Country $country = null)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return \Simbiotica\CalpBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set cartodbId
     *
     * @param integer $cartodbId
     * @return Region
     */
    public function setCartodbId($cartodbId)
    {
        $this->cartodbId = $cartodbId;
    
        return $this;
    }

    /**
     * Get cartodbId
     *
     * @return integer 
     */
    public function getCartodbId()
    {
        return $this->cartodbId;
    }

    /**
     * Set geom
     *
     * @param string $geom
     * @return Region
     */
    public function setGeom($geom)
    {
        $this->geom = $geom;
    
        return $this;
    }

    /**
     * Get geom
     *
     * @return string 
     */
    public function getGeom()
    {
        return $this->geom;
    }

    /**
     * Set centroid
     *
     * @param string $centroid
     * @return Region
     */
    public function setCentroid($centroid)
    {
        $this->centroid = $centroid;
    
        return $this;
    }

    /**
     * Get centroid
     *
     * @return string 
     */
    public function getCentroid()
    {
        return $this->centroid;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Region
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Get project count
     */
    public function getProjectCount()
    {
        return $this->projectCount;
    }
    
    /**
     * Set project count
     */
    public function setProjectCount($projectCount)
    {
        $this->projectCount = $projectCount;
        
        return $this;
    }
    
    /**
     * Get scale sum
     */
    public function getScaleSum()
    {
        return $this->scaleSum;
    }
    
    /**
     * Set scale sum
     */
    public function setScaleSum($scaleSum)
    {
        $this->scaleSum = $scaleSum;
        
        return $this;
    }

    public function getTranslations()
    {
        return $this->translations;
    }

    public function setTranslations(\Doctrine\Common\Collections\ArrayCollection $translations)
    {
        $this->translations = $translations;
        return $this;
    }

    public function addTranslation($translation)
    {
        $translation->setObject($this);
        $this->translations[] = $translation;
        return $this;
    }

    public function removeTranslation($translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Add projects
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectLocation $projects
     * @return Region
     */
    public function addProject(\Simbiotica\CalpBundle\Entity\ProjectLocation $project)
    {
        $project->setRegion($this);
        
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove projects
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectLocation $projects
     */
    public function removeProject(\Simbiotica\CalpBundle\Entity\ProjectLocation $projects)
    {
        $this->projects->removeElement($projects);
    }

    /**
     * Get projects
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProjects()
    {
        return $this->projects;
    }
}