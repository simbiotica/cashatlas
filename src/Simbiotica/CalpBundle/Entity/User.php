<?php

namespace Simbiotica\CalpBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\GroupInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;

/**
 * @ORM\Entity(repositoryClass="Simbiotica\CalpBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_user_user")
 * @Gedmo\Loggable(logEntryClass="Simbiotica\CalpBundle\Entity\Logs\UserLogs")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */

class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToMany(targetEntity="Simbiotica\CalpBundle\Entity\Group", inversedBy="users", cascade={"persist"})
     * @ORM\JoinTable(name="fos_user_user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;
    
    /**
     * @ORM\OneToMany(targetEntity="OrganizationUser", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     **/
    protected $organizations;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="other_organization", type="string", nullable=true)
     */
    protected $otherOrganization;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="position", type="string", length=255, nullable=true)
     */
    protected $position;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="pending_approval", type="boolean")
     */
    protected $pendingApproval;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="country", type="string", length=10, nullable=true)
     */
    protected $country;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="relevance", type="text", nullable=true)
     */
    protected $relevance;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="intent", type="integer", nullable=true)
     */
    protected $intent;
    
    /**
     * @ORM\ManyToMany(targetEntity="Project", mappedBy="owners", cascade={"persist"})
     */
    protected $ownedProjects;
    
    /**
     * @ORM\ManyToMany(targetEntity="Project", mappedBy="editors", cascade={"persist"})
     */
    protected $editableProjects;
    
    /**
     * @ORM\ManyToMany(targetEntity="Project", mappedBy="revisors", cascade={"persist"})
     */
    protected $revisableProjects;
    
    /**
     * @ORM\ManyToMany(targetEntity="Media", mappedBy="users")
     */
    protected $media;
            
    /**
     * @ORM\OneToMany(targetEntity="Field", mappedBy="user")
     */
    protected $fields;
    
    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="createdUsers")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    protected $createdBy;
    
    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="updatedUsers")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    protected $updatedBy;
    
    /**
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;
    
    /**
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;
    
    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;
    
    /**
     * @ORM\OneToMany(targetEntity="Continent", mappedBy="updatedBy")
     */
    protected $updatedContinents;
    
    /**
     * @ORM\OneToMany(targetEntity="Continent", mappedBy="createdBy")
     */
    protected $createdContinents;
    
    /**
     * @ORM\OneToMany(targetEntity="CoordinationMechanism", mappedBy="updatedBy")
     */
    protected $updatedCoordinationMechanisms;
    
    /**
     * @ORM\OneToMany(targetEntity="CoordinationMechanism", mappedBy="createdBy")
     */
    protected $createdCoordinationMechanisms;
    
    /**
     * @ORM\OneToMany(targetEntity="Field", mappedBy="updatedBy")
     */
    protected $updatedFields;
    
    /**
     * @ORM\OneToMany(targetEntity="Field", mappedBy="createdBy")
     */
    protected $createdFields;
    
    /**
     * @ORM\OneToMany(targetEntity="Modality", mappedBy="updatedBy")
     */
    protected $updatedModalities;
    
    /**
     * @ORM\OneToMany(targetEntity="Modality", mappedBy="createdBy")
     */
    protected $createdModalities;
    
    /**
     * @ORM\OneToMany(targetEntity="Organization", mappedBy="updatedBy")
     */
    protected $updatedOrganizations;
    
    /**
     * @ORM\OneToMany(targetEntity="Organization", mappedBy="createdBy")
     */
    protected $createdOrganizations;
    
    /**
     * @ORM\OneToMany(targetEntity="OrganizationUser", mappedBy="updatedBy")
     */
    protected $updatedOrganizationUsers;
    
    /**
     * @ORM\OneToMany(targetEntity="OrganizationUser", mappedBy="createdBy")
     */
    protected $createdOrganizationUsers;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectCoordinationMechanism", mappedBy="updatedBy")
     */
    protected $updatedProjectCoordinationMechanisms;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectCoordinationMechanism", mappedBy="createdBy")
     */
    protected $createdProjectCoordinationMechanisms;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectDonor", mappedBy="updatedBy")
     */
    protected $updatedProjectDonors;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectDonor", mappedBy="createdBy")
     */
    protected $createdProjectDonors;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectField", mappedBy="updatedBy")
     */
    protected $updatedProjectFields;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectField", mappedBy="createdBy")
     */
    protected $createdProjectFields;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectOrganization", mappedBy="updatedBy")
     */
    protected $updatedProjectOrganizations;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectOrganization", mappedBy="createdBy")
     */
    protected $createdProjectOrganizations;
    
    /**
     * @ORM\OneToMany(targetEntity="Region", mappedBy="updatedBy")
     */
    protected $updatedRegions;
    
    /**
     * @ORM\OneToMany(targetEntity="Region", mappedBy="createdBy")
     */
    protected $createdRegions;
    
    /**
     * @ORM\OneToMany(targetEntity="Country", mappedBy="updatedBy")
     */
    protected $updatedCountries;
    
    /**
     * @ORM\OneToMany(targetEntity="Country", mappedBy="createdBy")
     */
    protected $createdCountries;
    
    /**
     * @ORM\OneToMany(targetEntity="Content", mappedBy="updatedBy")
     */
    protected $updatedContents;
    
    /**
     * @ORM\OneToMany(targetEntity="Content", mappedBy="createdBy")
     */
    protected $createdContents;
    
    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="updatedBy")
     */
    protected $updatedProjects;
    
    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="createdBy")
     */
    protected $createdProjects;
    
    /**
     * @ORM\OneToMany(targetEntity="Sector", mappedBy="updatedBy")
     */
    protected $updatedSectors;
    
    /**
     * @ORM\OneToMany(targetEntity="Sector", mappedBy="createdBy")
     */
    protected $createdSectors;
    
    /**
     * @ORM\OneToMany(targetEntity="WhiteList", mappedBy="updatedBy")
     */
    protected $updatedWhitelists;
    
    /**
     * @ORM\OneToMany(targetEntity="WhiteList", mappedBy="createdBy")
     */
    protected $createdWhitelists;
    
    /**
     * @ORM\OneToMany(targetEntity="Media", mappedBy="updatedBy")
     */
    protected $updatedMedia;
    
    /**
     * @ORM\OneToMany(targetEntity="Media", mappedBy="createdBy")
     */
    protected $createdMedia;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectMedia", mappedBy="updatedBy")
     */
    protected $updatedProjectMedia;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectMedia", mappedBy="createdBy")
     */
    protected $createdProjectMedia;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectLocation", mappedBy="updatedBy")
     */
    protected $updatedProjectLocations;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectLocation", mappedBy="createdBy")
     */
    protected $createdProjectLocations;
    
    /**
     * @ORM\OneToMany(targetEntity="SimilarProject", mappedBy="markedBy")
     */
    protected $markedSimilarProjects;
    
    /**
     * @ORM\OneToMany(targetEntity="SimilarProject", mappedBy="updatedBy")
     */
    protected $updatedSimilarProjects;
    
    /**
     * @ORM\OneToMany(targetEntity="SimilarProject", mappedBy="createdBy")
     */
    protected $createdSimilarProjects;
    
    /**
     * @ORM\OneToMany(targetEntity="Contact", mappedBy="requestedBy")
     */
    protected $requestedContacts;
    
    /**
     * @ORM\ManyToMany(targetEntity="Contact", mappedBy="assignedTo")
     */
    protected $assignedContacts;
    
    /**
     * @ORM\OneToMany(targetEntity="Contact", mappedBy="updatedBy")
     */
    protected $updatedContacts;
    
    /**
     * @ORM\OneToMany(targetEntity="Contact", mappedBy="createdBy")
     */
    protected $createdContacts;
    
    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="updatedBy")
     */
    protected $updatedUsers;
    
    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="createdBy")
     */
    protected $createdUsers;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function __toString()
    {
        if (!empty($this->firstname) && !empty($this->lastname))
            return $this->firstname.' '.$this->lastname;
        elseif(!empty($this->email))
            return $this->email;
        else
            return "New User";
    }

    /**
     * Constructor
     */
    public function __construct()
    {
    	parent::__construct();
        $this->groups = new ArrayCollection();
        $this->enabled = true;
        $this->pendingApproval = false;
        $this->organizations = new ArrayCollection();
        $this->ownedProjects = new ArrayCollection();
        $this->editableProjects = new ArrayCollection();
        $this->revisableProjects = new ArrayCollection();
    }
    
    public function setEmail($email)
    {
        parent::setEmail($email);
        $this->setUsername($email);

        return $this;
    }
    
    /**
     * Add groups
     *
     * @param \Simbiotica\CalpBundle\Entity\Group $groups
     * @return User
     */
    public function addGroup(GroupInterface $groups)
    {
        $this->groups[] = $groups;
    
        return $this;
    }

    /**
     * Remove groups
     *
     * @param \Simbiotica\CalpBundle\Entity\Group $groups
     */
    public function removeGroup(GroupInterface $groups)
    {
        $this->groups->removeElement($groups);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Add updatedProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $updatedProjects
     * @return User
     */
    public function addUpdatedProject(\Simbiotica\CalpBundle\Entity\Project $updatedProjects)
    {
        $this->updatedProjects[] = $updatedProjects;
    
        return $this;
    }

    /**
     * Remove updatedProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $updatedProjects
     */
    public function removeUpdatedProject(\Simbiotica\CalpBundle\Entity\Project $updatedProject)
    {
        $this->updatedProjects->removeElement($updatedProjects);
    }

    /**
     * Get updatedProjects
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedProjects()
    {
        return $this->updatedProjects;
    }

    /**
     * Add createdProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $createdProjects
     * @return User
     */
    public function addCreatedProject(\Simbiotica\CalpBundle\Entity\Project $createdProjects)
    {
        $this->createdProjects[] = $createdProjects;
    
        return $this;
    }

    /**
     * Remove createdProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $createdProjects
     */
    public function removeCreatedProject(\Simbiotica\CalpBundle\Entity\Project $createdProjects)
    {
        $this->createdProjects->removeElement($createdProjects);
    }

    /**
     * Get createdProjects
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedProjects()
    {
        return $this->createdProjects;
    }

    /**
     * Add updatedMedia
     *
     * @param \Simbiotica\CalpBundle\Entity\Media $updatedMedia
     * @return User
     */
    public function addUpdatedMedia(\Simbiotica\CalpBundle\Entity\Media $updatedMedia)
    {
        $this->updatedMedia[] = $updatedMedia;
    
        return $this;
    }

    /**
     * Remove updatedMedia
     *
     * @param \Simbiotica\CalpBundle\Entity\Media $updatedMedia
     */
    public function removeUpdatedMedia(\Simbiotica\CalpBundle\Entity\Media $updatedMedia)
    {
        $this->updatedMedia->removeElement($updatedMedia);
    }

    /**
     * Get updatedMedia
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedMedia()
    {
        return $this->updatedMedia;
    }

    /**
     * Add createdMedia
     *
     * @param \Simbiotica\CalpBundle\Entity\Media $createdMedia
     * @return User
     */
    public function addCreatedMedia(\Simbiotica\CalpBundle\Entity\Media $createdMedia)
    {
        $this->createdMedia[] = $createdMedia;
    
        return $this;
    }

    /**
     * Remove createdMedia
     *
     * @param \Simbiotica\CalpBundle\Entity\Media $createdMedia
     */
    public function removeCreatedMedia(\Simbiotica\CalpBundle\Entity\Media $createdMedia)
    {
        $this->createdMedia->removeElement($createdMedia);
    }

    /**
     * Get createdMedia
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedMedia()
    {
        return $this->createdMedia;
    }

    /**
     * Add updatedProjectMedia
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectMedia $updatedProjectMedia
     * @return User
     */
    public function addUpdatedProjectMedia(\Simbiotica\CalpBundle\Entity\ProjectMedia $updatedProjectMedia)
    {
        $this->updatedProjectMedia[] = $updatedProjectMedia;
    
        return $this;
    }

    /**
     * Remove updatedProjectMedia
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectMedia $updatedProjectMedia
     */
    public function removeUpdatedProjectMedia(\Simbiotica\CalpBundle\Entity\ProjectMedia $updatedProjectMedia)
    {
        $this->updatedProjectMedia->removeElement($updatedProjectMedia);
    }

    /**
     * Get updatedProjectMedia
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedProjectMedia()
    {
        return $this->updatedProjectMedia;
    }

    /**
     * Add createdProjectMedia
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectMedia $createdProjectMedia
     * @return User
     */
    public function addCreatedProjectMedia(\Simbiotica\CalpBundle\Entity\ProjectMedia $createdProjectMedia)
    {
        $this->createdProjectMedia[] = $createdProjectMedia;
    
        return $this;
    }

    /**
     * Remove createdProjectMedia
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectMedia $createdProjectMedia
     */
    public function removeCreatedProjectMedia(\Simbiotica\CalpBundle\Entity\ProjectMedia $createdProjectMedia)
    {
        $this->createdProjectMedia->removeElement($createdProjectMedia);
    }

    /**
     * Get createdProjectMedia
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedProjectMedia()
    {
        return $this->createdProjectMedia;
    }

    /**
     * Add media
     *
     * @param \Simbiotica\CalpBundle\Entity\Media $media
     * @return User
     */
    public function addMedia(\Simbiotica\CalpBundle\Entity\Media $media)
    {
        $this->media[] = $media;

        return $this;
    }

    /**
     * Remove media
     *
     * @param \Simbiotica\CalpBundle\Entity\Media $media
     */
    public function removeMedia(\Simbiotica\CalpBundle\Entity\Media $media)
    {
        $this->media->removeElement($media);
    }

    /**
     * Get media
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Add organizations
     *
     * @param \Simbiotica\CalpBundle\Entity\OrganizationUser $organizations
     * @return User
     */
    public function addOrganization(\Simbiotica\CalpBundle\Entity\OrganizationUser $organizations)
    {
        $organizations->setUser($this);
        $this->organizations[] = $organizations;

        return $this;
    }

    /**
     * Remove organizations
     *
     * @param \Simbiotica\CalpBundle\Entity\OrganizationUser $organizations
     */
    public function removeOrganization(\Simbiotica\CalpBundle\Entity\OrganizationUser $organizations)
    {
        $this->organizations->removeElement($organizations);
    }
    
    /**
     * Remove organizations
     *
     * @param \Simbiotica\CalpBundle\Entity\OrganizationUser $organizations
     */
    public function removeOrganizations(\Simbiotica\CalpBundle\Entity\OrganizationUser $organizations)
    {
        $this->organizations->removeElement($organizations);
    }

    /**
     * Get organizations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrganizations()
    {
        return $this->organizations;
    }
    

    public function setOrganizations($organizations)
    {
        $this->organizations = new ArrayCollection();
        
        foreach($organizations as $organization)
            $this->addOrganization($organization);
    }
    
    /**
     * Set otherOrganization
     *
     * @param string $otherOrganization
     * @return User
     */
    public function setOtherOrganization($otherOrganization)
    {
        $this->otherOrganization = $otherOrganization;

        return $this;
    }

    /**
     * Get otherOrganization
     *
     * @return string 
     */
    public function getOtherOrganization()
    {
        return $this->otherOrganization;
    }

    /**
     * Add fields
     *
     * @param \Simbiotica\CalpBundle\Entity\Field $fields
     * @return User
     */
    public function addField(\Simbiotica\CalpBundle\Entity\Field $fields)
    {
        $this->fields[] = $fields;

        return $this;
    }

    /**
     * Remove fields
     *
     * @param \Simbiotica\CalpBundle\Entity\Field $fields
     */
    public function removeField(\Simbiotica\CalpBundle\Entity\Field $fields)
    {
        $this->fields->removeElement($fields);
    }

    /**
     * Get fields
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set relevance
     *
     * @param string $relevance
     * @return User
     */
    public function setRelevance($relevance)
    {
        $this->relevance = $relevance;

        return $this;
    }

    /**
     * Get relevance
     *
     * @return string 
     */
    public function getRelevance()
    {
        return $this->relevance;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return User
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Add updatedWhitelists
     *
     * @param \Simbiotica\CalpBundle\Entity\WhiteList $updatedWhitelists
     * @return User
     */
    public function addUpdatedWhitelist(\Simbiotica\CalpBundle\Entity\WhiteList $updatedWhitelists)
    {
        $this->updatedWhitelists[] = $updatedWhitelists;
    
        return $this;
    }

    /**
     * Remove updatedWhitelists
     *
     * @param \Simbiotica\CalpBundle\Entity\WhiteList $updatedWhitelists
     */
    public function removeUpdatedWhitelist(\Simbiotica\CalpBundle\Entity\WhiteList $updatedWhitelists)
    {
        $this->updatedWhitelists->removeElement($updatedWhitelists);
    }

    /**
     * Get updatedWhitelists
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedWhitelists()
    {
        return $this->updatedWhitelists;
    }

    /**
     * Add createdWhitelists
     *
     * @param \Simbiotica\CalpBundle\Entity\WhiteList $createdWhitelists
     * @return User
     */
    public function addCreatedWhitelist(\Simbiotica\CalpBundle\Entity\WhiteList $createdWhitelists)
    {
        $this->createdWhitelists[] = $createdWhitelists;
    
        return $this;
    }

    /**
     * Remove createdWhitelists
     *
     * @param \Simbiotica\CalpBundle\Entity\WhiteList $createdWhitelists
     */
    public function removeCreatedWhitelist(\Simbiotica\CalpBundle\Entity\WhiteList $createdWhitelists)
    {
        $this->createdWhitelists->removeElement($createdWhitelists);
    }

    /**
     * Get createdWhitelists
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedWhitelists()
    {
        return $this->createdWhitelists;
    }

    /**
     * Set pendingApproval
     *
     * @param boolean $pendingApproval
     * @return User
     */
    public function setPendingApproval($pendingApproval)
    {
        $this->pendingApproval = $pendingApproval;
    
        return $this;
    }

    /**
     * Get pendingApproval
     *
     * @return boolean 
     */
    public function getPendingApproval()
    {
        return $this->pendingApproval;
    }

    /**
     * Add updatedProjectLocation
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectLocation $updatedProjectLocation
     * @return User
     */
    public function addUpdatedProjectLocation(\Simbiotica\CalpBundle\Entity\ProjectLocation $updatedProjectLocation)
    {
        $this->updatedProjectLocation[] = $updatedProjectLocation;
    
        return $this;
    }

    /**
     * Remove updatedProjectLocation
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectLocation $updatedProjectLocation
     */
    public function removeUpdatedProjectLocation(\Simbiotica\CalpBundle\Entity\ProjectLocation $updatedProjectLocation)
    {
        $this->updatedProjectLocation->removeElement($updatedProjectLocation);
    }

    /**
     * Get updatedProjectLocation
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedProjectLocation()
    {
        return $this->updatedProjectLocation;
    }

    /**
     * Add createdProjectLocation
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectLocation $createdProjectLocation
     * @return User
     */
    public function addCreatedProjectLocation(\Simbiotica\CalpBundle\Entity\ProjectLocation $createdProjectLocation)
    {
        $this->createdProjectLocation[] = $createdProjectLocation;
    
        return $this;
    }

    /**
     * Remove createdProjectLocation
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectLocation $createdProjectLocation
     */
    public function removeCreatedProjectLocation(\Simbiotica\CalpBundle\Entity\ProjectLocation $createdProjectLocation)
    {
        $this->createdProjectLocation->removeElement($createdProjectLocation);
    }

    /**
     * Get createdProjectLocation
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedProjectLocation()
    {
        return $this->createdProjectLocation;
    }

    /**
     * Add updatedSectors
     *
     * @param \Simbiotica\CalpBundle\Entity\Sector $updatedSectors
     * @return User
     */
    public function addUpdatedSector(\Simbiotica\CalpBundle\Entity\Sector $updatedSectors)
    {
        $this->updatedSectors[] = $updatedSectors;
    
        return $this;
    }

    /**
     * Remove updatedSectors
     *
     * @param \Simbiotica\CalpBundle\Entity\Sector $updatedSectors
     */
    public function removeUpdatedSector(\Simbiotica\CalpBundle\Entity\Sector $updatedSectors)
    {
        $this->updatedSectors->removeElement($updatedSectors);
    }

    /**
     * Get updatedSectors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedSectors()
    {
        return $this->updatedSectors;
    }

    /**
     * Add createdSectors
     *
     * @param \Simbiotica\CalpBundle\Entity\Sector $createdSectors
     * @return User
     */
    public function addCreatedSector(\Simbiotica\CalpBundle\Entity\Sector $createdSectors)
    {
        $this->createdSectors[] = $createdSectors;
    
        return $this;
    }

    /**
     * Remove createdSectors
     *
     * @param \Simbiotica\CalpBundle\Entity\Sector $createdSectors
     */
    public function removeCreatedSector(\Simbiotica\CalpBundle\Entity\Sector $createdSectors)
    {
        $this->createdSectors->removeElement($createdSectors);
    }

    /**
     * Get createdSectors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedSectors()
    {
        return $this->createdSectors;
    }

    /**
     * Add updatedContinents
     *
     * @param \Simbiotica\CalpBundle\Entity\Continent $updatedContinents
     * @return User
     */
    public function addUpdatedContinent(\Simbiotica\CalpBundle\Entity\Continent $updatedContinents)
    {
        $this->updatedContinents[] = $updatedContinents;
    
        return $this;
    }

    /**
     * Remove updatedContinents
     *
     * @param \Simbiotica\CalpBundle\Entity\Continent $updatedContinents
     */
    public function removeUpdatedContinent(\Simbiotica\CalpBundle\Entity\Continent $updatedContinents)
    {
        $this->updatedContinents->removeElement($updatedContinents);
    }

    /**
     * Get updatedContinents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedContinents()
    {
        return $this->updatedContinents;
    }

    /**
     * Add createdContinents
     *
     * @param \Simbiotica\CalpBundle\Entity\Continent $createdContinents
     * @return User
     */
    public function addCreatedContinent(\Simbiotica\CalpBundle\Entity\Continent $createdContinents)
    {
        $this->createdContinents[] = $createdContinents;
    
        return $this;
    }

    /**
     * Remove createdContinents
     *
     * @param \Simbiotica\CalpBundle\Entity\Continent $createdContinents
     */
    public function removeCreatedContinent(\Simbiotica\CalpBundle\Entity\Continent $createdContinents)
    {
        $this->createdContinents->removeElement($createdContinents);
    }

    /**
     * Get createdContinents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedContinents()
    {
        return $this->createdContinents;
    }

    /**
     * Add updatedCoordinationMechanisms
     *
     * @param \Simbiotica\CalpBundle\Entity\CoordinationMechanism $updatedCoordinationMechanisms
     * @return User
     */
    public function addUpdatedCoordinationMechanism(\Simbiotica\CalpBundle\Entity\CoordinationMechanism $updatedCoordinationMechanisms)
    {
        $this->updatedCoordinationMechanisms[] = $updatedCoordinationMechanisms;
    
        return $this;
    }

    /**
     * Remove updatedCoordinationMechanisms
     *
     * @param \Simbiotica\CalpBundle\Entity\CoordinationMechanism $updatedCoordinationMechanisms
     */
    public function removeUpdatedCoordinationMechanism(\Simbiotica\CalpBundle\Entity\CoordinationMechanism $updatedCoordinationMechanisms)
    {
        $this->updatedCoordinationMechanisms->removeElement($updatedCoordinationMechanisms);
    }

    /**
     * Get updatedCoordinationMechanisms
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedCoordinationMechanisms()
    {
        return $this->updatedCoordinationMechanisms;
    }

    /**
     * Add createdCoordinationMechanisms
     *
     * @param \Simbiotica\CalpBundle\Entity\CoordinationMechanism $createdCoordinationMechanisms
     * @return User
     */
    public function addCreatedCoordinationMechanism(\Simbiotica\CalpBundle\Entity\CoordinationMechanism $createdCoordinationMechanisms)
    {
        $this->createdCoordinationMechanisms[] = $createdCoordinationMechanisms;
    
        return $this;
    }

    /**
     * Remove createdCoordinationMechanisms
     *
     * @param \Simbiotica\CalpBundle\Entity\CoordinationMechanism $createdCoordinationMechanisms
     */
    public function removeCreatedCoordinationMechanism(\Simbiotica\CalpBundle\Entity\CoordinationMechanism $createdCoordinationMechanisms)
    {
        $this->createdCoordinationMechanisms->removeElement($createdCoordinationMechanisms);
    }

    /**
     * Get createdCoordinationMechanisms
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedCoordinationMechanisms()
    {
        return $this->createdCoordinationMechanisms;
    }

    /**
     * Add updatedFields
     *
     * @param \Simbiotica\CalpBundle\Entity\Field $updatedFields
     * @return User
     */
    public function addUpdatedField(\Simbiotica\CalpBundle\Entity\Field $updatedFields)
    {
        $this->updatedFields[] = $updatedFields;
    
        return $this;
    }

    /**
     * Remove updatedFields
     *
     * @param \Simbiotica\CalpBundle\Entity\Field $updatedFields
     */
    public function removeUpdatedField(\Simbiotica\CalpBundle\Entity\Field $updatedFields)
    {
        $this->updatedFields->removeElement($updatedFields);
    }

    /**
     * Get updatedFields
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedFields()
    {
        return $this->updatedFields;
    }

    /**
     * Add createdFields
     *
     * @param \Simbiotica\CalpBundle\Entity\Field $createdFields
     * @return User
     */
    public function addCreatedField(\Simbiotica\CalpBundle\Entity\Field $createdFields)
    {
        $this->createdFields[] = $createdFields;
    
        return $this;
    }

    /**
     * Remove createdFields
     *
     * @param \Simbiotica\CalpBundle\Entity\Field $createdFields
     */
    public function removeCreatedField(\Simbiotica\CalpBundle\Entity\Field $createdFields)
    {
        $this->createdFields->removeElement($createdFields);
    }

    /**
     * Get createdFields
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedFields()
    {
        return $this->createdFields;
    }

    /**
     * Add updatedModalities
     *
     * @param \Simbiotica\CalpBundle\Entity\Modality $updatedModalities
     * @return User
     */
    public function addUpdatedModalitie(\Simbiotica\CalpBundle\Entity\Modality $updatedModalities)
    {
        $this->updatedModalities[] = $updatedModalities;
    
        return $this;
    }

    /**
     * Remove updatedModalities
     *
     * @param \Simbiotica\CalpBundle\Entity\Modality $updatedModalities
     */
    public function removeUpdatedModalitie(\Simbiotica\CalpBundle\Entity\Modality $updatedModalities)
    {
        $this->updatedModalities->removeElement($updatedModalities);
    }

    /**
     * Get updatedModalities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedModalities()
    {
        return $this->updatedModalities;
    }

    /**
     * Add createdModalities
     *
     * @param \Simbiotica\CalpBundle\Entity\Modality $createdModalities
     * @return User
     */
    public function addCreatedModalitie(\Simbiotica\CalpBundle\Entity\Modality $createdModalities)
    {
        $this->createdModalities[] = $createdModalities;
    
        return $this;
    }

    /**
     * Remove createdModalities
     *
     * @param \Simbiotica\CalpBundle\Entity\Modality $createdModalities
     */
    public function removeCreatedModalitie(\Simbiotica\CalpBundle\Entity\Modality $createdModalities)
    {
        $this->createdModalities->removeElement($createdModalities);
    }

    /**
     * Get createdModalities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedModalities()
    {
        return $this->createdModalities;
    }

    /**
     * Add updatedOrganizations
     *
     * @param \Simbiotica\CalpBundle\Entity\Organization $updatedOrganizations
     * @return User
     */
    public function addUpdatedOrganization(\Simbiotica\CalpBundle\Entity\Organization $updatedOrganizations)
    {
        $this->updatedOrganizations[] = $updatedOrganizations;
    
        return $this;
    }

    /**
     * Remove updatedOrganizations
     *
     * @param \Simbiotica\CalpBundle\Entity\Organization $updatedOrganizations
     */
    public function removeUpdatedOrganization(\Simbiotica\CalpBundle\Entity\Organization $updatedOrganizations)
    {
        $this->updatedOrganizations->removeElement($updatedOrganizations);
    }

    /**
     * Get updatedOrganizations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedOrganizations()
    {
        return $this->updatedOrganizations;
    }

    /**
     * Add createdOrganizations
     *
     * @param \Simbiotica\CalpBundle\Entity\Organization $createdOrganizations
     * @return User
     */
    public function addCreatedOrganization(\Simbiotica\CalpBundle\Entity\Organization $createdOrganizations)
    {
        $this->createdOrganizations[] = $createdOrganizations;
    
        return $this;
    }

    /**
     * Remove createdOrganizations
     *
     * @param \Simbiotica\CalpBundle\Entity\Organization $createdOrganizations
     */
    public function removeCreatedOrganization(\Simbiotica\CalpBundle\Entity\Organization $createdOrganizations)
    {
        $this->createdOrganizations->removeElement($createdOrganizations);
    }

    /**
     * Get createdOrganizations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedOrganizations()
    {
        return $this->createdOrganizations;
    }

    /**
     * Add updatedCountries
     *
     * @param \Simbiotica\CalpBundle\Entity\Country $updatedCountries
     * @return User
     */
    public function addUpdatedCountrie(\Simbiotica\CalpBundle\Entity\Country $updatedCountries)
    {
        $this->updatedCountries[] = $updatedCountries;
    
        return $this;
    }

    /**
     * Remove updatedCountries
     *
     * @param \Simbiotica\CalpBundle\Entity\Country $updatedCountries
     */
    public function removeUpdatedCountrie(\Simbiotica\CalpBundle\Entity\Country $updatedCountries)
    {
        $this->updatedCountries->removeElement($updatedCountries);
    }

    /**
     * Get updatedCountries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedCountries()
    {
        return $this->updatedCountries;
    }

    /**
     * Add createdCountries
     *
     * @param \Simbiotica\CalpBundle\Entity\Country $createdCountries
     * @return User
     */
    public function addCreatedCountrie(\Simbiotica\CalpBundle\Entity\Country $createdCountries)
    {
        $this->createdCountries[] = $createdCountries;
    
        return $this;
    }

    /**
     * Remove createdCountries
     *
     * @param \Simbiotica\CalpBundle\Entity\Country $createdCountries
     */
    public function removeCreatedCountrie(\Simbiotica\CalpBundle\Entity\Country $createdCountries)
    {
        $this->createdCountries->removeElement($createdCountries);
    }

    /**
     * Get createdCountries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedCountries()
    {
        return $this->createdCountries;
    }

    /**
     * Add updatedContents
     *
     * @param \Simbiotica\CalpBundle\Entity\Content $updatedContents
     * @return User
     */
    public function addUpdatedContent(\Simbiotica\CalpBundle\Entity\Content $updatedContents)
    {
        $this->updatedContents[] = $updatedContents;
    
        return $this;
    }

    /**
     * Remove updatedContents
     *
     * @param \Simbiotica\CalpBundle\Entity\Content $updatedContents
     */
    public function removeUpdatedContent(\Simbiotica\CalpBundle\Entity\Content $updatedContents)
    {
        $this->updatedContents->removeElement($updatedContents);
    }

    /**
     * Get updatedContents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedContents()
    {
        return $this->updatedContents;
    }

    /**
     * Add createdContents
     *
     * @param \Simbiotica\CalpBundle\Entity\Content $createdContents
     * @return User
     */
    public function addCreatedContent(\Simbiotica\CalpBundle\Entity\Content $createdContents)
    {
        $this->createdContents[] = $createdContents;
    
        return $this;
    }

    /**
     * Remove createdContents
     *
     * @param \Simbiotica\CalpBundle\Entity\Content $createdContents
     */
    public function removeCreatedContent(\Simbiotica\CalpBundle\Entity\Content $createdContents)
    {
        $this->createdContents->removeElement($createdContents);
    }

    /**
     * Get createdContents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedContents()
    {
        return $this->createdContents;
    }

    /**
     * Get updatedProjectLocations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedProjectLocations()
    {
        return $this->updatedProjectLocations;
    }

    /**
     * Get createdProjectLocations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedProjectLocations()
    {
        return $this->createdProjectLocations;
    }

    /**
     * Add updatedOrganizationUsers
     *
     * @param \Simbiotica\CalpBundle\Entity\OrganizationUser $updatedOrganizationUsers
     * @return User
     */
    public function addUpdatedOrganizationUser(\Simbiotica\CalpBundle\Entity\OrganizationUser $updatedOrganizationUsers)
    {
        $this->updatedOrganizationUsers[] = $updatedOrganizationUsers;
    
        return $this;
    }

    /**
     * Remove updatedOrganizationUsers
     *
     * @param \Simbiotica\CalpBundle\Entity\OrganizationUser $updatedOrganizationUsers
     */
    public function removeUpdatedOrganizationUser(\Simbiotica\CalpBundle\Entity\OrganizationUser $updatedOrganizationUsers)
    {
        $this->updatedOrganizationUsers->removeElement($updatedOrganizationUsers);
    }

    /**
     * Get updatedOrganizationUsers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedOrganizationUsers()
    {
        return $this->updatedOrganizationUsers;
    }

    /**
     * Add createdOrganizationUsers
     *
     * @param \Simbiotica\CalpBundle\Entity\OrganizationUser $createdOrganizationUsers
     * @return User
     */
    public function addCreatedOrganizationUser(\Simbiotica\CalpBundle\Entity\OrganizationUser $createdOrganizationUsers)
    {
        $this->createdOrganizationUsers[] = $createdOrganizationUsers;
    
        return $this;
    }

    /**
     * Remove createdOrganizationUsers
     *
     * @param \Simbiotica\CalpBundle\Entity\OrganizationUser $createdOrganizationUsers
     */
    public function removeCreatedOrganizationUser(\Simbiotica\CalpBundle\Entity\OrganizationUser $createdOrganizationUsers)
    {
        $this->createdOrganizationUsers->removeElement($createdOrganizationUsers);
    }

    /**
     * Get createdOrganizationUsers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedOrganizationUsers()
    {
        return $this->createdOrganizationUsers;
    }

    /**
     * Add updatedProjectCoordinationMechanisms
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $updatedProjectCoordinationMechanisms
     * @return User
     */
    public function addUpdatedProjectCoordinationMechanism(\Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $updatedProjectCoordinationMechanisms)
    {
        $this->updatedProjectCoordinationMechanisms[] = $updatedProjectCoordinationMechanisms;
    
        return $this;
    }

    /**
     * Remove updatedProjectCoordinationMechanisms
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $updatedProjectCoordinationMechanisms
     */
    public function removeUpdatedProjectCoordinationMechanism(\Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $updatedProjectCoordinationMechanisms)
    {
        $this->updatedProjectCoordinationMechanisms->removeElement($updatedProjectCoordinationMechanisms);
    }

    /**
     * Get updatedProjectCoordinationMechanisms
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedProjectCoordinationMechanisms()
    {
        return $this->updatedProjectCoordinationMechanisms;
    }

    /**
     * Add createdProjectCoordinationMechanisms
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $createdProjectCoordinationMechanisms
     * @return User
     */
    public function addCreatedProjectCoordinationMechanism(\Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $createdProjectCoordinationMechanisms)
    {
        $this->createdProjectCoordinationMechanisms[] = $createdProjectCoordinationMechanisms;
    
        return $this;
    }

    /**
     * Remove createdProjectCoordinationMechanisms
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $createdProjectCoordinationMechanisms
     */
    public function removeCreatedProjectCoordinationMechanism(\Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $createdProjectCoordinationMechanisms)
    {
        $this->createdProjectCoordinationMechanisms->removeElement($createdProjectCoordinationMechanisms);
    }

    /**
     * Get createdProjectCoordinationMechanisms
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedProjectCoordinationMechanisms()
    {
        return $this->createdProjectCoordinationMechanisms;
    }

    /**
     * Add updatedProjectDonors
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectDonor $updatedProjectDonors
     * @return User
     */
    public function addUpdatedProjectDonor(\Simbiotica\CalpBundle\Entity\ProjectDonor $updatedProjectDonors)
    {
        $this->updatedProjectDonors[] = $updatedProjectDonors;
    
        return $this;
    }

    /**
     * Remove updatedProjectDonors
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectDonor $updatedProjectDonors
     */
    public function removeUpdatedProjectDonor(\Simbiotica\CalpBundle\Entity\ProjectDonor $updatedProjectDonors)
    {
        $this->updatedProjectDonors->removeElement($updatedProjectDonors);
    }

    /**
     * Get updatedProjectDonors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedProjectDonors()
    {
        return $this->updatedProjectDonors;
    }

    /**
     * Add createdProjectDonors
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectDonor $createdProjectDonors
     * @return User
     */
    public function addCreatedProjectDonor(\Simbiotica\CalpBundle\Entity\ProjectDonor $createdProjectDonors)
    {
        $this->createdProjectDonors[] = $createdProjectDonors;
    
        return $this;
    }

    /**
     * Remove createdProjectDonors
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectDonor $createdProjectDonors
     */
    public function removeCreatedProjectDonor(\Simbiotica\CalpBundle\Entity\ProjectDonor $createdProjectDonors)
    {
        $this->createdProjectDonors->removeElement($createdProjectDonors);
    }

    /**
     * Get createdProjectDonors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedProjectDonors()
    {
        return $this->createdProjectDonors;
    }

    /**
     * Add updatedProjectFields
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectField $updatedProjectFields
     * @return User
     */
    public function addUpdatedProjectField(\Simbiotica\CalpBundle\Entity\ProjectField $updatedProjectFields)
    {
        $this->updatedProjectFields[] = $updatedProjectFields;
    
        return $this;
    }

    /**
     * Remove updatedProjectFields
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectField $updatedProjectFields
     */
    public function removeUpdatedProjectField(\Simbiotica\CalpBundle\Entity\ProjectField $updatedProjectFields)
    {
        $this->updatedProjectFields->removeElement($updatedProjectFields);
    }

    /**
     * Get updatedProjectFields
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedProjectFields()
    {
        return $this->updatedProjectFields;
    }

    /**
     * Add createdProjectFields
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectField $createdProjectFields
     * @return User
     */
    public function addCreatedProjectField(\Simbiotica\CalpBundle\Entity\ProjectField $createdProjectFields)
    {
        $this->createdProjectFields[] = $createdProjectFields;
    
        return $this;
    }

    /**
     * Remove createdProjectFields
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectField $createdProjectFields
     */
    public function removeCreatedProjectField(\Simbiotica\CalpBundle\Entity\ProjectField $createdProjectFields)
    {
        $this->createdProjectFields->removeElement($createdProjectFields);
    }

    /**
     * Get createdProjectFields
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedProjectFields()
    {
        return $this->createdProjectFields;
    }

    /**
     * Add updatedProjectOrganizations
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectOrganization $updatedProjectOrganizations
     * @return User
     */
    public function addUpdatedProjectOrganization(\Simbiotica\CalpBundle\Entity\ProjectOrganization $updatedProjectOrganizations)
    {
        $this->updatedProjectOrganizations[] = $updatedProjectOrganizations;
    
        return $this;
    }

    /**
     * Remove updatedProjectOrganizations
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectOrganization $updatedProjectOrganizations
     */
    public function removeUpdatedProjectOrganization(\Simbiotica\CalpBundle\Entity\ProjectOrganization $updatedProjectOrganizations)
    {
        $this->updatedProjectOrganizations->removeElement($updatedProjectOrganizations);
    }

    /**
     * Get updatedProjectOrganizations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedProjectOrganizations()
    {
        return $this->updatedProjectOrganizations;
    }

    /**
     * Add createdProjectOrganizations
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectOrganization $createdProjectOrganizations
     * @return User
     */
    public function addCreatedProjectOrganization(\Simbiotica\CalpBundle\Entity\ProjectOrganization $createdProjectOrganizations)
    {
        $this->createdProjectOrganizations[] = $createdProjectOrganizations;
    
        return $this;
    }

    /**
     * Remove createdProjectOrganizations
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectOrganization $createdProjectOrganizations
     */
    public function removeCreatedProjectOrganization(\Simbiotica\CalpBundle\Entity\ProjectOrganization $createdProjectOrganizations)
    {
        $this->createdProjectOrganizations->removeElement($createdProjectOrganizations);
    }

    /**
     * Get createdProjectOrganizations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedProjectOrganizations()
    {
        return $this->createdProjectOrganizations;
    }

    /**
     * Add updatedRegions
     *
     * @param \Simbiotica\CalpBundle\Entity\Region $updatedRegions
     * @return User
     */
    public function addUpdatedRegion(\Simbiotica\CalpBundle\Entity\Region $updatedRegions)
    {
        $this->updatedRegions[] = $updatedRegions;
    
        return $this;
    }

    /**
     * Remove updatedRegions
     *
     * @param \Simbiotica\CalpBundle\Entity\Region $updatedRegions
     */
    public function removeUpdatedRegion(\Simbiotica\CalpBundle\Entity\Region $updatedRegions)
    {
        $this->updatedRegions->removeElement($updatedRegions);
    }

    /**
     * Get updatedRegions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedRegions()
    {
        return $this->updatedRegions;
    }

    /**
     * Add createdRegions
     *
     * @param \Simbiotica\CalpBundle\Entity\Region $createdRegions
     * @return User
     */
    public function addCreatedRegion(\Simbiotica\CalpBundle\Entity\Region $createdRegions)
    {
        $this->createdRegions[] = $createdRegions;
    
        return $this;
    }

    /**
     * Remove createdRegions
     *
     * @param \Simbiotica\CalpBundle\Entity\Region $createdRegions
     */
    public function removeCreatedRegion(\Simbiotica\CalpBundle\Entity\Region $createdRegions)
    {
        $this->createdRegions->removeElement($createdRegions);
    }

    /**
     * Get createdRegions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedRegions()
    {
        return $this->createdRegions;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt(\DateTime $createdAt = NULL)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt(\DateTime $updatedAt = NULL)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return User
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set createdBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $createdBy
     * @return User
     */
    public function setCreatedBy(\Simbiotica\CalpBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $updatedBy
     * @return User
     */
    public function setUpdatedBy(\Simbiotica\CalpBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;
    
        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Add ownedProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $ownedProjects
     * @return User
     */
    public function addOwnedProject(\Simbiotica\CalpBundle\Entity\Project $ownedProjects)
    {
        if (!$this->getOwnedProjects()->contains($ownedProjects)) {
            $ownedProjects->addOwner($this);
            $this->getOwnedProjects()->add($ownedProjects);
        }
    
        return $this;
    }

    /**
     * Remove ownedProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $ownedProjects
     */
    public function removeOwnedProject(\Simbiotica\CalpBundle\Entity\Project $ownedProjects)
    {
        $ownedProjects->removeOwner($this);
        $this->ownedProjects->removeElement($ownedProjects);
    }

    /**
     * Get ownedProjects
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOwnedProjects()
    {
        return $this->ownedProjects ?: $this->ownedProjects = new ArrayCollection();
    }

    /**
     * Add editableProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $editableProjects
     * @return User
     */
    public function addEditableProject(\Simbiotica\CalpBundle\Entity\Project $editableProjects)
    {
        if (!$this->getEditableProjects()->contains($editableProjects)) {
            $editableProjects->addEditor($this);
            $this->getEditableProjects()->add($editableProjects);
        }
        
        return $this;
    }

    /**
     * Remove editableProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $editableProjects
     */
    public function removeEditableProject(\Simbiotica\CalpBundle\Entity\Project $editableProjects)
    {
        $editableProjects->removeEditor($this);
        $this->editableProjects->removeElement($editableProjects);
    }

    /**
     * Get editableProjects
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEditableProjects()
    {
        return $this->editableProjects ?: $this->editableProjects = new ArrayCollection();
    }

    /**
     * Add revisableProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $revisableProjects
     * @return User
     */
    public function addRevisableProject(\Simbiotica\CalpBundle\Entity\Project $revisableProjects)
    {
        if (!$this->getRevisableProjects()->contains($revisableProjects)) {
            $revisableProjects->addRevisor($this);
            $this->getRevisableProjects()->add($revisableProjects);
        }
    
        return $this;
    }

    /**
     * Remove revisableProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $revisableProjects
     */
    public function removeRevisableProject(\Simbiotica\CalpBundle\Entity\Project $revisableProjects)
    {
        $revisableProjects->removeRevisor($this);
        $this->revisableProjects->removeElement($revisableProjects);
    }

    /**
     * Get revisableProjects
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRevisableProjects()
    {
        return $this->revisableProjects ?: $this->revisableProjects = new ArrayCollection();
    }

    /**
     * Add markedSimilarProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\SimilarProject $markedSimilarProjects
     * @return User
     */
    public function addMarkedSimilarProject(\Simbiotica\CalpBundle\Entity\SimilarProject $markedSimilarProjects)
    {
        $this->markedSimilarProjects[] = $markedSimilarProjects;
    
        return $this;
    }

    /**
     * Remove markedSimilarProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\SimilarProject $markedSimilarProjects
     */
    public function removeMarkedSimilarProject(\Simbiotica\CalpBundle\Entity\SimilarProject $markedSimilarProjects)
    {
        $this->markedSimilarProjects->removeElement($markedSimilarProjects);
    }

    /**
     * Get markedSimilarProjects
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMarkedSimilarProjects()
    {
        return $this->markedSimilarProjects;
    }

    /**
     * Add updatedSimilarProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\SimilarProject $updatedSimilarProjects
     * @return User
     */
    public function addUpdatedSimilarProject(\Simbiotica\CalpBundle\Entity\SimilarProject $updatedSimilarProjects)
    {
        $this->updatedSimilarProjects[] = $updatedSimilarProjects;
    
        return $this;
    }

    /**
     * Remove updatedSimilarProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\SimilarProject $updatedSimilarProjects
     */
    public function removeUpdatedSimilarProject(\Simbiotica\CalpBundle\Entity\SimilarProject $updatedSimilarProjects)
    {
        $this->updatedSimilarProjects->removeElement($updatedSimilarProjects);
    }

    /**
     * Get updatedSimilarProjects
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedSimilarProjects()
    {
        return $this->updatedSimilarProjects;
    }

    /**
     * Add createdSimilarProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\SimilarProject $createdSimilarProjects
     * @return User
     */
    public function addCreatedSimilarProject(\Simbiotica\CalpBundle\Entity\SimilarProject $createdSimilarProjects)
    {
        $this->createdSimilarProjects[] = $createdSimilarProjects;
    
        return $this;
    }

    /**
     * Remove createdSimilarProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\SimilarProject $createdSimilarProjects
     */
    public function removeCreatedSimilarProject(\Simbiotica\CalpBundle\Entity\SimilarProject $createdSimilarProjects)
    {
        $this->createdSimilarProjects->removeElement($createdSimilarProjects);
    }

    /**
     * Get createdSimilarProjects
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedSimilarProjects()
    {
        return $this->createdSimilarProjects;
    }

    /**
     * Set intent
     *
     * @param integer $intent
     * @return User
     */
    public function setIntent($intent)
    {
        $this->intent = $intent;
    
        return $this;
    }

    /**
     * Get intent
     *
     * @return integer 
     */
    public function getIntent()
    {
        return $this->intent;
    }

    /**
     * Add updatedContacts
     *
     * @param \Simbiotica\CalpBundle\Entity\Contact $updatedContacts
     * @return User
     */
    public function addUpdatedContact(\Simbiotica\CalpBundle\Entity\Contact $updatedContacts)
    {
        $this->updatedContacts[] = $updatedContacts;
    
        return $this;
    }

    /**
     * Remove updatedContacts
     *
     * @param \Simbiotica\CalpBundle\Entity\Contact $updatedContacts
     */
    public function removeUpdatedContact(\Simbiotica\CalpBundle\Entity\Contact $updatedContacts)
    {
        $this->updatedContacts->removeElement($updatedContacts);
    }

    /**
     * Get updatedContacts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedContacts()
    {
        return $this->updatedContacts;
    }

    /**
     * Add createdContacts
     *
     * @param \Simbiotica\CalpBundle\Entity\Contact $createdContacts
     * @return User
     */
    public function addCreatedContact(\Simbiotica\CalpBundle\Entity\Contact $createdContacts)
    {
        $this->createdContacts[] = $createdContacts;
    
        return $this;
    }

    /**
     * Remove createdContacts
     *
     * @param \Simbiotica\CalpBundle\Entity\Contact $createdContacts
     */
    public function removeCreatedContact(\Simbiotica\CalpBundle\Entity\Contact $createdContacts)
    {
        $this->createdContacts->removeElement($createdContacts);
    }

    /**
     * Get createdContacts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedContacts()
    {
        return $this->createdContacts;
    }

    /**
     * Add requestedContacts
     *
     * @param \Simbiotica\CalpBundle\Entity\Contact $requestedContacts
     * @return User
     */
    public function addRequestedContact(\Simbiotica\CalpBundle\Entity\Contact $requestedContacts)
    {
        $this->requestedContacts[] = $requestedContacts;
    
        return $this;
    }

    /**
     * Remove requestedContacts
     *
     * @param \Simbiotica\CalpBundle\Entity\Contact $requestedContacts
     */
    public function removeRequestedContact(\Simbiotica\CalpBundle\Entity\Contact $requestedContacts)
    {
        $this->requestedContacts->removeElement($requestedContacts);
    }

    /**
     * Get requestedContacts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRequestedContacts()
    {
        return $this->requestedContacts;
    }

    /**
     * Add updatedUsers
     *
     * @param \Simbiotica\CalpBundle\Entity\User $updatedUsers
     * @return User
     */
    public function addUpdatedUser(\Simbiotica\CalpBundle\Entity\User $updatedUsers)
    {
        $this->updatedUsers[] = $updatedUsers;
    
        return $this;
    }

    /**
     * Remove updatedUsers
     *
     * @param \Simbiotica\CalpBundle\Entity\User $updatedUsers
     */
    public function removeUpdatedUser(\Simbiotica\CalpBundle\Entity\User $updatedUsers)
    {
        $this->updatedUsers->removeElement($updatedUsers);
    }

    /**
     * Get updatedUsers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdatedUsers()
    {
        return $this->updatedUsers;
    }

    /**
     * Add createdUsers
     *
     * @param \Simbiotica\CalpBundle\Entity\User $createdUsers
     * @return User
     */
    public function addCreatedUser(\Simbiotica\CalpBundle\Entity\User $createdUsers)
    {
        $this->createdUsers[] = $createdUsers;
    
        return $this;
    }

    /**
     * Remove createdUsers
     *
     * @param \Simbiotica\CalpBundle\Entity\User $createdUsers
     */
    public function removeCreatedUser(\Simbiotica\CalpBundle\Entity\User $createdUsers)
    {
        $this->createdUsers->removeElement($createdUsers);
    }

    /**
     * Get createdUsers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedUsers()
    {
        return $this->createdUsers;
    }

    /**
     * Add assignedContacts
     *
     * @param \Simbiotica\CalpBundle\Entity\Contact $assignedContacts
     * @return User
     */
    public function addAssignedContact(\Simbiotica\CalpBundle\Entity\Contact $assignedContacts)
    {
        $this->assignedContacts[] = $assignedContacts;
    
        return $this;
    }

    /**
     * Remove assignedContacts
     *
     * @param \Simbiotica\CalpBundle\Entity\Contact $assignedContacts
     */
    public function removeAssignedContact(\Simbiotica\CalpBundle\Entity\Contact $assignedContacts)
    {
        $this->assignedContacts->removeElement($assignedContacts);
    }

    /**
     * Get assignedContacts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAssignedContacts()
    {
        return $this->assignedContacts;
    }
}