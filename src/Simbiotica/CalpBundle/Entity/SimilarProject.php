<?php

namespace Simbiotica\CalpBundle\Entity;

use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Simbiotica\CalpBundle\Repository\SimilarProjectRepository")
 * @ORM\Table(name="similar_project")
 * @Gedmo\Loggable(logEntryClass="Simbiotica\CalpBundle\Entity\Logs\SimilarProjectLogs")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 * @ORM\HasLifecycleCallbacks()
 */

class SimilarProject
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="similarProjectReference", cascade={"persist"})
     * @ORM\JoinColumn(name="similar_project_reference", referencedColumnName="id")
     */
    protected $reference;
    
    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="similarProjectTarget", cascade={"persist"})
     * @ORM\JoinColumn(name="similar_project_target", referencedColumnName="id")
     */
    protected $target;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="markedSimilarProjects", cascade={"persist"})
     * @ORM\JoinColumn(name="marked_by", referencedColumnName="id")
     */
    protected $markedBy;
    
    /**
     * @ORM\Column(name="marked", type="boolean")
     * @Gedmo\Versioned
     */
    protected $marked;
    
    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="createdSimilarProjects", cascade={"persist"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    protected $createdBy;
    
    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="updatedSimilarProjects", cascade={"persist"})
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    protected $updatedBy;
    
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at" ,type="datetime")
     */
    protected $createdAt;
    
    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at" ,type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->marked = false;
    }
    
    public function __toString()
    {
        return empty($this->name)?"New Similar Project":$this->name;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Project
     */
    public function setCreatedAt(\DateTime $createdAt = null)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Project
     */
    public function setUpdatedAt(\DateTime $updatedAt = null)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Project
     */
    public function setDeletedAt(\DateTime $deletedAt)
    {
        $this->deletedAt = $deletedAt;
    
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set createdBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $createdBy
     * @return Project
     */
    public function setCreatedBy(\Simbiotica\CalpBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $updatedBy
     * @return Project
     */
    public function setUpdatedBy(\Simbiotica\CalpBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;
    
        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set reference
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $reference
     * @return SimilarProject
     */
    public function setReference(\Simbiotica\CalpBundle\Entity\Project $reference = null)
    {
        $this->reference = $reference;
    
        return $this;
    }

    /**
     * Get reference
     *
     * @return \Simbiotica\CalpBundle\Entity\Project 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set target
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $target
     * @return SimilarProject
     */
    public function setTarget(\Simbiotica\CalpBundle\Entity\Project $target = null)
    {
        $this->target = $target;
    
        return $this;
    }

    /**
     * Get target
     *
     * @return \Simbiotica\CalpBundle\Entity\Project 
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set markedBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $markedBy
     * @return SimilarProject
     */
    public function setMarkedBy(\Simbiotica\CalpBundle\Entity\User $markedBy = null)
    {
        $this->markedBy = $markedBy;
    
        return $this;
    }

    /**
     * Get markedBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getMarkedBy()
    {
        return $this->markedBy;
    }

    /**
     * Set marked
     *
     * @param boolean $marked
     * @return SimilarProject
     */
    public function setMarked($marked)
    {
        $this->marked = $marked;
    
        return $this;
    }

    /**
     * Get marked
     *
     * @return boolean 
     */
    public function getMarked()
    {
        return $this->marked;
    }
}