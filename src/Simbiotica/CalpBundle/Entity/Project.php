<?php

namespace Simbiotica\CalpBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Sonata\MediaBundle\Model\Gallery;
use Sonata\MediaBundle\Model\GalleryHasMediaInterface;
use Doctrine\ORM\Mapping as ORM;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Gedmo\Mapping\Annotation as Gedmo;
use Simbiotica\CartoDBBundle\CartoDBLink\Mapping as CartoDB;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Simbiotica\CalpBundle\Repository\ProjectRepository")
 * @ORM\Table(name="project")
 * @Gedmo\Loggable(logEntryClass="Simbiotica\CalpBundle\Entity\Logs\ProjectLogs")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 * @CartoDB\CartoDBLink(connection="cashatlas", table="project_sync", cascade={"persist", "remove"})
 * @ORM\HasLifecycleCallbacks()
 */

class Project extends Gallery
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"projectPreview", "project"})
     */
    protected $id;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectOrganization", mappedBy="project", cascade={"all"}, orphanRemoval=true)
     * @Groups({"projectPreview", "project", "search"})
     */
    protected $organizations;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectLocation", mappedBy="project", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"scale" = "ASC"})
     * @Groups({"projectPreview", "project", "search"})
     */
    protected $locations;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectDonor", mappedBy="project", cascade={"all"}, orphanRemoval=true)
     * @ORM\OrderBy({"amount" = "ASC"})
     * @Groups({"project", "search"})
     */
    protected $donors;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectCoordinationMechanism", mappedBy="project", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     * @Groups({"project", "search"})
     */
    protected $coordinationMechanisms;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectField", mappedBy="project", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $fields;
    
    /**
     * @ORM\OneToMany(targetEntity="Modality", mappedBy="project", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     * @Groups({"project", "search"})
     */
    protected $modalities;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectMedia", mappedBy="gallery", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     * @Groups({"project"})
     */
    protected $galleryHasMedias;
    
    /**
     * @ORM\ManyToMany(targetEntity="Sector", inversedBy="projects")
     * @ORM\JoinTable(name="project_sector")
     * @Groups({"projectPreview", "project", "search"})
     */
    protected $sectors;
    
    /**
     * @ORM\OneToMany(targetEntity="SimilarProject", mappedBy="reference", cascade={"all"})
     */
    protected $similarProjectReference;
    
    /**
     * @ORM\OneToMany(targetEntity="SimilarProject", mappedBy="target", cascade={"all"})
     */
    protected $similarProjectTarget;
    
    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="ownedProjects", cascade={"persist"})
     * @ORM\JoinTable(name="project_user_owned",
     *      joinColumns={@ORM\JoinColumn(name="project_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    protected $owners;
    
    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="editableProjects", cascade={"persist"})
     * @ORM\JoinTable(name="project_user_editable",
     *      joinColumns={@ORM\JoinColumn(name="project_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    protected $editors;
    
    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="revisableProjects", cascade={"persist"})
     * @ORM\JoinTable(name="project_user_revisable",
     *      joinColumns={@ORM\JoinColumn(name="project_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    protected $revisors;
    
    /**
     * @ORM\Column(name="cartodb_index", type="integer", nullable=true)
     * @CartoDB\CartoDBColumn(column="calp_id", index=true)
     */
    protected $cartodbId;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="language", type="string", nullable=true)
     * @Groups({"search"})
     */
    protected $language;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="disaster_philippines", type="boolean")
     */
    protected $disasterPhilippines;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="published", type="smallint")
     * @Groups({"search"})
     */
    protected $published;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="status", type="smallint", nullable=true)
     * @Groups({"search", "project"})
     */
    protected $status;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="affiliates", type="text", nullable=true)
     * @Groups({"project", "search"})
     */
    protected $affiliates;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="implementing_partners", type="text", nullable=true)
     * @Groups({"project", "search"})
     */
    protected $implementingPartners;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="specific_objective", type="text", nullable=true)
     * @CartoDB\CartoDBColumn(column="specific_objective")
     * @Groups({"projectPreview", "project", "search"})
     */
    protected $specificObjective;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="other_sectors", type="text", nullable=true)
     * @Groups({"search"})
     */
    protected $otherSectors;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="location_city", type="text", nullable=true)
     * @Groups({"project", "search"})
     */
    protected $city;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="scale", type="integer", nullable=true)
     * @CartoDB\CartoDBColumn(column="scale")
     * @Groups({"project", "search"})
     */
    protected $scale;

   
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="currency", type="string",length=5)
     * @Groups({"project", "projectPreview", "search"})
     */
    protected $currency;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="initial_scale", type="integer", nullable=true)
     * @Groups({"project", "search"})
     */
    protected $initialScale;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="budget_overall", type="string", nullable=true)
     * @CartoDB\CartoDBColumn(column="budget_overall")
     * @Groups({"projectPreview", "project", "search"})
     */
    protected $budgetOverall;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="initial_budget_overall", type="string", nullable=true)
     * @Groups({"projectPreview", "project", "search"})
     */
    protected $initialBudgetOverall;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="budget_allocated", type="string", nullable=true)
     * @CartoDB\CartoDBColumn(column="budget_allocated")
     * @Groups({"project", "projectPreview", "search"})
     */
    protected $budgetAllocated;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="initial_budget_allocated", type="string", nullable=true)
     * @Groups({"project", "projectFull", "search"})
     */
    protected $initialBudgetAllocated;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="project_context", type="array", nullable=true)
     * @Groups({"project", "search"})
     */
    protected $projectContext;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="starting_date", type="array", nullable=true)
     * @Groups({"projectPreview", "project", "search"})
     */
    protected $startingDate;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="ending_date", type="array", nullable=true)
     * @Groups({"projectPreview", "project", "search"})
     */
    protected $endingDate;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="first_cash_date", type="array", nullable=true)
     * @Groups({"projectPreview", "project", "search"})
     */
    protected $firstCashDate;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="duration", type="integer", nullable=true)
     * @Groups({"project", "search"})
     */
    protected $duration;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="market_assessment", type="array", nullable=true)
     * @Groups({"project", "search"})
     */
    protected $marketAssessment;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="type_intervention", type="smallint", nullable=true)
     * @Groups({"project", "search"})
     */
    protected $typeOfIntervention;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="type_intervention_specify", type="text", nullable=true)
     * @Groups({"project", "search"})
     */
    protected $typeOfInterventionSpecify;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="contact_person_name", type="text", nullable=true)
     * @Groups({"project", "search"})
     */
    protected $contactPersonName;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="contact_person_position", type="text", nullable=true)
     * @Groups({"project", "search"})
     */
    protected $contactPersonPosition;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="contact_person_email_one", type="text", nullable=true)
     * @Groups({"project", "search"})
     */
    protected $contactPersonEmailOne;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="contact_person_email_two", type="text", nullable=true)
     * @Groups({"project", "search"})
     */
    protected $contactPersonEmailTwo;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="contact_organization_url", type="text", nullable=true)
     * @Groups({"project", "search"})
     */
    protected $contactOrganizationUrl;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups({"project", "search"})
     */
    protected $description;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="revision_notes", type="text", nullable=true)
     */
    protected $revisionNotes;
    
    /**
     * @ORM\Column(name="notified_at" ,type="datetime", nullable=true)
     */
    protected $notifiedAt;
    
    /**
     * @ORM\OneToMany(targetEntity="Contact", mappedBy="project")
     */
    protected $relatedContacts;
    
    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="createdProjects")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    protected $createdBy;
    
    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="updatedProjects")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    protected $updatedBy;
    
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at" ,type="datetime")
     */
    protected $createdAt;
    
    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at" ,type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;

   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->organizations = new ArrayCollection();
        $this->donors = new ArrayCollection();
        $this->coordinationMechanisms = new ArrayCollection();
        $this->fields = new ArrayCollection();
        $this->modalities = new ArrayCollection();
        $this->locations = new ArrayCollection();
        $this->sectors = new ArrayCollection();
        $this->galleryHasMedias = new ArrayCollection();
        $this->owners = new ArrayCollection();
        $this->editors = new ArrayCollection();
        $this->projectContext = array();
        $this->status = SimbioticaCalpBundle::VALIDATION_INCOMPLETE;
        $this->disasterPhilippines = false;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function __toString()
    {
        $name = array();
        
        if($this->id)
        {
            $name[] = $this->id;
        }
        if($this->specificObjective)
        {
            $name[] = strlen($this->specificObjective)>30?trim(substr($this->specificObjective, 0, 30)).'...':$this->specificObjective;
        }
        
        return count($name)?implode(' - ', $name):"New Project";
    }
    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Project
     */
    public function setCreatedAt(\DateTime $createdAt = null)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Project
     */
    public function setUpdatedAt(\DateTime $updatedAt = null)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Project
     */
    public function setDeletedAt(\DateTime $deletedAt)
    {
        $this->deletedAt = $deletedAt;
    
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set createdBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $createdBy
     * @return Project
     */
    public function setCreatedBy(\Simbiotica\CalpBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $updatedBy
     * @return Project
     */
    public function setUpdatedBy(\Simbiotica\CalpBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;
    
        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Add donors
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectDonor $donors
     * @return Project
     */
    public function addDonor(\Simbiotica\CalpBundle\Entity\ProjectDonor $donor)
    {
        $donor->setProject($this);
        $this->donors[] = $donor;
    
        return $this;
    }

    /**
     * Remove donors
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectDonor $donors
     */
    public function removeDonor(\Simbiotica\CalpBundle\Entity\ProjectDonor $donors)
    {
        $this->donors->removeElement($donors);
    }

    /**
     * Get donors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDonors()
    {
        return $this->donors;
    }
    
    public function setDonors($donors)
    {
        $this->donors = new ArrayCollection();
    
        foreach ($donors as $donor) {
            $this->addDonor($donor);
        }
    }

    /**
     * Add organizations
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectOrganization $organizations
     * @return Project
     */
    public function addOrganization($organization)
    {
        $organization->setProject($this);
        $this->organizations[] = $organization;
    
        return $this;
    }

    /**
     * Remove organizations
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectOrganization $organizations
     */
    public function removeOrganization(\Simbiotica\CalpBundle\Entity\ProjectOrganization $organizations)
    {
        $this->organizations->removeElement($organizations);
    }
    
    public function setOrganizations($organizations)
    {
        $this->organizations = new ArrayCollection();
        
        foreach($organizations as $organization)
            $this->addOrganization($organization);
    }

    /**
     * Set specificObjective
     *
     * @param string $specificObjective
     * @return Project
     */
    public function setSpecificObjective($specificObjective)
    {
        $this->specificObjective = $specificObjective;
    
        return $this;
    }

    /**
     * Get specificObjective
     *
     * @return string 
     */
    public function getSpecificObjective()
    {
        return $this->specificObjective;
    }

    /**
     * Set scale
     *
     * @param string $scale
     * @return Project
     */
    public function setScale($scale)
    {
        $this->scale = $scale;
    
        return $this;
    }

    /**
     * Get scale
     *
     * @return string 
     */
    public function getScale()
    {
        return $this->scale;
    }

    /**
     * Set context
     *
     * @param array $context
     * @return Project
     */
    public function setContext($context)
    {
        $this->context = $context;
    
        return $this;
    }

    /**
     * Get context
     *
     * @return array 
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Set startingDate
     *
     * @param \DateTime $startingDate
     * @return Project
     */
    public function setStartingDate($startingDate)
    {
        $this->startingDate = $startingDate;
    
        return $this;
    }

    /**
     * Get startingDate
     *
     * @return array
     */
    public function getStartingDate()
    {
        return $this->startingDate;
    }
    
    public function getStartingDateYear()
    {
        $startingDate = $this->getStartingDate();
        
        if($startingDate && array_key_exists('year', $startingDate))
            return $startingDate['year'];
        
        return null;
    }
    
    public function getStartingDateMonth()
    {
        $startingDate = $this->getStartingDate();
        
        if($startingDate && array_key_exists('month', $startingDate))
            return $startingDate['month'];
        
        return null;
    }

    /**
     * Set endingDate
     *
     * @param \DateTime $endingDate
     * @return Project
     */
    public function setEndingDate($endingDate)
    {
        $this->endingDate = $endingDate;
    
        return $this;
    }

    /**
     * Get endingDate
     *
     * @return array
     */
    public function getEndingDate()
    {
        return $this->endingDate;
    }
    
    public function getEndingDateYear()
    {
        $endingDate = $this->getEndingDate();
        
        if($endingDate && array_key_exists('year', $endingDate))
            return $endingDate['year'];
        
        return null;
    }
    
    public function getEndingDateMonth()
    {
        $endingDate = $this->getEndingDate();
        
        if($endingDate && array_key_exists('month', $endingDate))
            return $endingDate['month'];
        
        return null;
    }

    /**
     * Set typeOfIntervention
     *
     * @param integer $typeOfIntervention
     * @return Project
     */
    public function setTypeOfIntervention($typeOfIntervention)
    {
        $this->typeOfIntervention = $typeOfIntervention;
    
        return $this;
    }

    /**
     * Get typeOfIntervention
     *
     * @return integer 
     */
    public function getTypeOfIntervention()
    {
        return $this->typeOfIntervention;
    }

    /**
     * Get organizations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrganizations()
    {
        return $this->organizations;
    }

    /**
     * Add modalities
     *
     * @param \Simbiotica\CalpBundle\Entity\Modalities $modalities
     * @return Project
     */
    public function addModalitie(\Simbiotica\CalpBundle\Entity\Modality $modality)
    {
        $modality->setProject($this);
        $this->modalities[] = $modality;
    
        return $this;
    }
    
        
        /**
     * Add modalities
     *
     * @param \Simbiotica\CalpBundle\Entity\Modalities $modalities
     * @return Project
     */
    public function addModality(\Simbiotica\CalpBundle\Entity\Modality $modality)
    {
        $modality->setProject($this);
        $this->modalities[] = $modality;
    
        return $this;
    }

    /**
     * Remove modalities
     *
     * @param \Simbiotica\CalpBundle\Entity\Modalities $modalities
     */
    public function removeModality(\Simbiotica\CalpBundle\Entity\Modality $modalities)
    {
        $this->modalities->removeElement($modalities);
    }

    /**
     * Set modalities
     *
     */
    public function setModalities($modalities)
    {
        $this->modalities = new ArrayCollection();
        
        foreach ($modalities as $modality) {
            $this->addModality($modality);
        }
        
        return $this;
    }
    
    /**
     * Get modalities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getModalities()
    {
        return $this->modalities;
    }

    /**
     * Set typeOfInterventionSpecify
     *
     * @param string $typeOfInterventionSpecify
     * @return Project
     */
    public function setTypeOfInterventionSpecify($typeOfInterventionSpecify)
    {
        $this->typeOfInterventionSpecify = $typeOfInterventionSpecify;
    
        return $this;
    }

    /**
     * Get typeOfInterventionSpecify
     *
     * @return string 
     */
    public function getTypeOfInterventionSpecify()
    {
        return $this->typeOfInterventionSpecify;
    }

    /**
     * Set cartodbId
     *
     * @param \integer $cartodbId
     * @return Project
     */
    public function setCartodbId($cartodbId)
    {
        $this->cartodbId = $cartodbId;
    
        return $this;
    }

    /**
     * Get cartodbId
     *
     * @return \integer
     */
    public function getCartodbId()
    {
        return $this->cartodbId;
    }

    /**
     * Set implementingPartners
     *
     * @param string $implementingPartners
     * @return Project
     */
    public function setImplementingPartners($implementingPartners)
    {
        $this->implementingPartners = $implementingPartners;
    
        return $this;
    }

    /**
     * Get implementingPartners
     *
     * @return string 
     */
    public function getImplementingPartners()
    {
        return $this->implementingPartners;
    }

    /**
     * Set firstCashDate
     *
     * @param \DateTime $firstCashDate
     * @return Project
     */
    public function setFirstCashDate($firstCashDate)
    {
        $this->firstCashDate = $firstCashDate;
    
        return $this;
    }

    /**
     * Get firstCashDate
     *
     * @return array
     */
    public function getFirstCashDate()
    {
        return $this->firstCashDate;
    }
    
    public function getFirstCashDateYear()
    {
        $firstCashDate = $this->getFirstCashDate();
        
        if($firstCashDate && array_key_exists('year', $firstCashDate))
            return $firstCashDate['year'];
        
        return null;
    }
    
    public function getFirstCashDateMonth()
    {
        $firstCashDate = $this->getFirstCashDate();
        
        if($firstCashDate && array_key_exists('month', $firstCashDate))
            return $firstCashDate['month'];
        
        return null;
    }

    /**
     * Set marketAssessment
     *
     * @param array $marketAssessment
     * @return Project
     */
    public function setMarketAssessment($marketAssessment)
    {
        $this->marketAssessment = $marketAssessment;
    
        return $this;
    }

    /**
     * Get marketAssessment
     *
     * @return array 
     */
    public function getMarketAssessment()
    {
        return $this->marketAssessment;
    }

    public function getMarketAssessmentOptions()
    {
        $marketAssessment = $this->getMarketAssessment();
        
        if($marketAssessment && array_key_exists('options', $marketAssessment))
            return $marketAssessment['options'];
        
        return null;
    }
    
    public function getMarketAssessmentOthers()
    {
        $marketAssessment = $this->getMarketAssessment();
        
        if($marketAssessment && array_key_exists('others', $marketAssessment))
            return $marketAssessment['others'];
        
        return null;
    }
    
    /**
     * Set contactPersonName
     *
     * @param string $contactPersonName
     * @return Project
     */
    public function setContactPersonName($contactPersonName)
    {
        $this->contactPersonName = $contactPersonName;
    
        return $this;
    }

    /**
     * Get contactPersonName
     *
     * @return string 
     */
    public function getContactPersonName()
    {
        return $this->contactPersonName;
    }

    /**
     * Set contactPersonPosition
     *
     * @param string $contactPersonPosition
     * @return Project
     */
    public function setContactPersonPosition($contactPersonPosition)
    {
        $this->contactPersonPosition = $contactPersonPosition;
    
        return $this;
    }

    /**
     * Get contactPersonPosition
     *
     * @return string 
     */
    public function getContactPersonPosition()
    {
        return $this->contactPersonPosition;
    }

    /**
     * Set contactOrganizationUrl
     *
     * @param string $contactOrganizationUrl
     * @return Project
     */
    public function setContactOrganizationUrl($contactOrganizationUrl)
    {
        $this->contactOrganizationUrl = $contactOrganizationUrl;
    
        return $this;
    }

    /**
     * Get contactOrganizationUrl
     *
     * @return string 
     */
    public function getContactOrganizationUrl()
    {
        return $this->contactOrganizationUrl;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set projectContext
     *
     * @param array $projectContext
     * @return Project
     */
    public function setProjectContext($projectContext)
    {
        $this->projectContext = $projectContext;
    
        return $this;
    }

    /**
     * Get projectContext
     *
     * @return array 
     */
    public function getProjectContext()
    {
        return $this->projectContext;
    }
    
    /**
     * Add galleryHasMedias
     *
     * @param GalleryHasMediaInterface $galleryHasMedias
     * @return Project
     */
    public function addGalleryHasMedias(GalleryHasMediaInterface $galleryHasMedias)
    {
        $galleryHasMedias->setGallery($this);
    
        $this->galleryHasMedias[] = $galleryHasMedias;
    }

    /**
     * Add galleryHasMedias
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectMedia $galleryHasMedias
     * @return Project
     */
    public function addGalleryHasMedia(GalleryHasMediaInterface $galleryHasMedias)
    {
        $galleryHasMedias->setGallery($this);
        
        $this->galleryHasMedias[] = $galleryHasMedias;
    
        return $this;
    }

    /**
     * Remove galleryHasMedias
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectMedia $galleryHasMedias
     */
    public function removeGalleryHasMedia(GalleryHasMediaInterface $galleryHasMedias)
    {
        $this->galleryHasMedias->removeElement($galleryHasMedias);
    }

    /**
     * Get galleryHasMedias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGalleryHasMedias()
    {
        return $this->galleryHasMedias;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setGalleryHasMedias($galleryHasMedias)
    {
        foreach ($galleryHasMedias as $galleryHasMedia) {
            $this->addGalleryHasMedias($galleryHasMedia);
        }
    }

    /**
     * Add field
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectField $fields
     * @return Project
     */
    public function addField(\Simbiotica\CalpBundle\Entity\ProjectField $field)
    {
        $field->setProject($this);
        
        $this->fields[] = $field;
    
        return $this;
    }
    
    /**
     * Remove fields
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectField $fields
     */
    public function removeField(\Simbiotica\CalpBundle\Entity\ProjectField $fields)
    {
        $this->fields->removeElement($fields);
    }

    /**
     * Get fields
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFields()
    {
        return $this->fields;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setFields($fields)
    {
        $this->fields = new ArrayCollection();
        
        foreach ($fields as $field) {
            $this->addField($field);
        }
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Project
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set language
     *
     * @param string $language
     * @return Project
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    
        return $this;
    }

    /**
     * Get language
     *
     * @return string 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set affiliates
     *
     * @param string $affiliates
     * @return Project
     */
    public function setAffiliates($affiliates)
    {
        $this->affiliates = $affiliates;
    
        return $this;
    }

    /**
     * Get affiliates
     *
     * @return string 
     */
    public function getAffiliates()
    {
        return $this->affiliates;
    }

    /**
     * Add sectors
     *
     * @param \Simbiotica\CalpBundle\Entity\Sector $sectors
     * @return Project
     */
    public function addSector(\Simbiotica\CalpBundle\Entity\Sector $sectors)
    {
        $this->sectors[] = $sectors;
    
        return $this;
    }

    /**
     * Remove sectors
     *
     * @param \Simbiotica\CalpBundle\Entity\Sector $sectors
     */
    public function removeSector(\Simbiotica\CalpBundle\Entity\Sector $sectors)
    {
        $this->sectors->removeElement($sectors);
    }

    /**
     * Get sectors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSectors()
    {
        return $this->sectors;
    }
    
    public function setSectors($sectors)
    {
        $this->sectors = $sectors;
    
        return $this;
    }

    /**
     * Set otherSectors
     *
     * @param string $otherSectors
     * @return Project
     */
    public function setOtherSectors($otherSectors)
    {
        $this->otherSectors = $otherSectors;
    
        return $this;
    }

    /**
     * Get otherSectors
     *
     * @return string 
     */
    public function getOtherSectors()
    {
        return $this->otherSectors;
    }

    /**
     * Set contactPersonEmailOne
     *
     * @param string $contactPersonEmailOne
     * @return Project
     */
    public function setContactPersonEmailOne($contactPersonEmailOne)
    {
        $this->contactPersonEmailOne = $contactPersonEmailOne;
    
        return $this;
    }

    /**
     * Get contactPersonEmailOne
     *
     * @return string 
     */
    public function getContactPersonEmailOne()
    {
        return $this->contactPersonEmailOne;
    }

    /**
     * Set contactPersonEmailTwo
     *
     * @param string $contactPersonEmailTwo
     * @return Project
     */
    public function setContactPersonEmailTwo($contactPersonEmailTwo)
    {
        $this->contactPersonEmailTwo = $contactPersonEmailTwo;
    
        return $this;
    }

    /**
     * Get contactPersonEmailTwo
     *
     * @return string 
     */
    public function getContactPersonEmailTwo()
    {
        return $this->contactPersonEmailTwo;
    }

    /**
     * Add countries
     *
     * @param \Simbiotica\CalpBundle\Entity\Country $countries
     * @return Project
     */
    public function addCountrie(\Simbiotica\CalpBundle\Entity\Country $countries)
    {
        $this->countries[] = $countries;
    
        return $this;
    }

    /**
     * Remove modalities
     *
     * @param \Simbiotica\CalpBundle\Entity\Modality $modalities
     */
    public function removeModalitie(\Simbiotica\CalpBundle\Entity\Modality $modalities)
    {
        $this->modalities->removeElement($modalities);
    }

    /**
     * Set published
     *
     * @param integer $published
     * @return Project
     */
    public function setPublished($published)
    {
        $this->published = $published;
    
        return $this;
    }

    /**
     * Get published
     *
     * @return integer 
     */
    public function getPublished()
    {
        return $this->published;
    }
    
    /**
     * Get published
     *
     * @return integer 
     */
    public function getPublishedString()
    {
        $statusArray = SimbioticaCalpBundle::getPublishedStatus();
        
        return isset($this->published)?$statusArray[$this->published]:null;
    }

    /**
     * Add coordinationMechanism
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $coordinationMechanism
     * @return Project
     */
    public function addCoordinationMechanism(\Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $coordinationMechanism)
    {
        $coordinationMechanism->setProject($this);
        
        $this->coordinationMechanisms[] = $coordinationMechanism;
    
        return $this;
    }
    
    /**
     * Add coordinationMechanism
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $coordinationMechanism
     * @return Project
     */
    public function addCoordinationMechanisms(\Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $coordinationMechanism)
    {
        $coordinationMechanism->setProject($this);
        
        $this->coordinationMechanisms[] = $coordinationMechanism;
    
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setCoordinationMechanisms($coordinationMechanisms)
    {
        $this->coordinationMechanisms = new ArrayCollection();
        
        foreach ($coordinationMechanisms as $coordinationMechanism) {
            $this->addCoordinationMechanism($coordinationMechanism);
        }
    }

    /**
     * Get coordinationMechanisms
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCoordinationMechanisms()
    {
        return $this->coordinationMechanisms;
    }

    /**
     * Remove coordinationMechanisms
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $coordinationMechanisms
     */
    public function removeCoordinationMechanism(\Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism $coordinationMechanisms)
    {
        $this->coordinationMechanisms->removeElement($coordinationMechanisms);
    }

    /**
     * Add owners
     *
     * @param \Simbiotica\CalpBundle\Entity\User $owners
     * @return Project
     */
    public function addOwner(\Simbiotica\CalpBundle\Entity\User $owners)
    {
        $this->owners[] = $owners;

        return $this;
    }

    /**
     * Remove owners
     *
     * @param \Simbiotica\CalpBundle\Entity\User $owners
     */
    public function removeOwner(\Simbiotica\CalpBundle\Entity\User $owners)
    {
        $this->owners->removeElement($owners);
    }

    /**
     * Get owners
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOwners()
    {
        return $this->owners;
    }

    /**
     * Add location
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectLocation $location
     * @return Project
     */
    public function addLocation(\Simbiotica\CalpBundle\Entity\ProjectLocation $location)
    {
        $location->setProject($this);
        
        $this->locations[] = $location;

        return $this;
    }

    /**
     * Remove locations
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectLocation $location
     */
    public function removeLocation(\Simbiotica\CalpBundle\Entity\ProjectLocation $location)
    {
        $this->locations->removeElement($location);
    }

    /**
     * Get locations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocations()
    {
        return $this->locations;
    }
    
    public function setLocations($locations)
    {
        $this->locations = new ArrayCollection();
        
        foreach ($locations as $location) {
            $this->addLocation($location);
        }
    }

    /**
     * Add editors
     *
     * @param \Simbiotica\CalpBundle\Entity\User $editors
     * @return Project
     */
    public function addEditor(\Simbiotica\CalpBundle\Entity\User $editors)
    {
        $this->editors[] = $editors;
    
        return $this;
    }

    /**
     * Remove editors
     *
     * @param \Simbiotica\CalpBundle\Entity\User $editors
     */
    public function removeEditor(\Simbiotica\CalpBundle\Entity\User $editors)
    {
        $this->editors->removeElement($editors);
    }

    /**
     * Get editors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEditors()
    {
        return $this->editors;
    }

    /**
     * Set initialScale
     *
     * @param integer $initialScale
     * @return Project
     */
    public function setInitialScale($initialScale)
    {
        $this->initialScale = $initialScale;
    
        return $this;
    }

    /**
     * Get initialScale
     *
     * @return integer 
     */
    public function getInitialScale()
    {
        return $this->initialScale;
    }

    /**
     * Set budgetOverall
     *
     * @param string $budgetOverall
     * @return Project
     */
    public function setBudgetOverall($budgetOverall)
    {
        $this->budgetOverall = $budgetOverall;
    
        return $this;
    }

    /**
     * Get budgetOverall
     *
     * @return string 
     */
    public function getBudgetOverall()
    {
        return $this->budgetOverall;
    }

    /**
     * Set initialBudgetOverall
     *
     * @param string $initialBudgetOverall
     * @return Project
     */
    public function setInitialBudgetOverall($initialBudgetOverall)
    {
        $this->initialBudgetOverall = $initialBudgetOverall;
    
        return $this;
    }

    /**
     * Get initialBudgetOverall
     *
     * @return string 
     */
    public function getInitialBudgetOverall()
    {
        return $this->initialBudgetOverall;
    }

    /**
     * Set budgetAllocated
     *
     * @param string $budgetAllocated
     * @return Project
     */
    public function setBudgetAllocated($budgetAllocated)
    {
        $this->budgetAllocated = $budgetAllocated;
    
        return $this;
    }

    /**
     * Get budgetAllocated
     *
     * @return string 
     */
    public function getBudgetAllocated()
    {
        return $this->budgetAllocated;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Project
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    
        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Add revisors
     *
     * @param \Simbiotica\CalpBundle\Entity\User $revisors
     * @return Project
     */
    public function addRevisor(\Simbiotica\CalpBundle\Entity\User $revisors)
    {
        $this->revisors[] = $revisors;
    
        return $this;
    }

    /**
     * Remove revisors
     *
     * @param \Simbiotica\CalpBundle\Entity\User $revisors
     */
    public function removeRevisor(\Simbiotica\CalpBundle\Entity\User $revisors)
    {
        $this->revisors->removeElement($revisors);
    }

    /**
     * Get revisors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRevisors()
    {
        return $this->revisors;
    }

    /**
     * Set revisionNotes
     *
     * @param string $revisionNotes
     * @return Project
     */
    public function setRevisionNotes($revisionNotes)
    {
        $this->revisionNotes = $revisionNotes;
    
        return $this;
    }

    /**
     * Get revisionNotes
     *
     * @return string 
     */
    public function getRevisionNotes()
    {
        return $this->revisionNotes;
    }

    /**
     * Add similarProjectReference
     *
     * @param \Simbiotica\CalpBundle\Entity\SimilarProject $similarProjectReference
     * @return Project
     */
    public function addSimilarProjectReference(\Simbiotica\CalpBundle\Entity\SimilarProject $similarProjectReference)
    {
        $this->similarProjectReference[] = $similarProjectReference;
    
        return $this;
    }

    /**
     * Remove similarProjectReference
     *
     * @param \Simbiotica\CalpBundle\Entity\SimilarProject $similarProjectReference
     */
    public function removeSimilarProjectReference(\Simbiotica\CalpBundle\Entity\SimilarProject $similarProjectReference)
    {
        $this->similarProjectReference->removeElement($similarProjectReference);
    }

    /**
     * Get similarProjectReference
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSimilarProjectReference()
    {
        return $this->similarProjectReference;
    }

    /**
     * Add similarProjectTarget
     *
     * @param \Simbiotica\CalpBundle\Entity\SimilarProject $similarProjectTarget
     * @return Project
     */
    public function addSimilarProjectTarget(\Simbiotica\CalpBundle\Entity\SimilarProject $similarProjectTarget)
    {
        $this->similarProjectTarget[] = $similarProjectTarget;
    
        return $this;
    }

    /**
     * Remove similarProjectTarget
     *
     * @param \Simbiotica\CalpBundle\Entity\SimilarProject $similarProjectTarget
     */
    public function removeSimilarProjectTarget(\Simbiotica\CalpBundle\Entity\SimilarProject $similarProjectTarget)
    {
        $this->similarProjectTarget->removeElement($similarProjectTarget);
    }

    /**
     * Get similarProjectTarget
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSimilarProjectTarget()
    {
        return $this->similarProjectTarget;
    }

    /**
     * Set InitialBudgetAllocated
     *
     * @param string $initialBudgetAllocated
     * @return Project
     */
    public function setInitialBudgetAllocated($initialBudgetAllocated)
    {
        $this->initialBudgetAllocated = $initialBudgetAllocated;
    
        return $this;
    }

    /**
     * Get InitialBudgetAllocated
     *
     * @return string 
     */
    public function getInitialBudgetAllocated()
    {
        return $this->initialBudgetAllocated;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Project
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatusString()
    {
        $statusArray = SimbioticaCalpBundle::getValidationStatus();
        
        return isset($this->status)?$statusArray[$this->status]:null;
    }

     /**
     * Set notifiedAt
     *
     * @param \DateTime $notifiedAt
     * @return Project
     */
    public function setNotifiedAt($notifiedAt)
    {
        $this->notifiedAt = $notifiedAt;
    
        return $this;
    }     

    /**
     * Get notifiedAt
     *
     * @return datetime
     */
    public function getNotifiedAt()
    {
        return $this->notifiedAt;
    }

    
    public function indexable() 
    {
        return empty($this->deletedAt);
    }


    /**
     * Add relatedContacts
     *
     * @param \Simbiotica\CalpBundle\Entity\Contact $relatedContacts
     * @return Project
     */
    public function addRelatedContact(\Simbiotica\CalpBundle\Entity\Contact $relatedContacts)
    {
        $this->relatedContacts[] = $relatedContacts;
    
        return $this;
    }

    /**
     * Remove relatedContacts
     *
     * @param \Simbiotica\CalpBundle\Entity\Contact $relatedContacts
     */
    public function removeRelatedContact(\Simbiotica\CalpBundle\Entity\Contact $relatedContacts)
    {
        $this->relatedContacts->removeElement($relatedContacts);
    }

    /**
     * Get relatedContacts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRelatedContacts()
    {
        return $this->relatedContacts;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return Project
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    
        return $this;
    }

    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $disasterPhilippines
     * @deprecated
     */
    public function setDisasterPhilippines($disasterPhilippines)
    {
        $this->disasterPhilippines = $disasterPhilippines;
    }

    /**
     * @return mixed
     * @deprecated
     */
    public function getDisasterPhilippines()
    {
        return $this->disasterPhilippines;
    }


}