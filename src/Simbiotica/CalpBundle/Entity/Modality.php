<?php

namespace Simbiotica\CalpBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Simbiotica\CalpBundle\Repository\ModalityRepository")
 * @ORM\Table(name="modality")
 * @Gedmo\Loggable(logEntryClass="Simbiotica\CalpBundle\Entity\Logs\ModalityLogs")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */

class Modality
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="modalities", cascade={"persist"})
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * @Gedmo\Versioned
     */
    protected $project;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="modality", type="smallint", nullable=true)
     * @Groups({"project"})
     */
    protected $modality;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="modality_specify", type="text", nullable=true)
     * @Groups({"project"})
     */
    protected $modalitySpecify;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="delivery_mechanism", type="smallint", nullable=true)
     * @Groups({"project"})
     */
    protected $deliveryMechanism;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="delivery_mechanism_specify", type="text", nullable=true)
     * @Groups({"project"})
     */
    protected $deliveryMechanismSpecify;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="delivery_agent", type="smallint", nullable=true)
     * @Groups({"project"})
     */
    protected $deliveryAgent;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="delivery_agent_specify", type="text", nullable=true)
     * @Groups({"project"})
     */
    protected $deliveryAgentSpecify;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="position", type="smallint", nullable=true)
     */
    protected $position;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="scale", type="decimal", nullable=true)
     * @Groups({"project"})
     */
    protected $scale;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="amount", type="decimal", nullable=true)
     * @Groups({"project"})
     */
    protected $amount;
    
    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="transfers", type="decimal", nullable=true)
     * @Groups({"project"})
     */
    protected $transfers;
    
    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="createdModalities")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    protected $createdBy;
    
    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="updatedModalities")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    protected $updatedBy;
    
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at" ,type="datetime")
     */
    protected $createdAt;
    
    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at" ,type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }
    
    public function __toString()
    {
        return 'Modality';
    }
    
    public function getModalities(array $index = null, $level = null)
    {
        return SimbioticaCalpBundle::getModalities($index, $level);
    }
    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Project
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Project
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Project
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set createdBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $createdBy
     * @return Project
     */
    public function setCreatedBy(\Simbiotica\CalpBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $updatedBy
     * @return Project
     */
    public function setUpdatedBy(\Simbiotica\CalpBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;
    
        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set project
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $project
     * @return Modality
     */
    public function setProject(\Simbiotica\CalpBundle\Entity\Project $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \Simbiotica\CalpBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Modality
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set modality
     *
     * @param integer $modality
     * @return Modality
     */
    public function setModality($modality)
    {
        $this->modality = $modality;
    
        return $this;
    }

    /**
     * Get modality
     *
     * @return integer 
     */
    public function getModality()
    {
        return $this->modality;
    }

    /**
     * Set deliveryMechanism
     *
     * @param integer $deliveryMechanism
     * @return Modality
     */
    public function setDeliveryMechanism($deliveryMechanism)
    {
        $this->deliveryMechanism = $deliveryMechanism;
    
        return $this;
    }

    /**
     * Get deliveryMechanism
     *
     * @return integer 
     */
    public function getDeliveryMechanism()
    {
        return $this->deliveryMechanism;
    }

    /**
     * Set deliveryAgent
     *
     * @param integer $deliveryAgent
     * @return Modality
     */
    public function setDeliveryAgent($deliveryAgent)
    {
        $this->deliveryAgent = $deliveryAgent;
    
        return $this;
    }

    /**
     * Get deliveryAgent
     *
     * @return integer 
     */
    public function getDeliveryAgent()
    {
        return $this->deliveryAgent;
    }

    /**
     * Set deliveryMechanismSpecify
     *
     * @param string $deliveryMechanismSpecify
     * @return Modality
     */
    public function setDeliveryMechanismSpecify($deliveryMechanismSpecify)
    {
        $this->deliveryMechanismSpecify = $deliveryMechanismSpecify;
    
        return $this;
    }

    /**
     * Get deliveryMechanismSpecify
     *
     * @return string 
     */
    public function getDeliveryMechanismSpecify()
    {
        return $this->deliveryMechanismSpecify;
    }

    /**
     * Set deliveryAgentSpecify
     *
     * @param string $deliveryAgentSpecify
     * @return Modality
     */
    public function setDeliveryAgentSpecify($deliveryAgentSpecify)
    {
        $this->deliveryAgentSpecify = $deliveryAgentSpecify;
    
        return $this;
    }

    /**
     * Get deliveryAgentSpecify
     *
     * @return string 
     */
    public function getDeliveryAgentSpecify()
    {
        return $this->deliveryAgentSpecify;
    }

    /**
     * Set modalitySpecify
     *
     * @param string $modalitySpecify
     * @return Modality
     */
    public function setModalitySpecify($modalitySpecify)
    {
        $this->modalitySpecify = $modalitySpecify;

        return $this;
    }

    /**
     * Get modalitySpecify
     *
     * @return string 
     */
    public function getModalitySpecify()
    {
        return $this->modalitySpecify;
    }

    /**
     * Set scale
     *
     * @param float $scale
     * @return Modality
     */
    public function setScale($scale)
    {
        $this->scale = $scale;
    
        return $this;
    }

    /**
     * Get scale
     *
     * @return float 
     */
    public function getScale()
    {
        return $this->scale;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Modality
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set transfers
     *
     * @param float $transfers
     * @return Modality
     */
    public function setTransfers($transfers)
    {
        $this->transfers = $transfers;
    
        return $this;
    }

    /**
     * Get transfers
     *
     * @return float 
     */
    public function getTransfers()
    {
        return $this->transfers;
    }
}