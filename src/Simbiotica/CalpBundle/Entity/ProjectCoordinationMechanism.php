<?php

namespace Simbiotica\CalpBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\Table(name="project_coordination_mechanism")
 * @Gedmo\Loggable(logEntryClass="Simbiotica\CalpBundle\Entity\Logs\ProjectCoordinationMechanismLogs")
 * @Gedmo\TranslationEntity(class="Simbiotica\CalpBundle\Entity\Translations\ProjectCoordinationMechanismTranslation")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */

class ProjectCoordinationMechanism
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"project"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="coordinationMechanisms", cascade={"persist"})
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * @Gedmo\Versioned
     */
    protected $project;

    /**
     * @ORM\ManyToOne(targetEntity="CoordinationMechanism", inversedBy="projects", cascade={"persist"})
     * @ORM\JoinColumn(name="coordination_mechanism_id", referencedColumnName="id")
     * @Gedmo\Versioned
     * @Groups({"project"})
     */
    protected $coordinationMechanism;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="specify", type="text", nullable=true)
     * @Groups({"project"})
     */
    protected $specify;
    
    /**
     * @ORM\Column(name="position", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected $position;
    
    /**
     * @ORM\Column(name="enabled", type="boolean")
     * @Gedmo\Versioned
     */
    protected $enabled;
    
    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="createdProjectCoordinationMechanisms")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    protected $createdBy;
    
    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="updatedProjectCoordinationMechanisms")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    protected $updatedBy;
    
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at" ,type="datetime")
     */
    protected $createdAt;
    
    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at" ,type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;
    
    /**
     * @ORM\OneToMany(targetEntity="Simbiotica\CalpBundle\Entity\Translations\ProjectCoordinationMechanismTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function __toString()
    {
        $name = array();
        if($this->project)
            $name[] = $this->project->__toString();
        if($this->coordinationMechanism)
            $name[] = $this->coordinationMechanism->__toString();
        
        return count($name)?implode(' - ', $name):"New Project - Coordination Mechanism";
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->enabled = true;
        $this->translations = new ArrayCollection();
    }
    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Project
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Project
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Project
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set createdBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $createdBy
     * @return Project
     */
    public function setCreatedBy(\Simbiotica\CalpBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $updatedBy
     * @return Project
     */
    public function setUpdatedBy(\Simbiotica\CalpBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;
    
        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set project
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $project
     * @return ProjectCoordinationMechanism
     */
    public function setProject(\Simbiotica\CalpBundle\Entity\Project $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \Simbiotica\CalpBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return ProjectCoordinationMechanism
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    
        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set specify
     *
     * @param string $specify
     * @return ProjectCoordinationMechanism
     */
    public function setSpecify($specify)
    {
        $this->specify = $specify;
    
        return $this;
    }

    /**
     * Get specify
     *
     * @return string 
     */
    public function getSpecify()
    {
        return $this->specify;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return ProjectCoordinationMechanism
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set coordinationMechanism
     *
     * @param \Simbiotica\CalpBundle\Entity\CoordinationMechanism $coordinationMechanism
     * @return ProjectCoordinationMechanism
     */
    public function setCoordinationMechanism(\Simbiotica\CalpBundle\Entity\CoordinationMechanism $coordinationMechanism = null)
    {
        $this->coordinationMechanism = $coordinationMechanism;
    
        return $this;
    }

    /**
     * Get coordinationMechanism
     *
     * @return \Simbiotica\CalpBundle\Entity\CoordinationMechanism 
     */
    public function getCoordinationMechanism()
    {
        return $this->coordinationMechanism;
    }

    public function getTranslations()
    {
        return $this->translations;
    }

    public function setTranslations(\Doctrine\Common\Collections\ArrayCollection $translations)
    {
        $this->translations = $translations;
        return $this;
    }

    public function addTranslation($translation)
    {
        $translation->setObject($this);
        $this->translations[] = $translation;
        return $this;
    }

    public function removeTranslation($translation)
    {
        $this->translations->removeElement($translation);
    }
}