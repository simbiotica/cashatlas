<?php

namespace Simbiotica\CalpBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="email_log")
 */

class EmailLog
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="array")
     */
    protected $receiver;    

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    protected $bcc;    

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    protected $cc;    

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $sender;
    
    /**
     * @ORM\Column(name="real_receiver", type="array")
     */
    protected $realReceiver;

    /**
     * @ORM\Column(type="text")
     */
    protected $body;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $sendAt;

    /**
     * @ORM\Column(type="string")
     */
    protected $subject;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set receiver
     *
     * @param string $receiver
     * @return EmailLogs
     */
    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;
    
        return $this;
    }

    /**
     * Get receiver
     *
     * @return string 
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Set realReceiver
     *
     * @param string $realReceiver
     * @return EmailLogs
     */
    public function setRealReceiver($realReceiver)
    {
        $this->realReceiver = $realReceiver;
    
        return $this;
    }

    /**
     * Get realReceiver
     *
     * @return string 
     */
    public function getRealReceiver()
    {
        return $this->realReceiver;
    }

    /**
     * Set sendAt
     *
     * @param \DateTime $sendAt
     * @return EmailLogs
     */
    public function setSendAt($sendAt)
    {
        $this->sendAt = $sendAt;
    
        return $this;
    }

    /**
     * Get sendAt
     *
     * @return \DateTime 
     */
    public function getSendAt()
    {
        return $this->sendAt;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return EmailLogs
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return EmailLogs
     */
    public function setBody($body)
    {
        $this->body = $body;
    
        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set bcc
     *
     * @param string $bcc
     * @return EmailLog
     */
    public function setBcc($bcc)
    {
        $this->bcc = $bcc;
    
        return $this;
    }

    /**
     * Get bcc
     *
     * @return string 
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * Set cc
     *
     * @param string $cc
     * @return EmailLog
     */
    public function setCc($cc)
    {
        $this->cc = $cc;
    
        return $this;
    }

    /**
     * Get cc
     *
     * @return string 
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * Set sender
     *
     * @param string $sender
     * @return EmailLog
     */
    public function setSender($sender)
    {
        $this->sender = $sender;
    
        return $this;
    }

    /**
     * Get sender
     *
     * @return string 
     */
    public function getSender()
    {
        return $this->sender;
    }
}