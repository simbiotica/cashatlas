<?php

namespace Simbiotica\CalpBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\Table(name="organization")
 * @Gedmo\Loggable(logEntryClass="Simbiotica\CalpBundle\Entity\Logs\OrganizationLogs")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 * @Gedmo\TranslationEntity(class="Simbiotica\CalpBundle\Entity\Translations\OrganizationTranslation")
 * @ORM\Entity(repositoryClass="Simbiotica\CalpBundle\Repository\OrganizationRepository")
 * @ORM\HasLifecycleCallbacks()
 */

class Organization
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"dashboard", "projectPreview", "project", "constants", "search"})
     */
    protected $id;

    /**
     * @Gedmo\Versioned
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=128, nullable=true)
     * @Groups({"constants", "search"})
     */
    protected $name;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectOrganization", mappedBy="organization", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $projects;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectDonor", mappedBy="organization", cascade={"persist"})
     * @ORM\OrderBy({"amount" = "ASC"})
     */
    protected $donatingProjects;
    
    /**
     * @ORM\OneToMany(targetEntity="OrganizationUser", mappedBy="organization", cascade={"persist"}, orphanRemoval=true)
     */
    protected $users;
    
    /**
     * @ORM\ManyToOne(targetEntity="Media", inversedBy="organizations", cascade={"persist"})
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     * @Gedmo\Versioned
     */
    protected $media;
    
    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="createdOrganizations")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    protected $createdBy;
    
    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="updatedOrganizations")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    protected $updatedBy;
    
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at" ,type="datetime")
     */
    protected $createdAt;
    
    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at" ,type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;
    
    /**
     * @ORM\OneToMany(targetEntity="Simbiotica\CalpBundle\Entity\Translations\OrganizationTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function __toString()
    {
    	return empty($this->name)?"":$this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->projects = new ArrayCollection();
        $this->translations = new ArrayCollection();
        $this->users = new ArrayCollection();
    }
    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Project
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Project
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Project
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set createdBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $createdBy
     * @return Project
     */
    public function setCreatedBy(\Simbiotica\CalpBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $updatedBy
     * @return Project
     */
    public function setUpdatedBy(\Simbiotica\CalpBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;
    
        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Add projects
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectOrganization $projects
     * @return Organization
     */
    public function addProject(\Simbiotica\CalpBundle\Entity\ProjectOrganization $project)
    {
        $project->setOrganization($this);
        $this->projects[] = $project;
    
        return $this;
    }

    /**
     * Remove projects
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectOrganization $projects
     */
    public function removeProject(\Simbiotica\CalpBundle\Entity\ProjectOrganization $projects)
    {
        $this->projects->removeElement($projects);
    }

    /**
     * Get projects
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * Set media
     *
     * @param \Simbiotica\CalpBundle\Entity\Media $media
     * @return Organization
     */
    public function setMedia(\Simbiotica\CalpBundle\Entity\Media $media = null)
    {
        $this->media = $media;
    
        return $this;
    }

    /**
     * Get media
     *
     * @return \Simbiotica\CalpBundle\Entity\Media 
     */
    public function getMedia()
    {
        return $this->media;
    }

    public function getTranslations()
    {
        return $this->translations;
    }

    public function setTranslations(\Doctrine\Common\Collections\ArrayCollection $translations)
    {
        $this->translations = $translations;
        return $this;
    }

    public function addTranslation($translation)
    {
        $translation->setObject($this);
        $this->translations[] = $translation;
        return $this;
    }

    public function removeTranslation($translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Add users
     *
     * @param OrganizationUser $user
     * @internal param \Simbiotica\CalpBundle\Entity\OrganizationUser $users
     * @return Organization
     */
    public function addUser(\Simbiotica\CalpBundle\Entity\OrganizationUser $user)
    {
        $user->setOrganization($this);
        
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Simbiotica\CalpBundle\Entity\OrganizationUser $users
     */
    public function removeUser(\Simbiotica\CalpBundle\Entity\OrganizationUser $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }
    
    public function setUsers($users)
    {
        $this->users = new ArrayCollection();

        foreach($users as $user)
            $this->addUser($user);
        
        return $this;
    }

    /**
     * Add donatingProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectDonor $donatingProject
     * @return Organization
     */
    public function addDonatingProject(ProjectDonor $donatingProject)
    {
        $donatingProject->setOrganization($this);
        $this->donatingProjects[] = $donatingProject;
    
        return $this;
    }

    /**
     * Remove donatingProjects
     *
     * @param \Simbiotica\CalpBundle\Entity\ProjectDonor $donatingProjects
     */
    public function removeDonatingProject(ProjectDonor $donatingProjects)
    {
        $this->donatingProjects->removeElement($donatingProjects);
    }

    /**
     * Get donatingProjects
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDonatingProjects()
    {
        return $this->donatingProjects;
    }
}