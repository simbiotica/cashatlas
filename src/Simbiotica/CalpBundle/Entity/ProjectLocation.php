<?php

namespace Simbiotica\CalpBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\Table(name="project_location")
 * @Gedmo\Loggable(logEntryClass="Simbiotica\CalpBundle\Entity\Logs\ProjectLocationLogs")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */

class ProjectLocation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="locations", cascade={"persist"})
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * @Gedmo\Versioned
     */
    protected $project;

    /**
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="projects", cascade={"persist"})
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * @Gedmo\Versioned
     * @Groups({"projectPreview", "project"})
     */
    protected $country;

    /**
     * @ORM\ManyToOne(targetEntity="Region", inversedBy="projects", cascade={"persist"})
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * @Gedmo\Versioned
     * @Groups({"projectPreview", "project"})
     */
    protected $region;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="location_city", type="text", nullable=true)
     * @Groups({"project"})
     */
    protected $city;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(name="scale", type="decimal", nullable=true)
     * @Groups({"projectPreview", "project"})
     */
    protected $scale;
    
    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="createdProjectLocations")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    protected $createdBy;
    
    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="updatedProjectLocations")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    protected $updatedBy;
    
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at" ,type="datetime")
     */
    protected $createdAt;
    
    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at" ,type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function __toString()
    {
        $name = array();
        if($this->project)
            $name[] = $this->project;
        if($this->region)
            $name[] = $this->region;
        if($this->scale)
            $name[] = $this->scale;
        
        return count($name)?implode(' - ', $name):"New Project - Region";
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->enabled = true;
        $this->translations = new ArrayCollection();
    }

    /**
     * Set scale
     *
     * @param float $scale
     * @return ProjectLocation
     */
    public function setScale($scale)
    {
        $this->scale = $scale;

        return $this;
    }

    /**
     * Get scale
     *
     * @return float 
     */
    public function getScale()
    {
        return $this->scale;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ProjectLocation
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return ProjectLocation
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return ProjectLocation
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set project
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $project
     * @return ProjectLocation
     */
    public function setProject(\Simbiotica\CalpBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \Simbiotica\CalpBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set region
     *
     * @param \Simbiotica\CalpBundle\Entity\Region $region
     * @return ProjectLocation
     */
    public function setRegion(\Simbiotica\CalpBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Simbiotica\CalpBundle\Entity\Region 
     */
    public function getRegion()
    {
        return $this->region;
    }
    
    /**
     * Set country
     *
     * @param \Simbiotica\CalpBundle\Entity\Country $country
     * @return ProjectLocation
     */
    public function setCountry(\Simbiotica\CalpBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Simbiotica\CalpBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set createdBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $createdBy
     * @return ProjectLocation
     */
    public function setCreatedBy(\Simbiotica\CalpBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $updatedBy
     * @return ProjectLocation
     */
    public function setUpdatedBy(\Simbiotica\CalpBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return ProjectLocation
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }
}