<?php

namespace Simbiotica\CalpBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="project_field")
 * @Gedmo\Loggable(logEntryClass="Simbiotica\CalpBundle\Entity\Logs\ProjectFieldLogs")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */

class ProjectField
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="fields", cascade={"persist"})
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * @Gedmo\Versioned
     */
    protected $project;

    /**
     * @ORM\ManyToOne(targetEntity="Field", inversedBy="projects", cascade={"persist"})
     * @ORM\JoinColumn(name="field_id", referencedColumnName="id")
     * @Gedmo\Versioned
     */
    protected $field;
    
    /**
     * @ORM\Column(name="value", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected $value;
    
    /**
     * @ORM\Column(name="position", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected $position;
    
    /**
     * @ORM\Column(name="public", type="boolean")
     * @Gedmo\Versioned
     */
    protected $public;
    
    /**
     * @ORM\Column(name="enabled", type="boolean")
     * @Gedmo\Versioned
     */
    protected $enabled;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="createdProjectFields")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    protected $createdBy;
    
    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="updatedProjectFields")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    protected $updatedBy;
    
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at" ,type="datetime")
     */
    protected $createdAt;
    
    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at" ,type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function __toString()
    {
        $name = array();
        if($this->project)
            $name[] = $this->project;
        if($this->field)
            $name[] = $this->field;
        if($this->value)
            $name[] = $this->value;
        
        return count($name)?implode(' - ', $name):"New Project - Field";
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->enabled = true;
        $this->public = true;
        $this->position = 0;
        $this->translations = new ArrayCollection();
    }
    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Project
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Project
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Project
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set createdBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $createdBy
     * @return Project
     */
    public function setCreatedBy(\Simbiotica\CalpBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $updatedBy
     * @return Project
     */
    public function setUpdatedBy(\Simbiotica\CalpBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;
    
        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set project
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $project
     * @return ProjectField
     */
    public function setProject(\Simbiotica\CalpBundle\Entity\Project $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \Simbiotica\CalpBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return ProjectField
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return ProjectField
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    
        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return ProjectField
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set field
     *
     * @param \Simbiotica\CalpBundle\Entity\Field $field
     * @return ProjectField
     */
    public function setField(\Simbiotica\CalpBundle\Entity\Field $field = null)
    {
        $this->field = $field;
    
        return $this;
    }

    /**
     * Get field
     *
     * @return \Simbiotica\CalpBundle\Entity\Field 
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set public
     *
     * @param boolean $public
     * @return ProjectField
     */
    public function setPublic($public)
    {
        $this->public = $public;
    
        return $this;
    }

    /**
     * Get public
     *
     * @return boolean 
     */
    public function getPublic()
    {
        return $this->public;
    }
}