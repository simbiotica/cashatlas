<?php

namespace Simbiotica\CalpBundle\Entity;

use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Simbiotica\CalpBundle\Repository\ContactRepository")
 * @ORM\Table(name="contact")
 * @Gedmo\Loggable(logEntryClass="Simbiotica\CalpBundle\Entity\Logs\ContactLogs")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 * @ORM\HasLifecycleCallbacks()
 */
class Contact
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    protected $language;
    
    /**
     * @ORM\Column(name="contact_type", type="string", nullable=true)
     */
    protected $contactType;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="requestedContacts", cascade={"persist"})
     * @ORM\JoinColumn(name="requested_by", referencedColumnName="id")
     */
    protected $requestedBy;
    
    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="assignedContacts", cascade={"persist"})
     * @ORM\JoinTable(name="contact_user")
     */
    protected $assignedTo;
    
    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="relatedContacts", cascade={"persist"})
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    protected $project;
    
    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="createdContacts", cascade={"persist"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    protected $createdBy;
    
    /**
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="updatedContacts", cascade={"persist"})
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    protected $updatedBy;
    
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at" ,type="datetime")
     */
    protected $createdAt;
    
    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at" ,type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;


    public function __toString()
    {
        return $this->getContent()?:"New Contact";
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set language
     *
     * @param string $language
     * @return Contact
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    
        return $this;
    }

    /**
     * Get language
     *
     * @return string 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set contactType
     *
     * @param string $contactType
     * @return Contact
     */
    public function setContactType($contactType)
    {
        $this->contactType = $contactType;
    
        return $this;
    }

    /**
     * Get contactType
     *
     * @return string 
     */
    public function getContactType()
    {
        return $this->contactType;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Contact
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Contact
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Contact
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Contact
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Contact
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Contact
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set createdBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $createdBy
     * @return Contact
     */
    public function setCreatedBy(\Simbiotica\CalpBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $updatedBy
     * @return Contact
     */
    public function setUpdatedBy(\Simbiotica\CalpBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;
    
        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set requestedBy
     *
     * @param \Simbiotica\CalpBundle\Entity\User $requestedBy
     * @return Contact
     */
    public function setRequestedBy(\Simbiotica\CalpBundle\Entity\User $requestedBy = null)
    {
        $this->requestedBy = $requestedBy;
    
        return $this;
    }

    /**
     * Get requestedBy
     *
     * @return \Simbiotica\CalpBundle\Entity\User 
     */
    public function getRequestedBy()
    {
        return $this->requestedBy;
    }

    /**
     * Set project
     *
     * @param \Simbiotica\CalpBundle\Entity\Project $project
     * @return Contact
     */
    public function setProject(\Simbiotica\CalpBundle\Entity\Project $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \Simbiotica\CalpBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Contact
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->assignedTo = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add assignedTo
     *
     * @param \Simbiotica\CalpBundle\Entity\User $assignedTo
     * @return Contact
     */
    public function addAssignedTo(\Simbiotica\CalpBundle\Entity\User $assignedTo)
    {
        $this->assignedTo[] = $assignedTo;
    
        return $this;
    }

    /**
     * Remove assignedTo
     *
     * @param \Simbiotica\CalpBundle\Entity\User $assignedTo
     */
    public function removeAssignedTo(\Simbiotica\CalpBundle\Entity\User $assignedTo)
    {
        $this->assignedTo->removeElement($assignedTo);
    }

    /**
     * Get assignedTo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAssignedTo()
    {
        return $this->assignedTo;
    }
    
    public function setAssignedTo($assignees)
    {
        $this->assignedTo = new ArrayCollection();
        
        foreach ($assignees as $assignee) 
        {
            $this->addAssignedTo($assignee);
        }
        
        return $this;
    }
    
    public function getStatusString()
    {
        $statusArray = SimbioticaCalpBundle::getContactStatus();
        
        return isset($this->status)?$statusArray[$this->status]:null;
    }
}