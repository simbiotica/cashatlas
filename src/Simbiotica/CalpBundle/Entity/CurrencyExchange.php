<?php

namespace Simbiotica\CalpBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Simbiotica\CalpBundle\Repository\CurrencyExchangeRepository")
 * @ORM\Table(name="currency_exchange")
 * @Gedmo\Loggable(logEntryClass="Simbiotica\CalpBundle\Entity\Logs\CountryLogs")
 * @ORM\HasLifecycleCallbacks()
 */

class CurrencyExchange
{
	/**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

   	/**
    * @ORM\Column(type="datetime")
    */
    protected $date;   	

    /**
    * @ORM\Column(type="decimal")
    */
    protected $rate;

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->setDate(strtotime('today'));
    }
    
    /**
     * Get id
     *
     * @return integer 
     */

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return CurrencyExchange
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set rate
     *
     * @param float $rate
     * @return CurrencyExchange
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    
        return $this;
    }

    /**
     * Get rate
     *
     * @return float 
     */
    public function getRate()
    {
        return $this->rate;
    }

}