<?php

namespace Simbiotica\CalpBundle\Exporter;

use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Simbiotica\CalpBundle\Entity\Project;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;

class ExcelExporter{
    
    private $titles;
    private $translator;
    
    public function __construct(Translator $translator) {
        $this->translator = $translator;
        $this->titles = array_map(function($item) use ($translator) {
            /** @Ignore */
            return $translator->trans($item, array(), 'SimbioticaFront');
        }, array(
            'excel.id',
            'excel.organizations',
            'excel.affiliates',
            'excel.implementing_partners',
            'excel.countries',
            'excel.regions',
            'excel.cities',
            'excel.specific_objectives',
            'excel.sectors',
            'excel.scale',
            'excel.donors',
            'excel.overall_budget',
            'excel.allocated_budget',
            'excel.context',
            'excel.starting_date',
            'excel.ending_date',
            'excel.first_cash_date',
            'excel.maket_assessment',
            'excel.coordination_mechanisms',
            'excel.modalities',
            'excel.type_of_intervention',
            'excel.contact_person_name',
            'excel.contact_person_position',
            'excel.contact_person_emails',
            'excel.organization_url',
            'excel.language',
            'excel.description',
            'excel.media',
            'excel.additional_fields',
        ));
    }
    
    public function exportProject(array $projects, $format = 'xlsx') {
        if(!in_array($format, array('xlsx', 'csv')))
            return null;
        
        $objPHPExcel = new \PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Cash Atlas");
        $objPHPExcel->getProperties()->setLastModifiedBy("Cash Atlas");
        $objPHPExcel->getProperties()->setTitle("Cash Atlas - Project List Report");

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->fromArray($this->titles, null, 'A1');
        $objPHPExcel->getActiveSheet()->SetCellValue('A2', count($projects));
        
        foreach($projects as $index => $project)
        {
            $objPHPExcel->getActiveSheet()->fromArray($this->projectToArray($project), null, 'A'.($index+2));
        }
        
        switch ($format) {
            case 'xlsx':
                $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
                break;
            case 'csv':
                $objWriter = new \PHPExcel_Writer_CSV($objPHPExcel);
                $objWriter->setDelimiter(';');
                break;
        }
        
        $objWriter->save('php://output');
    }
    
    private function projectToArray(Project $project)
    {
        $result = array();
        
        $result[] = $project->getId();
        $result[] = implode(', ', array_map(function($po){return $po->getOrganization()->__toString();}, $project->getOrganizations()->toArray()));
        $result[] = $project->getAffiliates();
        $result[] = $project->getImplementingPartners();
        
        //Countries & Regions
        $countries = array();
        $regions = array();
        foreach($project->getLocations() as $location)
        {
            if ($location->getCountry() && !array_key_exists($location->getCountry()->getId(), $countries))
            {
                $countries[$location->getCountry()->getId()] = $location->getCountry()->__toString();
            }
            if ($region = $location->getRegion())
            {
                $country = $location->getCountry()?:$region->getCountry();
                if (!array_key_exists($country->getId(), $countries))
                {
                    $countries[$country->getId()] = $country->__toString();
                }
                if (!array_key_exists($region->getId(), $regions))
                {
                    $regions[$region->getId()] = ($region->__toString().($location->getScale()?' - '.$location->getScale():''));
                }
            }
        }
        $result[] = implode(', ', array_values($countries));
        $result[] = implode(', ', array_values($regions));
        
        $result[] = $project->getCity();
        $result[] = $project->getSpecificObjective();
        
        //Sectors
        $sectors = array_map(function($sector){return $sector->__toString();}, $project->getSectors()->toArray());
        $otherSector = $project->getOtherSectors();
        if(!empty($otherSector))
            array_push($sectors, $otherSector);
        $result[] = implode(', ', $sectors);
        
        $result[] = $project->getScale();
        $result[] = implode(', ', array_map(function($pd){return $pd->getOrganization()->__toString().($pd->getAmount()?' - '.$pd->getAmount():'');}, $project->getDonors()->toArray()));
        $result[] = round($project->getBudgetOverall(), 0);
        $result[] = round($project->getBudgetAllocated(), 0);

        //Contexts
        $contextIndexes = $project->getProjectContext();
        $contextList = SimbioticaCalpBundle::getContexts();
        $contexts = array();
        foreach($contextIndexes as $contextIndex)
        {
            /** @Ignore */
            $contexts[] = $this->translator->trans($contextList[(int) $contextIndex], array(), 'SimbioticaFront');
        }
        $result[] = implode(', ', $contexts);
        
        //3 dates
        $startingDate = $project->getStartingDate();
        if(count($startingDate))
            $result[] = ($startingDate['month']+1).'/'.$startingDate['year'];
        else
            $result[] = '';
        
        $endingDate = $project->getEndingDate();
        if(count($endingDate))
            $result[] = ($endingDate['month']+1).'/'.$endingDate['year'];
        else
            $result[] = '';
        
        $firstcashDate = $project->getFirstCashDate();
        if(count($firstcashDate))
            $result[] = ($firstcashDate['month']+1).'/'.$firstcashDate['year'];
        else
            $result[] = '';
        
        //Market Assessment
        $marketAssessmentArray = $project->getMarketAssessment();
        if(count($marketAssessmentArray) && $marketAssessmentArray['options'])
        {
            $marketAssessmentList = SimbioticaCalpBundle::getMarketAsssessmentTypes();
            /** @Ignore */
            $marketAssessment = $this->translator->trans($marketAssessmentList[$marketAssessmentArray['options']], array(), 'SimbioticaFront');
            
            $marketAssessmentOther = $marketAssessmentArray['other'];
            if(!empty($marketAssessmentOther))
            {
                $marketAssessment .= ' - '.$marketAssessmentOther;
            }
            $result[] = $marketAssessment;
        }
        else
            $result[] = '';
        
        $result[] = implode(', ', array_map(function($pcm){
            if ($pcm->getCoordinationMechanism())
                return $pcm->getCoordinationMechanism()->__toString().($pcm->getSpecify()?' - '.$pcm->getSpecify():'');
            else
                return '';
            
        }, $project->getCoordinationMechanisms()->toArray()));

        //Modalities
        $modalitiesArray = $project->getModalities();
        if(count($modalitiesArray))
        {
            $modalities = array();
            foreach($modalitiesArray as $modalityElement)
            {
                $modality = array();
                if($modalityElement->getModality())
                {
                    $modalityList = SimbioticaCalpBundle::getModalities();
                    /** @Ignore */
                    $modality[] = $this->translator->trans($modalityList[$modalityElement->getModality()], array(), 'SimbioticaFront');
                    if($modalityElement->getModalitySpecify())
                    {
                        $modality[] = $modalityElement->getModalitySpecify();
                    }
                    if($modalityElement->getDeliveryMechanism())
                    {
                        $deliveryMechanismList = SimbioticaCalpBundle::getModalities(null, 1);
                        /** @Ignore */
                        $modality[] = $this->translator->trans($deliveryMechanismList[$modalityElement->getDeliveryMechanism()], array(), 'SimbioticaFront');
                        if($modalityElement->getDeliveryMechanismSpecify())
                        {
                            $modality[] = $modalityElement->getDeliveryMechanismSpecify();
                        }
                        if($modalityElement->getDeliveryAgent())
                        {
                            $deliveryAgentList = SimbioticaCalpBundle::getModalities(null, 2);
                            /** @Ignore */
                            $modality[] = $this->translator->trans($deliveryAgentList[$modalityElement->getDeliveryAgent()], array(), 'SimbioticaFront');
                            if($modalityElement->getDeliveryAgentSpecify())
                            {
                                $modality[] = $modalityElement->getDeliveryAgentSpecify();
                            }

                        }
                        if($modalityElement->getDeliveryAgentSpecify())
                        {
                            $modality[] = $modalityElement->getDeliveryAgentSpecify();
                        }

                    }
                    if($modalityElement->getScale())
                    {
                        $modality[] = $modalityElement->getScale();
                    }
                    if($modalityElement->getAmount())
                    {
                        $modality[] = $modalityElement->getAmount();
                    }
                    if($modalityElement->getTransfers())
                    {
                        $modality[] = $modalityElement->getTransfers();
                    }
                }
                // $modalities[] = '['.implode(', ', $modality).']';
                $modalities[] = implode(', ', $modality).'#';
            }
            // $result[] = implode(', ',$modalities);
            $result[] = implode($modalities);
        }
        else
            $result[] = '';
        
        //Type of Intervention
        if ($project->getTypeOfIntervention())
        {
            $typeOfInterventionList = SimbioticaCalpBundle::getInterventionTypes();
            /** @Ignore */
            $typeOfIntervention = $this->translator->trans($typeOfInterventionList[$project->getTypeOfIntervention()], array(), 'SimbioticaFront');
            if($project->getTypeOfInterventionSpecify())
            {
                $typeOfIntervention .= ' - '.$project->getTypeOfInterventionSpecify();
            }
            $result[] = $typeOfIntervention;
        }
        else
            $result[] = '';
        
        $result[] = $project->getContactPersonName();
        $result[] = $project->getContactPersonPosition();
        $result[] = $project->getContactPersonEmailOne().($project->getContactPersonEmailTwo()?(', '.$project->getContactPersonEmailTwo()):'');
        $result[] = $project->getContactOrganizationUrl();
        /** @Ignore */
        $result[] = $this->translator->trans($project->getLanguage(), array(), 'SimbioticaFront');
        $result[] = $project->getDescription();
        
        //Media
        $mediaList = array();
        foreach($project->getGalleryHasMedias() as $ghm)
        {
            if ($ghm->getEnabled() && $ghm->getMedia())
                $mediaList[] = $ghm->getMedia()->__toString();
        }
        $result[] = implode(', ',$mediaList);
        
        //Fields
        $fieldList = array();
        foreach($project->getFields() as $pf)
        {
            $fieldList[] = $pf->getField()->__toString().' - '.$pf->getValue();
        }
        $result[] = implode(', ',$fieldList);
        return $result;
    }
}
?>