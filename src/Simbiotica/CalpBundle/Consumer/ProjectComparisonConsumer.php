<?php

namespace Simbiotica\CalpBundle\Consumer;

use Simbiotica\CalpBundle\Entity\Project;
use Doctrine\ORM\EntityManager;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use Elastica\Query\Bool;
use Elastica\Query\Terms;
use Elastica\Query\Text;
use Simbiotica\CalpBundle\Entity\SimilarProject;
use Sonata\NotificationBundle\Consumer\ConsumerInterface;
use Sonata\NotificationBundle\Consumer\ConsumerEvent;

class ProjectComparisonConsumer implements ConsumerInterface {
    
    private $elasticConnection;
    private $entityManager;
    
    public function __construct(TransformedFinder $elasticConnection, EntityManager $entityManager) {
        $this->elasticConnection = $elasticConnection;
        $this->entityManager = $entityManager;
    }
    
    public function process(ConsumerEvent $event)
    {
        $message = $event->getMessage();
        $projectId = $message->getValue('projectId');

        if (is_null($projectId)) {
            return;
        }

        $project = $this->entityManager->getRepository('SimbioticaCalpBundle:Project')->find($projectId);
        $this->compareProject($project);
    }
    
    public function compareProject(Project $project) {
        $boolQuery = new Bool();
        $minimumNumberShouldMatch = -1;

        $publishedQuery = new Text();
        $publishedQuery->setFieldQuery('published', '3');
        $boolQuery->addMust($publishedQuery);

        $orgs = array();
        foreach($project->getOrganizations() as $po)
        {
            if ($po->getOrganization())
                $orgs[] = $po->getOrganization()->getId();
        }
        
        if(count($orgs))
        {
            $organizationQuery = new Terms();
            $organizationQuery->setTerms('organizations.organization.id', $orgs);
            $boolQuery->addShould($organizationQuery);
        }
        
        $donors = array();
        foreach($project->getDonors() as $pd)
        {
            if ($pd->getOrganization())
                $donors[] = $pd->getOrganization()->getId();
        }
        
        if(count($donors))
        {
            $donorQuery = new Terms();
            $donorQuery->setTerms('donors.organization.id', $donors);
            $boolQuery->addShould($donorQuery);
        }

        $locations = array();
        foreach($project->getLocations() as $pl)
        {
            if($pl->getRegion())
                $locations[] = $pl->getRegion()->getId();
        }

        if(count($locations))
        {
            $locationsQuery = new Terms();
            $locationsQuery->setTerms('locations.region.id', $locations);
            $boolQuery->addMust($locationsQuery);
        }        

        $sectors = array();
        foreach($project->getSectors() as $ps)
        {
            if($ps->getId())
                $sectors[] = $ps->getId();
        }

        if(count($sectors))
        {
            $sectorsQuery = new Terms();
            $sectorsQuery->setTerms('sectors.id', $sectors);
            $boolQuery->addMust($sectorsQuery);
        }


        $boolQuery->setMinimumNumberShouldMatch($minimumNumberShouldMatch);
        $similarProjects = $this->elasticConnection->find($boolQuery, 1000);
        
        if (count($similarProjects))
        {
            $rep = $this->entityManager->getRepository('SimbioticaCalpBundle:SimilarProject');
            
            if(!$project->getId())
            {
                foreach($similarProjects as $match)
                {
                    $similarProject = new SimilarProject();
                    $similarProject->setReference($project);
                    $similarProject->setTarget($match);

                    $this->entityManager->persist($similarProject);
                }
            }
            else
            {
                $knownMatches = $rep->getNewMatches($project->getId());
                $knownMatches[] = $project->getId();
                foreach($similarProjects as $match)
                {
                    if(!in_array($match->getId(), $knownMatches))
                    {
                        $similarProject = new SimilarProject();
                        $similarProject->setReference($project);
                        $similarProject->setTarget($match);

                        $this->entityManager->persist($similarProject);
                    }
                }
            }
           
            $this->entityManager->flush();
        }
    }
}
?>