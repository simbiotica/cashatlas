<?php

namespace Simbiotica\CalpBundle\Consumer;

use Simbiotica\CalpBundle\Entity\Project;
use Doctrine\ORM\EntityManager;
use Sonata\NotificationBundle\Consumer\ConsumerInterface;
use Sonata\NotificationBundle\Consumer\ConsumerEvent;
use Simbiotica\CalpBundle\Utils\AccountMailer;

class ProjectNotificationConsumer implements ConsumerInterface {
    
    private $entityManager;
    private $mailer;
    
    public function __construct(EntityManager $entityManager, AccountMailer $mailer) {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
    }
    
    public function process(ConsumerEvent $event)
    {
        $message = $event->getMessage();
        $type = $message->getValue('type');

        switch ($type) {
            case 'notification':
                $this->sendIdleNotification($message);
                break;
        }
    }
    
    private function sendIdleNotification($message) {
        $projectIds = $message->getValue('ids');
        $projects = $this->entityManager->getRepository('SimbioticaCalpBundle:Project')->findById($projectIds);
        
        $notifications = array();
        
        foreach($projects as $project)
        {
            if(!$project->getNotifiedAt() || date_add($project->getNotifiedAt(), date_interval_create_from_date_string('7 days')) < new \Datetime() )
            {
                $editors = $this->getEditors($project);
                foreach($editors as $id)
                {
                    if(!array_key_exists($id, $notifications))
                        $notifications[$id] = array();
                    $notifications[$id][] = $project;
                }
            }
            $project->setNotifiedAt(new \Datetime());
        }
        $this->entityManager->flush();
        
        foreach($notifications as $id => $projects)
        {
            $projectsCollection = array();
            $user = $this->entityManager->getRepository('SimbioticaCalpBundle:User')->find($id);
            foreach ($projects as $project) 
            {
                    $projectsCollection[] = $project;
            }
            $this->mailer->sendProjectNotificationEmailMessage($user, $projectsCollection);
        }
        
        $this->entityManager->flush();
    }
    
    private function getEditors(Project $project) {
        $editors = array();
        foreach ($project->getOwners() as $user)
        {
            $editors[$user->getId()] = $user->getId();
        }
        foreach ($project->getEditors() as $user)
        {
            $editors[$user->getId()] = $user->getId();
        }
        foreach ($project->getOrganizations() as $po)
        {
            if($po->getOrganization())
                foreach ($po->getOrganization()->getUsers() as $ou)
                {
                    if($ou->getUser() && $ou->getEnabled())
                        $editors[$ou->getUser()->getId()] = $ou->getUser()->getId();
                }
        }
        
        return $editors;
    }
}
?>