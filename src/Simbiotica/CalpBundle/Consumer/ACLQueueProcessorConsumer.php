<?php

namespace Simbiotica\CalpBundle\Consumer;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;

use Sonata\NotificationBundle\Consumer\ConsumerInterface;
use Sonata\NotificationBundle\Consumer\ConsumerEvent;

use Simbiotica\CalpBundle\Entity\Project;
use Simbiotica\CalpBundle\Entity\Media;
use Simbiotica\CalpBundle\Entity\Organization;
use Simbiotica\CalpBundle\Entity\Group;
use Simbiotica\CalpBundle\Entity\User;
use Simbiotica\CalpBundle\Entity\Contact;


class ACLQueueProcessorConsumer implements ConsumerInterface{

    protected $container;
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }
    
    public function process(ConsumerEvent $event)
    {
        $message = $event->getMessage();
        $rawQueue = $message->getValue('payload');
        
        $queue = $this->container->get('simbiotica.calp.acl.queue');
        $queue->setRawQueue($rawQueue);
        $processedQueue = $queue->getProcessedQueue();
        
        if (count($processedQueue))
        {
            $this->generateACLs($processedQueue);
        }
    }
    
    private function generateACLs($queue) {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->clear();
        
        foreach($queue as $class => $entityList)
        {
            switch ($class) {
                case "Simbiotica\CalpBundle\Entity\Project":
                    foreach($entityList as $id)
                    {
                        $project = $em->getRepository($class)->find($id);
                        if($project)
                            $this->recomputeProjectAcls($project);
                        unset($queue[$class][$id]);
                    }
                    break;
                case "Simbiotica\CalpBundle\Entity\Media":
                    foreach($entityList as $id)
                    {
                        $media = $em->getRepository($class)->find($id);
                        $this->recomputeMediaAcls($media);
                        unset($queue[$class][$id]);
                    }
                    break;
                case "Simbiotica\CalpBundle\Entity\User":
                    foreach($entityList as $id)
                    {
                        $user = $em->getRepository($class)->find($id);
                        $this->recomputeUserAcls($user);
                        unset($queue[$class][$id]);
                    }
                    break;
                case "Simbiotica\CalpBundle\Entity\Organization":
                    foreach($entityList as $id)
                    {
                        $organization = $em->getRepository($class)->find($id);
                        $this->recomputeOrganizationAcls($organization);
                        unset($queue[$class][$id]);
                    }
                    break;
                case "Simbiotica\CalpBundle\Entity\Contact":
                    foreach($entityList as $id)
                    {
                        $contact = $em->getRepository($class)->find($id);
                        $this->recomputeContactAcls($contact);
                        unset($queue[$class][$id]);
                    }
                    break;
                default:
                    foreach($entityList as $id)
                    {
                        $entity = $em->getRepository($class)->find($id);
                        $this->recomputeGeneralAcls($entity, ($entity instanceof Group || $entity instanceof User));
                        unset($queue[$class][$id]);
                    }
                    break;
            }
        }
    }
    
    private function recomputeGeneralAcls($entity, $publicVisibility = false)
    {
        $aclProvider = $this->container->get('security.acl.provider');
        
        $objectIdentity = ObjectIdentity::fromDomainObject($entity);

        try {
            $aclProvider->deleteAcl($objectIdentity);
        } catch (AclNotFoundException $anfe) {
        }

        $acl = $aclProvider->createAcl($objectIdentity);

        $roleSA = new RoleSecurityIdentity('ROLE_SUPER_ADMIN');
        $acl->insertObjectAce($roleSA, MaskBuilder::MASK_OWNER);
        
        if($publicVisibility)
        {
            $roleA = new RoleSecurityIdentity('ROLE_ADMIN');
            $acl->insertObjectAce($roleA, MaskBuilder::MASK_VIEW);
        }
        
        $aclProvider->updateAcl($acl);
    }
    
    private function recomputeContactAcls(Contact $contact)
    {
        $aclProvider = $this->container->get('security.acl.provider');
        
        $objectIdentity = ObjectIdentity::fromDomainObject($contact);

        try {
            $aclProvider->deleteAcl($objectIdentity);
        } catch (AclNotFoundException $anfe) {
        }

        $acl = $aclProvider->createAcl($objectIdentity);

        $roleSA = new RoleSecurityIdentity('ROLE_SUPER_ADMIN');
        $acl->insertObjectAce($roleSA, MaskBuilder::MASK_OWNER);
        
        foreach($contact->getAssignedTo() as $user)
        {
            $acl->insertObjectAce(UserSecurityIdentity::fromAccount($user), MaskBuilder::MASK_EDIT);
        }
        
        $aclProvider->updateAcl($acl);
    }
    
    private function recomputeOrganizationAcls(Organization $organization)
    {
        $aclProvider = $this->container->get('security.acl.provider');
        
        $objectIdentity = ObjectIdentity::fromDomainObject($organization);

        try {
            $aclProvider->deleteAcl($objectIdentity);
        } catch (AclNotFoundException $anfe) {
        }

        $acl = $aclProvider->createAcl($objectIdentity);

        $roleSA = new RoleSecurityIdentity('ROLE_SUPER_ADMIN');
        $acl->insertObjectAce($roleSA, MaskBuilder::MASK_OWNER);
        
        foreach($organization->getUsers() as $ou)
        {
            if ($ou->getEnabled() && $ou->getUser())
            {
                $acl->insertObjectAce(UserSecurityIdentity::fromAccount($ou->getUser()), MaskBuilder::MASK_EDIT);
            }
        }
        
        $aclProvider->updateAcl($acl);
    }
    
    private function recomputeProjectAcls(Project $entity)
    {
        /**
         * This wipes all acls from all project associated entities and recomputes them.
         * It's not the optimal solution, but it's simple, so easier to understand and debug
         */
        $aclProvider = $this->container->get('security.acl.provider');
        
        $objects = array_merge(
                array($entity),
                $entity->getOrganizations()->toArray(),
                $entity->getLocations()->toArray(),
                $entity->getDonors()->toArray(),
                $entity->getCoordinationMechanisms()->toArray(),
                $entity->getFields()->toArray(),
                $entity->getModalities()->toArray(),
                $entity->getGalleryHasMedias()->toArray()
        );

        $viewers = array();
        $editors = array();
        $owners = array();
        
        foreach($entity->getOwners() as $user)
        {
            $owners[] = UserSecurityIdentity::fromAccount($user);
        }
        foreach($entity->getEditors() as $user)
        {
            $editors[] = UserSecurityIdentity::fromAccount($user);
        }
        
        foreach ($entity->getOrganizations() as $pos) 
        {
            if($pos->getOrganization())
            {
                foreach ($pos->getOrganization()->getUsers() as $ou)
                {
                    if ($ou->getEnabled() && $ou->getAdmin() && $ou->getUser())
                    {
                        $owners[] = UserSecurityIdentity::fromAccount($ou->getUser());
                    }
                    elseif ($ou->getEnabled() && !$ou->getAdmin() && $ou->getUser())
                    {
                        $editors[] = UserSecurityIdentity::fromAccount($ou->getUser());
                    }
                }
            }
        }
        
        $uniqueViewers = array_unique($viewers);
        $uniqueEditors = array_unique($editors);
        $uniqueOwners = array_unique($owners);

        foreach($objects as $object)
        {
            $objectIdentity = ObjectIdentity::fromDomainObject($object);

            try {
                $aclProvider->deleteAcl($objectIdentity);
            } catch (AclNotFoundException $anfe) {
            }
            
            $acl = $aclProvider->createAcl($objectIdentity);

            $roleSA = new RoleSecurityIdentity('ROLE_SUPER_ADMIN');
            $acl->insertObjectAce($roleSA, MaskBuilder::MASK_OWNER);
            
            foreach ($uniqueOwners as $owner)
            {
                $acl->insertObjectAce($owner, MaskBuilder::MASK_OWNER);
            }
            foreach ($uniqueEditors as $editor)
            {
                $acl->insertObjectAce($editor, MaskBuilder::MASK_EDIT);
            }
            foreach ($uniqueViewers as $viewer)
            {
                $acl->insertObjectAce($viewer, MaskBuilder::MASK_VIEW);
            }
            
            $aclProvider->updateAcl($acl);
        }
    }
    
    private function recomputeMediaAcls(Media $entity)
    {
        /**
         * This wipes all acls from the given media and recomputes them
         * It's not the optimal solution, but it's simple, so easier to understand and debug
         */
        $aclProvider = $this->container->get('security.acl.provider');
        
        $viewers = array();
        $editors = array();
        $owners = array();
        
        foreach($entity->getUsers() as $user)
        {
            $owners[] = UserSecurityIdentity::fromAccount($user);
        }
        foreach($entity->getGalleryHasMedias() as $ghm)
        {
            if($ghm->getGallery())
            {
                foreach($ghm->getGallery()->getOwners() as $user)
                {
                    $owners[] = UserSecurityIdentity::fromAccount($user);
                }
                foreach($ghm->getGallery()->getEditors() as $user)
                {
                    $editors[] = UserSecurityIdentity::fromAccount($user);
                }
                foreach ($ghm->getGallery()->getOrganizations() as $pos) 
                {
                    foreach ($entity->getOrganizations() as $pos) 
                    {
                        if($pos->getOrganization())
                        {
                            foreach ($pos->getOrganization()->getUsers() as $ou)
                            {
                                if ($ou->getEnabled() && $ou->getAdmin() && $ou->getUser())
                                {
                                    $owners[] = UserSecurityIdentity::fromAccount($ou->getUser());
                                }
                                elseif ($ou->getEnabled() && !$ou->getAdmin() && $ou->getUser())
                                {
                                    $viewers[] = UserSecurityIdentity::fromAccount($ou->getUser());
                                }
                            }
                        }
                    }
                }
            }
        }
        
        $uniqueViewers = array_unique($viewers);
        $uniqueEditors = array_unique($editors);
        $uniqueOwners = array_unique($owners);
        
        $objectIdentity = ObjectIdentity::fromDomainObject($entity);

        try {
            $aclProvider->deleteAcl($objectIdentity);
        } catch (AclNotFoundException $anfe) {
        }

        $acl = $aclProvider->createAcl($objectIdentity);

        $roleSA = new RoleSecurityIdentity('ROLE_SUPER_ADMIN');
        $acl->insertObjectAce($roleSA, MaskBuilder::MASK_OWNER);

        foreach ($uniqueOwners as $owner)
        {
            $acl->insertObjectAce($owner, MaskBuilder::MASK_OWNER);
        }
        foreach ($uniqueEditors as $editor)
        {
            $acl->insertObjectAce($editor, MaskBuilder::MASK_EDIT);
        }
        foreach ($uniqueViewers as $viewer)
        {
            $acl->insertObjectAce($viewer, MaskBuilder::MASK_VIEW);
        }

        $aclProvider->updateAcl($acl);
    }
    
    private function recomputeUserAcls(User $entity)
    {
        $aclProvider = $this->container->get('security.acl.provider');
        
        $viewers = array();
        $owners = array(UserSecurityIdentity::fromAccount($entity));
        
        foreach($entity->getOrganizations() as $tou)
        {
            if($tou->getOrganization() instanceof Organization)
            {
                foreach($tou->getOrganization()->getUsers() as $ou)
                {
                    if ($ou->getEnabled() && $ou->getAdmin() && $ou->getUser() && $ou->getUser()->getId() != $entity->getId())
                    {
                        $viewers[] = UserSecurityIdentity::fromAccount($ou->getUser());
                    }
                }
            }
        }

        $uniqueViewers = array_unique($viewers);
        $uniqueOwners = array_unique($owners);

        $objectIdentity = ObjectIdentity::fromDomainObject($entity);

        try {
            $aclProvider->deleteAcl($objectIdentity);
        } catch (AclNotFoundException $anfe) {
        }

        $acl = $aclProvider->createAcl($objectIdentity);

        $roleSA = new RoleSecurityIdentity('ROLE_SUPER_ADMIN');
        $acl->insertObjectAce($roleSA, MaskBuilder::MASK_OWNER);

        foreach ($uniqueOwners as $owner)
        {
            $acl->insertObjectAce($owner, MaskBuilder::MASK_OWNER);
        }
        foreach ($uniqueViewers as $viewer)
        {
            $acl->insertObjectAce($viewer, MaskBuilder::MASK_VIEW);
        }

        $aclProvider->updateAcl($acl);
    }
}

?>
