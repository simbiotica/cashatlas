<?php

namespace Simbiotica\CalpBundle\Form\Type;

use Sonata\UserBundle\Form\Type\ProfileType as ParentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProfileFormType extends ParentType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('firstname', null, array('label' => 'form.registration.firstname'))
                ->add('lastname', null, array('label' => 'form.registration.lastname'))
                ->add('email', 'email', array('label' => 'form.registration.email'))
                ->add('position', null, array('label' => 'form.registration.position'))
                ->add('country', 'country', array('label' => 'form.registration.country'))
                ->add('plainPassword', 'repeated', array(
                    'required' => false,
                    'type' => 'password',
                    'first_options' => array('label' => 'form.registration.password'),
                    'second_options' => array('label' => 'form.registration.password_confirmation'),
                    'invalid_message' => 'fos_user.password.mismatch',
                ))
        ;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array(
            'translation_domain' => 'SimbioticaFront'
        ));
    }

    public function getName()
    {
        return 'simbiotica_user_profile';
    }
}
