<?php

namespace Simbiotica\CalpBundle\Form\Type;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType as BaseType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Symfony\Component\Form\Extension\Core\ChoiceList\SimpleChoiceList;

class ReportFormType extends BaseType
{
    protected $container;
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $organizationsQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Organization")->getListWithProjectsAsOrganization($this->container->get('request')->getLocale());
        $organizationsChoices = array();
        foreach($organizationsQuery as $org)
            $organizationsChoices[$org['id']] = $org['name'];
        $organizations = new SimpleChoiceList($organizationsChoices);
        
        $donorsQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Organization")->getListWithProjectsAsDonor($this->container->get('request')->getLocale());
        $donorsChoices = array();
        foreach($donorsQuery as $donor)
            $donorsChoices[$donor['id']] = $donor['name'];
        $donors = new SimpleChoiceList($donorsChoices);
        
        $countriesQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Country")->getListWithProjects($this->container->get('request')->getLocale());
        $countriesChoices = array();
        foreach($countriesQuery as $country)
            $countriesChoices[$country['id']] = $country['name'];
        $countries = new SimpleChoiceList($countriesChoices);
        
        $builder
                ->add('country', 'choice', array(
                    'label' => 'report.country', 
                    'required' => false, 
                    'attr' => array('placeholder' => 'report.placeholder'), 
                    'multiple' => true,
                    'choice_list' => $countries
                ))
                ->add('after', 'partial_date', array('label' => 'report.after', 'required' => false))
                ->add('before', 'partial_date', array('label' => 'report.before', 'required' => false))
                ->add('selector', 'choice', array('label' => false, 'required' => true, 'expanded' => true, 'choices' => array('organization' => 'report.organization', 'donor' => 'report.donor')))
                ->add('organization', 'choice', array(
                    'label' => 'report.organization', 
                    'required' => false, 
                    'multiple' => true, 
                    'attr' => array('placeholder' => 'report.placeholder'),
                    'choice_list' => $organizations
                ))
                ->add('donor', 'choice', array(
                    'label' => 'report.donor', 
                    'required' => false, 
                    'multiple' => true, 
                    'attr' => array('placeholder' => 'report.placeholder'),
                    'choice_list' => $donors
                ))
                ->add('fields', 'choice', array('choices' => SimbioticaCalpBundle::getReportFields(), 'label' => 'report.fields', 'required' => true, 'expanded' => true, 'multiple' => true))
        ;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array(
            'translation_domain' => 'SimbioticaFront'
        ));
    }

    public function getName()
    {
        return 'simbiotica_form_report';
    }
}