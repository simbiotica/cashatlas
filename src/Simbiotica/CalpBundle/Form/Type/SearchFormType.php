<?php 

namespace Simbiotica\CalpBundle\Form\Type;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\ChoiceList\SimpleChoiceList;

class SearchFormType extends AbstractType
{
    protected $container;
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $organizationsQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Organization")->getListWithProjectsAsOrganization($this->container->get('request')->getLocale());
        $organizationsChoices = array();
        foreach($organizationsQuery as $org)
            $organizationsChoices[$org['id']] = $org['name'];
        $organizations = new SimpleChoiceList($organizationsChoices);

        
        $donorsQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Organization")->getListWithProjectsAsDonor($this->container->get('request')->getLocale());
        $donorsChoices = array();
        foreach($donorsQuery as $donor)
            $donorsChoices[$donor['id']] = $donor['name'];
        $donors = new SimpleChoiceList($donorsChoices);
        
        $sectorsQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Sector")->getList($this->container->get('request')->getLocale());
        $sectorsChoices = array();
        foreach($sectorsQuery as $sector)
            $sectorsChoices[$sector['id']] = $sector['name'];
        $sectors = new SimpleChoiceList($sectorsChoices);
        
        $modalities = $this->container->get('simbiotica.utils.array_translator')->translateArray(SimbioticaCalpBundle::getModalities(), 'SimbioticaFront');
        $deliveryMechanisms = $this->container->get('simbiotica.utils.array_translator')->translateArray(SimbioticaCalpBundle::getModalities(null, 1), 'SimbioticaFront');
        $deliveryAgents = $this->container->get('simbiotica.utils.array_translator')->translateArray(SimbioticaCalpBundle::getModalities(null, 2), 'SimbioticaFront');

        $maxScale = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Project")->getMaxScale();
        
        $maxYearsArray = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Project")->getYearRange();
        $maxYear = max($maxYearsArray[0]['first_cash_max'], $maxYearsArray[0]['end_max']);
        $minYear = min($maxYearsArray[0]['first_cash_min'], $maxYearsArray[0]['end_min']);
        
        asort($modalities);
        asort($deliveryMechanisms);
        asort($deliveryAgents);
        
        $builder
        ->add('organization', 'entity', array(
            'label' => 'search.organization', 
            'required' => false, 
            'class' => 'Simbiotica\CalpBundle\Entity\Organization', 
            'multiple' => true, 
            'choice_list' => $organizations
        ))
        ->add('donor', 'entity', array(
            'label' => 'search.donor', 
            'required' => false, 
            'class' => 'Simbiotica\CalpBundle\Entity\Organization', 
            'multiple' => true, 
            'choice_list' => $donors
        ))
        ->add('sector', 'entity', array(
            'label' => 'search.sector', 
            'required' => false, 
            'class' => 'Simbiotica\CalpBundle\Entity\Sector', 
            'multiple' => true, 
            'choice_list' => $sectors
        ))
        ->add('implementingPartners', 'text', array('label' => 'search.implementing_partners', 'required' => false))
        ->add('scaleMin', 'hidden', array('attr' => array('data-min' => '0')))
        ->add('scaleMax', 'hidden', array('attr' => array('data-max' => $maxScale)))
        ->add('modality', 'choice', array('label' => 'search.modality', 'required' => false, 'choices' => $modalities, 'multiple' => true))
        ->add('deliveryMechanism', 'choice', array('label' => 'search.delivery_mechanism', 'required' => false, 'choices' => $deliveryMechanisms, 'multiple' => true))
        ->add('deliveryAgent', 'choice', array('label' => 'search.delivery_agent', 'required' => false, 'choices' => $deliveryAgents, 'multiple' => true))
        ->add('status', 'choice', array('label' => 'search.status', 'required' => false, 'choices' => SimbioticaCalpBundle::getProjectStatus(), 'multiple' => true))
        ->add('years', 'choice', array(
                'choices' => array_combine(range($minYear, $maxYear), range($minYear, $maxYear)),
                'label' => 'search.year',
                'multiple' => true,
                'required' => false,
        ))
        ->add('duration', 'choice', array('label' => 'search.duration', 'required' => false, 'multiple' => true, 'choices' => SimbioticaCalpBundle::getDurationRanges()))
        ->add('keyword', 'text', array('label' => 'search.keyword', 'required' => false))
        ;
        
        if($this->container->get('security.context')->isGranted('ROLE_EDITOR'))
        {
            $builder->add('owner', 'checkbox', array('label' => 'search.owner', 'required' => false));
        }
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array(
            'translation_domain' => 'SimbioticaFront'
        ));
    }

    public function getName()
    {
        return 'simbiotica_form_search';
    }
}

?>