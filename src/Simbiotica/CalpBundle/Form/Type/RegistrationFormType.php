<?php

namespace Simbiotica\CalpBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;

class RegistrationFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('firstname', null, array('required' => true, 'label' => 'form.registration.firstname', 'attr' => array('class' => 'required')))
                ->add('lastname', null, array('required' => true, 'label' => 'form.registration.lastname', 'attr' => array('class' => 'required')))
                ->add('email', 'email', array('required' => true, 'label' => 'form.registration.email', 'attr' => array('class' => 'required')))
                ->add('organizations', 'collection', array(
                    /** @Ignore */
                    'label' => false, 
                    'required' => true, 
                    'type' => new OrganizationUserFormType(),
                ))
                ->add('otherOrganization', null, array('label' => 'form.registration.other_organization', 'attr' => array('class' => 'required')))
                ->add('position', null, array('label' => 'form.registration.position', 'attr' => array('class' => 'required')))
                ->add('intent', 'choice', array('label' => 'form.registration.intent', 'choices' => SimbioticaCalpBundle::getIntents(), 'attr' => array('class' => 'required')))
                ->add('country', 'country', array('required' => true, 'label' => 'form.registration.country', 'empty_value' => 'form.registration.country.empty_value', 'attr' => array('placeholder' => 'form.registration.country.empty_value', 'class' => 'required')))
                ->add('relevance', null, array('required' => true, 'label' => 'form.registration.relevance', 'attr' => array('class' => 'required')))
                ->add('plainPassword', 'repeated', array(
                    'required' => true,
                    'type' => 'password',
                    'first_options' => array('label' => 'form.registration.password', 'attr' => array('class' => 'required-password')),
                    'second_options' => array('label' => 'form.registration.password_confirmation', 'attr' => array('class' => 'required-password')),
                    'invalid_message' => 'fos_user.password.mismatch',
                ))
        ;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array(
            'translation_domain' => 'SimbioticaFront'
        ));
    }

    public function getName()
    {
        return 'simbiotica_user_registration';
    }
}