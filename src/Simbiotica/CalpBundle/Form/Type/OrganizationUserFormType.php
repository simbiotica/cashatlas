<?php

namespace Simbiotica\CalpBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class OrganizationUserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('organization', null, array(
                'required' => false, 
                'label' => 'form.registration.organization', 
                'attr' => array(
                    'data-placeholder' => 'form.registration.organization.empty_value', 
                    'class' => 'organization-required'), 
                'empty_value' => 'form.registration.organization.empty_value',
                'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('o')
                            ->where('o.name IS NOT NULL')
                            ->orderBy('o.name', 'asc');
                    },
                ))
        ;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => 'Simbiotica\CalpBundle\Entity\OrganizationUser',
            'translation_domain' => 'SimbioticaFront'
        ));
    }

    public function getName()
    {
        return 'simbiotica_organization_user';
    }
}