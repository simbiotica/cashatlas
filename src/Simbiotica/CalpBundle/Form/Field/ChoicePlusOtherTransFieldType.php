<?php

namespace Simbiotica\CalpBundle\Form\Field;

use Symfony\Component\Form\FormBuilderInterface;
use Simbiotica\CalpBundle\Form\Transformer\SerializerTransformer;

class ChoicePlusOtherTransFieldType extends ChoicePlusOtherFieldType
{
    private $choiceListCache = array();
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        
        $transformer = new SerializerTransformer();
        $builder->addModelTransformer($transformer);
    }
    
    
    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'choice_plus_other_trans';
    }
}

?>