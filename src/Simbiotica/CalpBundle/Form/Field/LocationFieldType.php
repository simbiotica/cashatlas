<?php

namespace Simbiotica\CalpBundle\Form\Field;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LocationFieldType extends AbstractType
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $factory = $builder->getFormFactory();
        
        $refreshData = function ($form, $countries, $regions, $city) use ($factory, $options) {
            $form->add($factory->createNamed('countries', 'entity', $countries, array(
                    'class'         => 'Simbiotica\CalpBundle\Entity\Country',
                    'property'      => 'name',
                    'label'         => 'location.country',
                    'required'      => false,
                    'empty_value' => 'country.empty_value',
                    'attr' => array('class' => 'location country'),
                    'multiple' => true,
                    'group_by' => 'continent.name',
                    'translation_domain' => $options['translation_domain']?:'SimbioticaAdmin',
            )));
            $form->add($factory->createNamed('regions', 'entity', $regions, array(
                    'class'         => 'Simbiotica\CalpBundle\Entity\Region',
                    'property'      => 'name',
                    'label'         => 'location.region',
                    'required' => true,
                    'empty_value' => 'region.empty_value',
                    'attr' => array('class' => 'location region'),
                    'multiple' => true,
                    'group_by' => 'country.name',
                    'translation_domain' => $options['translation_domain']?:'SimbioticaAdmin',
                    'query_builder' => function (EntityRepository $repository) use ($countries) {
                               $qb = $repository->createQueryBuilder('region')
                                                ->innerJoin('region.country', 'country');

                               if(count($countries) == 0) {
                                   $qb = $qb->where('1 = 2');
                               } else {
                                   $qb = $qb->where('country.id IN (:country_ids)')
                                            ->setParameter('country_ids', array_map(function($item){return $item->getId();}, $countries->toArray()));
                               }

                               return $qb;
                           }
            )));
            $form->add($factory->createNamed('city', 'textarea', $city, array(
                    'label' => 'location.city',
                    'attr' => array('class' => 'location city'),
                    'required' => false,
                    'translation_domain' => $options['translation_domain']?:'SimbioticaAdmin',
            )));
        };
        
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) use ($refreshData) {
            $form = $event->getForm();
            $data = $form->getParent()->getData();
            
            $countries = ($data?$data->getCountries():new ArrayCollection());
            $regions = ($data?$data->getRegions():new ArrayCollection());
            $city = ($data?$data->getCity():null);
            
            $refreshData($form, $countries, $regions, $city);
            
        });
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'virtual' => true
        ));
    }

    public function getName()
    {
        return 'location';
    }
}