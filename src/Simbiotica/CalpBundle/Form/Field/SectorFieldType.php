<?php

namespace Simbiotica\CalpBundle\Form\Field;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SectorFieldType extends AbstractType
{
    protected $container;
    
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $sectors = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Sector")->getList($this->container->get('request')->getLocale(), false);
        
        $builder->add('sectors', 'entity', array(
                /** @Ignore */
                'label' => false,
                'multiple' => $options['multiple'],
                'required' => false,
                'attr' => array('class' => "toggle_select"),
                'translation_domain' => $options['translation_domain']?:'SimbioticaAdmin',
                'class' => 'Simbiotica\CalpBundle\Entity\Sector',
                'choices' => $sectors
        ))
        ->add('otherSectors', 'text', array(
                'required' => false,
        		'label' => 'form.label_other_sectors',
                'attr' => array('class' => "toggle_other"),
                'translation_domain' => $options['translation_domain']?:'SimbioticaAdmin',
        ));
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'multiple' => false,
            'virtual' => true,
        ));
    }
    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'sector';
    }
}

?>