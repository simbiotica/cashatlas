<?php

namespace Simbiotica\CalpBundle\Form\Field;

use Simbiotica\CalpBundle\Form\Transformer\ArrayTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;

class PartialDateFieldType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new ArrayTransformer();
        $builder->addModelTransformer($transformer);
        $builder
        ->add('month', 'choice', array(
                'choices' => SimbioticaCalpBundle::getMonths(),
                'required' => $options['required'],
                'label' => 'date.month',
                'translation_domain' => $options['translation_domain']?:'SimbioticaAdmin',
        ))
        ->add('year', 'choice', array(
                'choices' => SimbioticaCalpBundle::getYearRange(),
                'required' => $options['required'],
                'label' => 'date.year',
                'translation_domain' => $options['translation_domain']?:'SimbioticaAdmin',
        ));
    }
    
    public function getName()
    {
        return 'partial_date';
    }
}

?>