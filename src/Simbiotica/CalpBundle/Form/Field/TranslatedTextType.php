<?php

namespace Simbiotica\CalpBundle\Form\Field;

use Symfony\Component\Form\AbstractType;

class TranslatedTextType extends AbstractType
{
    public function getParent()
    {
        return 'text';
    }

    public function getName()
    {
    	return 'translated_text';
    }
}