<?php

namespace Simbiotica\CalpBundle\Form\Field;

use Simbiotica\CalpBundle\Form\Transformer\ArrayTransformer;
use Symfony\Component\Form\Extension\Core\ChoiceList\SimpleChoiceList;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Event\DataEvent;
use Symfony\Component\Form\FormEvents;

class ChoicePlusOtherFieldType extends AbstractType
{
    private $choiceListCache = array();
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new ArrayTransformer();
        $builder->addModelTransformer($transformer);
        
        $builder
        ->add('options', 'choice', array(
                /** @Ignore */
                'label' => false,
                'choices' => $options['choices'],
                'multiple' => $options['multiple'],
                'preferred_choices' => $options['preferred_choices'],
                'required' => false,
                'attr' => array('class' => ($options['invert'])?"toggle_select_invert":"toggle_select"),
                'translation_domain' => $options['translation_domain']?:'SimbioticaAdmin',
                'empty_value' => $options['empty_value'],
        ))
        ->add('other', 'text', array(
                'required' => false,
                'attr' => array('class' => "toggle_other"),
                'translation_domain' => $options['translation_domain']?:'SimbioticaAdmin',
        ));
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $choiceListCache =& $this->choiceListCache;

        $choiceList = function (Options $options) use (&$choiceListCache) {
            // Harden against NULL values (like in EntityType and ModelType)
            $choices = null !== $options['choices'] ? $options['choices'] : array();

            // Reuse existing choice lists in order to increase performance
            $hash = md5(json_encode(array($choices, $options['preferred_choices'])));

            if (!isset($choiceListCache[$hash])) {
                $choiceListCache[$hash] = new SimpleChoiceList($choices, $options['preferred_choices']);
            }

            return $choiceListCache[$hash];
        };

        $emptyData = function (Options $options) {
            if ($options['multiple'] || $options['expanded']) {
                return array();
            }

            return '';
        };

        $emptyValue = function (Options $options) {
            return $options['required'] ? null : '';
        };

        $emptyValueNormalizer = function (Options $options, $emptyValue) {
            if ($options['multiple'] || $options['expanded']) {
                // never use an empty value for these cases
                return null;
            } elseif (false === $emptyValue) {
                // an empty value should be added but the user decided otherwise
                return null;
            }

            // empty value has been set explicitly
            return $emptyValue;
        };

        $compound = function (Options $options) {
            return $options['expanded'];
        };

        $resolver->setDefaults(array(
            'multiple'          => false,
            'expanded'          => false,
            'choice_list'       => $choiceList,
            'choices'           => array(),
            'preferred_choices' => array(),
            'empty_data'        => $emptyData,
            /** @Ignore */
            'empty_value'       => $emptyValue,
            'error_bubbling'    => false,
            'compound'          => true,
            'invert'            => false,
            // The view data is always a string, even if the "data" option
            // is manually set to an object.
            // See https://github.com/symfony/symfony/pull/5582
            'data_class'        => null,
        ));

        $resolver->setNormalizers(array(
            /** @Ignore */
            'empty_value' => $emptyValueNormalizer,
        ));

        $resolver->setAllowedTypes(array(
            'choice_list' => array('null', 'Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceListInterface'),
        ));
    }
    
    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'choice_plus_other';
    }
}

?>