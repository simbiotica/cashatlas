<?php

namespace Simbiotica\CalpBundle\Form\Field;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;

class DatePickerFieldType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'empty_value' => '',
                'attr' => array(
                    'autocomplete' => 'off',
                    'class' => 'date_picker',
                ),
        ));
    }
 
    public function getParent()
    {
        return 'date';
    }
 
    public function getName()
    {
        return 'date_picker';
    }
}

?>