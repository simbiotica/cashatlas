<?php 

namespace Simbiotica\CalpBundle\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * @author simbiotica
 *
 */
class SerializerTransformer implements DataTransformerInterface
{
    public function transform($text)
    {
        if (null === $text) {
            return array();
        }
        
        return unserialize($text);
    }

    public function reverseTransform($array)
    {
        if (!is_array($array)) {
            return null;
        }
        
        return serialize($array);
    }
}

?>