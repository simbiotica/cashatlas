<?php 

namespace Simbiotica\CalpBundle\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * This is needed because of some weird bug when hadling choice_plus_other, where the array is not created
 * It doesn't actually transform anything, it just ensures that an array is passed when
 * the values are empty, rather than an empty string that would cause a casting warning
 * 
 * @author simbiotica
 *
 */
class ArrayTransformer implements DataTransformerInterface
{
    public function transform($text)
    {
        if (null === $text) {
            return array();
        }

        return $text;
    }

    public function reverseTransform($array)
    {
        if (!is_array($array)) {
            return null;
        }
        
        return $array;
    }
}

?>