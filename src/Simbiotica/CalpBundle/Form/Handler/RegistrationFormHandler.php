<?php

namespace Simbiotica\CalpBundle\Form\Handler;

use Simbiotica\CalpBundle\Entity\OrganizationUser;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Model\UserInterface;
use Simbiotica\CalpBundle\Utils\AccountMailer;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;

class RegistrationFormHandler
{
    protected $request;
    protected $userManager;
    protected $form;
    protected $mailer;
    protected $tokenGenerator;
    protected $entityManager;

    public function __construct(FormInterface $form, Request $request, UserManagerInterface $userManager, AccountMailer $mailer, EntityManager $entityManager)
    {
        $this->form = $form;
        $this->request = $request;
        $this->userManager = $userManager;
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
    }
    
    /**
     * @param boolean $confirmation
     */
    public function process(&$whitelisted, $locale)
    {
        $user = $this->createUser();
        $this->form->setData($user);

        if ('POST' === $this->request->getMethod()) {
            $this->form->bind($this->request);

            if ($this->form->isValid()) {
                
                $whitelist = $this->entityManager->getRepository('SimbioticaCalpBundle:WhiteList')->findByEmail($user->getEmail());
                $whitelisted = array_shift($whitelist);
                
                $user->setLocale($locale);
                $this->onSuccess($user, $whitelisted);

                return true;
            }
        }

        return false;
    }
    
    protected function onSuccess(UserInterface $user, $whitelisted)
    {
        //If the user didn't choose an organization from the list, remove $ou
        $ou = $user->getOrganizations()->first();
        if ($ou->getOrganization() == null)
            $user->removeOrganization($ou);
        
        $user->setEnabled($whitelisted instanceof \Simbiotica\CalpBundle\Entity\WhiteList);
        
        if (!$user->isEnabled())
        {
            $user->setPendingApproval(true);
            $this->userManager->updateUser($user);
            $this->mailer->sendRegistrationEmailMessage($user);
        }
        
        else
        {
            $this->userManager->updateUser($user);
            /**
             * Pending resolution of https://github.com/symfony/symfony/issues/6417
             * In the meantime, email is sent on the landing page, which is not ideal
             * as the email gets sent every time someone visits the page
             */
//            $this->mailer->sendWelcomeEmailMessage($user);
        }
    }
    
    protected function createUser()
    {
        //On form generation, an OrganizationUser needs to exist for the
        //corresponding fields to be displayed
        $user = $this->userManager->createUser();
        $ou = new OrganizationUser();
        $user->addOrganization($ou);
        return $user;
    }
}
