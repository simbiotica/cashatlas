<?php

namespace Simbiotica\CalpBundle\Form\Handler;

use Sonata\UserBundle\Form\Handler\ProfileFormHandler as ParentHandler;
use FOS\UserBundle\Model\UserInterface;

class ProfileFormHandler extends ParentHandler
{
    public function process(UserInterface $user)
    {
        $currentPassword = $user->getPlainPassword();
        $this->form->setData($user);
        
        if ('POST' == $this->request->getMethod()) {
            $this->form->bind($this->request);

            if ($this->form->isValid()) {
                if(strlen($user->getPassword()) == 0)
                    $user->setPlainPassword ($currentPassword);
                
                $this->onSuccess($user);

                return true;
            }

            // Reloads the user to reset its username. This is needed when the
            // username or password have been changed to avoid issues with the
            // security layer.
            $this->userManager->reloadUser($user);
        }

        return false;
    }
}
