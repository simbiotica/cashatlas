<?php

namespace Simbiotica\CalpBundle\MediaProvider;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\MediaBundle\Provider\FileProvider;
use Sonata\MediaBundle\Model\MediaInterface;

class SimbioticaExcelProvider extends FileProvider
{
    public function generatePublicUrl(MediaInterface $media, $format)
    {
        if ($format == 'reference') {
            $path = $this->getReferenceImage($media);
        } else {
            return sprintf('/bundles/simbioticacalp/img/assets/excel.png', $format);
        }

        return $this->getCdn()->getPath($path, $media->getCdnIsFlushable());
    }

    public function buildEditForm(FormMapper $formMapper) {
        parent::buildEditForm($formMapper);
        
        $formMapper
            ->add('importedAt', 'datetime', array(
                'required' => false, 
                'read_only' => true, 
                'date_widget' => 'single_text', 
                'date_format' => 'dd/MM/yyyy', 
                'time_widget' => 'single_text', 
                'translation_domain' => 'SimbioticaAdmin'
            ))
        ;
    }

}