<?php

namespace Simbiotica\CalpBundle\MediaProvider;

use Sonata\MediaBundle\Metadata\MetadataBuilderInterface;
use Sonata\MediaBundle\Provider\Pool;
use Buzz\Browser;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\MediaBundle\Resizer\ResizerInterface;
use Sonata\MediaBundle\Thumbnail\ThumbnailInterface;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\MediaBundle\Provider\YouTubeProvider;
use Gaufrette\Filesystem;
use Sonata\MediaBundle\CDN\CDNInterface;
use Sonata\MediaBundle\Generator\GeneratorInterface;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;

class SimbioticaURLProvider extends YouTubeProvider {

    protected $pool;

    public function __construct(Pool $pool, $name, Filesystem $filesystem, CDNInterface $cdn, GeneratorInterface $pathGenerator, ThumbnailInterface $thumbnail, Browser $browser, MetadataBuilderInterface $metadata, ResizerInterface $resizer = null) {
        parent::__construct($name, $filesystem, $cdn, $pathGenerator, $thumbnail, $browser, $metadata);
        $this->resizer = $resizer;
        $this->pool = $pool;
    }

    public function getDownloadResponse(MediaInterface $media, $format, $mode, array $headers = array()) {
        return new RedirectResponse($media->getProviderReference());
    }

    public function getHelperProperties(MediaInterface $media, $format, $options = array()) {
        return array_merge(array(
// 				'title'       => $media->getTitle(),
            'thumbnail' => $this->getReferenceImage($media),
            'file' => $this->getReferenceImage($media),
                ), $options);
    }

    public function generatePublicUrl(MediaInterface $media, $format)
    {
        if ($format == 'reference') {
            $path = $this->getReferenceImage($media);
        } else {
            return sprintf('/bundles/simbioticacalp/img/assets/url.png', $format);
        }

        return $this->getCdn()->getPath($path, $media->getCdnIsFlushable());
    }

    /**
     * {@inheritdoc}
     */
    public function getReferenceImage(MediaInterface $media) {
        return sprintf(__DIR__ . '/../Resources/public/images/assets/url-reference.png');
    }

    public function buildEditForm(FormMapper $formMapper, $isxml = false) {
        $formMapper
                ->add('context', 'text', array('attr' => array('class' => 'pass_disabled'), 'disabled' => true, 'translation_domain' => 'SonataMediaBundle'))
                ->add('providerName', 'text', array('attr' => array('class' => 'pass_disabled'), 'disabled' => true, 'translation_domain' => 'SonataMediaBundle'))
                ->add('name', null, array('required' => false, 'read_only' => true,))
                ->add('providerReference', null, array('read_only' => true))
                ->add('binaryContent', 'text', array('required' => false))
                ->end()
        ;
    }

    public function buildCreateForm(FormMapper $formMapper, $isxml = false) {
        $contexts = array();
        foreach ((array) $this->pool->getContexts() as $contextItem => $format) {
            if (in_array($this->name, $format['providers']))
                $contexts[$contextItem] = $contextItem;
        }

        $formMapper
                ->add('context', 'sonata_type_translatable_choice', array('catalogue' => 'SonataMediaBundle', 'required' => true, 'choices' => $contexts))
                ->add('binaryContent', 'text', array('label' => "URL"))
        ;
    }

    public function prePersist(MediaInterface $media) {
        $media->setName($media->getBinaryContent());
        $media->setProviderName($this->name);
        $media->setProviderStatus(MediaInterface::STATUS_OK);
        $url = parse_url($media->getBinaryContent());
        if (!isset($url['scheme'])) {
            $url['scheme'] = 'http';
            $media->setProviderReference(SimbioticaCalpBundle::unparse_url($url));
        } else {
            $media->setProviderReference($media->getBinaryContent());
        }
    }

    public function preUpdate(MediaInterface $media) {
        $media->setName($media->getBinaryContent());
        if ($media->getBinaryContent() != '') {
            $url = parse_url($media->getBinaryContent());
            if (!isset($url['scheme'])) {
                $url['scheme'] = 'http';
                $media->setProviderReference(SimbioticaCalpBundle::unparse_url($url));
            } else {
                $media->setProviderReference($media->getBinaryContent());
            }
        }
    }

}