<?php

namespace Simbiotica\CalpBundle\Menu;

use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Translation\Translator;

class MenuBuilder
{
    private $factory;
    private $doctrine;
    private $translator;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory, $doctrine, Translator $translator)
    {
        $this->factory = $factory;
        $this->doctrine = $doctrine;
        $this->translator = $translator;
    }

    public function createMainMenu(Request $request, SecurityContext $securityContext)
    {
        $menu = $this->factory->createItem('root', array('attributes' => array('class' => 'top-nav')));
        $menu->setCurrentUri($request->getRequestUri());
        
        $locale = $request->getLocale();
        $languages = array_flip(SimbioticaCalpBundle::getLanguages());
        $language = $languages[$locale];
        
        $contents = $this->doctrine->getRepository('SimbioticaCalpBundle:Content')
                ->getMenuContents($language);
        
        if(strpos($request->get('_route'),'simbiotica') !== false || $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED'))
            $menu->addChild($this->translator->trans('menu.the_map', array(), 'SimbioticaFront'), array('attributes' => array('class' => 'app-menu'), 'route' => 'simbiotica_calp_homepage'));

        $menu->addChild($this->translator->trans('menu.report', array(), 'SimbioticaFront'), array('route' => 'ajax_simbiotica_report_index'));
        
        $about = $menu->addChild($this->translator->trans('menu.about', array(), 'SimbioticaFront'), array('attributes' => array('class' => 'child-menu')));

        foreach ($contents as $content) {
            //if (strpos($request->get('_route'),'simbiotica') !== false || $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
                $about->addChild($content->getName(), array('route' => 'simbiotica_calp_content', 'routeParameters' => array('uri' => $content->getUri())));
            //}
        }

        $menu->addChild($this->translator->trans('menu.tutorial', array(), 'SimbioticaFront'), array('route' => 'simbiotica_calp_tutorial'));

        return $menu;
    }
}