<?php

namespace Simbiotica\CalpBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class AddContactProviderCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $pool = $container->getDefinition('simbiotica.contact.pool');
        foreach ($container->findTaggedServiceIds('simbiotica.contact.provider') as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition = $container->getDefinition($id);

                $arguments = $definition->getArguments();

                if (strlen($arguments[0]) == 0) {
                    $definition->replaceArgument(0, $id);
                }
                
                $contexts = array_key_exists('contexts', $attributes)?$attributes['contexts']:null;

                $pool->addMethodCall('addProvider', array($id, new Reference($id), $contexts));
            }
        }
    }
}
