<?php

namespace Simbiotica\CalpBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;

class ContentController extends Controller
{
    public function indexAction(Request $request, $uri)
    {
        $locale = $request->getLocale();
        $languages = SimbioticaCalpBundle::getLanguages();
        $flipLanguages = array_flip(SimbioticaCalpBundle::getLanguages());
        $language = $flipLanguages[$locale];
        
        $content = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Content')
            ->findOneBy(array('published' => 3, 'language' => $language, 'uri' => $uri));
        
        if (!$content)
            throw $this->createNotFoundException('No page found!');
        
        $translator =  $this->container->get('translator');
        $router = $this->container->get('router');
        
        $translations = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Content')
            ->findBy(array('tid' => $content->getTid(), 'published' => 3));
        
        $switcherData = array();
        foreach ($languages as $key => $value)
        {
            /** Ignore JMSTranslationBundle error when detecting translations */
            /** @Ignore */
            $switcherData[$translator->trans($key , array(), 'SimbioticaFront', $key)] = $router->generate('simbiotica_calp_homepage', array("_locale" => $value));
        }
        
        foreach ($translations as $translation)
        {
            /** Ignore JMSTranslationBundle error when detecting translations */
            /** @Ignore */
            $switcherData[$translator->trans($translation->getLanguage() , array(), 'SimbioticaFront', $translation->getLanguage())] = $router->generate('simbiotica_calp_content', array("uri" => $translation->getUri(), "_locale" => $languages[$translation->getLanguage()]));
        }
        
        $template = $content->getView()?:'SimbioticaCalpBundle:Content:standard.html.twig';

        return $this->render($template, array(
            'content' => $content,
            'switcherData' => $switcherData,
        ));
    }
}
