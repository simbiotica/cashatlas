<?php

namespace Simbiotica\CalpBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UnsupportedBrowserController extends Controller
{
    public function indexAction()
    {
        return $this->render('SimbioticaCalpBundle:Exception:unsupported_browser.html.twig', array());
    }
}
