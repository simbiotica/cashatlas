<?php

namespace Simbiotica\CalpBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as ParentController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Simbiotica\CalpBundle\Entity\WhiteList;

class RegistrationController extends ParentController
{
    public function registerAction()
    {
        $form = $this->container->get('fos_user.registration.form');
        $formHandler = $this->container->get('fos_user.registration.form.handler');
        $whitelisted = false;

        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            return $this->container->get('templating')->renderResponse('SimbioticaCalpBundle:Registration:already_registered.html.twig', array(
            ));
        }
        
        $process = $formHandler->process($whitelisted, $this->container->get('request')->getLocale());
        if ($process) {
            $user = $form->getData();
            $authUser = false;
            
            if($whitelisted instanceof WhiteList)
            {
                $authUser = true;
                $route = 'fos_user_registration_confirmed';
                $em = $this->container->get('doctrine')->getManager();
                
                foreach($user->getOrganizations() as $ou)
                {
                    $ou->setEnabled(true);
                    $em->persist($ou);
                }
                $em->persist($user);
                
                $whitelisted->setImported(true);
                
                $em->persist($whitelisted);
                $em->flush();
            }
            else
            {
                $route = 'fos_user_registration_check_email';
            }
            
            $url = $this->container->get('router')->generate($route);
            $response = new RedirectResponse($url);

            if ($authUser) {
                $this->authenticateUser($user, $response);
            }

            return $response;
            
        }

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:register.html.'.$this->getEngine(), array(
            'form' => $form->createView(),
        ));
    }
    
    /**
     * Tell the user to check his email provider
     */
    public function checkEmailAction()
    {
        return $this->container->get('templating')->renderResponse('SimbioticaCalpBundle:Registration:check_email.html.twig', array());
    }
    
    /**
     * Tell the user his account is now confirmed
     */
    public function confirmedAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        /**
         * Pending resolution of https://github.com/symfony/symfony/issues/6417
         * This email should be sent on RegistrationFormHandler, but is sent here
         * Remove after bug is fixed
         */
        $this->container->get('simbiotica.utils.account_mailer')->sendWelcomeEmailMessage($user);

        return $this->container->get('templating')->renderResponse('SimbioticaCalpBundle:Registration:confirmed.html.twig', array());
    }
    
    public function registrationPending(Request $request) {
        
    }
}
?>
