<?php

namespace Simbiotica\CalpBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NotificationController extends Controller
{
    public function sendAction(Request $request)
    {

        // create and publish a message
        try{
	        $backend = $this->get('sonata.notification.backend');

	        $backend->createAndPublish('dashboard', array(
	            'message' => 'This is a custom notification'
	        ));
        } catch (Exception $e) {
        	$message = \Swift_Message::newInstance()
  					->setSubject('RabbitMQ exception')
  					->setFrom($this->container->getParameter('mailer_from'))
  					->setTo($this->container->getParameter('notification_mail'))
  					->setBody($e->getMessage());
				
				  $this->get('mailer')->send($message);
          
          return new \Symfony\Component\HttpFoundation\Response('RabbitMQ Error');
        }
        
        return new \Symfony\Component\HttpFoundation\Response('Done');
    }
}
