<?php

namespace Simbiotica\CalpBundle\Controller\Ajax;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LocationFieldTypeController extends Controller
{
    public function countriesAction(Request $request)
    {
        $continents = $request->query->get('continents');
        $choices = array();
        
        foreach($continents as $continentId)
        {
            $continent = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Continent')
            ->find($continentId);

            $choices[$continent->getName()] = array();
            foreach($continent->getCountries() as $country)
            {
                $choices[$continent->getName()][$country->getId()] = $country->getName();
            }
        }

        $return = json_encode(array("choices" => $choices ));//jscon encode the array
        return new Response($return, 200, array('Content-Type'=>'application/json'));//make sure it has the correct content type
    }

    public function regionsAction(Request $request)
    {
        $countries = $request->query->get('countries');
        $filtered = $request->query->get('filtered', false);
        $choices = array();
        
        $regions = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Region')
            ->getByCountry($countries, $filtered);

        foreach($regions as $region)
        {
            if (!array_key_exists($region->getCountry()->getName(), $choices))
                $choices[$region->getCountry()->getName()] = array();
            $choices[$region->getCountry()->getName()][$region->getId()] = $region->getName();
        }

        $return = json_encode(array("choices" => $choices ));//jscon encode the array
        return new Response($return, 200, array('Content-Type'=>'application/json'));//make sure it has the correct content type
    }
}
