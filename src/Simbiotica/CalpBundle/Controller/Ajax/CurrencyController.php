<?php

namespace Simbiotica\CalpBundle\Controller\Ajax;

use Simbiotica\CalpBundle\SimbioticaCalpBundle;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CurrencyController extends Controller
{
    public function setAction(Request $request)
    {
        $currency = $request->get('currency');
        if(isset($currency)) {
        	$session = $this->get('session');
        	$session->set('currency', $currency);
		}
        return new Response('ok', 200);
    }
}
