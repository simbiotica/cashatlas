<?php

namespace Simbiotica\CalpBundle\Controller\Ajax;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS\Serializer\SerializationContext;

class MapController extends Controller
{
    public function continentsAction(Request $request)
    {
        $serializer = $this->container->get('jms_serializer');
        $continentIds = $request->query->get('continent');
        $currency = $request->query->get('currency');
        if(!isset($currency))
            $currency = 'EUR';
        
        if (is_array($continentIds))
        {
            $continents = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Continent')
            ->findBy(array('id' => $continentIds));
        }
        else
        {
            $continents = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Continent')
            ->findAll();
        }
        
        $result = array();
        $filterProjectIds = $this->get('simbiotica.manager.project')->projectList($request, $this->getDoctrine(), $this->get('security.context'));

        foreach($continents as $continent)
        {
            $projectIds = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Project')
            ->searchProject($continent->getId());
            
            $resultingIds = array_intersect($projectIds, $filterProjectIds);
            
            $continent->setProjectCount(count($resultingIds));
            
            if (count($resultingIds))
            {
                $sumScale = $this->getDoctrine()
                    ->getRepository('SimbioticaCalpBundle:Project')
                    ->getSumScales($resultingIds);
                
                $sumCash = $this->getDoctrine()
                    ->getRepository('SimbioticaCalpBundle:Project')
                    ->getSumCash($resultingIds, $currency);

                $continent->setScaleSum($sumScale);
                $continent->setBudgetAllocated($sumCash['budgetAllocated']);
                $continent->setBudgetOverall($sumCash['budgetOverall']);
            }
            else
            {
                $continent->setScaleSum(0);
                $continent->setBudgetAllocated(0);
                $continent->setBudgetOverall(0);
            }

            $result[] = $continent;
        }
        
        $output = $serializer->serialize($result, 'json', SerializationContext::create()->setGroups(array('continent')));
        
        if($request->query->get('forceHtml') && $this->container->getParameter('kernel.environment') === 'dev')
        {
            return $this->render('SimbioticaCalpBundle:Test:debug.html.twig', array(
                'data' => $output,
            ));
        }
        else
            return new Response($output, 200, array('Content-Type'=>'application/json'));
    }
    
    public function countriesAction(Request $request)
    {
        $continentIds = $request->query->get('continent');
        $countryIds = $request->query->get('country');
        $serializer = $this->container->get('jms_serializer');
        $currency = $request->query->get('currency');
        if(!isset($currency))
            $currency = 'EUR';

        if (is_array($continentIds))
        {
            $countries = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Country')
            ->findBy(array('continent' => $continentIds));
        }
        elseif (is_array($countryIds))
        {
            $countries = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Country')
            ->findBy(array('id' => $countryIds));
        }
        else
        {
            $countries = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Country')
            ->findAll();
        }
        $result = array();
        $filterProjectIds = $this->get('simbiotica.manager.project')->projectList($request, $this->getDoctrine(), $this->get('security.context'));

        foreach($countries as $country)
        {
            $projectIds = $this->getDoctrine()
                ->getRepository('SimbioticaCalpBundle:Project')
                ->searchProject(null,$country->getId());
            
            $resultingIds = array_intersect($projectIds, $filterProjectIds);
            
            $country->setProjectCount(count($resultingIds));
            
            if (count($resultingIds))
            {
                $sumScale = $this->getDoctrine()
                    ->getRepository('SimbioticaCalpBundle:Project')
                    ->getSumScales($resultingIds);

                $sumCash = $this->getDoctrine()
                    ->getRepository('SimbioticaCalpBundle:Project')
                    ->getSumCash($resultingIds, $currency);

                $country->setScaleSum($sumScale);
                $country->setBudgetAllocated($sumCash['budgetAllocated']);
                $country->setBudgetOverall($sumCash['budgetOverall']);
            } else {
                $country->setScaleSum(0);
                $country->setBudgetAllocated(0);
                $country->setBudgetOverall(0);
            }
            $result[] = $country;
        }
        
        $output = $serializer->serialize($result, 'json', SerializationContext::create()->setGroups(array('country')));
        
        if($request->query->get('forceHtml') && $this->container->getParameter('kernel.environment') === 'dev')
        {
            return $this->render('SimbioticaCalpBundle:Test:debug.html.twig', array(
                'data' => $output,
            ));
        }
        else
            return new Response($output, 200, array('Content-Type'=>'application/json'));
    }

    public function regionsAction(Request $request)
    {
        $continentIds = $request->query->get('continent');
        $countryIds = $request->query->get('country');
        $regionIds = $request->query->get('region');
        $serializer = $this->container->get('jms_serializer');
        $currency = $request->query->get('currency');
        if(!isset($currency))
            $currency = 'EUR';
        
        if (is_array($continentIds))
        {
            $countryIds = array_map(function($item) {return $item->getId();}, $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Country')
            ->findBy(array('continent' => $continentIds)));
            
            $regions = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Region')
            ->findBy(array('country' => $countryIds));
        }
        elseif (is_array($countryIds))
        {
            $regions = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Region')
            ->findBy(array('country' => $countryIds));
        }
        elseif (is_array($regionIds))
        {
            $regions = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Region')
            ->findBy(array('id' => $regionIds));
        }
        else
        {
            $regions = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Region')
            ->findAll();
        }
        
        $result = array();
        $filterProjectIds = $this->get('simbiotica.manager.project')->projectList($request, $this->getDoctrine(), $this->get('security.context'));
        
        foreach($regions as $region)
        {
            $projectIds = $this->getDoctrine()
                ->getRepository('SimbioticaCalpBundle:Project')
                ->searchProject(null, null, $region->getId());
            
            $resultingIds = array_intersect($projectIds, $filterProjectIds);
            
            $region->setProjectCount(count($resultingIds));
            
            if (count($resultingIds))
            {
                $sumScale = $this->getDoctrine()
                    ->getRepository('SimbioticaCalpBundle:Project')
                    ->getSumScales($resultingIds);
                $sumCash = $this->getDoctrine()
                    ->getRepository('SimbioticaCalpBundle:Project')
                    ->getSumCash($resultingIds, $currency);

                $region->setScaleSum($sumScale);
                $region->setBudgetAllocated($sumCash['budgetAllocated']);
                $region->setBudgetOverall($sumCash['budgetOverall']);
            } else {
                $region->setScaleSum(0);
                $region->setBudgetAllocated(0);
                $region->setBudgetOverall(0);
            }
            $result[] = $region;
        }
        
        $output = $serializer->serialize($result, 'json', SerializationContext::create()->setGroups(array('region')));
        
        if($request->query->get('forceHtml') && $this->container->getParameter('kernel.environment') === 'dev')
        {
            return $this->render('SimbioticaCalpBundle:Test:debug.html.twig', array(
                'data' => $output,
            ));
        }
        else
            return new Response($output, 200, array('Content-Type'=>'application/json'));
    }
}