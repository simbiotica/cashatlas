<?php

namespace Simbiotica\CalpBundle\Controller\Ajax;

use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ModalityController extends Controller
{
    public function indexAction()
    {
    	$selected = $this->get("request")->request->get('modality');
        
    	$modalities = SimbioticaCalpBundle::getModalities($selected);
    	$modalities = json_encode($this->translateArray($modalities));//jscon encode the array
    	
        return new Response($modalities, 200, array('Content-Type'=>'application/json'));//make sure it has the correct content type
    }

    private function translateArray($inputArray) {
        $translator = $this->get('translator');
        return array_map(function($item) use ($translator) {
            /** @Ignore */
            return $translator->trans($item, array(), 'SimbioticaFront');
        }, $inputArray);
    }
}
