<?php

namespace Simbiotica\CalpBundle\Controller\Ajax;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Simbiotica\CalpBundle\Entity\Contact;

class ContactController extends Controller
{
    protected $templates = array(
        'success' => 'SimbioticaCalpBundle:Contact:success.html.twig',
        'failure' => 'SimbioticaCalpBundle:Contact:failure.html.twig',
    );
    
    public function providersAction(Request $request)
    {
        $context  = $request->get('context', null);
        $return = json_encode($this->get('simbiotica.contact.pool')->getProviderNames($context));
        
        return new Response($return, 200, array('Content-Type'=>'application/json'));
    }
    
    public function formAction(Request $request)
    {
        $contact = new Contact();
        $providerName  = $request->get('provider');
        $showProvider  = $request->get('showProvider');
        $context  = $request->get('context', null);

        $choices = array();
        foreach ($this->get('simbiotica.contact.pool')->getProviderNames($context) as $name) {
            $choices[$name] = $name;
        }

        if (count($choices) == 1) {
            $providerName = array_shift($choices);
            $contact->setContactType($providerName);
        }

        $provider = $this->get('simbiotica.contact.pool')->getProvider($providerName);
        if ($provider) {
            $provider->setDefaults($contact, $request);
        }

        $formBuilder = $this->createFormBuilder(
            $contact,
            array('translation_domain' => 'SimbioticaFront')
        );
        $formBuilder->add('context', 'hidden', array(
            'mapped' => false, 
            'attr' => array('class' => 'context'),
            'data' => $context,
        ));
            
        if($showProvider || empty($providerName))
        {
            $formBuilder->add('contactType', 'choice', array(
                'choices' => $choices,
                'label' => 'form.contact.contact_type',
                'empty_value' => 'form.label_empty',
                'attr' => array('class' => 'type'),
            ));
        } else {
            $formBuilder->add('contactType', 'hidden');
        }
        
        if ($provider) {
            $provider->getForm($formBuilder);
        }
        $form = $formBuilder->getForm();
        
        return $this->render('SimbioticaCalpBundle:Contact:standard.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
    public function saveAction(Request $request)
    {
        $formData  = $request->request->get('form');
        
        if(!$formData || !array_key_exists('contactType', $formData))
            return $this->render('SimbioticaCalpBundle:Contact:failure.html.twig');
        
        $providerName  = $formData['contactType'];
        $provider = $this->get('simbiotica.contact.pool')->getProvider($providerName);
        
        if(!$provider)
            return $this->render('SimbioticaCalpBundle:Contact:failure.html.twig');
        
        $contact = new Contact();
        $contact->setLanguage($request->getLocale());
        $provider->setDefaults($contact, $request);
        $formBuilder = $this->createFormBuilder($contact);
        $formBuilder->add('context', 'hidden', array(
            'mapped' => false, 
            'attr' => array('class' => 'context'),
        ));
        
        $choices = array();
        foreach($this->get('simbiotica.contact.pool')->getProviderNames() as $providerName)
        {
            $choices[$providerName] = $providerName;
        }

        $formBuilder->add('contactType', 'choice', array(
            'choices' => $choices,
            'label' => 'form.contact.contact_type',
            'translation_domain' => 'SimbioticaFront',
            'empty_value' => 'form.label_empty',
            'attr' => array('class' => 'type'),
        ));
        
        $provider->getForm($formBuilder);
        $form = $formBuilder->getForm();
        
        $form->handleRequest($request);
        if ($form->isValid()) {
            $provider->handleSubmission($contact);
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();
            
            return $this->renderTemplate('success', $provider);
        }
        return $this->renderTemplate('failure', $provider);
    }
    
    private function renderTemplate($template, $provider, $options = array()) {
        $templates = array_merge($this->templates, $provider->getTemplates());
        
        return $this->render($templates[$template], $options);
    }
}
