<?php

namespace Simbiotica\CalpBundle\Controller\Ajax;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ReportController extends Controller {

    public function indexAction(Request $request) {
        if (!$this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
            throw new AccessDeniedException();

        $indexes = array(
                'continent',
                'country',
                'organization',
                'donor',
                'years',
        );
        
        $validParameters = array_intersect_key($request->query->all(), array_flip($indexes));
        $country = null;
        
        if(!array_key_exists('country', $validParameters) && array_key_exists('continent', $validParameters))
        {
            $country = array_map(function($item){return $item['id'];}, $this->getDoctrine()
                        ->getRepository('SimbioticaCalpBundle:Country')->createQueryBuilder('c')
                        ->select('c.id')
                        ->leftJoin('c.continent', 'ctn')
                        ->where('ctn.id IN (:ids)')
                        ->setParameter('ids', $validParameters['continent'])
                        ->getQuery()
                        ->getResult()
            );
        }
        
        if (array_key_exists('country', $validParameters))
            $country = $validParameters['country'];
        
        if (array_key_exists('organization', $validParameters) && !array_key_exists('donor', $validParameters))
            $organization = $validParameters['organization'];
        else
            $organization = null; 
        
        if (!array_key_exists('organization', $validParameters) && array_key_exists('donor', $validParameters))
            $donor = $validParameters['donor'];
        else
            $donor = null;
        
        if(array_key_exists('years', $validParameters))
        {
            $after = array(
                'month' => 1,
                'year' => min($validParameters['years'])
            );
            $before = array(
                'month' => 12,
                'year' => max($validParameters['years'])
            );
        }
        else
        {
            $before = array();
            $after = array();
        }
        
        $data = array(
            'country' => $country,
            'selector' => (is_null($donor)?'organization':'donor'),
            'organization' => $organization,
            'donor' => $donor,
            'before' => $before,
            'after' => $after,
            'fields' => range(1,8)
        );
        
        $form = $this->createForm('simbiotica_form_report', $data);
        
        $translator =  $this->container->get('translator');
        $router = $this->container->get('router');
        
        $switcherData = array();
        foreach (SimbioticaCalpBundle::getLanguages() as $key => $value)
        {
            /** Ignore JMSTranslationBundle error when detecting translations */
            /** @Ignore */
            $switcherData[$translator->trans($key , array(), 'SimbioticaFront', $key)] = $router->generate('ajax_simbiotica_report_index', array("_locale" => $value));
        }

        return $this->render('SimbioticaCalpBundle:Report:index.html.twig', array(
                    'form' => $form->createView(),
                    'switcherData' => $switcherData,
        ));
    }

    public function generateAction(Request $request) {
        if (!$this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
            throw new AccessDeniedException();
        
        $translator = $this->get('translator');
        $serializer = $this->container->get('jms_serializer');
        $params = $request->query->all();

        $indexes = array(
            'country',
            'after',
            'before',
            'organization',
            'donor',
            'fields',
        );

        $validParameters = array_intersect_key($params["simbiotica_form_report"], array_flip($indexes));

        $after = $validParameters['after'];
        $before = $validParameters['before'];
        if(!array_key_exists('fields', $validParameters))
        {
            $output = $serializer->serialize(array('error' => $this->get('translator')->trans('report.error.nofields', array(), 'SimbioticaFront')), 'json');
            return new Response($output, 200, array('Content-Type' => 'application/json'));
        }
        
        if (count($validParameters) > 2 || strcmp(implode('', $after), '') != 0 || strcmp(implode('', $before), '') != 0) {
            $request->query->replace($params["simbiotica_form_report"]);

            $resultingIds = $this->get('simbiotica.manager.project')->projectList($request, $this->getDoctrine(), $this->get('security.context'));

            if (count($resultingIds) == 0)
            {
                $output = $serializer->serialize(array('error' => $this->get('translator')->trans('report.error.noproject', array(), 'SimbioticaFront')), 'json');
                return new Response($output, 200, array('Content-Type' => 'application/json'));
            }

            $projects = $this->getDoctrine()
                    ->getRepository('SimbioticaCalpBundle:Project')
                    ->findBy(array('id' => $resultingIds, 'published' => 3));

            $result = array();
            
            $result['params'] = $validParameters;
            $result['timeline'] = array();
            $result['totals'] = array(
                'scale' => 0,
                'amount' => 0,
                'number' => count($projects)
            );
            foreach ($projects as $project) {
                $result['totals']['amount'] += intval(($project->getBudgetAllocated()==0?$project->getInitialBudgetAllocated():$project->getBudgetAllocated())? : 0);
                $result['totals']['scale'] += intval(($project->getScale()==0?$project->getInitialScale():$project->getScale())? : 0);
                
                $firstCashDate = $project->getFirstCashDate();
                $endingDate = $project->getEndingDate();
                if ($firstCashDate && $endingDate
                        && array_key_exists('year', $firstCashDate) && array_key_exists('month', $firstCashDate) 
                        && array_key_exists('year', $endingDate) && array_key_exists('month', $endingDate) 
                        && in_array($endingDate['year'], SimbioticaCalpBundle::getYearRange()) && in_array($firstCashDate['year'], SimbioticaCalpBundle::getYearRange())
                ) {
                    $month = $firstCashDate['month'];
                    foreach(range($firstCashDate['year'], $endingDate['year']) as $year)
                    {
                        foreach(array_keys(SimbioticaCalpBundle::getMonths()) as $month)
                        {
                            if(!array_key_exists($year, $result['timeline']))
                                $result['timeline'][$year] = array();
                            if(!array_key_exists($month, $result['timeline'][$year]))
                                $result['timeline'][$year][$month] = array(
                                    'scale' => 0,
                                    'amount' => 0,
                                    'number' => 0
                                );
                            $result['timeline'][$year][$month]['amount'] += intval(($project->getBudgetAllocated()==0?$project->getInitialBudgetAllocated():$project->getBudgetAllocated())? : 0);
                            $result['timeline'][$year][$month]['number']++;
                            $result['timeline'][$year][$month]['scale'] += intval(($project->getScale()==0?$project->getInitialScale():$project->getScale())? : 0);
                            if($year == $endingDate['year'] && $month == $endingDate['month'])
                                break;
                        }
                    }
                }
            }
            
            ksort($result['timeline']);

            if (in_array(SimbioticaCalpBundle::REPORT_FIELDS_DONORS, $validParameters['fields'])) {
                $result['donors'] = array();
                $donorsList = $this->getDoctrine()
                    ->getRepository('SimbioticaCalpBundle:Organization')
                    ->getSumsAsDonor($resultingIds, 'number');

                $donors = array();
                $donorsSum = array(
                    "name" => $this->get('translator')->trans('donors.other', array(), 'SimbioticaFront'), 
                    "scale" => 0,
                    "amount" => 0, 
                    "number" => 0
                );
                $index = 0;
                
                foreach ($donorsList as $index => $donor)
                {
                    if ($index++ < 9)
                        $donors[] = array(
                            'name' => $this->getDoctrine()
                                ->getRepository('SimbioticaCalpBundle:Organization')
                                ->find($donor['id'])->getName(),
                            "scale" => intval($donor["scale"]),
                            "amount" => intval($donor["amount"]),
                            "number" => intval($donor["number"]),
                            
                        );
                    else
                    {
                        $donorsSum["scale"] += intval($donor['scale']);
                        $donorsSum["amount"] += intval($donor['amount']);
                        $donorsSum["number"] += intval($donor['number']);
                    }
                }
                $donors[] = $donorsSum;
                
                $result['donors'] = array_values($donors);
            }
            if (in_array(SimbioticaCalpBundle::REPORT_FIELDS_ORGANIZATIONS, $validParameters['fields'])) {
                $result['organizations'] = array();
                
                $organizationList = $this->getDoctrine()
                    ->getRepository('SimbioticaCalpBundle:Organization')
                    ->getSums($resultingIds, 'number');
                
                $organizations = array();
                $organizationsSum = array(
                    "name" => $this->get('translator')->trans('organization.other', array(), 'SimbioticaFront'), 
                    "scale" => 0, 
                    "amount" => 0,
                    "number" => 0
                );
                $index = 0;

                foreach ($organizationList as $index => $organization)
                {
                    if ($index++ < 9)
                    {
                        $organizations[] = array(
                            'name' => $this->getDoctrine()
                                ->getRepository('SimbioticaCalpBundle:Organization')
                                ->find($organization['id'])->getName(),
                            'amount' => intval($organization['amount']),
                            'scale' => intval($organization['scale']),
                            'number' => intval($organization['number']),
                        );
                    }
                    else
                    {
                        $organizationsSum["scale"] += $organization['scale'];
                        $organizationsSum["amount"] += $organization['amount'];
                        $organizationsSum["number"] += $organization['number'];
                    }
                }
                $organizations[] = $organizationsSum;
                $result['organizations'] = array_values($organizations);
            }
            if (in_array(SimbioticaCalpBundle::REPORT_FIELDS_SECTORS, $validParameters['fields'])) {
                $result['sectors'] = array();
                foreach ($projects as $project) {
                    if (count($project->getSectors()->toArray()) > 1)
                    {
                        if (array_key_exists(0, $result['sectors']))
                                $result['sectors'][0]['number']++;
                            else
                                $result['sectors'][0] = array(
                                    'number' => 1,
                                    'name' => 'Multiple'
                                );
                    }
                    else
                    {
                        foreach ($project->getSectors()->toArray() as $sector) {
                            if (array_key_exists($sector->getId(), $result['sectors']))
                                $result['sectors'][$sector->getId()]['number']++;
                            else
                                $result['sectors'][$sector->getId()] = array(
                                    'number' => 1,
                                    'name' => $sector->getName(),
                                );
                        }
                    }
                }
                $result['sectors'] = array_values($result['sectors']);
            }
            if (in_array(SimbioticaCalpBundle::REPORT_FIELDS_MODALITIES, $validParameters['fields'])) {
                $result['modalities'] = array();
                $modalities = SimbioticaCalpBundle::getModalities();
                foreach ($projects as $project) {
                    foreach ($project->getModalities()->toArray() as $modality) {
                        if($modality->getModality())
                        {
                            if (array_key_exists($modality->getModality(), $result['modalities']))
                                $result['modalities'][$modality->getModality()]['number']++;
                            else
                                $result['modalities'][$modality->getModality()] = array(
                                    'number' => 1,
                                    'name' => $translator->trans($modalities[$modality->getModality()], array(), 'SimbioticaFront'),
                                );
                        }
                    }
                }
                $result['modalities'] = array_values($result['modalities']);
            }
            if (in_array(SimbioticaCalpBundle::REPORT_FIELDS_DELIVERY_MECHANISMS, $validParameters['fields'])) {
                $result['delivery_mechanisms'] = array();
                $deliveryMechanisms = SimbioticaCalpBundle::getModalities(null, 1);
                foreach ($projects as $project) {
                    foreach ($project->getModalities()->toArray() as $modality) {
                        if($modality->getDeliveryMechanism())
                        {
                            if (array_key_exists($modality->getDeliveryMechanism(), $result['delivery_mechanisms']))
                                $result['delivery_mechanisms'][$modality->getDeliveryMechanism()]['number']++;
                            else
                                $result['delivery_mechanisms'][$modality->getDeliveryMechanism()] = array(
                                    'number' => 1,
                                    'name' => $translator->trans($deliveryMechanisms[$modality->getDeliveryMechanism()], array(), 'SimbioticaFront'),
                                );
                        }
                    }
                }
                $result['delivery_mechanisms'] = array_values($result['delivery_mechanisms']);
            }
            if (in_array(SimbioticaCalpBundle::REPORT_FIELDS_DELIVERY_AGENTS, $validParameters['fields'])) {
                $result['delivery_agents'] = array();
                $deliveryAgents = SimbioticaCalpBundle::getModalities(null, 2);
                foreach ($projects as $project) {
                    foreach ($project->getModalities()->toArray() as $modality) {
                        if($modality->getDeliveryAgent())
                        {
                            if ($modality->getDeliveryAgent() && array_key_exists($modality->getDeliveryAgent(), $result['delivery_agents']))
                                $result['delivery_agents'][$modality->getDeliveryAgent()]['number']++;
                            else
                                $result['delivery_agents'][$modality->getDeliveryAgent()] = array(
                                    'number' => 1,
                                    'name' => $translator->trans($deliveryAgents[$modality->getDeliveryAgent()], array(), 'SimbioticaFront'),
                                );
                        }
                    }
                }
                $result['delivery_agents'] = array_values($result['delivery_agents']);
            }
            if (in_array(SimbioticaCalpBundle::REPORT_FIELDS_CONDITIONALITIES, $validParameters['fields'])) {
                $result['conditionalities'] = array(
                    array(
                        'name' => 'conditional',
                        'number' => 0,
                    ), array(
                        'name' => 'unconditional',
                        'number' => 0,
                    )
                );
                foreach ($projects as $project) {
                    foreach ($project->getModalities()->toArray() as $modality) {
                        if($modality->getModality())
                        {
                            switch ($modality->getModality()) {
                                case 1:
                                case 3:
                                case 4:
                                case 5:
                                    $result['conditionalities'][0]['number']++;
                                    break;
                                case 2:
                                    $result['conditionalities'][1]['number']++;
                                    break;
                            }
                        }
                    }
                }
                $result['conditionalities'] = array_values($result['conditionalities']);
            }
            if (in_array(SimbioticaCalpBundle::REPORT_FIELDS_CONTEXTS, $validParameters['fields'])) {
                $result['contexts'] = array();
                $contexts = $this->translateArray(SimbioticaCalpBundle::getContexts());
                foreach ($projects as $project) {
                    foreach ($project->getProjectContext() as $context) {
                        if (array_key_exists(intval($context), $result['contexts']))
                            $result['contexts'][$context]['number']++;
                        else
                            $result['contexts'][$context] = array(
                                'number' => 1,
                                'name' => $contexts[$context],
                            );
                    }
                }
                $result['contexts'] = array_values($result['contexts']);
            }

            $output = $serializer->serialize($result, 'json');
            
            if($request->query->get('forceHtml') && $this->container->getParameter('kernel.environment') === 'dev')
            {
                return $this->render('SimbioticaCalpBundle:Test:debug.html.twig', array(
                    'data' => $output,
                ));
            }
            else
                return new Response($output, 200, array('Content-Type' => 'application/json'));
        }
        else
            return new RedirectResponse($this->generateUrl('ajax_simbiotica_report_index'));
    }
    
    public function filterAction(Request $request) {
        if (!$this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
            throw new AccessDeniedException();
        
        $serializer = $this->container->get('jms_serializer');

        $resultingIds = $this->get('simbiotica.manager.project')->projectList($request, $this->getDoctrine(), $this->get('security.context'));
        
        $projects = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Project')
            ->findBy(array('id' => $resultingIds, 'published' => 3));
        
        $result = array(
            'country' => array(),
            'organization' => array(),
            'donor' => array(),
            'years' => array(),
        );
        
        foreach($projects as $project)
        {
            foreach($project->getCountries() as $country)
            {
                if (!array_key_exists($country->getId(), $result['country']))
                {
                    $result['country'][$country->getId()] = array(
                        'id' => $country->getId(),
                        'name' => $country->getName(),
                    );
                }
            }
            foreach($project->getOrganizations() as $po)
            {
                if (!array_key_exists($po->getOrganization()->getId(), $result['organization']))
                {
                    $result['organization'][$po->getOrganization()->getId()] = array(
                        'id' => $po->getOrganization()->getId(),
                        'name' => $po->getOrganization()->getName(),
                    );
                }
            }
            foreach($project->getDonors() as $pd)
            {
                if (!array_key_exists($pd->getOrganization()->getId(), $result['donor']))
                {
                    $result['donor'][$pd->getOrganization()->getId()] = array(
                        'id' => $pd->getOrganization()->getId(),
                        'name' => $pd->getOrganization()->getName(),
                    );
                }
            }
            
            $projectFirstCashDate = $project->getFirstCashDate();
            $projectEndingDate = $project->getEndingDate();
            if (!empty($projectFirstCashDate['year']) && !empty($projectEndingDate['year'])) {
                foreach(range($projectFirstCashDate['year'], $projectEndingDate['year']) as $year) {
                    $result['years'][$year] = $year;
                }
            }
            asort($result['years']);
        }
        
        foreach($result as $key => $elem)
        {
            $result[$key] = array_values($elem);
        }
        
        $output = $serializer->serialize($result, 'json');
        
        if($request->query->get('forceHtml') && $this->container->getParameter('kernel.environment') === 'dev')
        {
            return $this->render('SimbioticaCalpBundle:Test:debug.html.twig', array(
                'data' => $output,
            ));
        }
        else
            return new Response($output, 200, array('Content-Type' => 'application/json'));
    }
    
    private function translateArray($inputArray) {
        $translator = $this->get('translator');
        return array_map(function($item) use ($translator) {
            /** @Ignore */
            return $translator->trans($item, array(), 'SimbioticaFront');
        }, $inputArray);
    }
}
