<?php

namespace Simbiotica\CalpBundle\Controller\Ajax;

use Simbiotica\CalpBundle\SimbioticaCalpBundle;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ContextFieldTypeController extends Controller
{
    public function contextsAction($contexts)
    {
        $contextArray = null;
        if (isset($contexts) && is_string($contexts))
            $contextArray = explode('/', $contexts);
            
        $options = SimbioticaCalpBundle::getContexts($contextArray);
        $return = json_encode($options);//jscon encode the array
        
        return new Response($return, 200, array('Content-Type'=>'application/json'));//make sure it has the correct content type
    }
}
