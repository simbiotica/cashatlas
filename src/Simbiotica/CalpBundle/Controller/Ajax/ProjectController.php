<?php

namespace Simbiotica\CalpBundle\Controller\Ajax;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS\Serializer\SerializationContext;

class ProjectController extends Controller
{
    public function searchAction(Request $request)
    {
        $params = $request->query->all();
        $request->query->replace($params["search_form"]);
        return $this->projectsAction($request);
    }
    
    public function projectsAction(Request $request)
    {
        $projectIds = $request->query->get('id');
        $full = $request->query->get('full');
        $currency = $request->query->get('currency');
        $serializer = $this->container->get('jms_serializer');
        $exchangeService = $this->container->get('simbiotica.calp.currency.exchange');
        
        $filterProjectIds = $this->get('simbiotica.manager.project')->projectList($request, $this->getDoctrine(), $this->get('security.context'));
        
        if(!empty($projectIds))
        {
            $resultingIds = array_intersect($projectIds, $filterProjectIds);
        }
        else 
        {
            $resultingIds = $filterProjectIds;
        }
        
        if (empty($resultingIds))
            $projects = array();
        else
            $projects = $this->getDoctrine()
                ->getRepository('SimbioticaCalpBundle:Project')
                ->findBy(array('id' => $resultingIds, 'published' => 3), array('updatedAt' => 'DESC'));

        foreach($projects as &$project)
        {
            if($project->getCurrency() !== $currency)
            {
                $project->setBudgetOverall($exchangeService->exchangeData($project->getBudgetOverall(), $currency));
                $project->setBudgetAllocated($exchangeService->exchangeData($project->getBudgetAllocated(), $currency));
            }
        }

        $result = array();
        $result['projects'] = array_values($projects);
        
        if ($full)
        {
            $securityContext = $this->get('security.context');
            $result['acls'] = array();
            foreach($projects as $project) // *
            {
                $result['acls'][$project->getId()] = $securityContext->isGranted('EDIT', $project);
            }
            
            $result['media'] = array();
            foreach($projects as $project) // *
            {
                foreach($project->getGalleryHasMedias() as $ghm)
                {
                    if(!$ghm->getEnabled())
                        continue;
                    $medium = $ghm->getMedia();
                    if ($medium && !array_key_exists($medium->getId(), $result['media']))
                    {
                        $result['media'][$medium->getId()] = array(
                            'type' => $medium->getProviderName(),
                            'name' => $medium->getName(),
                            'route' => $this->get('router')->generate('sonata_media_download', array('id' => $medium->getId()))
                        );
                    }
                }
            }
            
            $output = $serializer->serialize($result, 'json', SerializationContext::create()->setGroups(array('project')));
        }
        else
            $output = $serializer->serialize($result, 'json', SerializationContext::create()->setGroups(array('projectPreview')));

        if($request->query->get('forceHtml') && $this->container->getParameter('kernel.environment') === 'dev')
        {
            return $this->render('SimbioticaCalpBundle:Test:debug.html.twig', array(
                'data' => $output,
            ));
        }
        else
            return new Response($output, 200, array('Content-Type'=>'application/json'));
    }
    
    public function dashboardAction(Request $request)
    {
        $serializer = $this->container->get('jms_serializer');
        $includeIds = $request->query->get('ids');
        $currency = $request->query->get('currency');
        
        $result = array();
        $filterProjectIds = $this->get('simbiotica.manager.project')->projectList();
        
        if (!count($filterProjectIds))
        {
            $output = $serializer->serialize(array(), 'json');
        
            return new Response($output, 200, array('Content-Type'=>'application/json'));
        }
        
        $organizationList = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Organization')
            ->getSums($filterProjectIds);
        
        $organizations = array();
        $organizationsSum = array(
            "name" => $this->get('translator')->trans('organization.other', array(), 'SimbioticaFront'), 
            "scale" => 0, 
            "count" => 0
        );
        $index = 0;
        
        foreach ($organizationList as $index => $organization)
        {
            if ($index++ <= 4)
                $organizations[] = array(
                    'id' => $organization['id'],
                    'scale' => $organization['scale'],
                );
            else
            {
                $organizationsSum["scale"] += $organization['scale'];
                $organizationsSum["count"]++;
            }
        }
        $organizations[] = $organizationsSum;
        
        $modalities = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Modality')
            ->getSums($filterProjectIds);
        
        $sectors = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Sector')
            ->getSums($filterProjectIds);
        
        $donorsList = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Organization')
            ->getSumsAsDonor($filterProjectIds);
        
        $donors = array();
        $donorsSum = array(
            "name" => $this->get('translator')->trans('donors.other', array(), 'SimbioticaFront'), 
            "amount" => 0, 
            "count" => 0
        );
        $index = 0;
        
        foreach ($donorsList as $index => $donor)
        {
            if ($index++ <= 4)
                $donors[] = array(
                    "id" => $donor["id"],
                    "amount" => $donor["amount"],
                    "count" => 1,
                );
            else
            {
                $donorsSum["amount"] += $donor['amount'];
                $donorsSum["count"]++;
            }
        }
        $donors[] = $donorsSum;
        
        $projectSums = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Project')
            ->getAgregatedValuesWithExchange($filterProjectIds, $currency);

        
        $result['organizations'] = $organizations;
        $result['sectors'] = $sectors;
        $result['modalities'] = $modalities;
        $result['donors'] = $donors;
        $result['projects'] = reset($projectSums);
        
        if($includeIds)
            $result['ids'] = array_values($filterProjectIds);
        
        $output = $serializer->serialize($result, 'json', SerializationContext::create()->setGroups(array('dashboard')));
        
        if($request->query->get('forceHtml') && $this->container->getParameter('kernel.environment') === 'dev')
        {
            return $this->render('SimbioticaCalpBundle:Test:debug.html.twig', array(
                'data' => $output,
            ));
        }
        else
            return new Response($output, 200, array('Content-Type'=>'application/json'));
    }
}
