<?php

namespace Simbiotica\CalpBundle\Controller;

use FOS\UserBundle\Controller\ResettingController as ParentController;
use FOS\UserBundle\Model\UserInterface;

class ResettingController extends ParentController
{
    protected function getRedirectionUrl(UserInterface $user)
    {
        return $this->container->get('router')->generate('simbiotica_calp_homepage');
    }
}
