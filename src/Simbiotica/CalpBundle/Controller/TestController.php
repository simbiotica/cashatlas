<?php

namespace Simbiotica\CalpBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TestController extends Controller
{
    public function indexAction()
    {
        return $this->render('SimbioticaCalpBundle:Templates:test.html.twig', array());
    }
}
