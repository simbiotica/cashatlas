<?php

namespace Simbiotica\CalpBundle\Controller;

use Sonata\UserBundle\Controller\ProfileController as ParentController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;

class ProfileController extends ParentController
{
    public function editProfileAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $form = $this->container->get('sonata.user.profile.form');
        $formHandler = $this->container->get('sonata.user.profile.form.handler');

        $process = $formHandler->process($user);
        if ($process) {
            $this->setFlash('fos_user_success', 'profile.flash.updated');
        }

        return $this->render('SonataUserBundle:Profile:edit_profile.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
