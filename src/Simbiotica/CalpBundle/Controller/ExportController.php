<?php

namespace Simbiotica\CalpBundle\Controller;

use Simbiotica\CalpBundle\Exporter\ExcelExporter;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ExportController extends Controller
{
    public function excelAction(Request $request)
    {
        $projectIds = $request->query->get('id');
        $format = $request->query->get('format', 'xlsx');
        
        $filterProjectIds = $this->get('simbiotica.manager.project')->projectList($request, $this->getDoctrine(), $this->get('security.context'));
        
        if(!empty($projectIds))
        {
            $resultingIds = array_intersect($projectIds, $filterProjectIds);
        }
        else 
        {
            $resultingIds = $filterProjectIds;
        }
        
        $projects = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Project')
            ->findBy(array('id' => $resultingIds, 'published' => 3));
        
        uasort($projects, function ($first, $second) {
            if ($first === $second) {
                return 0;
            }
            
            $firstStartingDate = $first->getStartingDate();
            $secondStartingDate = $second->getStartingDate();
            
            if(!$firstStartingDate)
                return -1;
            if(!$secondStartingDate)
                return 1;
            
            if ($firstStartingDate['year'] != $secondStartingDate['year'])
                return $firstStartingDate['year'] < $secondStartingDate['year'];
            else
                return $firstStartingDate['month'] < $secondStartingDate['month'];
        });
        
        $translator = $this->get('translator');
        $callback = function() use ($projects, $translator, $format) {
            $exporter = new ExcelExporter($translator);
            $exporter->exportProject($projects, $format);
        };
        
        return new StreamedResponse($callback, 200, array(
            'Content-Type'        => 'application/vnd.ms-excel',
            'Content-Disposition' => sprintf('attachment; filename=%s', "cashatlasexport.".$format)
        ));
    }
        
    public function pdfAction(Request $request)
    {
        $projectId = $request->query->get('id');
        $format = $request->query->get('format', 'pdf');
        
        if (is_null($projectId))
            throw new \RuntimeException('A project id must be specified');
        
        $project = $this->getDoctrine()
            ->getRepository('SimbioticaCalpBundle:Project')
            ->findOneBy(array('id' => $projectId, 'published' => 3));
        
        $countries = array();
        $regions = array();
        foreach($project->getLocations() as $location)
        {
            if($country = $location->getCountry())
            {
                if (!array_key_exists($country->getId(), $countries))
                {
                    $countries[$country->getId()] = $country;
                }
            }
            if ($region = $location->getRegion())
            {
                $country = $location->getCountry()?:$region->getCountry();
                if (!array_key_exists($country->getId(), $countries))
                {
                    $countries[$country->getId()] = $country;
                }
                if (!array_key_exists($region->getId(), $regions))
                {
                    $regions[$region->getId()] = $region;
                }
            }
        }
        $countries = array_values($countries);
        $regions = array_values($regions);
        
        $html = $this->renderView('SimbioticaCalpBundle:Export:pdf.html.twig', array(
            'contexts' => SimbioticaCalpBundle::getContexts(),
            'marketAssessmentTools' => SimbioticaCalpBundle::getMarketAsssessmentTypes(),
            'modalityList' => SimbioticaCalpBundle::getModalities(),
            'deliveryMechanismList' => SimbioticaCalpBundle::getModalities(null, 1),
            'deliveryAgentList' => SimbioticaCalpBundle::getModalities(null, 2),
            'typeOfInterventionList' => SimbioticaCalpBundle::getInterventionTypes(),
            'months' => SimbioticaCalpBundle::getMonths(),
            'countries' => $countries,
            'regions' => $regions,
            'project'  => $project,
        ));
        
        
        
        if($format == 'html')
        {
            return new Response($html);
        }
        else
        {
            return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                200,
                array(
                    'Content-Type'          => 'application/pdf',
                    'Content-Disposition'   => 'attachment; filename="cashatlasexport.pdf"'
                )
            );
        }
        
    }
}
