<?php

namespace Simbiotica\CalpBundle\Controller\Admin;

use Sonata\MediaBundle\Controller\MediaAdminController as Controller;
use Symfony\Component\Process\Process;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class MediaAdminController extends Controller {

    /**
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @return \Symfony\Bundle\FrameworkBundle\Controller\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction() {
        if (false === $this->admin->isGranted('CREATE')) {
            throw new AccessDeniedException();
        }

        $parameters = $this->admin->getPersistentParameters();
        
        if($parameters['context'])
            $contexts = array($parameters['context'] => $this->get('sonata.media.pool')->getContext($parameters['context']));
        else
            $contexts = $this->get('sonata.media.pool')->getContexts();

        if (!$parameters['provider']) {
            return $this->render('SimbioticaCalpBundle:MediaAdmin:select_provider.html.twig', array(
                        'contexts' => $contexts,
                        'base_template' => $this->getBaseTemplate(),
                        'admin' => $this->admin,
                        'action' => 'create'
            ));
        }

        return parent::createAction();
    }
    
    public function batchActionImport(ProxyQueryInterface $query) {
        if (false === $this->admin->isGranted('EDIT')) {
            throw new AccessDeniedException();
        }

        $media = $query->execute();
        
        foreach ($media as $medium)
        {
            if (strcmp('import', $medium->getContext()) != 0)
            {
                $this->addFlash('sonata_flash_error', $this->get('translator')->trans('flash_import_wrong_context', array(), 'SimbioticaAdmin'));
                return new RedirectResponse($this->admin->generateUrl('list', array()));
            }
        }
        $mediaQuery = array_map(function ($medium) {return '--media='.$medium->getId();}, $media);
        
        if(count($mediaQuery) > 0)
        {
            $userId = $this->get('security.context')->getToken()->getUser()->getId();

            $process = new Process(sprintf('php ../app/console calp:excel:import %s --notify --user=%s --env=prod', implode(' ', $mediaQuery), $userId));
            $process->setTimeout(7200);
            $process->start();
            
            $this->addFlash('sonata_flash_success', $this->get('translator')->trans('flash_import_started', array(), 'SimbioticaAdmin'));
        }
        return new RedirectResponse($this->admin->generateUrl('list', array()));
    }

    /**
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @return \Symfony\Bundle\FrameworkBundle\Controller\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function importAction() {
        if (false === $this->admin->isGranted('CREATE')) {
            throw new AccessDeniedException();
        }
        
        $id = $this->getRequest()->get('id');
        
        $media = $this->admin->getObject($id);
        
        if ($media && strcmp($media->getContext(), 'import') != 0)
        {
            throw new \RuntimeException("Invalid media type");
        }
        
        if ($this->getRestMethod() == 'POST') {
            $confirmation = $this->get('request')->get('confirmation', false);
            
            if(!$confirmation)
            {
                return parent::editAction();
            }
            
            $userId = $this->get('security.context')->getToken()->getUser()->getId();
            
            $process = new Process(sprintf('php ../app/console calp:excel:import --media=%d --notify --user=%s --env=prod', $id, $userId));
            $process->setTimeout(3600);
            $process->start();
            
            $this->addFlash('sonata_flash_success', $this->get('translator')->trans('flash_import_started', array(), 'SimbioticaAdmin'));
            
            return new RedirectResponse($this->admin->generateUrl('list', array()));
        }
        else
        {
            return $this->render('SimbioticaCalpBundle:MediaAdmin:import_info_confirmation.html.twig', array(
                    'admin' => $this->admin,
                    'action' => 'import',
                    'object' => $media,
            ));
        }
    }

}

