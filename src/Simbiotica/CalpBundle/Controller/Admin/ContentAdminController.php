<?php 

namespace Simbiotica\CalpBundle\Controller\Admin;

use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Sonata\MediaBundle\Controller\GalleryAdminController;

class ContentAdminController extends GalleryAdminController
{
    /**
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @return \Symfony\Bundle\FrameworkBundle\Controller\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        if (false === $this->admin->isGranted('CREATE')) {
            throw new AccessDeniedException();
        }
    
        $parameters = $this->admin->getPersistentParameters();
    
        if (!$parameters['language']) {
            return $this->render('SimbioticaCalpBundle:ContentAdmin:select_language.html.twig', array(
                    'languages'     => SimbioticaCalpBundle::getLanguages(),
                    'base_template' => $this->getBaseTemplate(),
                    'admin'         => $this->admin,
                    'action'        => 'create'
            ));
        }
    
        return parent::createAction();
    }
}

?>