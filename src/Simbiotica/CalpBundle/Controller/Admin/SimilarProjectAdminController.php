<?php

namespace Simbiotica\CalpBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SimilarProjectAdminController extends CRUDController {
    
    public function markAction(Request $request, $id) {
        if (false === $this->admin->isGranted('EDIT')) {
            throw new AccessDeniedException();
        }

        $similarProject = $this->admin->getObject($id);
        $similarProject->setMarked(true);
        $this->admin->update($similarProject);
        
        if($request->isXmlHttpRequest())
            return new Response(json_encode(true), 200, array('Content-Type'=>'application/json'));

        $this->setFlash('sonata_flash_success', 'flash_mark_success');

        return new RedirectResponse($this->admin->generateUrl('edit', array('id' => $id)));
    }
}

?>