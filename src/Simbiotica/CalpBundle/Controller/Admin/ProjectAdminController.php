<?php

namespace Simbiotica\CalpBundle\Controller\Admin;

use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Sonata\MediaBundle\Controller\GalleryAdminController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class ProjectAdminController extends GalleryAdminController {

    public function sidebarAction($id = null) {
        $id = $id?:$this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('Unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }
        $validationList = $this->get('simbiotica.utils.project_validator')->validateProject($object);

        $em = $this->getDoctrine()->getManager();
        $similarProjectsCount = $em->getRepository('SimbioticaCalpBundle:SimilarProject')->numberSimilarProjects($object);
        
        return $this->render('SimbioticaCalpBundle:ProjectAdmin:sidebar.html.twig', array(
            'object' => $object,
            'validationList' => $validationList,
            'similarProjectsCount' => $similarProjectsCount,
        ));
    }
    
    public function changeStatusAction($id) {
        $id = $this->get('request')->get($this->admin->getIdParameter());
        $status = intval($this->get('request')->get('status'));

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }
        
        if (!is_null($status) && in_array($status, array_keys(SimbioticaCalpBundle::getPublishedStatus())))
        {
            $validationList = $this->get('simbiotica.utils.project_validator')->validateProject($object);
            if ($status != SimbioticaCalpBundle::PUBLISHED_PUBLIC || count($validationList) == 0) {
                $object->setPublished($status);
            }
            
            $this->getDoctrine()->getManager()->flush();
            
            $this->addFlash('sonata_flash_success', 'flash_edit_success');
        }
        else
        {
            $this->addFlash('sonata_flash_error', 'flash_edit_error');
        }
        
        return new RedirectResponse($this->admin->generateUrl('edit', array('id' => $id)));
    }
    
    public function notificationAction($id) {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }
        
        if($object) {
            $projectsIds = array($id);
            try {
                $backend = $this->get('sonata.notification.backend');
                $backend->createAndPublish('notification', array(
                    'ids' => $projectsIds,
                    'type' => 'notification'
                ));
            } catch (\Exception $e) 
            {
                $message = \Swift_Message::newInstance()
                    ->setSubject('RabbitMQ exception')
                    ->setFrom($this->container->getParameter('mailer_from'))
                    ->setTo($this->container->getParameter('notification_mail'))
                    ->setBody("Notification error: <br/>".$e->getMessage());
                
                $this->get('mailer')->send($message);

                return new RedirectResponse($this->admin->generateUrl('edit', array('id' => $id)));
            }
        }
        
        $this->addFlash('sonata_flash_success', 'flash_batch_notification_success');
        
        return new RedirectResponse($this->admin->generateUrl('edit', array('id' => $id)));
    }
    
    /**
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @return \Symfony\Bundle\FrameworkBundle\Controller\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction() {
        if (false === $this->admin->isGranted('CREATE')) {
            throw new AccessDeniedException();
        }

        $container = $this->get('service_container');
        
        $parameters = $this->admin->getPersistentParameters();

        if (!$parameters['language']) {
            return $this->render('SimbioticaCalpBundle:ProjectAdmin:select_language.html.twig', array(
                        'languages' => SimbioticaCalpBundle::getLanguages(),
                        'base_template' => $this->getBaseTemplate(),
                        'admin' => $this->admin,
                        'action' => 'create'
            ));
        }

        return parent::createAction();
    }

    public function batchActionCompare(ProxyQueryInterface $query) {
        if (false === $this->admin->isGranted('EDIT')) {
            throw new AccessDeniedException();
        }

        $projects = $query->execute();

        return $this->render('SimbioticaCalpBundle:ProjectAdmin:project_compare.html.twig', array(
            'contexts' => SimbioticaCalpBundle::getContexts(),
            'marketAssessmentTools' => SimbioticaCalpBundle::getMarketAsssessmentTypes(),
            'modalityList' => SimbioticaCalpBundle::getModalities(),
            'deliveryMechanismList' => SimbioticaCalpBundle::getModalities(null, 1),
            'deliveryAgentList' => SimbioticaCalpBundle::getModalities(null, 2),
            'typeOfInterventionList' => SimbioticaCalpBundle::getInterventionTypes(),
            'months' => SimbioticaCalpBundle::getMonths(),
            'action'   => 'compare',
            'projects' => $projects,
        ));
    }
    
    public function batchActionPublish(ProxyQueryInterface $query) {
        if (false === $this->admin->isGranted('EDIT')) {
            throw new AccessDeniedException();
        }

        $projects = $query->execute();
        
        foreach ($projects as $project)
        {
            if (count($this->get('simbiotica.utils.project_validator')->validateProject($project)) == 0)
                $project->setPublished(SimbioticaCalpBundle::PUBLISHED_PUBLIC);
        }
        $this->getDoctrine()->getManager()->flush();
        
        $this->addFlash('sonata_flash_success', 'flash_batch_publish_success');

        return new RedirectResponse($this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())));
    }

    public function batchActionNotification(ProxyQueryInterface $query)
    {
        if (false === $this->admin->isGranted('EDIT')) {
            throw new AccessDeniedException();
        }

        $projects = $query->execute();

        if($projects) {
            try {
                $backend = $this->get('sonata.notification.backend');
                
                $projectsIds = array_map(function ($project) {return $project->getId();}, $projects);
                
                $backend->createAndPublish('notification', array(
                    'ids' => $projectsIds,
                    'type' => 'notification'
                ));
            } catch (\Exception $e)
            {       
                $message = \Swift_Message::newInstance()
                    ->setSubject('RabbitMQ exception')
                    ->setFrom($this->container->getParameter('mailer_from'))
                    ->setTo($this->container->getParameter('notification_mail'))
                    ->setBody("Batch action notification error: <br/>".$e->getMessage());
                
                $this->get('mailer')->send($message);

                return new RedirectResponse($this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())));
            }
        }
        
        $this->addFlash('sonata_flash_success', 'flash_batch_notification_success');
        
        return new RedirectResponse($this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())));
    }


    public function similarProjectsAction(Request $request) {
        $id = $request->get($this->admin->getIdParameter(), 0);

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('Unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }
        
        $em = $this->getDoctrine()->getManager();
        $similarProjectsList = $em->getRepository('SimbioticaCalpBundle:SimilarProject')->findByReference($object);
        
        return $this->render('SimbioticaCalpBundle:ProjectAdmin:similar_projects.html.twig', array(
            'action' => 'similar_projects',
            'object' => $object,
            'similarProjectsList' => $similarProjectsList,
        ));
    }

}

?>