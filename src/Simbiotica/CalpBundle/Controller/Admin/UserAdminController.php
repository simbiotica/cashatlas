<?php

namespace Simbiotica\CalpBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Simbiotica\CalpBundle\Entity\Organization;
use Simbiotica\CalpBundle\Entity\OrganizationUser;

class UserAdminController extends CRUDController {
    
    public function approveAction(Request $request, $id) {
        if (false === $this->admin->isGranted('EDIT')) {
            throw new AccessDeniedException();
        }

        $user = $this->admin->getObject($id);
        
        if(!$user->getPendingApproval())
        {
            $this->setFlash('sonata_flash_info', 'flash_approval_done');

            return new RedirectResponse($this->admin->generateUrl('edit', array('id' => $id)));
        }
        
        $user->setEnabled(true);
        $user->setPendingApproval(false);

        if($request->query->get('editor') == 1) {
            $user->addRole('ROLE_EDITOR');
            
            $otherOrg = $user->getOtherOrganization();
            if(!empty($otherOrg))
            {
                $ou = new OrganizationUser();
                $org = new Organization();
                $org->setName($user->getOtherOrganization());
                $ou->setOrganization($org);
                $ou->setEnabled(true);
                $user->addOrganization($ou);
            }
        } else {
            $user->addRole('ROLE_USER');
        }

        foreach($user->getOrganizations() as $ou)
        {
            $ou->setEnabled(true);
        }
        
        
        $this->admin->update($user);

        $this->container->get('simbiotica.utils.account_mailer')->sendWelcomeEmailMessage($user);

        $this->setFlash('sonata_flash_success', 'flash_approval_success');

        return new RedirectResponse($this->admin->generateUrl('edit', array('id' => $id)));
    }
    
    public function declineAction($id) {
        if (false === $this->admin->isGranted('EDIT')) {
            throw new AccessDeniedException();
        }

        $user = $this->admin->getObject($id);
        
        if(!$user->getPendingApproval())
        {
            $this->setFlash('sonata_flash_info', 'flash_approval_done');

            return new RedirectResponse($this->admin->generateUrl('edit', array('id' => $id)));
        }
        
        $user->setPendingApproval(false);
        $this->admin->update($user);

        $this->setFlash('sonata_flash_success', 'flash_declination_success');

        return new RedirectResponse($this->admin->generateUrl('edit', array('id' => $id)));
    }
    
    /**
     * @param string $action
     * @param string $value
     */
    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->set($action, $value);
    }
}

?>