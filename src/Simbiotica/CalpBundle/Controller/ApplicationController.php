<?php

namespace Simbiotica\CalpBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use JMS\Serializer\SerializationContext;

class ApplicationController extends Controller
{
    public function indexAction(Request $request)
    {
        $session = $request->getSession();
        
        if($request->query->get('public', false))
        {
            $session->set('visited', true);
            return new RedirectResponse($this->generateUrl('simbiotica_calp_homepage'));
        }
        
        if(!$session->get('visited', false) && !$this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            return new RedirectResponse($this->generateUrl('fos_user_security_login'));
        }
        
        $data = array();
        $form = $this->createForm('simbiotica_form_search', $data);

        $serializer = $this->container->get('jms_serializer');
        
        $result = array();
        $result['modalities'] = $this->translateArray(SimbioticaCalpBundle::getModalities());
        $result['deliveryMechanism'] = $this->translateArray(SimbioticaCalpBundle::getModalities(null, 1));
        $result['deliveryAgent'] = $this->translateArray(SimbioticaCalpBundle::getModalities(null, 2));
        $result['projectContext'] = $this->translateArray(SimbioticaCalpBundle::getContexts());
        $result['marketAssessment'] = $this->translateArray(SimbioticaCalpBundle::getMarketAsssessmentTypes());
        $result['typeOfIntervention'] = $this->translateArray(SimbioticaCalpBundle::getInterventionTypes());
        $result['status'] = $this->translateArray(SimbioticaCalpBundle::getProjectStatus());
        $result['durations'] = $this->translateArray(SimbioticaCalpBundle::getDurationRanges());
        
        $em = $this->getDoctrine()->getManager();
        
        $organizations = $em->getRepository('SimbioticaCalpBundle:Organization')->getListWithProjects($request->getLocale());
        
        $pool = $this->get('sonata.media.pool');
        $organizationMedias = $em->getRepository('SimbioticaCalpBundle:Organization')->getMedia(array_map(function($item){return $item['id'];}, $organizations));
        foreach ($organizationMedias as $organization)
        {
            $medium = $organization->getMedia();
            $provider = $pool->getProvider($medium->getProviderName());
            $format = $provider->getFormatName($medium, "big");
            $mediaList[$organization->getId()] = $provider->generatePublicUrl($medium, $format);
        }

        $result['media'] = $mediaList;
            
        $result['organization'] = $organizations;
        $result['sector'] = $em->getRepository('SimbioticaCalpBundle:Sector')->getList($request->getLocale());
        
        $output = $serializer->serialize($result, 'json', SerializationContext::create()->setGroups(array('constants')));
        
        $translator =  $this->container->get('translator');
        $router = $this->container->get('router');
        
        $switcherData = array();
        foreach (SimbioticaCalpBundle::getLanguages() as $key => $value)
        {
            /** Ignore JMSTranslationBundle error when detecting translations */
            /** @Ignore */
            $switcherData[$translator->trans($key , array(), 'SimbioticaFront', $key)] = $router->generate('simbiotica_calp_homepage', array("_locale" => $value));
        }
        
        return $this->render('SimbioticaCalpBundle:Application:index.html.twig', array(
                'form' => $form->createView(),
                'data' => $output,
                'switcherData' => $switcherData,
        ));
    }
    
    public function languageSwitcherAction(Request $request, $route, $params) {
        $translator =  $this->container->get('translator');
        $router = $this->container->get('router');
        
        $switcherData = array();
        foreach (SimbioticaCalpBundle::getLanguages() as $key => $value)
        {
            /** Ignore JMSTranslationBundle error when detecting translations */
            /** @Ignore */
            $switcherData[$translator->trans($key , array(), 'SimbioticaFront', $key)] = $router->generate($route, array_merge($params, array("_locale" => $value)));
        }
        
        return $this->render('SimbioticaCalpBundle:Helpers:language_switcher.html.twig', array(
                'switcherData' => $switcherData,
        ));
    }
    
    public function tutorialAction()
    {
        $translator =  $this->container->get('translator');
        $router = $this->container->get('router');
        
        $switcherData = array();
        foreach (SimbioticaCalpBundle::getLanguages() as $key => $value)
        {
            /** Ignore JMSTranslationBundle error when detecting translations */
            /** @Ignore */
            $switcherData[$translator->trans($key , array(), 'SimbioticaFront', $key)] = $router->generate('simbiotica_calp_tutorial', array("_locale" => $value));
        }
        
        return $this->render('SimbioticaCalpBundle:Application:tutorial.html.twig', array(
            'switcherData' => $switcherData,
        ));
    }
    
    private function translateArray($inputArray) {
        $translator = $this->get('translator');
        return array_map(function($item) use ($translator) {
            /** @Ignore */
            return $translator->trans($item, array(), 'SimbioticaFront');
        }, $inputArray);
    }
}
