<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Simbiotica\CalpBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as ParentController;

class SecurityController extends ParentController
{
    public function loginAction()
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->container->get('templating')->renderResponse('SimbioticaCalpBundle:Security:already_loggedin.html.twig', array(
            ));
        }
        return parent::loginAction();
    }
}
