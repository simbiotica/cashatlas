<?php

namespace Simbiotica\CalpBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Simbiotica\CalpBundle\Entity\OrganizationUser;

class OrganizationAdmin extends Admin {

    public function getNewInstance()
    {
        $organization = parent::getNewInstance();
        $ou = new OrganizationUser();
        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $ou->setUser($user);
        $ou->setEnabled(true);
        $organization->addUser($ou);
        
        return $organization;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                )))
                ->add('name')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper) {
        $filterMapper
                ->add('name')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $link_parameters = array('context' => 'organization');

        $formMapper
                ->with('General')
                ->add('name', null, array(), array(
                    'help' => 'form.organization.help_name',
                ))
                ->add('media', 'sonata_type_model_list', array(
                    'label' => 'form.label_logo',
                    'required' => false,
                    'btn_list' => 'link_logo_list',
                    'btn_catalogue' => 'SimbioticaAdmin',
                ), array(
                    'link_parameters' => $link_parameters,
                    'limit' => 1,
                    'help' => 'form.organization.help_media',
                ))
                ->add('users', 'sonata_type_collection', array(
                    'cascade_validation' => true,
                    'required' => false,
                        ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'admin_code' => 'simbiotica.calp.admin.organization_user'
                ))
                ->add('projects', 'sonata_type_collection', array(
                    'cascade_validation' => true,
                    'required' => false,
                    'btn_add' => false,
                        ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'admin_code' => 'simbiotica.calp.admin.project_organization'
                ))
                ->add('donatingProjects', 'sonata_type_collection', array(
                    'cascade_validation' => true,
                    'required' => false,
                    'btn_add' => false,
                        ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'admin_code' => 'simbiotica.calp.admin.project_donor'
                ))
                ->add('translations', 'a2lix_translations_gedmo', array(
                    'translatable_class' => "Simbiotica\CalpBundle\Entity\Organization",
                 ))
                ->end()
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function prePersist($organization) {
        $organization->setUsers($organization->getUsers());
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($organization) {
        $organization->setUsers($organization->getUsers());
    }


    public function getExportFormats()
    {
        return array('xls', 
        );
    }
}