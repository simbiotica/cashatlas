<?php

namespace Simbiotica\CalpBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Sonata\MediaBundle\Admin\ORM\MediaAdmin as BaseAdmin;
use Symfony\Component\Security\Core\Role\RoleHierarchy;

class MediaAdmin extends BaseAdmin
{
    protected $maxPerPage = 10;
    
    public function getPersistentParameters()
    {
        if (!$this->hasRequest()) {
            return array();
        }

        $context   = $this->getRequest()->get('context');
        $providers = $this->pool->getProvidersByContext($context);
        $provider  = $this->getRequest()->get('provider');

        // if the context has only one provider, set it into the request
        // so the intermediate provider selection is skipped
        if (count($providers) == 1 && null === $provider) {
            $provider = array_shift($providers)->getName();
            $this->getRequest()->query->set('provider', $provider);
        }

        return array(
            'provider' => $provider,
            'context'  => $context,
        );
    }
    
    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('view', $this->getRouterIdParameter() . '/view');
        $collection->add('show', $this->getRouterIdParameter() . '/show', array(
            '_controller' => sprintf('%s:%s', $this->baseControllerName, 'view')
        ));
        $collection->add('import', $this->getRouterIdParameter() . '/import');
    }
    
    /**
     * {@inheritdoc}
     */
    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!in_array($action, array('edit', 'import', 'view'))) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;

        $id = $this->getRequest()->get('id');
        
        $media = $this->getSubject();
        
        $menu->addChild(
            $this->trans('sidemenu.link_edit_media'),
            array('uri' => $admin->generateUrl('edit', array('id' => $id)))
        );

        $menu->addChild(
            $this->trans('sidemenu.link_media_view'),
            array('uri' => $admin->generateUrl('view', array('id' => $id)))
        );
        
        if ($media && $media->getContext() == 'import')
        {
            $menu->addChild(
                $this->trans('sidemenu.link_media_import', array(), 'SimbioticaAdmin'),
                array('uri' => $admin->generateUrl('import', array('id' => $id)))
            );
        }
    }
    
    public function getBatchActions() {
        // retrieve the default (currently only the delete action) actions
        $actions = parent::getBatchActions();

        // check user permissions
        if ($this->isGranted('VIEW')) {
            $actions['import'] = array(
                /** @Ignore */
                'label' => $this->trans('action_import', array(), 'SimbioticaAdmin'),
                'ask_confirmation' => true
            );
        }

        return $actions;
    }
    
    /**
     * @param  \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $rawContexts = $this->pool->getContexts();
        $contexts = array();
        
        foreach($rawContexts as $name => $context)
            $contexts[$name] = $name;

        $datagridMapper
                ->add('name')
                ->add('providerReference')
                ->add('enabled')
                ->add('context', 'doctrine_orm_choice', array(
                    'field_options'=> array(
                        'choices'  => $contexts,
                        'required' => false,
                        'multiple' => false,
                        'expanded' => false,
                        'translation_domain' => 'SonataMediaBundle'
                    ),
                    'field_type'=> 'choice',
                ))
        ;

        $providers = array();
        
        $providerNames = (array) $this->pool->getProviderNamesByContext($this->getPersistentParameter('context', $this->pool->getDefaultContext()));
        foreach ($providerNames as $name) {
            $providers[$name] = $name;
        }

        $datagridMapper->add('providerName', 'doctrine_orm_choice', array(
            'field_options'=> array(
                'choices' => $providers,
                'required' => false,
                'multiple' => false,
                'expanded' => false,
                'translation_domain' => 'SonataMediaBundle'
            ),
            'field_type'=> 'choice',
        ));
    }
        
    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                )
            ))
//            ->add('image', 'string', array('template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig'))
            ->add('custom', 'string', array('template' => 'SonataMediaBundle:MediaAdmin:list_custom.html.twig'))
            ->add('enabled', 'boolean', array('editable' => true))
            
        ;
    }
    
    /**
     * DOWN: Copy of SimbioticaCalpBundle/Admin/Admin.php content
     */
    public function isAclEnabled()
    {
        return false;
    }
    
    public function createQuery($context = 'list') 
    {
        if ($this->getConfigurationPool()->getContainer()->get('security.context')->isGranted('ROLE_SUPER_ADMIN'))
            return parent::createQuery($context);
        
        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser(); 
        $qb = $this->createObjectsByMaskQuery($user, $this->getClass()); 
        return new ProxyQuery($qb); 
    } 
    
    public function createObjectsByMaskQuery($user, $class , $mask = MaskBuilder::MASK_EDIT) 
    { 
        $rsm = new ResultSetMapping; 
        $rsm->addScalarResult('object_identifier', 'object_identifier'); 

        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
        
        $roleHierachy = new RoleHierarchy($this->getConfigurationPool()->getContainer()->getParameter('security.role_hierarchy.roles'));
        $userRoles = $roleHierachy->getReachableRoles($securityContext->getToken()->getRoles());
        $userRoleNames = array_map(function ($role) {return $role->getRole();}, $userRoles);

        $em = $this->getModelManager($class)->getEntityManager($class); 

        $queryString = ' 
            SELECT oid.object_identifier 
        FROM 
            acl_security_identities sid 
        JOIN 
            acl_entries e ON ( 
            e.security_identity_id = sid.id 
        ) 
        JOIN acl_object_identities oid ON (e.class_id = oid.class_id 
            AND e.object_identity_id IS NOT NULL AND 
            e.object_identity_id = oid.id) 
        JOIN acl_classes c ON oid.class_id = c.id 
        WHERE 
            c.class_type LIKE :class AND 
            e.mask >= :mask';
        
        $sidList = array_merge(array(addslashes(get_class($user)).'-'.$user->getUsername()), $userRoleNames);
        $sidQuerySection = array();
        foreach($sidList as $index => $role)
        {
            $sidQuerySection[] = 'sid.identifier like (:param'.$index.')';
        }
        if ($role)
            $queryString .= ' AND ('.implode(' OR ', $sidQuerySection).')';
        
        $query = $em->createNativeQuery($queryString, $rsm); 

        $query->setParameter('class', addslashes($class)); 
        $query->setParameter('mask', $mask); 
        foreach($sidList as $index => $role)
        {
           $query->setParameter('param'.$index, $role);
        }

        $result = array(); 
        foreach ($query->getResult() as $i => $row) 
        { 
            $result[] = $row['object_identifier']; 
        } 

        if(!count($result)) { 
            $result = array(); 
            $result[] = 0; 
        } 

        $qb = $em->getRepository($class)->createQueryBuilder('c'); 
        $qb->where($qb->expr()->in('c.id',$result)); 

        return $qb; 
    }
    
    public function getExportFormats()
    {
        return array(
        );
    }
}
