<?php

namespace Simbiotica\CalpBundle\Admin;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Symfony\Component\Form\Extension\Core\ChoiceList\SimpleChoiceList;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class OrganizationUserAdmin extends Admin {

    protected $container;
    
    public function __construct($code, $class, $baseControllerName, ContainerInterface $container) {
        parent::__construct($code, $class, $baseControllerName);

        $this->container = $container;
    }
    
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                )))
                ->add('organization')
                ->add('user')
//                ->add('admin')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper) {
        $organizationsQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Organization")->getList($this->container->get('request')->getLocale());
        $organizationsChoices = array();
        foreach($organizationsQuery as $org)
            $organizationsChoices[$org['id']] = $org['name'];
        $organizations = new SimpleChoiceList($organizationsChoices);
        
        $filterMapper
                ->add('organization', 'doctrine_orm_choice', array(
                    'field_options'=> array(
                        'choice_list'  => $organizations,
                    ),
                    'field_type'=> 'choice',
                ))
                ->add('user')
//                ->add('admin')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $organizationsQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Organization")->getList($this->container->get('request')->getLocale());
        $organizationsChoices = array();
        foreach($organizationsQuery as $org)
            $organizationsChoices[$org['id']] = $org['name'];
        $organizations = new SimpleChoiceList($organizationsChoices);
        
        $formMapper
                ->with('General')
                ->add('user', 'sonata_type_model', array(
                    'required' => false,
                    'btn_add' => false,
                    'empty_value' => 'form.label_empty',
                    'translation_domain' => 'SimbioticaAdmin',
                ))
                ->add('organization', 'sonata_type_model', array(
                    'required' => false, 
                    'choice_list' => $organizations,
                    'btn_add' => false,
                    'empty_value' => 'form.label_empty',
                    'translation_domain' => 'SimbioticaAdmin',
                ))
//                ->add('admin')
                ->add('enabled', null, array('required' => false))
                ->end()
        ;
        
        $builder = $formMapper->getFormBuilder();
        
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function(FormEvent $event) use ($formMapper){
                $form = $event->getForm();
                
                if($form->getParent() && $form->getParent()->getParent())
                {
                    if ($form->getParent()->getParent()->getConfig()->getDataClass() == 'Simbiotica\CalpBundle\Entity\User')
                    {
                        $formMapper->remove('user');
                        $form->remove('user');
                    }
                    elseif ($form->getParent()->getParent()->getConfig()->getDataClass() == 'Simbiotica\CalpBundle\Entity\Organization')
                    {
                        $formMapper->remove('organization');
                        $form->remove('organization');
                    }
                }
            }
        );
        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function(FormEvent $event) use ($formMapper){
                $form = $event->getForm();
                
                if($form->getParent() && $form->getParent()->getParent())
                {
                    if ($form->getParent()->getParent()->getConfig()->getDataClass() == 'Simbiotica\CalpBundle\Entity\User')
                    {
                        $formMapper->remove('user');
                        $form->remove('user');
                    }
                    elseif ($form->getParent()->getParent()->getConfig()->getDataClass() == 'Simbiotica\CalpBundle\Entity\Organization')
                    {
                        $formMapper->remove('organization');
                        $form->remove('organization');
                    }
                }
            }
        );
    }
    
    public function getNewInstance() {
        $ou = parent::getNewInstance();
        $ou->setEnabled(true);

        return $ou;
    }

}