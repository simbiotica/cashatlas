<?php

namespace Simbiotica\CalpBundle\Admin;

use Sonata\AdminBundle\Admin\Admin as BaseAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

class ContinentAdmin extends BaseAdmin {

    protected $maxPerPage = 10;
    protected $securityContext;
    
    public function __construct($code, $class, $baseControllerName, $securityContext) {
        parent::__construct($code, $class, $baseControllerName);
        
        $this->securityContext = $securityContext;
    }
    
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                )))
                ->add('name')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper) {
        $filterMapper
                ->add('name')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('General')
                ->add('name')
                ->add('translations', 'a2lix_translations_gedmo', array(
                    'translatable_class' => "Simbiotica\CalpBundle\Entity\Continent",
                 ))
                ->end()
        ;
        if ($this->securityContext->getToken()->getUser()->hasRole('ROLE_SUPER_ADMIN')) {
            $formMapper
                ->with('Management')
                    ->add('centroid', 'text', array('required' => false))
                ->end()
            ;
        }
    }
    
    public function getExportFormats()
    {
        return array(
        );
    }
}