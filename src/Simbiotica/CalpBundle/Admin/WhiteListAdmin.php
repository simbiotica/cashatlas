<?php

namespace Simbiotica\CalpBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

class WhiteListAdmin extends Admin 
{
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                )))
                ->add('email')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper) {
        $filterMapper
                ->add('email')
                ->add('imported')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('General')
                ->add('email')
                ->add('imported', 'checkbox', array('disabled' => true, 'required' => false))
        ;
    }
    
    public function getExportFormats()
    {
        return array('xls','csv'
        );
    }

}