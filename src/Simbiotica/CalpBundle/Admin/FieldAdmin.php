<?php

namespace Simbiotica\CalpBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

class FieldAdmin extends Admin 
{
    protected $securityContext;
    
    public function __construct($code, $class, $baseControllerName, $securityContext) {
        parent::__construct($code, $class, $baseControllerName);
        
        $this->securityContext = $securityContext;
    }
    
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                )))
                ->add('name')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper) {
        $filterMapper
                ->add('name')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('General')
                ->add('name')
        ;
        if ($this->securityContext->getToken()->getUser()->hasRole('ROLE_SUPER_ADMIN')) {
            $formMapper
                    ->add('user', 'text', array('required' => false, 'read_only' => true, 'data_class' => 'Simbiotica\CalpBundle\Entity\User'))
            ;
        }
    }

}