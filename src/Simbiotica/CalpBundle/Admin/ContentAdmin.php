<?php

namespace Simbiotica\CalpBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

class ContentAdmin extends Admin
{
    public function __construct($code, $class, $baseControllerName, ContainerInterface $container)
    {
        parent::__construct($code, $class, $baseControllerName);
    
        $this->container = $container;
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                )))
                ->add('name')
                ->add('language', 'trans', array('catalogue' => 'SimbioticaAdmin'))
                ->add('uri')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
                ->add('name')
                ->add('language', 'doctrine_orm_choice', array(
                    'field_options'=> array(
                        'choices'  => SimbioticaCalpBundle::getLanguages(),
                        'required' => false,
                        'multiple' => false,
                        'expanded' => false,
                        'translation_domain' => 'SimbioticaAdmin',
                    ),
                    'field_type'=> 'choice',
                ))
                ->add('uri')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        ->with('General')
        ->add('language', 'translated_text', array(
            'required' => true, 
            'disabled' => true, 
            'translation_domain' => 'SimbioticaAdmin',
        ))
        ->add('name')
        ->add('title', null, array('required' => false))
        ->add('content', null, array('required' => false, 'attr' => array('class' => 'ckeditor')))
        ->add('description', null, array('required' => false))
        ->add('keywords', null, array('required' => false))
        ->add('uri')
        ->add('view', null, array(), array(
            'help' => 'form.content.help_view',
        ))
        ->add('published', 'choice', array(
            'choices' => SimbioticaCalpBundle::getPublishedStatus(), 
            'required' => true,
            'translation_domain' => 'SimbioticaAdmin'
        ))
        ->add('position', 'integer', array('required' => false))
        ->end()
        ;
    }
    
    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!in_array($action, array('edit', 'view'))) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;

        $id = $this->getRequest()->get('id');

        $languages = SimbioticaCalpBundle::getLanguages();
        $translations = $this->container->get('doctrine')->getRepository('SimbioticaCalpBundle:Content')->findTranslations($id, 0);

        $label = $menu->addChild($this->trans('form.content.translations', array(), 'SimbioticaAdmin'), array());

        foreach ($languages as $language => $languageLabel)
        {
            /** Ignore JMSTranslationBundle error when detecting translations */
            /** @Ignore */
            $localeLabel = $this->trans($languageLabel);

            foreach ($translations as $translation)
            {
                if (strcmp($translation->getLanguage(), $language) == 0)
                {
                    $label->addChild($localeLabel, array('uri' => $admin->generateUrl('edit', array('id' => $translation->getId()))));
                    continue 2;
                }
            }
            
            $label->addChild($localeLabel, array('uri' => $admin->generateUrl('create', array('language' => $languageLabel, 'tid' => $id))));
        }
    }
    
    public function getNewInstance()
    {
        $content = parent::getNewInstance();

        if ($this->hasRequest()) {
            $languageList = array_flip(SimbioticaCalpBundle::getLanguages());
            $content->setLanguage($languageList[$this->getRequest()->get('language')]);
            
            if ($this->getRequest()->get('tid') > 0)
            {
                $content->setTid($this->container->get('doctrine')->getRepository('SimbioticaCalpBundle:Content')->find($this->getRequest()->get('tid')));
            }
        }

        return $content;
    }
    
    public function getPersistentParameters()
    {
        if (!$this->hasRequest()) {
            return array();
        }

        $language = $this->getRequest()->get('language');
        $tid = $this->getRequest()->get('tid');

        return array(
            'language' => $language,
            'tid' => $tid,
        );
    }
}