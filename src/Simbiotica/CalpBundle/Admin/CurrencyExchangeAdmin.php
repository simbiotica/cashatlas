<?php

namespace Simbiotica\CalpBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

class CurrencyExchangeAdmin extends Admin
{
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('date')
                ->add('rate')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('General')
                ->add('date')
               	->add('rate')
                ->end()
        ;
    }
}