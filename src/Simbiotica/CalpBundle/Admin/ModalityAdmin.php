<?php

namespace Simbiotica\CalpBundle\Admin;

use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

class ModalityAdmin extends Admin {

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                )))
                ->add('project')
                ->add('modality', null, array('template' => 'SimbioticaCalpBundle:ModalityAdmin:list_modality.html.twig'))
                ->add('modalitySpecify')
                ->add('deliveryMechanism', null, array('template' => 'SimbioticaCalpBundle:ModalityAdmin:list_delivery_mechanism.html.twig'))
                ->add('deliveryMechanismSpecify')
                ->add('deliveryAgent', null, array('template' => 'SimbioticaCalpBundle:ModalityAdmin:list_delivery_agent.html.twig'))
                ->add('deliveryAgentSpecify')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper) {
        $filterMapper
                ->add('project')
                ->add('modality', 'doctrine_orm_choice', array(
                    'field_options'=> array(
                        'choices'  => SimbioticaCalpBundle::getModalities(),
                        'required' => false,
                        'multiple' => false,
                        'expanded' => false,
                        'translation_domain' => 'SimbioticaAdmin',
                    ),
                    'field_type'=> 'choice',
                ))
                ->add('modalitySpecify')
                ->add('deliveryMechanism', 'doctrine_orm_choice', array(
                    'field_options'=> array(
                        'choices'  => SimbioticaCalpBundle::getModalities(null, 1),
                        'required' => false,
                        'multiple' => false,
                        'expanded' => false,
                        'translation_domain' => 'SimbioticaAdmin',
                    ),
                    'field_type'=> 'choice',
                ))
                ->add('deliveryMechanismSpecify')
                ->add('deliveryAgent', 'doctrine_orm_choice', array(
                    'field_options'=> array(
                        'choices'  => SimbioticaCalpBundle::getModalities(null, 2),
                        'required' => false,
                        'multiple' => false,
                        'expanded' => false,
                        'translation_domain' => 'SimbioticaAdmin',
                    ),
                    'field_type'=> 'choice',
                ))
                ->add('deliveryAgentSpecify')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('General')
                ->add('modality', 'choice', array(
                    'attr' => array('class' => 'modality_select'),
                    'required' => false,
                    'choices' => SimbioticaCalpBundle::getModalities(),
                    'translation_domain' => 'SimbioticaAdmin',
                    'empty_value' => 'form.label_empty',
                    'label_attr' => array('class' => 'required'),
                ))
                ->add('modalitySpecify', 'textarea', array(
                    'attr' => array('class' => 'modality_specify'),
                    'required' => false,
                ))
                ->add('deliveryMechanism', 'choice', array(
                    'attr' => array('class' => 'delivery_mechanism_select'),
                    'label_attr' => array('class' => 'required'),
                    'required' => false,
                    'choices' => array(),
                    'translation_domain' => 'SimbioticaAdmin',
                    'empty_value' => 'form.label_empty',
                    'disabled' => true,
                ))
                ->add('deliveryMechanismSpecify', 'textarea', array(
                    'attr' => array('class' => 'delivery_mechanism_specify'),
                    'required' => false,
                ))
                ->add('deliveryAgent', 'choice', array(
                    'attr' => array('class' => 'delivery_agent_select'),
                    'label_attr' => array('class' => 'required'),
                    'required' => false,
                    'choices' => array(),
                    'translation_domain' => 'SimbioticaAdmin',
                    'empty_value' => 'form.label_empty',
                    'disabled' => true,
                ))
                ->add('deliveryAgentSpecify', 'textarea', array(
                    'attr' => array('class' => 'delivery_agent_specify'),
                    'required' => false,
                ))
                ->add('scale', null, array(
                    'required' => false,
                    'label' => 'form.label_modality_scale',
                    'label_attr' => array('class' => 'required'),
                ))
                ->add('transfers', null, array(
                    'required' => false,
                    'label_attr' => array('class' => 'required'),
                ))
                ->add('amount', null, array(
                    'required' => false,
                    'label' => 'form.label_modality_amount',
                    'label_attr' => array('class' => 'required'),
                ))
                ->end()
        ;
        
        /**
         * Dynamic form
         */
        $builder = $formMapper->getFormBuilder();
        
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function(FormEvent $event) use ($formMapper){
                $form = $event->getForm();
                $data = $event->getData();
                
                if ($data && $data->getModality()) {
                    $deliveryMechanisms = SimbioticaCalpBundle::getModalities(array($data->getModality()));
                    if ($data && $data->getDeliveryMechanism()) {
                        $deliveryAgents = SimbioticaCalpBundle::getModalities(array($data->getModality(), $data->getDeliveryMechanism()));
                    } else {
                        $deliveryAgents = array();
                    }
                } else {
                    $deliveryMechanisms = array();
                    $deliveryAgents = array();
                }

                $form->add($formMapper->add('deliveryMechanism', 'choice', array(
                    'attr' => array('class' => 'delivery_mechanism_select'),
                    'label_attr' => array('class' => 'required'),
                    'required' => false,
                    'auto_initialize' => false,
                    'choices' => $deliveryMechanisms,
                    'translation_domain' => 'SimbioticaAdmin',
                    'empty_value' => 'form.label_empty',
                    'disabled' => empty($deliveryMechanisms),
                    ))->get('deliveryMechanism')->getForm());

                $form->add($formMapper->add('deliveryAgent', 'choice', array(
                    'attr' => array('class' => 'delivery_agent_select'),
                    'label_attr' => array('class' => 'required'),
                    'required' => false,
                    'auto_initialize' => false,
                    'choices' => $deliveryAgents,
                    'translation_domain' => 'SimbioticaAdmin',
                    'empty_value' => 'form.label_empty',
                    'disabled' => empty($deliveryAgents),
                    ))->get('deliveryAgent')->getForm());
                        
            }
        );
        
        
        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function(FormEvent $event) use ($formMapper){
                $form = $event->getForm();
                $data = $event->getData();
                
                if ($data && $data['modality']) {
                    $deliveryMechanisms = SimbioticaCalpBundle::getModalities(array($data['modality']));
                    if ($data && array_key_exists('deliveryMechanism', $data) && $data['deliveryMechanism']) {
                        $deliveryAgents = SimbioticaCalpBundle::getModalities(array($data['modality'], $data['deliveryMechanism']));
                    } else {
                        $deliveryAgents = array();
                    }
                } else {
                    $deliveryMechanisms = array();
                    $deliveryAgents = array();
                }
                
                $form->add($formMapper->add('deliveryMechanism', 'choice', array(
                    'attr' => array('class' => 'delivery_mechanism_select'),
                    'label_attr' => array('class' => 'required'),
                    'required' => false,
                    'auto_initialize' => false,
                    'choices' => $deliveryMechanisms,
                    'translation_domain' => 'SimbioticaAdmin',
                    'empty_value' => 'form.label_empty',
                    'disabled' => empty($deliveryMechanisms),
                    ))->get('deliveryMechanism')->getForm());

                $form->add($formMapper->add('deliveryAgent', 'choice', array(
                    'attr' => array('class' => 'delivery_agent_select'),
                    'label_attr' => array('class' => 'required'),
                    'required' => false,
                    'auto_initialize' => false,
                    'choices' => $deliveryAgents,
                    'translation_domain' => 'SimbioticaAdmin',
                    'empty_value' => 'form.label_empty',
                    'disabled' => empty($deliveryAgents),
                    ))->get('deliveryAgent')->getForm());
            }
        );
    }

}