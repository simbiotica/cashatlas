<?php

namespace Simbiotica\CalpBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

class ProjectCoordinationMechanismAdmin extends Admin {

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                )))
                ->add('project')
                ->add('coordinationMechanism')
                ->add('specify')
                
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper) {
        $filterMapper
                ->add('project')
                ->add('coordinationMechanism')
                ->add('specify')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('General')
                ->add('coordinationMechanism', 'entity', array(
                    'required' => false, 
                    'class' => 'Simbiotica\CalpBundle\Entity\CoordinationMechanism',
                    'empty_value' => 'form.label_empty',
                    'translation_domain' => 'SimbioticaAdmin',
                ))
                ->add('specify')
                ->end()
        ;
    }

}