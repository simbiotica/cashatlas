<?php

namespace Simbiotica\CalpBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ContactAdmin extends Admin
{
    public function __construct($code, $class, $baseControllerName, ContainerInterface $container)
    {
        parent::__construct($code, $class, $baseControllerName);
    
        $this->container = $container;
    }
    
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(array(
                'status' => array(
                    'value' => SimbioticaCalpBundle::CONTACT_STATUS_OPEN,
                )
            ),
            $this->datagridValues

        );
        return parent::getFilterParameters();
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                )))
                ->add('contactType', 'trans', array('catalogue' => 'SimbioticaAdmin'))
                ->add('language', 'trans', array('catalogue' => 'SimbioticaAdmin'))
                ->add('assignedTo')
                ->add('requestedBy')
                ->add('statusString', 'trans', array('catalogue' => 'SimbioticaAdmin'))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
                ->add('contactType', 'doctrine_orm_choice', array(
                    'field_options'=> array(
                        'choices'  => array(),
                        'required' => false,
                        'multiple' => false,
                        'expanded' => false,
                        'translation_domain' => 'SimbioticaAdmin',
                    ),
                    'field_type'=> 'choice',
                ))
                ->add('language', 'doctrine_orm_choice', array(
                    'field_options'=> array(
                        'choices'  => SimbioticaCalpBundle::getLanguages(),
                        'required' => false,
                        'multiple' => false,
                        'expanded' => false,
                        'translation_domain' => 'SimbioticaAdmin',
                    ),
                    'field_type'=> 'choice',
                ))
                ->add('assignedTo')
                ->add('requestedBy')
                ->add('status', 'doctrine_orm_choice', array(
                    'field_options'=> array(
                        'choices'  => SimbioticaCalpBundle::getContactStatus(),
                        'required' => false,
                        'multiple' => false,
                        'expanded' => false,
                        'translation_domain' => 'SimbioticaAdmin',
                    ),
                    'field_type'=> 'choice',
                ))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $contactTypes = array();
        foreach($this->container->get('simbiotica.contact.pool')->getProviderNames() as $providerName)
        {
            $contactTypes[$providerName] = $providerName;
        }
        
        $formMapper
        ->with('General')
        ->add('language', 'choice', array(
            'required' => true, 
            'choices'  => SimbioticaCalpBundle::getLanguages(),
            'translation_domain' => 'SimbioticaAdmin',
        ))
        ->add('contactType', 'choice', array(
            'required' => false, 
            'choices'  => $contactTypes,
            'translation_domain' => 'SimbioticaAdmin',
            'empty_value' => 'form.label_empty',
        ))
        ->add('firstname', null, array('required' => false))
        ->add('lastname', null, array('required' => false))
        ->add('email', null, array('required' => false))
        ->add('content', null, array('required' => false))
        ->add('status', 'choice', array(
            'required' => true, 
            'choices'  => SimbioticaCalpBundle::getContactStatus(),
            'translation_domain' => 'SimbioticaAdmin',
            'empty_value' => 'form.label_empty',
        ))
        ->add('requestedBy', 'entity', array(
            'disabled' => !$this->isGranted('ROLE_SUPER_ADMIN'),
            'class' => 'Simbiotica\CalpBundle\Entity\User',
            'empty_value' => 'form.label_empty',
            'translation_domain' => 'SimbioticaAdmin',
        ))
        ->add('assignedTo')
        ->add('project')
        ->end()
        ;
    }
    
    public function getNewInstance() {
        $instance = parent::getNewInstance();
        
        $instance->setRequestedBy($this->container->get('security.context')->getToken()->getUser());
        
        return $instance;
    }
}