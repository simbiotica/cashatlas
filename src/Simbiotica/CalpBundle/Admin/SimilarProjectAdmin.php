<?php

namespace Simbiotica\CalpBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sonata\AdminBundle\Route\RouteCollection;

class SimilarProjectAdmin extends Admin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                )))
                ->add('reference')
                ->add('target')
                ->add('marked')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
                ->add('reference')
                ->add('target')
                ->add('marked')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        ->with('General')
            ->add('reference', 'entity', array(
                    'class' => 'Simbiotica\CalpBundle\Entity\Project',
                    'empty_value' => 'form.label_empty',
                    'translation_domain' => 'SimbioticaAdmin',
                ))
            ->add('target', 'entity', array(
                    'class' => 'Simbiotica\CalpBundle\Entity\Project',
                    'empty_value' => 'form.label_empty',
                    'translation_domain' => 'SimbioticaAdmin',
                ))
            ->add('marked')
        ->end()
        ;
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('mark', $this->getRouterIdParameter() . '/mark');
    }
}