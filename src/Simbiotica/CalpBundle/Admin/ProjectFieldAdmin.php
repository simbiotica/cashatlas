<?php

namespace Simbiotica\CalpBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

class ProjectFieldAdmin extends Admin
{
    protected $securityContext;
    
    public function __construct($code, $class, $baseControllerName, $securityContext) {
        parent::__construct($code, $class, $baseControllerName);
        
        $this->securityContext = $securityContext;
    }
    
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                )))
                ->add('project')
                ->add('field')
                ->add('value')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper) {
        $filterMapper
                ->add('project')
                ->add('value')
                ->add('field')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $query = $this->getModelManager()->getEntityManager('SimbioticaCalpBundle:Field')->createQuery('SELECT s FROM Simbiotica\CalpBundle\Entity\Field f WHERE f.user = :user ORDER BY f.name ASC')->setParameter('user', $this->securityContext->getToken()->getUser());

        $formMapper
                ->with('General')
                ->add('field', 'sonata_type_model', array('required' => true))
                ->add('value')
                ->add('public')
                ->end()
        ;
    }

}