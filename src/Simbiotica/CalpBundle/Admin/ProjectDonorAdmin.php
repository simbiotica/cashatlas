<?php

namespace Simbiotica\CalpBundle\Admin;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Symfony\Component\Form\Extension\Core\ChoiceList\SimpleChoiceList;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ProjectDonorAdmin extends Admin {

    protected $container;
    
    public function __construct($code, $class, $baseControllerName, ContainerInterface $container) {
        parent::__construct($code, $class, $baseControllerName);

        $this->container = $container;
    }
    
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                )))
                ->add('project')
                ->add('organization', null, array('label' => 'list.label_donor'))
                ->add('amount')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper) {
        $organizationsQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Organization")->getList($this->container->get('request')->getLocale());
        $organizationsChoices = array();
        foreach($organizationsQuery as $organization)
            $organizationsChoices[$organization['id']] = $organization['name'];
        $organizations = new SimpleChoiceList($organizationsChoices);
        
        $filterMapper
                ->add('project')
                ->add('organization', 'doctrine_orm_choice', array(
                    'label' => 'filter.label_donor',
                    'field_options'=> array(
                        'choice_list'  => $organizations,
                    ),
                    'field_type'=> 'choice',
                ))
                ->add('amount', 'doctrine_orm_number', array(), 'sonata_type_filter_number')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $organizationsQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Organization")->getList($this->container->get('request')->getLocale());
        $organizationsChoices = array();
        foreach($organizationsQuery as $organization)
            $organizationsChoices[$organization['id']] = $organization['name'];
        $organizations = new SimpleChoiceList($organizationsChoices);

        $projectsQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Project")->getList($this->container->get('request')->getLocale());
        $projectsChoices = array();
        foreach($projectsQuery as $project)
            $projectsChoices[$project['id']] = ($project['id'].' - '.$project['specificObjective']);
        $projects = new SimpleChoiceList($projectsChoices);
        
        $formMapper
                ->with('General')
                ->add('project', 'sonata_type_model', array(
                    'disabled' => true,
                    'choice_list' => $projects,
                    'required' => false,
                    'btn_add' => false,
                    'empty_value' => 'form.label_empty',
                    'attr' => array('class' => 'project-select required'),
                ))
                ->add('organization', 'sonata_type_model', array(
                    'label' => 'form.label_donor',
                    'required' => false,
                    'choice_list' => $organizations,
                    'btn_add' => 'form.project_donor.add',
                    'btn_catalogue' => 'SimbioticaAdmin',
                    'empty_value' => 'form.label_empty',
                    'translation_domain' => 'SimbioticaAdmin',
                    'label_attr' => array('class' => 'required'),
                ))
                ->add('amount', 'integer', array(
                    'required' => false,
                    'attr' => array('class' => 'amount'),
                    'label_attr' => array('class' => 'required'),
                    ))
                ->end()
        ;
        
        $builder = $formMapper->getFormBuilder();
        
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function(FormEvent $event) use ($formMapper){
                $form = $event->getForm();
                
                if($form->getParent() && $form->getParent()->getParent())
                {
                    if ($form->getParent()->getParent()->getConfig()->getDataClass() == 'Simbiotica\CalpBundle\Entity\Project')
                    {
                        $formMapper->remove('project');
                        $form->remove('project');
                    }
                    elseif ($form->getParent()->getParent()->getConfig()->getDataClass() == 'Simbiotica\CalpBundle\Entity\Organization')
                    {
                        $formMapper->remove('organization');
                        $form->remove('organization');
                    }
                }
            }
        );
        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function(FormEvent $event) use ($formMapper){
                $form = $event->getForm();
                
                if($form->getParent() && $form->getParent()->getParent())
                {
                    if ($form->getParent()->getParent()->getConfig()->getDataClass() == 'Simbiotica\CalpBundle\Entity\Project')
                    {
                        $formMapper->remove('project');
                        $form->remove('project');
                    }
                    elseif ($form->getParent()->getParent()->getConfig()->getDataClass() == 'Simbiotica\CalpBundle\Entity\Organization')
                    {
                        $formMapper->remove('organization');
                        $form->remove('organization');
                    }
                }
            }
        );
    }

}