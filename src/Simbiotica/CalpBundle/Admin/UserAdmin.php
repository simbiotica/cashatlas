<?php

namespace Simbiotica\CalpBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Sonata\UserBundle\Admin\Entity\UserAdmin as BaseAdmin;
use Sonata\AdminBundle\Route\RouteCollection;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\ChoiceList\SimpleChoiceList;

class UserAdmin extends BaseAdmin
{
    protected $maxPerPage = 10;
    
    protected $container;
    
    protected $formOptions = array(
        'validation_groups' => 'Profile'
    );
    
    public function __construct($code, $class, $baseControllerName, ContainerInterface $container)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->container = $container;
    }
    
    
    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
            )))
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('organizations', null, array('template' => 'SimbioticaCalpBundle:UserAdmin:list_organization.html.twig'))
            ->add('enabled', null, array('editable' => true))
            ->add('locked', null, array('editable' => true))
            ->add('pendingApproval')
        ;

        if ($this->isGranted('ROLE_ALLOWED_TO_SWITCH')) {
            $listMapper
                ->add('impersonating', 'string', array('template' => 'SonataUserBundle:Admin:Field/impersonating.html.twig'))
            ;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $organizationsQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Organization")->getList($this->container->get('request')->getLocale());
        $organizationsChoices = array();
        foreach($organizationsQuery as $org)
            $organizationsChoices[$org['id']] = $org['name'];
        $organizations = new SimpleChoiceList($organizationsChoices);
        
        $filterMapper
                ->add('firstname')
                ->add('lastname')
                ->add('email')
                ->add('organizations.organization', 'doctrine_orm_choice', array(
                    'field_options'=> array(
                        'choice_list'  => $organizations,
                    ),
                    'field_type'=> 'choice',
                ))
                ->add('otherOrganization')
                ->add('enabled')
                ->add('locked')
                ->add('pendingApproval')
            
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General')
                ->add('email')
            ->end()
            ->with('Profile')
                ->add('firstname')
                ->add('lastname')
            ->end()
            ->with('Groups')
                ->add('groups')
            ->end()
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        ->with('General')
            ->add('email')
            ->add('plainPassword', 'text', array('required' => false))
        ->end()
        ->with('Profile')
                ->add('firstname', null, array('required' => false))
                ->add('lastname', null, array('required' => false))
                ->add('country', 'country', array('required' => false))
                ->add('relevance', null, array('required' => false))
            ->end()
        ->with('Organizations and Projects')
                ->add('organizations', 'sonata_type_collection', array(
                    'cascade_validation' => true,
                    'required' => false,
                        ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'admin_code' => 'simbiotica.calp.admin.organization_user'
                ))
                ->add('otherOrganization', null, array('required' => false))
                ->add('position', null, array('required' => false))
                ->add('intent', 'choice', array('disabled' => true, 'required' => false, 'choices' => SimbioticaCalpBundle::getIntents()))
                ->add('ownedProjects', 'entity', array(
                    'class' => 'Simbiotica\CalpBundle\Entity\Project', 
                    'required' => false, 
                    'multiple' => true,
                    'by_reference' => false,
                ))
                ->add('editableProjects', 'entity', array(
                    'class' => 'Simbiotica\CalpBundle\Entity\Project', 
                    'required' => false, 
                    'multiple' => true,
                    'by_reference' => false,
                ))
                ->add('revisableProjects', 'entity', array(
                    'class' => 'Simbiotica\CalpBundle\Entity\Project', 
                    'required' => false, 
                    'multiple' => true,
                    'by_reference' => false,
                ))
            ->end()
        ->with('Groups')
            ->add('groups', 'sonata_type_model', array('required' => false, 'expanded' => true, 'multiple' => true))
        ->end()
        ;
        if (!$this->getSubject()->hasRole('ROLE_SUPER_ADMIN')) {
            $formMapper
                ->with('Management')
                    ->add('roles', 'simbiotica_security_roles', array(
                            'expanded' => true,
                            'multiple' => true,
                            'required' => false
                    ))
                    ->add('locked', null, array('required' => false))
                    ->add('enabled', null, array('required' => false))
                    ->add('pendingApproval', null, array('required' => false, 'disabled' => true))
                ->end()
            ;
        }
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('approve', $this->getRouterIdParameter() . '/approve');
        $collection->add('decline', $this->getRouterIdParameter() . '/decline');
    }
    
    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!in_array($action, array('edit', 'view'))) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;

        $user = $this->getUserManager()->findUserBy(array('id' => $this->getRequest()->get('id')));

        if ($user->getPendingApproval())
        {
            $menu->addChild(
                        $this->trans('user.approval.approve_admin', array(), 'SimbioticaAdmin'), 
                        array('uri' => $admin->generateUrl('approve', array('id' => $user->getId(), 'editor' => 1))));
            $menu->addChild(
                        $this->trans('user.approval.approve_user', array(), 'SimbioticaAdmin'), 
                        array('uri' => $admin->generateUrl('approve', array('id' => $user->getId(), 'editor' => 0))));
            $menu->addChild(
                        $this->trans('user.approval.decline', array(), 'SimbioticaAdmin'), 
                        array('uri' => $admin->generateUrl('decline', array('id' => $user->getId()))));
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function prePersist($user) {
        $user->setOrganizations($user->getOrganizations());
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($user) {
        $user->setOrganizations($user->getOrganizations());
    }
    
    /**
     * DOWN: Copy of SimbioticaCalpBundle/Admin/Admin.php content
     */
    
    public function isAclEnabled()
    {
        return false;
    }
    
    public function createQuery($context = 'list') 
    {
        if ($this->getConfigurationPool()->getContainer()->get('security.context')->isGranted('ROLE_SUPER_ADMIN'))
            return parent::createQuery($context);
        
        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser(); 
        $qb = $this->createObjectsByMaskQuery($user, $this->getClass()); 
        return new ProxyQuery($qb); 
    } 
    
    public function createObjectsByMaskQuery($user, $class , $mask = MaskBuilder::MASK_EDIT) 
    { 
        $rsm = new ResultSetMapping; 
        $rsm->addScalarResult('object_identifier', 'object_identifier'); 

        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
        
        $roleHierachy = new RoleHierarchy($this->getConfigurationPool()->getContainer()->getParameter('security.role_hierarchy.roles'));
        $userRoles = $roleHierachy->getReachableRoles($securityContext->getToken()->getRoles());
        $userRoleNames = array_map(function ($role) {return $role->getRole();}, $userRoles);

        $em = $this->getModelManager($class)->getEntityManager($class); 

        $queryString = ' 
            SELECT oid.object_identifier 
        FROM 
            acl_security_identities sid 
        JOIN 
            acl_entries e ON ( 
            e.security_identity_id = sid.id 
        ) 
        JOIN acl_object_identities oid ON (e.class_id = oid.class_id 
            AND e.object_identity_id IS NOT NULL AND 
            e.object_identity_id = oid.id) 
        JOIN acl_classes c ON oid.class_id = c.id 
        WHERE 
            c.class_type LIKE :class AND 
            e.mask >= :mask';
        
        $sidList = array_merge(array(addslashes(get_class($user)).'-'.$user->getUsername()), $userRoleNames);
        $sidQuerySection = array();
        foreach($sidList as $index => $role)
        {
            $sidQuerySection[] = 'sid.identifier like (:param'.$index.')';
        }
        if ($role)
            $queryString .= ' AND ('.implode(' OR ', $sidQuerySection).')';
        
        $query = $em->createNativeQuery($queryString, $rsm); 

        $query->setParameter('class', addslashes($class)); 
        $query->setParameter('mask', $mask); 
        foreach($sidList as $index => $role)
        {
           $query->setParameter('param'.$index, $role);
        }

        $result = array(); 
        foreach ($query->getResult() as $i => $row) 
        { 
            $result[] = $row['object_identifier']; 
        } 

        if(!count($result)) { 
            $result = array(); 
            $result[] = 0; 
        } 

        $qb = $em->getRepository($class)->createQueryBuilder('c'); 
        $qb->where($qb->expr()->in('c.id',$result)); 

        return $qb;
    }
    
    public function getExportFormats()
    {
        return array('xls','csv'
        );
    }
}
