<?php

namespace Simbiotica\CalpBundle\Admin;

use Sonata\AdminBundle\Admin\Admin as BaseAdmin;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Symfony\Component\Security\Core\Role\RoleHierarchy;

class Admin extends BaseAdmin
{
    protected $maxPerPage = 10;
    
    public function isAclEnabled()
    {
        return false;
    }
    
    public function createQuery($context = 'list') 
    {
        if ($this->getConfigurationPool()->getContainer()->get('security.context')->isGranted('ROLE_SUPER_ADMIN'))
            return parent::createQuery($context);
        
        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser(); 
        $qb = $this->createObjectsByMaskQuery($user, $this->getClass()); 
        return new ProxyQuery($qb); 
    }
    
    public function createObjectsByMaskQuery($user, $class , $mask = MaskBuilder::MASK_VIEW) 
    { 
        $rsm = new ResultSetMapping; 
        $rsm->addScalarResult('object_identifier', 'object_identifier'); 

        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
        
        $roleHierachy = new RoleHierarchy($this->getConfigurationPool()->getContainer()->getParameter('security.role_hierarchy.roles'));
        $userRoles = $roleHierachy->getReachableRoles($securityContext->getToken()->getRoles());
        $userRoleNames = array_map(function ($role) {return $role->getRole();}, $userRoles);

        $em = $this->getModelManager($class)->getEntityManager($class); 

        $queryString = ' 
            SELECT oid.object_identifier 
        FROM 
            acl_security_identities sid 
        JOIN 
            acl_entries e ON ( 
            e.security_identity_id = sid.id 
        ) 
        JOIN acl_object_identities oid ON (e.class_id = oid.class_id 
            AND e.object_identity_id IS NOT NULL AND 
            e.object_identity_id = oid.id) 
        JOIN acl_classes c ON oid.class_id = c.id 
        WHERE 
            c.class_type LIKE :class AND 
            e.mask >= :mask';
        
        $sidList = array_merge(array(addslashes(get_class($user)).'-'.$user->getUsername()), $userRoleNames);
        $sidQuerySection = array();
        foreach($sidList as $index => $role)
        {
            $sidQuerySection[] = 'sid.identifier like (:param'.$index.')';
        }
        if ($role)
            $queryString .= ' AND ('.implode(' OR ', $sidQuerySection).')';
        
        $query = $em->createNativeQuery($queryString, $rsm); 

        $query->setParameter('class', addslashes($class)); 
        $query->setParameter('mask', $mask); 
        foreach($sidList as $index => $role)
        {
           $query->setParameter('param'.$index, $role);
        }

        $result = array(); 
        foreach ($query->getResult() as $i => $row) 
        { 
            $result[] = $row['object_identifier']; 
        } 

        if(!count($result)) { 
            $result = array(); 
            $result[] = 0; 
        } 

        $qb = $em->getRepository($class)->createQueryBuilder('c'); 
        $qb->where($qb->expr()->in('c.id',$result)); 

        return $qb;
    }
    
    public function getExportFormats()
    {
        return array(
        );
    }
}