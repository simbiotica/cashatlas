<?php

namespace Simbiotica\CalpBundle\Admin;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Symfony\Component\Form\Extension\Core\ChoiceList\SimpleChoiceList;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ProjectLocationAdmin extends Admin {
    
    protected $container;
    
    public function __construct($code, $class, $baseControllerName, ContainerInterface $container) {
        parent::__construct($code, $class, $baseControllerName);
    
        $this->container = $container;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                )))
                ->add('project')
                ->add('country')
                ->add('region')
                ->add('scale')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper) {
        $countriesQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Country")->getList($this->container->get('request')->getLocale());
        $countriesChoices = array();
        foreach($countriesQuery as $country)
            $countriesChoices[$country['id']] = $country['name'];
        $countries = new SimpleChoiceList($countriesChoices);
        
        $regionsQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Region")->getList($this->container->get('request')->getLocale());
        $regionsChoices = array();
        foreach($regionsQuery as $region)
            $regionsChoices[$region['id']] = $region['name'];
        $regions = new SimpleChoiceList($regionsChoices);
        
        $filterMapper
                ->add('project')
                ->add('country', 'doctrine_orm_choice', array(
                    'field_options'=> array(
                        'choice_list'  => $countries,
                    ),
                    'field_type'=> 'choice',
                ))
                ->add('region', 'doctrine_orm_choice', array(
                    'field_options'=> array(
                        'choice_list'  => $regions,
                    ),
                    'field_type'=> 'choice',
                ))
                ->add('scale', 'doctrine_orm_number', array(), 'sonata_type_filter_number')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {
//        $locale = $this->container->get('request')->getLocale();
        
        $countriesQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Country")->getList($this->container->get('request')->getLocale());
        $countriesChoices = array();
        foreach($countriesQuery as $country)
            $countriesChoices[$country['id']] = $country['name'];
        $countries = new SimpleChoiceList($countriesChoices);
        
        $formMapper
            ->with('General')
            ->add('country', 'sonata_type_model', array(
                'required' => false, 
                'attr' => array('class' => 'country-select'),
                'label_attr' => array('class' => 'required'),
                'class' => 'Simbiotica\CalpBundle\Entity\Country',
                'btn_add' => false,
                'choice_list' => $countries,
                'empty_value' => 'form.label_empty',
                'translation_domain' => 'SimbioticaAdmin',
                /**
                 * https://github.com/symfony/symfony/pull/8378
                 */
//                'query_builder' => function(EntityRepository $er) {
//                        $qb = $er->createQueryBuilder('c')
//                                ->orderBy('c.name', 'ASC')
//                                ;
//                        return $qb;
//                        },
//                'query_hints' => array(
//                    \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER => 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker',
//                    \Gedmo\Translatable\TranslatableListener::HINT_FALLBACK => 1,
//                    \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE => $locale,
//                ),
            ))
            ->add('region', 'sonata_type_model', array(
                'required' => false, 
                'attr' => array('class' => 'region-select'),
                'label_attr' => array('class' => 'required'),
                'class' => 'Simbiotica\CalpBundle\Entity\Region',
                'choices' => array(),
                'empty_value' => 'form.label_empty',
                'translation_domain' => 'SimbioticaAdmin',
                'disabled' => true,
            ))
            ->add('scale', 'integer', array(
                'required' => false,
                'label' => 'form.label_location_scale',
                'attr' => array('class' => 'scale'),
                'label_attr' => array('class' => 'required'),
            ))
            ->add('city', 'text', array(
                'required' => false,
            ))
            ->end()
        ;
        
        $builder = $formMapper->getFormBuilder();
        $container = $this->container;
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function(FormEvent $event) use ($formMapper, $container){
                $form = $event->getForm();
                $data = $event->getData();
                
                $country = ($data && $data->getCountry()?$data->getCountry()->getId():null);
                
                $regionsQuery = $container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Region")->getListByCountry($container->get('request')->getLocale(), $country);
                $regionsChoices = array();
                foreach($regionsQuery as $region)
                    $regionsChoices[$region['id']] = $region['name'];
                $regions = new SimpleChoiceList($regionsChoices);
                
                $form->add($formMapper->add('region', 'sonata_type_model', array(
                    'required' => false, 
                    'attr' => array('class' => 'region-select'),
                    'label_attr' => array('class' => 'required'),
                    'class' => 'Simbiotica\CalpBundle\Entity\Region',
                    'empty_value' => 'form.label_empty',
                    'translation_domain' => 'SimbioticaAdmin',
                    'auto_initialize' => false,
                    'disabled' => !$country,
                    'btn_add' => false,
                    'choice_list' => $regions,
                ))->get('region')->getForm());
            }
        );
        
        
        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function(FormEvent $event) use ($formMapper, $container){
                $form = $event->getForm();
                $data = $event->getData();
                
                $country = ($data && !empty($data['country'])?$data['country']:null);
                
                $regionsQuery = $container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Region")->getListByCountry($container->get('request')->getLocale(), $country);
                $regionsChoices = array();
                foreach($regionsQuery as $region)
                    $regionsChoices[$region['id']] = $region['name'];
                $regions = new SimpleChoiceList($regionsChoices);
                
                $form->add($formMapper->add('region', 'sonata_type_model', array(
                    'required' => false, 
                    'class' => 'Simbiotica\CalpBundle\Entity\Region',
                    'label_attr' => array('class' => 'required'),
                    'empty_value' => 'form.label_empty',
                    'translation_domain' => 'SimbioticaAdmin',
                    'auto_initialize' => false,
                    'disabled' => !$country,
                    'btn_add' => false,
                    'choice_list' => $regions,
                    ))->get('region')->getForm());
                
            }
        );
    }

}