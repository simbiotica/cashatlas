<?php

namespace Simbiotica\CalpBundle\Admin;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Simbiotica\CalpBundle\Entity\ProjectOrganization;
use Simbiotica\CalpBundle\Entity\ProjectDonor;
use Simbiotica\CalpBundle\Entity\ProjectLocation;
use Simbiotica\CalpBundle\Entity\Modality;
use Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism;
use Sonata\MediaBundle\Admin\GalleryAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\MediaBundle\Provider\Pool;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Symfony\Component\Form\Extension\Core\ChoiceList\SimpleChoiceList;
use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Sonata\AdminBundle\Route\RouteCollection;

class ProjectAdmin extends GalleryAdmin {

    protected $maxPerPage = 10;
    protected $container;
    protected $supportsPreviewMode = false;
    protected $translationDomain = 'SimbioticaAdmin';

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     * @param \Sonata\MediaBundle\Provider\Pool $pool
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct($code, $class, $baseControllerName, Pool $pool, ContainerInterface $container) {
        parent::__construct($code, $class, $baseControllerName, $pool);

        $this->container = $container;
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('changeStatus', $this->getRouterIdParameter() . '/change_status');
        $collection->add('notification', $this->getRouterIdParameter() . '/notification');
        $collection->add('similarProjects', $this->getRouterIdParameter(). '/similar_projects');
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
//                        'view' => array(),
                        'edit' => array(),
                )))
                ->add('countriesList', 'string', array('template' => 'SimbioticaCalpBundle:ProjectAdmin:list_country.html.twig'))
                ->add('regionsList', 'string', array('template' => 'SimbioticaCalpBundle:ProjectAdmin:list_region.html.twig'))
                ->add('organizationsList', 'string', array('template' => 'SimbioticaCalpBundle:ProjectAdmin:list_organization.html.twig'))
                ->add('donorsList', 'string', array('template' => 'SimbioticaCalpBundle:ProjectAdmin:list_donor.html.twig'))
                ->add('scale')
                ->add('publishedString', 'trans', array('catalogue' => 'SimbioticaAdmin'))
                ->add('statusString', 'trans', array('catalogue' => 'SimbioticaAdmin'))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper) {
        $countriesQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Country")->getList($this->container->get('request')->getLocale());
        $countriesChoices = array();
        foreach($countriesQuery as $country)
            $countriesChoices[$country['id']] = $country['name'];
        $countries = new SimpleChoiceList($countriesChoices);
        
        $organizationsQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Organization")->getList($this->container->get('request')->getLocale());
        $organizationsChoices = array();
        foreach($organizationsQuery as $org)
            $organizationsChoices[$org['id']] = $org['name'];
        $organizations = new SimpleChoiceList($organizationsChoices);
        
        $filterMapper
                ->add('specificObjective')
                ->add('locations.country', 'doctrine_orm_choice', array(
                    'field_options'=> array(
                        'choice_list'  => $countries,
                    ),
                    'field_type'=> 'choice',
                ))
                ->add('organizations.organization', 'doctrine_orm_choice', array(
                    'field_options'=> array(
                        'choice_list'  => $organizations,
                    ),
                    'field_type'=> 'choice',
                ))
                ->add('donors.organization', 'doctrine_orm_choice', array(
                    'field_options'=> array(
                        'choice_list'  => $organizations,
                    ),
                    'field_type'=> 'choice',
                ))
                ->add('published', 'doctrine_orm_choice', array(), 'choice' , array('translation_domain' => 'SimbioticaAdmin', 'choices' => SimbioticaCalpBundle::getPublishedStatus()))
                ->add('status', 'doctrine_orm_choice', array(), 'choice' , array('translation_domain' => 'SimbioticaAdmin', 'choices' => SimbioticaCalpBundle::getValidationStatus()))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $project = $this->getSubject();

        if (!$project) {
            $project = $this->getNewInstance();
        }

        $context = $this->getPersistentParameter('context');
        $language = $this->getPersistentParameter('language');
        $allowOwnerChange = $this->isGranted('OWNER', $this->getSubject()) || $this->isGranted('ROLE_SUPER_ADMIN');

        if (!$context) {
            $context = $this->pool->getDefaultContext();
        }

        $formats = array();
        foreach ((array) $this->pool->getFormatNamesByContext($context) as $name => $options) {
            $formats[$name] = $name;
        }

        $contexts = array();
        foreach ((array) $this->pool->getContexts() as $contextItem => $format) {
            $contexts[$contextItem] = $contextItem;
        }
        
        $formMapper
                ->with('project_admin.form.background_information')
                ->add('language', 'translated_text', array(
                    'required' => false,
                    'disabled' => true, 
                    'translation_domain' => 'SimbioticaAdmin',
                    ))
                ->add('currency', 'text', array('disabled' => true))
                ->add('organizations', 'sonata_type_collection', array(
                    'cascade_validation' => true,
                    'required' => false,
                    'translation_domain' => 'SimbioticaAdmin',
                    'attr' => array('class' => 'organizations'),
                    'label_attr' => array('class' => 'required'),
                    'btn_add' => 'form.project.add_line',
                    'type_options' => array('delete' => false, 'attr' => array('class'=> 'drum')),
                    ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                    'admin_code' => 'simbiotica.calp.admin.project_organization',
                    'limit' => 1,
                    'help' => 'form.project.help_organizations',
                       
                ))
                ->add('affiliates', 'text', array('required' => false), array(
                    'help' => 'form.project.help_affiliates',
                ))
                ->add('implementingPartners', 'text', array('required' => false), array('help' => 'form.project.help_implementing_partners',))
                ->add('locations', 'sonata_type_collection', array(
                    'required' => false,
                    'cascade_validation' => true,
                    'btn_add' => 'form.project.add_line',
                    'btn_catalogue' => 'SimbioticaAdmin',
                    'attr' => array('class' => 'locations'),
                    'label_attr' => array('class' => 'required'),
                        ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                    'admin_code' => 'simbiotica.calp.admin.project_location',
                    'help' => 'form.project.help_locations',
                ));
        if($project->getCity())
                $formMapper->add('city', 'textarea', array(
                    'required' => false,
                    'disabled' => true,
                ));
        $formMapper
                ->add('scale', 'integer', array('required' => false, 'disabled' => true, 'attr' => array('class' => 'scale-sum')));
        if($project->getInitialScale())
                $formMapper->add('initialScale', 'integer', array('required' => false, 'disabled' => true,));
        $formMapper
                ->add('specificObjective', null, array(
                    'required' => false,
                    'label_attr' => array('class' => 'required'),
                    ), array('help' => 'form.project.help_specific_objective'))
                ->add('description', null, array(
                    'required' => false
                ), array(
                    'help' => 'form.project.help_description'
                ))
                ->add('sectors', 'sector', array(
                    'required' => false,
                    'attr' => array('class' => 'sectors'), 
                    'label_attr' => array('class' => 'required'),
                    'multiple' => true,
                    'translation_domain' => 'SimbioticaAdmin',
                    'data_class' => 'Simbiotica\CalpBundle\Entity\Project',
                    ), array(
                    'help' => 'form.project.help_sectors',
                ))
                ->add('donors', 'sonata_type_collection', array(
                    'required' => false,
                    'cascade_validation' => true,
                    'btn_add' => 'form.project.add_line',
                    'btn_catalogue' => 'SimbioticaAdmin',
                    'label_attr' => array('class' => 'required'),
                        ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'help' => 'form.project.help_donors',
                ))
                ->add('budgetOverall', 'integer', array(
                    'required' => false, 
                    'disabled' => true, 
                    'attr' => array('class' => 'amount-sum')
                ), array(
                    'help' => 'form.project.help_budget_overall'
                ));
        if($project->getInitialBudgetOverall())
                $formMapper->add('initialBudgetOverall', 'integer', array('required' => false, 'disabled' => true));
        $formMapper
                ->add('projectContext', 'choice', array('required' => false, 'translation_domain' => 'SimbioticaAdmin', 'choices' => SimbioticaCalpBundle::getContexts(), 'multiple' => true))
                ->add('startingDate', 'partial_date', array(
                    'required' => false, 
                    'attr' => array('class' => 'date-selects'),
                    'label_attr' => array('class' => 'required'),
                    ))
                ->add('firstCashDate', 'partial_date', array(
                    'required' => false, 
                    'attr' => array('class' => 'date-selects'),
                    'label_attr' => array('class' => 'required'),
                    ))
                ->add('endingDate', 'partial_date', array(
                    'required' => false, 
                    'attr' => array('class' => 'date-selects'),
                    'label_attr' => array('class' => 'required'),
                    ))
                ->add('duration', 'integer', array(
                    'required' => false, 
                    'disabled' => true, 
                    'attr' => array('class' => 'duration-sum')
                ))
                ->with('project_admin.form.analysis_information')
                ->add('marketAssessment', 'choice_plus_other', array(
                    'choices' => SimbioticaCalpBundle::getMarketAsssessmentTypes(), 
                    'required' => false, 
                    'invert' => true, 
                    'translation_domain' => 'SimbioticaAdmin',
                    'empty_value' => 'form.label_empty',
                    'attr' => array('class' => 'marker_assessment'),
                ), array(
                    'help' => 'form.project.help_market_assessment'
                ))
                ->add('coordinationMechanisms', 'sonata_type_collection', array(
                    'cascade_validation' => true,
                    'required' => false, 
                    'attr' => array('class' => 'coordinationMechanisms'),
                    'btn_add' => 'form.project.add_line',
                    'btn_catalogue' => 'SimbioticaAdmin',
                        ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                    'admin_code' => 'simbiotica.calp.admin.project_coordination_mechanism',
                    'help' => 'form.project.help_coordination_mechanisms',
                ))
                ->with('project_admin.form.technical_information')
                ->add('modalities', 'sonata_type_collection', array(
                    'cascade_validation' => true,
                    'required' => false, 
                    'btn_add' => 'form.project.add_line',
                    'btn_catalogue' => 'SimbioticaAdmin',
                    'attr' => array('class' => 'modalities'),
                    'label_attr' => array('class' => 'required'),
                        ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                    'admin_code' => 'simbiotica.calp.admin.modality',
                    'help' => 'form.project.help_modalities',
                ))
                ->add('budgetAllocated', 'integer', array(
                    'required' => false, 
                    'disabled' => true, 
                    'attr' => array('class' => 'modality-sum')
                ), array(
                    'help' => 'form.project.help_budget_allocated'
                ));
        if($project->getInitialBudgetAllocated())
                $formMapper->add('initialBudgetAllocated', 'integer', array('required' => false, 'disabled' => true));
        $formMapper
                ->add('typeOfIntervention', 'choice', array(
                    'choices' => SimbioticaCalpBundle::getInterventionTypes(), 
                    'required' => false, 
                    'attr' => array('class' => 'type_of_intervention'),
                    'empty_value' => 'form.label_empty',
                    'translation_domain' => 'SimbioticaAdmin'), array(
                        'help' => 'form.project.help_type_of_intervention',
                ))
                ->add('typeOfInterventionSpecify', null, array('required' => false, 'attr' => array('class' => 'type_of_intervention_specify')))
                ->with('project_admin.form.general_information')
                ->add('contactPersonName', 'text', array('required' => false), array(
                    'help' => 'form.project.help_contact_person_name',
                ))
                ->add('contactPersonPosition', 'text', array('required' => false), array(
                    'help' => 'form.project.help_contact_person_position',
                ))
                ->add('contactPersonEmailOne', 'text', array('required' => false), array(
                    'help' => 'form.project.help_contact_person_email_one',
                ))
                ->add('contactPersonEmailTwo', 'text', array('required' => false), array(
                    'help' => 'form.project.help_contact_person_email_two',
                ))
                ->add('contactOrganizationUrl', 'text', array('required' => false), array(
                    'help' => 'form.project.help_contact_organization_url',
                ))
                ->add('galleryHasMedias', 'sonata_type_collection', array('required' => false, 
                    'cascade_validation' => true,
                    'btn_add' => 'form.project.add_line',
                    'btn_catalogue' => 'SimbioticaAdmin',
                        ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                    'link_parameters' => array('context' => $context, 'language' => $language),
                    'admin_code' => 'simbiotica.calp.admin.project_media',
                    'help' => 'form.project.help_media',
                ))
                ->add('fields', 'sonata_type_collection', array(
                    'cascade_validation' => true,
                    'required'=> false,
                    'btn_add' => 'form.project.add_line',
                    'btn_catalogue' => 'SimbioticaAdmin',
                        ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                    'admin_code' => 'simbiotica.calp.admin.project_field',
                    'help' => 'form.project.help_fields',
                ))
                ->add('owners', 'entity', array(
                        'class' => 'Simbiotica\CalpBundle\Entity\User', 
                        'multiple' => true, 
                        'required' => false, 
                        'disabled' => !$allowOwnerChange,
                        'query_builder' => function(EntityRepository $er) {
                                return $er->createQueryBuilder('u')
                                    ->orderBy('u.firstname, u.lastname')
                                    ;
                            },
                        ), 
                    array(
                        'help' => 'form.project.help_owners',
                ))
                
                ->add('editors', 'entity', array(
                    'class' => 'Simbiotica\CalpBundle\Entity\User', 
                    'multiple' => true, 
                    'required' => false, 
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->orderBy('u.firstname, u.lastname')
                            ;
                    },
                ), array(
                    'help' => 'form.project.help_editors',
                ))
                ->add('revisors', 'entity', array(
                    'class' => 'Simbiotica\CalpBundle\Entity\User', 
                    'multiple' => true, 
                    'required' => false, 
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->orderBy('u.firstname, u.lastname')
                            ;
                    },
                ), array(
                    'help' => 'form.project.help_revisors',
                ))
                ->add('revisionNotes', null, array(
                    'required' => false
                ), array(
                    'help' => 'form.project.help_revision_notes'
                ))
                ->end();
    }
    
    protected function HIDDENconfigureShowFields(ShowMapper $showMapper) {
        $showMapper
                ->with('project_admin.form.background_information')
                ->add('language', null, array('label' => 'list.label_language'))
                ->add('organizations', null, array('label' => 'list.label_organizations'))
                ->add('affiliates', null, array('label' => 'list.label_affiliates'))
                ->add('implementingPartners', null, array('label' => 'list.label_implementing_partners'))
                ->add('locations', null, array('label' => 'list.label_locations'))
                ->add('city', null, array('label' => 'list.label_city'))
                ->add('scale', null, array('label' => 'list.label_scale'))
                ->add('initialScale', null, array('label' => 'list.label_initial_scale'))
                ->add('specificObjective', null, array('label' => 'list.label_specific_objective'))
                ->add('description', null, array('label' => 'list.label_description'))
                ->add('sectors', null, array('label' => 'list.label_sectors'))
                ->add('donors', null, array('label' => 'list.label_donors'))
                ->add('budgetOverall', null, array('label' => 'list.label_budget_overall'))
                ->add('initialBudgetOverall', null, array('label' => 'list.label_initial_budget_overall'))
                ->add('projectContext', null, array('label' => 'list.label_project_context'))
                ->add('startingDate', null, array('label' => 'list.label_starting_date'))
                ->add('firstCashDate', null, array('label' => 'list.label_first_cash_date'))
                ->add('endingDate', null, array('label' => 'list.label_ending_date'))
                ->add('duration', null, array('label' => 'list.label_duration'))
                ->with('project_admin.form.analysis_information')
                ->add('marketAssessment', null, array('label' => 'list.label_market_assessment'))
                ->add('coordinationMechanisms', null, array('label' => 'list.label_coordination_mechanisms'))
                ->with('project_admin.form.technical_information')
                ->add('modalities', null, array('label' => 'list.label_modalities'))
                ->add('budgetAllocated', null, array('label' => 'list.label_budget_allocated'))
                ->add('typeOfIntervention', null, array('label' => 'list.label_type_of_intervention'))
                ->add('typeOfInterventionSpecify', null, array('label' => 'list.label_type_of_intervention_specify'))
                ->with('project_admin.form.general_information')
                ->add('contactPersonName', null, array('label' => 'list.label_contact_person_name'))
                ->add('contactPersonPosition', null, array('label' => 'list.label_contact_person_position'))
                ->add('contactPersonEmailOne', null, array('label' => 'list.label_contact_person_email_one'))
                ->add('contactPersonEmailTwo', null, array('label' => 'list.label_contact_person_email_two'))
                ->add('contactOrganizationUrl', null, array('label' => 'list.label_contact_organization_url'))
                ->add('galleryHasMedias', null, array('label' => 'list.label_gallery_has_medias'))
                ->add('fields', null, array('label' => 'list.label_fields'))
                ->add('owners', null, array('label' => 'list.label_owners'))
                ->add('editors', null, array('label' => 'list.label_editors'))
                ->add('revisors', null, array('label' => 'list.label_revisors'))
                ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($project) {
        parent::prePersist($project);
        $this->preSave($project);
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($project) {
        parent::preUpdate($project);
        $this->preSave($project);
    }
    
    private function preSave($project) {
        $project->setOrganizations($project->getOrganizations());
        $project->setDonors($project->getDonors());
        $project->setCoordinationMechanisms($project->getCoordinationMechanisms());
        $project->setModalities($project->getModalities());
        $project->setFields($project->getFields());
        $project->setLocations($project->getLocations());
    }

    public function getNewInstance() {
        $project = parent::getNewInstance();
        $language = $this->getRequest()->get('language');
        $currency = $this->getRequest()->get('currency');
        $project->addOrganization(new ProjectOrganization());
        $project->addDonor(new ProjectDonor());
        $project->addLocation(new ProjectLocation());
        $project->addModality(new Modality());
        $project->addCoordinationMechanism(new ProjectCoordinationMechanism());


        if ($this->hasRequest() && !empty($language)) {
            $languageList = array_flip(SimbioticaCalpBundle::getLanguages());
            $project->setLanguage($languageList[$language]);
            $project->setCurrency($currency);
        }
        
        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $project->addOwner($user);
        $project->setPublished(SimbioticaCalpBundle::PUBLISHED_UNPUBLISHED);

        return $project;
    }

    public function getPersistentParameters() {
        if (!$this->hasRequest()) {
            return array();
        }

        $language = $this->getRequest()->get('language');
        $currency = $this->getRequest()->get('currency');

        return array(
            'language' => $language,
            'context' => 'project',
            'currency' => $currency
        );
    }
    
    public function getBatchActions() {
        // retrieve the default (currently only the delete action) actions
        $actions = parent::getBatchActions();

        // check user permissions
        if ($this->isGranted('VIEW')) {
            $actions['compare'] = array(
                /** @Ignore */
                'label' => $this->trans('action_compare', array(), 'SimbioticaAdmin'),
                'ask_confirmation' => false
            );
        }

        if ($this->isGranted('EDIT')) {
            $actions['publish'] = array(
                /** @Ignore */
                'label' => $this->trans('action_publish', array(), 'SimbioticaAdmin'),
                'ask_confirmation' => true
            );
        }

        if($this->isGranted('ROLE_SUPER_ADMIN')) {
            $actions['notification'] = array(
                'label' => $this->trans('action_notification', array(), 'SimbioticaAdmin'),
                'ask_confirmation' => true
            );
        }
        
        return $actions;
    }

    public function getExportFormats()
    {
        return array('xls');
    }

    public function isAclEnabled()
    {
        return false;
    }

    public function createQuery($context = 'list') 
    {
        if ($this->getConfigurationPool()->getContainer()->get('security.context')->isGranted('ROLE_SUPER_ADMIN'))
            return parent::createQuery($context);
        
        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser(); 
        $qb = $this->createObjectsByMaskQuery($user, $this->getClass()); 
        return new ProxyQuery($qb); 
    }
    
    public function createObjectsByMaskQuery($user, $class , $mask = MaskBuilder::MASK_EDIT) 
    { 
        $rsm = new ResultSetMapping; 
        $rsm->addScalarResult('object_identifier', 'object_identifier'); 

        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.context');
        
        $roleHierachy = new RoleHierarchy($this->getConfigurationPool()->getContainer()->getParameter('security.role_hierarchy.roles'));
        $userRoles = $roleHierachy->getReachableRoles($securityContext->getToken()->getRoles());
        $userRoleNames = array_map(function ($role) {return $role->getRole();}, $userRoles);

        $em = $this->getModelManager($class)->getEntityManager($class); 

        $queryString = ' 
            SELECT oid.object_identifier 
        FROM 
            acl_security_identities sid 
        JOIN 
            acl_entries e ON ( 
            e.security_identity_id = sid.id 
        ) 
        JOIN acl_object_identities oid ON (e.class_id = oid.class_id 
            AND e.object_identity_id IS NOT NULL AND 
            e.object_identity_id = oid.id) 
        JOIN acl_classes c ON oid.class_id = c.id 
        WHERE 
            c.class_type LIKE :class AND 
            e.mask >= :mask';
        
        $sidList = array_merge(array(addslashes(get_class($user)).'-'.$user->getUsername()), $userRoleNames);
        $sidQuerySection = array();
        foreach($sidList as $index => $role)
        {
            $sidQuerySection[] = 'sid.identifier like (:param'.$index.')';
        }
        if ($role)
            $queryString .= ' AND ('.implode(' OR ', $sidQuerySection).')';
        
        $query = $em->createNativeQuery($queryString, $rsm); 

        $query->setParameter('class', addslashes($class)); 
        $query->setParameter('mask', $mask); 
        foreach($sidList as $index => $role)
        {
           $query->setParameter('param'.$index, $role);
        }

        $result = array(); 
        foreach ($query->getResult() as $i => $row) 
        { 
            $result[] = $row['object_identifier']; 
        } 

        if(!count($result)) { 
            $result = array(); 
            $result[] = 0; 
        } 

        $qb = $em->getRepository($class)->createQueryBuilder('c'); 
        $qb->where($qb->expr()->in('c.id',$result)); 

        return $qb;
    }
}