<?php

namespace Simbiotica\CalpBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

class CoordinationMechanismAdmin extends Admin {

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                )))
                ->add('name')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper) {
        $filterMapper
                ->add('name')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('General')
                ->add('name')
                ->add('translations', 'a2lix_translations_gedmo', array(
                    'translatable_class' => "Simbiotica\CalpBundle\Entity\CoordinationMechanism",
                 ))
                ->end()
        ;
    }

}