<?php

namespace Simbiotica\CalpBundle\Admin;

use Sonata\AdminBundle\Admin\Admin as BaseAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Symfony\Component\Form\Extension\Core\ChoiceList\SimpleChoiceList;

class RegionAdmin extends BaseAdmin {

    protected $container;
    protected $maxPerPage = 10;

    public function __construct($code, $class, $baseControllerName, ContainerInterface $container) {
        parent::__construct($code, $class, $baseControllerName);

        $this->container = $container;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                )))
                ->add('name')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filterMapper) {
        $countriesQuery = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Country")->getList($this->container->get('request')->getLocale());
        $countriesChoices = array();
        foreach($countriesQuery as $country)
            $countriesChoices[$country['id']] = $country['name'];
        $countries = new SimpleChoiceList($countriesChoices);
        
        $filterMapper
                ->add('name')
                ->add('country', 'doctrine_orm_choice', array(
                    'field_options'=> array(
                        'choice_list'  => $countries,
                    ),
                    'field_type'=> 'choice',
                ))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('General')
                ->add('name', null, array('required' => false))
                ->add('country', 'entity', array(
                    'required' => false, 
                    'class' => 'Simbiotica\CalpBundle\Entity\Country',
                    'empty_value' => 'form.label_empty',
                    'translation_domain' => 'SimbioticaAdmin',
                ))
                ->add('translations', 'a2lix_translations_gedmo', array(
                    'translatable_class' => "Simbiotica\CalpBundle\Entity\Region",
                 ))
                ->end()
        ;
        if ($this->container->get('security.context')->getToken()->getUser()->hasRole('ROLE_SUPER_ADMIN')) {
            $formMapper
                ->with('Management')
                    ->add('centroid', 'text', array('required' => false))
                ->end()
            ;
        }
    }

    public function getExportFormats()
    {
        return array(
        );
    }
}