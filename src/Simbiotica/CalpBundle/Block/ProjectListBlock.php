<?php

namespace Simbiotica\CalpBundle\Block;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Security\Core\Role\RoleHierarchy;

class ProjectListBlock extends BaseBlockService {
    
    private $container;
    
    public function __construct($name, EngineInterface $templating, ContainerInterface $container)
    {
        parent::__construct($name, $templating);

        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        $qb = $this->createObjectsByMaskQuery($user, 'Simbiotica\CalpBundle\Entity\Project');
        $projects = $qb->getQuery()->getResult();
        
        return $this->renderResponse('SimbioticaCalpBundle:Block:project_list.html.twig', array(
            'projects'     => $projects,
            'settings'     => $blockContext->getSettings(),
            'block'        => $blockContext->getBlock()
        ), $response);
    }

    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {
        // TODO: Implement validateBlock() method.
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Projects List';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
        ));

        $resolver->setAllowedTypes(array(
        ));
    }
    
    private function createObjectsByMaskQuery($user, $class , $mask = MaskBuilder::MASK_EDIT) 
    { 
        $rsm = new ResultSetMapping; 
        $rsm->addScalarResult('object_identifier', 'object_identifier'); 

        $em = $this->container->get('doctrine.orm.entity_manager');
        $securityContext = $this->container->get('security.context');
        
        $roleHierachy = new RoleHierarchy($this->container->getParameter('security.role_hierarchy.roles'));
        $userRoles = $roleHierachy->getReachableRoles($securityContext->getToken()->getRoles());
        $userRoleNames = array_map(function ($role) {return $role->getRole();}, $userRoles);

        $queryString = ' 
            SELECT oid.object_identifier 
        FROM 
            acl_security_identities sid 
        JOIN 
            acl_entries e ON ( 
            e.security_identity_id = sid.id 
        ) 
        JOIN acl_object_identities oid ON (e.class_id = oid.class_id 
            AND e.object_identity_id IS NOT NULL AND 
            e.object_identity_id = oid.id) 
        JOIN acl_classes c ON oid.class_id = c.id 
        WHERE 
            c.class_type LIKE :class AND 
            e.mask >= :mask';
        
        $sidList = array_merge(array(addslashes(get_class($user)).'-'.$user->getUsername()), $userRoleNames);
        $sidQuerySection = array();
        foreach($sidList as $index => $role)
        {
            $sidQuerySection[] = 'sid.identifier like (:param'.$index.')';
        }
        if ($role)
            $queryString .= ' AND ('.implode(' OR ', $sidQuerySection).')';
        
        $query = $em->createNativeQuery($queryString, $rsm); 

        $query->setParameter('class', addslashes($class)); 
        $query->setParameter('mask', $mask); 
        foreach($sidList as $index => $role)
        {
           $query->setParameter('param'.$index, $role);
        }

        $result = array(); 
        foreach ($query->getResult() as $i => $row) 
        { 
            $result[] = $row['object_identifier']; 
        } 

        if(!count($result)) { 
            $result = array(); 
            $result[] = 0; 
        } 

        $qb = $em->getRepository($class)->createQueryBuilder('c'); 
        $qb->where($qb->expr()->in('c.id',$result))->orderBy('c.updatedAt', 'desc')->setMaxResults('10'); 

        return $qb;
    }
}
?>
