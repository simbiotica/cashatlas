<?php

namespace Simbiotica\CalpBundle\Block;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RevisionListBlock extends BaseBlockService {
    
    private $container;
    
    public function __construct($name, EngineInterface $templating, ContainerInterface $container)
    {
        parent::__construct($name, $templating);

        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        $projects = $this->container->get('doctrine')->getManager()->getRepository("SimbioticaCalpBundle:Project")->findByRevisors(array($user));
        
        return $this->renderResponse('SimbioticaCalpBundle:Block:revision_list.html.twig', array(
            'projects'     => $projects,
            'settings'     => $blockContext->getSettings(),
            'block'        => $blockContext->getBlock()
        ), $response);
    }

    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {
        // TODO: Implement validateBlock() method.
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Revision List';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
        ));

        $resolver->setAllowedTypes(array(
        ));
    }
}
?>
