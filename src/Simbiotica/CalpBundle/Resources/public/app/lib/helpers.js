/*jslint sloppy: true */
/*global Handlebars */

String.prototype.format = function() {
    var args = [].slice.call(arguments),
    result = this.slice(),
    regexp, i, len;

    for (i = 0, len = args.length; i < len; i++) {
        regexp = new RegExp("%"+(i+1), "g");
        result = result.replace(regexp, args[i]);
    }
    return result;
};

// Add points after every 3 digits
String.prototype.dots = function() {
    var number = this.slice();
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

//  return a comma-serperated list from an iterable object
// usage: {{#toSentance tags}}{{name}}{{/toSentance}}
Handlebars.registerHelper('toSentance', function(context, block) {
    var ret = "",
    fn = block.fn,
    i, len;

    if (typeof context === 'string') {
        return fn(context);
    }

    for(i = 0, len = context.length; i < len; i++) {
        ret = ret + fn(context[i]);
        if (i < len-1) {
            ret = ret + ", ";
        }
    }

    return ret;
});

// dosts Handlebars
Handlebars.registerHelper('dots', function(context, block) {
    if (typeof context === 'string') {
        return context.dots();
    } else if (typeof context === 'number') {
        var result = context.toString();
        return result.dots();
    }
});

// usage: {{{substring content}}}
Handlebars.registerHelper('subString', function(st, val, block) {
    var limit, result;
    
    limit = (val && typeof val === 'number') ? val : 150;
    
    if (st.length < limit) {
        result = st;
        return new Handlebars.SafeString(result);
    }
    
    if (st && typeof st === 'string') {
        result = st.substring(0, limit) + '...';
        return new Handlebars.SafeString(result);
    }
});

// Portions of a list
Handlebars.registerHelper('slice', function(context, from, to, block) {
    var ret = "",
    i = parseInt(from),
    j = parseInt(to);

    for(i,j; i<=j; i++) {
        ret += block.fn(context[i]);
    }

    return ret;
});

// Conditional comparison
Handlebars.registerHelper('ifequal', function (val1, val2, block, elseblock) {
    if (val1 === val2) {
        return block.fn(this);
    } else if (elseblock) {
        return elseblock.fn(this);
    }
});
Handlebars.registerHelper('ifgreater', function (val1, val2, block, elseblock) {
    if (parseInt(val1) > parseInt(val2)) {
        return block.fn(this);
    } else if (elseblock) {
        return elseblock.fn(this);
    }
    return false;
});

// Translator
Handlebars.registerHelper('translate', function (key, block) {
    var fn = block.fn,
        hash = block.hash,
        params = {};

    if (!_.isEmpty(hash)) {
        _.each(hash, function(value, key) {
            params[key] = value;
        });
    }

    return Translator.get('SimbioticaFront:' + key, params);
});

// Routing
Handlebars.registerHelper('route', function (key, data, block) {
    var fn = block.fn;

    return Routing.generate(key, { id: data }, true);
});

// Format URL: add http if no exist
Handlebars.registerHelper('addhttp', function(url, block) {
    var regcheck = url.match(/^(f|ht)tps?:\/\//i);
    if (!regcheck) {
        url = "http://" + url;
    }
    return url;
});