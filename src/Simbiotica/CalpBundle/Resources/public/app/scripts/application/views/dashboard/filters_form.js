define(['d3', 'select2'], function(d3) {
    'use strict';

    var FiltersFormView = Backbone.View.extend({
        el: '#filterForm',
        initialize: function() {
            var self = this;
            var $scaleMax = $('#simbiotica_form_search_scaleMax');
            var $scaleMin = $('#simbiotica_form_search_scaleMin');
            var sMax = $scaleMax.data('max');
            var selectValue, $selectTarget;
            var scale = d3.scale.pow().exponent(4);

            this.sMax = sMax;

            scale.domain([0, 100]);
            scale.rangeRound([0, sMax]);

            this.$countries = $('#search_form_country');
            this.$regions = $('#search_form_region');
            this.$resetBtn = $('#btnResetForm');
            this.$submitBtn = $('#btnSubmitForm');
            this.$scaleSlider = $('#scaleSlider');
            this.$scaleResult = $('#scaleResult');
            this.$selects = $(this.$el.find('select'));
            this.$limit = $('#simbiotica_form_search_owner');

            this.$selects.select2({
                width: 'element'
            });
            this.$selects.select2('val', '');

            this.$selects.on('change', function(e) {
                selectValue = $(e.currentTarget).select2('val');
                $selectTarget = $('#s2id_' + e.currentTarget.id);

                if (selectValue.length === 0 || selectValue === '') {
                    $selectTarget.removeClass('noempty');
                } else {
                    $selectTarget.addClass('noempty');
                }
            });

            this.$el.find('input[type="text"]').on('change', function(e) {
                $selectTarget = $(e.currentTarget);
                if ($selectTarget.val() === '') {
                    $selectTarget.removeClass('noempty');
                } else {
                    $selectTarget.addClass('noempty');
                }
            });

            this.$resetBtn.on('click', function() {
                Backbone.Events.trigger('filter:reset');
            });
            this.$submitBtn.on('click', function() {
                self.$el.submit();
            });

            this.$scaleSlider.slider({
                min: 0,
                max: 100,
                range: true,
                step: 1,
                values: [0, 100],
                slide: function(event, ui) {
                    self.$scaleResult.html(scale(ui.values[0]) + ' - ' + scale(ui.values[1]));
                },
                change: function(event, ui) {
                    self.$scaleResult.html(scale(ui.values[0]) + ' - ' + scale(ui.values[1]));
                    $scaleMin.val(scale(ui.values[0]));
                    $scaleMax.val(scale(ui.values[1]));
                }
            });

            this.$scaleResult.html('0 - ' + sMax);

            this.$el.on('submit', function(e) {
                var params, m, name, subname, inputs, $element, value, $owner;

                params = [];
                inputs = $('#simbiotica_form_search_keyword, #simbiotica_form_search_implementingPartners, #simbiotica_form_search_scaleMin, #simbiotica_form_search_scaleMax, select');
                $owner = $('#simbiotica_form_search_owner');

                $.each(inputs, function(index, element) {
                    $element = $(element);
                    m = $element.attr('name').match(/\[([\w\W]*?)\]/g);
                    name = m[0].replace(/[\[\]]/g, '');
                    subname = (m[1]) ? m[1].replace(/[\[\]]/g, '') : '';
                    value = ($element.val() !== '' && $element.val()) ? $element.val() : [];

                    if (params[name] && subname === '') {
                        params[name].push(value);
                    } else if (subname !== '') {
                        if (!params[name]) {
                            params[name] = [];
                            params[name].push(value);
                        } else {
                            params[name].push(value);
                        }
                    } else {
                        params[name] = value;
                    }
                });

                params.owner = $owner.is(':checked');

                Backbone.Events.trigger('filter:change', params);

                e.preventDefault();
            });

            Backbone.Events.on('filter:reset', this.resetForm , this);
        },
        resetForm: function() {
            this.$selects.select2('val', '');
            this.$el.find('input[type="text"]').val('');
            this.$scaleSlider.slider('option', 'values', [0, this.sMax]);
            this.$limit.attr('checked', false);
            $('#search_form_keyword').val('');
        }
    });

    return FiltersFormView;

});