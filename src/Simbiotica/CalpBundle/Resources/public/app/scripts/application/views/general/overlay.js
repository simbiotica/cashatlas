define(function() {
    'use strict';

    var OverlayView = Backbone.View.extend({
        el: '#overlay',
        events: {
            'click #bgOverlay': 'close',
            'click #closeOverlayBtn': 'close'
        },
        initialize: function() {
            Backbone.Events.on('profile:noauth', this.show, this);
        },
        show: function() {
            this.$el.fadeIn('fast');
        },
        close: function() {
            this.$el.fadeOut('fast');
        }
    });

    return OverlayView;

});