define(function() {
    'use strict';

    var InfoModel = Backbone.Model.extend({
        url: function() {
            var params = {};
            switch (app.filters.get('level')) {
            case 'areas':
                params = _.omit(app.filters.attributes, ['country', 'region']);
                break;
            case 'countries':
                params = _.omit(app.filters.attributes, ['continent', 'region']);
                break;
            case 'regions':
                params = _.omit(app.filters.attributes, ['continent', 'country']);
                break;
            default:
                params = app.filters.attributes;
            }
            return Routing.generate('ajax_simbiotica_dashboard', _.extend({
                ids: true,
                currency: CURRENCYDATA[CURRENCY].name
            }, params));
        },
        parse: function(data) {
            // function check(value) {
            //     if (!value || value === '0') {
            //         return 'unknown';
            //     }
            //     return value;
            // }

            if (data.length === 0) {
                this.clear();
                return {
                    error: 'No data'
                };
            }

            if (data.sectors && typeof data.sectors === 'array' && data.sectors[0] && data.sectors[0].name === 'Other') {
                data.sectors.shift();
            }

            data.organizations = _.map(data.organizations, function(d) {
                if (d.id) {
                    var result = _.find(CONSTANTS.organization, function(s) {
                        return s.id === d.id;
                    });
                    if (result) {
                        d.name = result.name;
                    }
                }
                return d;
            });

            data.donors = _.map(data.donors, function(d) {
                if (d.id) {
                    var result = _.find(CONSTANTS.organization, function(s) {
                        return s.id === d.id;
                    });
                    if (result) {
                        d.name = result.name;
                    }
                }
                return d;
            });

            data.sectors = _.map(data.sectors, function(d) {
                if (d.id) {
                    var result = _.find(CONSTANTS.sector, function(s) {
                        return s.id === d.id;
                    });
                    if (result) {
                        d.name = result.name;
                    }
                }
                return d;
            });

            data.modalities = _.map(data.modalities, function(d) {
                if (d.id) {
                    if (_.has(CONSTANTS.modalities, d.id)) {
                        d.name = CONSTANTS.modalities[d.id];
                    }
                }
                return d;
            });

            var model = {
                auth: AUTH,
                ids: data.ids,
                organizations: data.organizations,
                projects: {
                    allocated_amount: (data.projects.budgetAllocated) ? CURRENCYDATA[CURRENCY].symbol + ' ' + Math.round(data.projects.budgetAllocated).toString().dots() : 'unknown',
                    scale: data.projects.scale.dots(),
                    projectCount: data.projects.projectCount
                },
                sectors: data.sectors,
                modalities: data.modalities,
                donors: data.donors,
                error: null
            };
            return model;
        },
        getWorld: function(callback) {
            this.fetch({
                success: function(model) {
                    if (callback && typeof callback === 'function') {
                        callback(model);
                    }
                },
                error: function(response) {
                    $.error(response.responseText);
                }
            });
        },
        getArea: function(area_id, callback) {
            this.fetch({
                success: function(model) {
                    if (callback && typeof callback === 'function') {
                        callback(model);
                    }
                },
                error: function(response) {
                    $.error(response.responseText);
                }
            });
        },
        getCountry: function(country_id, callback) {
            this.fetch({
                success: function(model) {
                    if (callback && typeof callback === 'function') {
                        callback(model);
                    }
                },
                error: function(response) {
                    $.error(response.responseText);
                }
            });
        },
        getRegion: function(region_id, callback) {
            this.fetch({
                success: function(model) {
                    if (callback && typeof callback === 'function') {
                        callback(model);
                    }
                },
                error: function(response) {
                    $.error(response.responseText);
                }
            });
        },
        getData: function(filters, callback) {
            this.fetch({
                success: function(model) {
                    if (callback && typeof callback === 'function') {
                        callback(model);
                    }
                },
                error: function(response) {
                    $.error(response.responseText);
                }
            });
        }
    });

    return InfoModel;
});