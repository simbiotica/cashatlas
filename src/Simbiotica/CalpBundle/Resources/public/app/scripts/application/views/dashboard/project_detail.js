define([
    'handlebars',
    'text!../../../../templates/project_detail.handlebars'
], function(Handlebars, tpl) {
    'use strict';

    var ProjectDetailView = Backbone.View.extend({
        el: '#projectDetail',
        events: {
            'click .missing': 'showOverlay'
        },
        template: Handlebars.compile(tpl),
        initialize: function() {
            Backbone.Events.on('selection:project', this.showPanel, this);
            Backbone.Events.on('sidebar:toggle', this.hidePanel, this);
            Backbone.Events.on('project_detail:hide', this.hidePanel, this);
        },
        render: function(data) {
            var self = this;

            Backbone.Events.trigger('ajax:stop');

            this.$el.html(this.template(data));

            this.$el.find('.scrollable').jScrollPane({
                verticalGutter: 0,
                autoReinitialise: true
            });

            $('#btnToggleProject').on('click', function() {
                self.hidePanel();
            });
        },
        showPanel: function(id) {
            var self = this;

            this.$el.addClass('active');
            this.clearPanel();

            Backbone.Events.trigger('ajax:start', this.el);

            app.projectFull.getById(id, function(model) {
                self.render(model.attributes);
            });
        },
        clearPanel: function() {
            this.$el.html('');
        },
        hidePanel: function() {
            this.$el.removeClass('active');
        },
        showOverlay: function(e) {
            e.preventDefault();
            Backbone.Events.trigger('contact:overlay:show');
        }
    });

    return ProjectDetailView;

});