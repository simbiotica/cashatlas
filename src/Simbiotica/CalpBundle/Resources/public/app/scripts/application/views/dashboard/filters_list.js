define([
    'handlebars',
    'text!../../../../templates/filters_list.handlebars',
    'helpers'
], function(Handlebars, tpl) {
    'use strict';

    var FiltersListView = Backbone.View.extend({
        el: '#filtersList',
        initialize: function() {
            Backbone.Events.on('filter:done', this.setFilters, this);
        },
        render: function(data) {
            this.$el.html(this.template(data));
        },
        template: Handlebars.compile(tpl),
        setFilters: function() {
            var data = {},
                filters = {},
                f = '';

            switch (app.filters.get('level')) {
            case 'world':
                data.currentLocation = Translator.get('SimbioticaFront:breadcrumbs.world');
                break;
            case 'areas':
                data.currentLocation = app.areas.get(app.filters.get('continent')[0]).get('name');
                break;
            case 'countries':
                data.currentLocation = app.countries.get(app.filters.get('country')[0]).get('name');
                break;
            case 'regions':
                data.currentLocation = app.regions.get(app.filters.get('region')[0]).get('name');
                break;
            }

            for (f in app.filters.attributes) {
                if (app.filters.attributes[f].length === 0) {
                    filters[f] = null;
                } else {
                    filters[f] = app.filters.attributes[f];
                }
            }

            filters.organization = _.map(filters.organization, function(n) {
                var result = _.find(CONSTANTS.organization, function(c) {
                    return c.id === parseInt(n, 0);
                });

                if (result) {
                    return result.name;
                } else {
                    return n;
                }
            });

            filters.donor = _.map(filters.donor, function(n) {
                var result = _.find(CONSTANTS.organization, function(c) {
                    return c.id === parseInt(n, 0);
                });

                if (result) {
                    return result.name;
                } else {
                    return n;
                }
            });

            filters.sector = _.map(filters.sector, function(n) {
                var result = _.find(CONSTANTS.sector, function(c) {
                    return c.id === parseInt(n, 0);
                });

                if (result) {
                    return result.name;
                } else {
                    return n;
                }
            });

            filters.modality = _.map(filters.modality, function(n) {
                var result = CONSTANTS.modalities[n];

                if (result) {
                    return result;
                } else {
                    return n;
                }
            });

            filters.deliveryMechanism = _.map(filters.deliveryMechanism, function(n) {
                var result = CONSTANTS.deliveryMechanism[n];

                if (result) {
                    return result;
                } else {
                    return n;
                }
            });

            filters.deliveryAgent = _.map(filters.deliveryAgent, function(n) {
                var result = CONSTANTS.deliveryAgent[n];

                if (result) {
                    return result;
                } else {
                    return n;
                }
            });

            filters.status = _.map(filters.status, function(n) {
                var result = CONSTANTS.status[n];

                if (result) {
                    return result;
                } else {
                    return n;
                }
            });

            filters.duration = _.map(filters.duration, function(n) {
                var result = CONSTANTS.durations[n];

                if (result) {
                    return result;
                } else {
                    return n;
                }
            });

            data = _.extend(data, filters);

            this.render(data);
        }
    });

    return FiltersListView;

});