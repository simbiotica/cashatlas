define(function() {
    'use strict';

    var Region = Backbone.Model.extend({
        defaults: {
            id: null,
            country_id: null,
            area_id: null,
            name: null,
            cash_transfers: null,
            scale: null,
            feature: null,
            geometry: null
        }
    });

    var RegionsCollection = Backbone.Collection.extend({
        model: Region,
        url: function() {
            return Routing.generate('ajax_simbiotica_map_regions', _.omit(app.filters.attributes, ['continent', 'region', 'active']));
        },
        user: 'cashatlas',
        queryGeoJson: 'SELECT st_asgeojson(the_geom, 4326) AS the_geom FROM region_sync WHERE cartodb_id = \'%1\'',
        parse: function(data) {
            var models = _.map(data, function(m) {
                return {
                    id: m.id,
                    cartodb_id: m.cartodb_id,
                    code: m.code,
                    area_id: m.country.continent.id,
                    country_id: m.country.id,
                    name: m.name,
                    cash_transfers: m.project_count,
                    budget_allocated: m.budget_allocated,
                    scale: m.scale_sum,
                    feature: {
                        'type': 'Feature',
                        'properties': {
                            'id': m.id,
                            'code': m.code,
                            'area_id': m.country.continent.id,
                            'country_id': m.country.id,
                            'name': m.name,
                            'cash_transfers': m.project_count,
                            'budget_allocated': m.budget_allocated,
                            'scale': m.scale_sum,
                            'level': 2,
                            'current': 1
                        },
                        'geometry': $.parseJSON(m.centroid)
                    }
                };
            });
            return models;
        },
        getAll: function(callback) {
            if (this.length === 0) {
                this.fetch({
                    dataType: 'json',
                    success: function(models) {
                        if (callback && typeof callback === 'function') {
                            callback(models);
                        }
                    },
                    error: function(response) {
                        $.error(response.responseText);
                    }
                });
            } else {
                if (callback && typeof callback === 'function') {
                    callback(this);
                }
            }
        },
        getAllWithout: function(id, callback) { // TODO
            this.fetch({
                dataType: 'jsonp',
                cache: false,
                data: $.extend({}, $.param({
                    format: 'GeoJSON',
                    q: this.queryWithout().format(id)
                }), app.filters.attributes),
                success: function(models) {
                    if (callback && typeof callback === 'function') {
                        callback(models);
                    }
                },
                error: function(res, err) {
                    $.error(err.responseText);
                }
            });
            return this;
        },
        getByCountryWithout: function(id, country_id, callback) { // TODO
            var self, model;

            function processCollection(collection) {
                var _model, models, othermodels, newmodels;

                _model = collection.get(id);
                models = _.map(collection.models, function(m) {
                    return m.attributes;
                });
                othermodels = _.without(models, _model.attributes);
                newmodels = _.where(othermodels, {
                    country_id: country_id
                });
                return new RegionsCollection(newmodels);
            }

            self = this;
            model = this.get(id);

            if (model) {
                var result = processCollection(this);

                if (result.length === 0) {
                    $.ajax({
                        url: this.url(),
                        dataType: 'json',
                        data: $.param({
                            format: 'GeoJSON',
                            q: this.query.format(id),
                        }),
                        success: function(data) {
                            var models = self.parse(data);
                            self.add(models);
                            result = processCollection(self);
                            if (callback && typeof callback === 'function') {
                                callback(result);
                            }
                        },
                        error: function(res, err) {
                            $.error(err.responseText);
                        }
                    });
                } else {
                    if (callback && typeof callback === 'function') {
                        callback(result);
                    }
                }
            } else {
                this.getByCountry(country_id, processCollection);
            }
        },
        getByAreaWithout: function(id, area_id, callback) { // TODO
            var self = this;
            var model = this.get(id);

            function processCollection(collection) {
                model = collection.get(id);
                var models = _.map(collection.models, function(m) {
                    return m.attributes;
                });
                var othermodels = _.without(models, model.attributes);
                var newmodels = _.where(othermodels, {
                    area_id: area_id
                });
                return new RegionsCollection(newmodels);
            }

            if (model) {
                var result = processCollection(this);

                if (result.length === 0) {
                    $.ajax({
                        url: this.url(),
                        dataType: 'jsonp',
                        cache: false,
                        data: $.param({
                            format: 'GeoJSON',
                            q: this.query.format(id)
                        }),
                        success: function(data) {
                            var models = self.parse(data);
                            self.add(models);
                            result = processCollection(self);
                            if (callback && typeof callback === 'function') {
                                callback(result);
                            }
                        },
                        error: function(res, err) {
                            $.error(err.responseText);
                        }
                    });
                } else {
                    if (callback && typeof callback === 'function') {
                        callback(result);
                    }
                }
            } else {
                this.getByArea(area_id, processCollection);
            }
        },
        getByArea: function(area_id, callback) {
            var self, result;

            function processCollection(collection) {
                var models, modelsFiltered;

                models = _.map(collection.models, function(m) {
                    return m.attributes;
                });
                modelsFiltered = _.where(models, {
                    area_id: area_id
                });

                return new RegionsCollection(modelsFiltered);
            }

            self = this;
            result = processCollection(this);

            if (result.length === 0) {
                $.ajax({
                    url: this.url(),
                    dataType: 'json',
                    success: function(data) {
                        var models = self.parse(data);
                        self.add(models);
                        result = processCollection(self);
                        if (callback && typeof callback === 'function') {
                            callback(result);
                        }
                    },
                    error: function(res, err) {
                        $.error(err.responseText);
                    }
                });
            } else {
                if (callback && typeof callback === 'function') {
                    callback(result);
                }
            }
        },
        getByCountry: function(country_id, callback) {
            
            function processCollection(collection) {
                var models = _.map(collection.models, function(m) {
                    return m.attributes;
                });
                var modelsFiltered = _.where(models, {
                    country_id: country_id
                });
                return new RegionsCollection(modelsFiltered);
            }

            var self = this;
            var result = processCollection(this);

            if (result.length === 0) {
                $.ajax({
                    url: this.url(),
                    dataType: 'json',
                    success: function(data) {
                        var models = self.parse(data);
                        self.add(models);
                        result = processCollection(self);
                        if (callback && typeof callback === 'function') {
                            callback(result);
                        }
                    },
                    error: function(res, err) {
                        $.error(err.responseText);
                    }
                });
            } else {
                if (callback && typeof callback === 'function') {
                    callback(result);
                }
            }
        },
        getById: function(id, callback) {
            var self = this;
            var model = this.get(id);

            if (model) {
                var geom = model.get('geometry');

                if (geom) {
                    if (callback && typeof callback === 'function') {
                        callback(model);
                    }
                } else {
                    var sql = new cartodb.SQL({
                        user: this.user
                    });
                    sql.execute(this.queryGeoJson.format(model.get('cartodb_id')))
                        .on('done', function(data) {
                            geom = $.parseJSON(data.rows[0].the_geom);
                            model.set({
                                geometry: geom
                            });
                            if (callback && typeof callback === 'function') {
                                callback(model);
                            }
                        })
                        .on('error', function(errors) {
                            $.error(errors);
                        });
                }
            } else {
                $.ajax({
                    url: this.url(),
                    dataType: 'json',
                    success: function(data) {
                        var models = self.parse(data),
                            result;

                        self.add(models);
                        result = self.get(id);

                        if (callback && typeof callback === 'function') {
                            callback(result);
                        }
                    },
                    error: function(res, err) {
                        $.error(err.responseText);
                    }
                });
            }
        }
    });

    return RegionsCollection;

});