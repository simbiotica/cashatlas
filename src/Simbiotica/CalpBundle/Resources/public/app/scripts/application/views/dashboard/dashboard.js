define(function() {
    'use strict';

    var DashboardView = Backbone.View.extend({
        el: '#dashboard',
        events: {
            'click #reportBtn': 'reportAction'
        },
        initialize: function() {
            var self = this;

            function toggleSidebar(e) {
                var sidebarWidth = self.$el.width();

                $(e.currentTarget).toggleClass('active');

                self.active = (self.active) ? false : true;

                if (!self.active) {
                    self.$el.css('right', -self.$el.width() + 27 + 'px');
                } else {
                    self.$el.css('right', 0);
                }

                Backbone.Events.trigger('sidebar:toggle', self.active, sidebarWidth);
            }

            this.$tabs = $('#contentTabs');
            this.$btn = $('#btnToggleDashboard');

            this.$info = $('#infoPanel');
            this.$filter = $('#filterPanel');
            this.$infoScroll = this.$info.find('.scrollable');
            this.$filterScroll = this.$filter.find('.scrollable');
            this.$footer = $(this.$tabs.find('.footer'));

            this.active = true;
            this.expanded = false;
            this.filterTabInitialized = false;

            // Btn - Hide or show sidebar
            this.$btn.on('click', toggleSidebar);

            // jQuery UI - Tabs
            if (AUTH) {
                this.$tabs.tabs({
                    activate: function() {
                        self.toggleExpanded();
                        self.initFilterTab();
                    },
                    create: function() {
                        self.$tabs.show();
                    }
                });
            } else {
                self.$tabs.tabs({
                    disabled: [1]
                });
                self.$tabs.show();
                self.$tabs.find('.info_tab a').on('click', function(e) {
                    e.preventDefault();
                });
                self.$tabs.find('.filter_tab a').on('click', function(e) {
                    e.preventDefault();
                    Backbone.Events.trigger('profile:noauth');
                });
            }

            $('#exportInfoListToExcel').attr('href', Routing.generate('simbiotica_calp_export_excel', _.extend({}, app.filters.toJSON(), {
                format: 'xlsx'
            })));
            $('#exportInfoListToCSV').attr('href', Routing.generate('simbiotica_calp_export_excel', _.extend({}, app.filters.toJSON(), {
                format: 'csv'
            })));

            // Window
            //$(window).on('resize', function () {}); // TO-DO
            Backbone.Events.on('filter:change', this.viewOnInfo, this);
            Backbone.Events.on('info:done', this.updateInfoScroll, this);
            Backbone.Events.on('projectsList:done', this.updateInfoScroll, this);
        },
        updateInfoScroll: function() {
            var h, panelHeight, footerHeight, breadHeight;

            panelHeight = this.$el.height();
            footerHeight = this.$footer.height();
            breadHeight = $('.info_breadcrumbs').outerHeight();

            h = panelHeight - footerHeight - breadHeight - 47;

            this.$infoScroll
                .height(h)
                .jScrollPane({
                    verticalGutter: 0,
                    contentWidth: 392,
                    autoReinitialise: true
                });
        },
        initFilterTab: function() {
            if (!this.filterTabInitialized) {
                var hForm, h;

                hForm = $('#filterForm').height();
                h = this.$el.height() - 47 - 65 - 62;

                if (hForm > h) {
                    this.$filterScroll
                        .height(h)
                        .jScrollPane({
                            verticalGutter: 0,
                            autoReinitialise: true
                        });

                    this.filterTabInitialized = true;
                }

                Backbone.Events.trigger('filter:loaded');
            }
        },
        toggleExpanded: function() {
            var sidebarWidth = this.$el.width();

            this.expanded = (this.expanded) ? true : false;

            Backbone.Events.trigger('sidebar:toggle', this.active, sidebarWidth);
        },
        viewOnInfo: function() {
            this.$tabs.tabs({
                active: 0
            });
        },
        reportAction: function(e) {
            //        var url = Routing.generate('ajax_simbiotica_report_index', app.filters.attributes, true);
            //        window.open(url);
            e.preventDefault();
            var mywindow = window.open('', 'Report', 'height=400,width=400, top=200, left=200');
            mywindow.document.write('<html>');
            //mywindow.document.write('<head>');
            /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
            mywindow.document.write($('head').html());
            mywindow.document.write('<style> #filtersList {font-size:28px;  }  </style>');
            //mywindow.document.write('</head>');
            //mywindow.document.write($('header').html());
            mywindow.document.write($('#filtersList').html());
            mywindow.document.write($('#info').html());
            // $('#infoPanel').css('height', 'auto');
            mywindow.document.write('</body></html>');

            mywindow.print();
            mywindow.close();

            return true;
        }
    });

    return DashboardView;

});