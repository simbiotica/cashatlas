define([
    'select2',
    'validate'
], function() {
    'use strict';

    var ContactFormView = Backbone.View.extend({
        el: '#contactForm',
        events: {
            'change .type': 'onTypeChange'
        },
        initialize: function() {
            var self = this;
            this.$el = $('#contactForm');
            this.$typeSelect = this.$el.find('.type');
            this.$context = this.$el.find('.context');

            this.$page = $('html, body');
            this.$selects = $(this.$el.find('select'));

            this.$selects.select2({
                width: '240px',
                closeOnSelect: false,
                allowClear: true
            });

            _.each(this.$selects, function(sel) {
                if ($(sel).val() === '') {
                    $(sel).select2('val', '');
                }
            });

            this.$el.validate({
                submitHandler: function() {
                    self.submitForm();
                    return false;
                }
            });
        },
        swapForm: function(data) {
            this.$el.html(data);
            this.initialize();
        },
        onTypeChange: function(e) {
            var value = $(e.currentTarget).val();
            var self = this;

            $.ajax({
                url: Routing.generate('ajax_simbiotica_contact_form', {
                    'context': this.$context.val()
                }),
                dataType: 'html',
                data: {
                    provider: value,
                    showProvider: true
                },
                success: function(data) {
                    self.swapForm(data);
                }
            });
        },
        submitForm: function() {
            var self = this;

            $.ajax({
                url: $('form#contactForm').attr('action'),
                dataType: 'html',
                type: 'POST',
                data: this.$el.serializeArray(),
                success: function(data) {
                    self.swapForm(data);
                    Backbone.Events.trigger('contact:submit');
                }
            });

        }
    });

    return ContactFormView;

});