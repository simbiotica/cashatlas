define([
    'handlebars',
    'text!../../../../templates/contact_overlay.handlebars',
    'views/general/contact_form'
], function(Handlebars, tpl, ContactFormView) {
    'use strict';

    var ContactOverlayView = Backbone.View.extend({
        el: '#contactOverlay',
        events: {
            'click #bgOverlay': 'closeOverlay',
            'click #closeOverlayBtn': 'closeOverlay'
        },
        template: Handlebars.compile(tpl),
        initialize: function() {
            Backbone.Events.on('contact:overlay:show', this.render, this);
            Backbone.Events.on('contact:submit', this.clearForm, this);
        },
        render: function(data) {
            var self = this;

            function initContactFormView() {
                $('#contactFormWrapper').html(self.form);
                self.contactView = new ContactFormView();
                self.$el.fadeIn('fast');
            }

            this.$el.html(this.template(data));

            if (!this.form) {
                this.fetchForm(function() {
                    initContactFormView();
                });
            } else {
                initContactFormView();
            }
        },
        closeOverlay: function() {
            this.$el.fadeOut('fast');
        },
        fetchForm: function(callback) {
            var self = this;
            $.ajax({
                url: Routing.generate('ajax_simbiotica_contact_form', {
                    'projectId': $('.missing').data('project')
                }),
                dataType: 'html',
                data: {
                    provider: 'simbiotica.contact.missing_info'
                },
                success: function(data) {
                    self.form = data;
                    if (callback && typeof callback === 'function') {
                        callback(self);
                    }
                }
            });
        },
        clearForm: function() {
            this.$el.find('header').addClass('hidden');
            this.form = null;
        }
    });

    return ContactOverlayView;

});