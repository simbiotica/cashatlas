define([
    'd3',
    'handlebars',
    'text!../../../../templates/info.handlebars'
], function(d3, Handlebars, tpl) {
    'use strict';

    var InfoView = Backbone.View.extend({
        el: '#info',
        initialize: function() {
            Backbone.Events.on('filter:done', this.update, this);
        },
        template: Handlebars.compile(tpl),
        render: function(data) {
            var self = this,
                i,
                sum,
                color,
                $donorGraphic,
                $organizationGraphic,
                $sectorGraphic,
                $modalityGraphic;

            if (!data.error) {
                color = d3.scale.category10();
                for (i = 0; i < data.donors.length; i++) {
                    data.donors[i].color = color(i);
                }
                for (i = 0; i < data.organizations.length; i++) {
                    data.organizations[i].color = color(i);
                }
                for (i = 0; i < data.sectors.length; i++) {
                    data.sectors[i].color = color(i);
                    data.sectors[i].number = +data.sectors[i].number;
                }
                for (i = 0; i < data.modalities.length; i++) {
                    data.modalities[i].color = color(i);
                    data.modalities[i].number = +data.modalities[i].number;
                }
            }

            Backbone.Events.trigger('ajax:stop');

            sum = 0;

            _.each(data.donors, function(d) {
                sum += parseInt(d.amount, 0);
            });

            data.sum = sum;
            data.helper = {};

            if (_.isEmpty(app.filters.get('organization'))) {
                data.helper.organisations = false;
            } else {
                data.helper.organisations = true;
            }

            if (_.isEmpty(app.filters.get('sector'))) {
                data.helper.sectors = false;
            } else {
                data.helper.sectors = true;
            }

            if (_.isEmpty(app.filters.get('donor'))) {
                data.helper.donors = false;
            } else {
                data.helper.donors = true;
            }

            if (_.isEmpty(app.filters.get('modality'))) {
                data.helper.modalities = false;
            } else {
                data.helper.modalities = true;
            }

            data.currency = CURRENCYDATA[CURRENCY].symbol;

            self.$el.html(this.template(data));

            Backbone.Events.trigger('info:done', data.ids);

            $donorGraphic = $('#donorspie');
            $organizationGraphic = $('#organizationspie');
            $sectorGraphic = $('#sectorspie');
            $modalityGraphic = $('#modalitiespie');

            if (data.error) {
                return false;
            }

            if ($organizationGraphic.length > 0) {
                self.drawOrganizationPie(data.organizations);
            }

            if ($donorGraphic.length > 0) {
                self.drawDonorPie(data.donors);
            }
            if ($sectorGraphic.length > 0) {
                self.drawSectorPie(data.sectors);
            }
            if ($modalityGraphic.length > 0) {
                self.drawModalityPie(data.modalities);
            }

            $('#exportInfoListToExcel').attr('href', Routing.generate('simbiotica_calp_export_excel', _.extend({}, app.filters.toJSON(), {
                format: 'xlsx'
            })));
            $('#exportInfoListToCSV').attr('href', Routing.generate('simbiotica_calp_export_excel', _.extend({}, app.filters.toJSON(), {
                format: 'csv'
            })));
        },
        showWorld: function() {
            var self = this;

            this.clear();
            Backbone.Events.trigger('ajax:start', this.el);

            app.info.getWorld(function(model) {
                self.render(model.attributes);
            });
        },
        showArea: function(area_id) {
            var self = this;

            this.clear();
            Backbone.Events.trigger('ajax:start', this.el);

            app.info.getArea(area_id, function(model) {
                self.render(model.attributes);
            });
        },
        showCountry: function(country_id) {
            var self = this;

            this.clear();
            Backbone.Events.trigger('ajax:start', this.el);

            app.info.getCountry(country_id, function(model) {
                self.render(model.attributes);
            });
        },
        showRegion: function(region_id) {
            var self = this;

            this.clear();
            Backbone.Events.trigger('ajax:start', this.el);

            app.info.getRegion(region_id, function(model) {
                self.render(model.attributes);
            });
        },
        update: function() {
            var self = this;

            this.clear();

            Backbone.Events.trigger('info:update');

            Backbone.Events.trigger('ajax:start', this.el);

            app.info.getData(app.filters.attributes, function(model) {
                self.render(model.attributes);
            });
        },
        clear: function() {
            this.$el.html('');
        },
        drawOrganizationPie: function(data) {
            //var formatPercent = d3.format('.0%'); 
            function formatPercent(number) {
                return Math.round(number * 100) > 0 ? Math.round(number * 100).toString() + '%' : '';
            }
            var totalData = d3.sum(data, function(d) {
                return d.scale;
            });

            function prescale(number) {
                return number / totalData;
            }

            var color = d3.scale.category10();

            var pie = d3.layout.pie()
                .value(function(d) {
                    return d.scale;
                })
                .sort(null);

            var arc = d3.svg.arc()
                .innerRadius(20)
                .outerRadius(50);

            var svg = d3.select('#organizationspie').append('svg')
                .attr('width', 120)
                .attr('height', 100)
                .attr('style', 'top:0px;left:0px')
                .append('g')
                .attr('transform', 'translate(50,50)');

            svg.selectAll('path')
                .data(pie(data))
                .enter().append('path')
                .attr('fill', function(d, i) {
                    return color(i);
                })
                .attr('d', arc);

            svg.selectAll('g').data(pie(data))
                .enter().append('text')
                .attr('transform', function(d) {
                    return 'translate(' + arc.centroid(d) + ')';
                })
                .attr('dy', '.35em')
                .style('text-anchor', 'middle').style('font-size', '10px').style('fill', '#fff').style('font-weight', 'bold')
                .text(function(d) {
                    return formatPercent(prescale(d.data.scale));
                });

        },
        drawDonorPie: function(data) {
            //var formatPercent = d3.format('.0%'); 
            function formatPercent(number) {
                return Math.round(number * 100) > 0 ? Math.round(number * 100).toString() + '%' : '';
            }
            var totalData = d3.sum(data, function(d) {
                return d.amount;
            });

            function prescale(number) {
                return number / totalData;
            }

            var color = d3.scale.category10();

            var pie = d3.layout.pie()
                .value(function(d) {
                    return d.amount;
                })
                .sort(null);

            var arc = d3.svg.arc()
                .innerRadius(20)
                .outerRadius(50);

            var svg = d3.select('#donorspie').append('svg')
                .attr('width', 120)
                .attr('height', 100)
                .attr('style', 'top:0px;left:0px')
                .append('g')
                .attr('transform', 'translate(50,50)');

            svg.selectAll('path')
                .data(pie(data))
                .enter().append('path')
                .attr('fill', function(d, i) {
                    return color(i);
                })
                .attr('d', arc);

            svg.selectAll('g').data(pie(data))
                .enter().append('text')
                .attr('transform', function(d) {
                    return 'translate(' + arc.centroid(d) + ')';
                })
                .attr('dy', '.35em')
                .style('text-anchor', 'middle').style('font-size', '10px').style('fill', '#fff').style('font-weight', 'bold')
                .text(function(d) {
                    return formatPercent(prescale(d.data.amount));
                });

        },
        drawSectorPie: function(data) {
            var color = d3.scale.category10();
            var margin = {
                top: 5,
                right: 20,
                bottom: 5,
                left: 28
            },
                width = 200 - margin.left - margin.right,
                height = 120 - margin.top - margin.bottom;

            var x = d3.scale.ordinal()
                .rangeRoundBands([0, width], 0.1);

            var formatInt = d3.format('d');

            var y = d3.scale.linear()
                .range([height, 1]);

            d3.svg.axis()
                .scale(x)
                .orient('bottom');

            var yAxis = d3.svg.axis()
                .scale(y)
                .orient('left').tickFormat(formatInt);

            var svg = d3.select('#sectorspie').append('svg')
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom)
                .append('g')
                .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

            x.domain(data.map(function(d) {
                return d.id;
            }));
            y.domain([0, d3.max(data, function(d) {
                return d.number;
            })]);

            svg.append('g')
                .attr('class', 'x axis')
                .attr('transform', 'translate(0,' + height + ')');
            /* .call(xAxis); */

            svg.append('g')
                .attr('class', 'y axis')
                .call(yAxis)
                .append('text')
                .attr('transform', 'rotate(-90)')
                .attr('y', 6)
                .attr('dy', '.71em')
                .style('font-size', '10px')
                .style('text-anchor', 'end');
            /* .text('Frequency'); */

            svg.selectAll('.bar')
                .data(data)
                .enter().append('rect')
                .attr('class', 'bar')
                .attr('fill', function(d, i) {
                    return color(i);
                })
                .attr('x', function(d) {
                    return x(d.id);
                })
                .attr('width', x.rangeBand())
                .attr('y', function(d) {
                    return y(d.number);
                })
                .attr('height', function(d) {
                    return height - y(d.number);
                });
        },
        drawModalityPie: function(data) {
            var color = d3.scale.category10();
            var margin = {
                top: 5,
                right: 20,
                bottom: 5,
                left: 28
            },
                width = 200 - margin.left - margin.right,
                height = 120 - margin.top - margin.bottom;

            var x = d3.scale.ordinal()
                .rangeRoundBands([0, width], 0.1);

            var formatInt = d3.format('d');

            var y = d3.scale.linear()
                .range([height, 1]);

            d3.svg.axis()
                .scale(x)
                .orient('bottom');

            var yAxis = d3.svg.axis()
                .scale(y)
                .orient('left').tickFormat(formatInt);

            var svg = d3.select('#modalitiespie').append('svg')
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom)
                .append('g')
                .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

            x.domain(data.map(function(d) {
                return d.id;
            }));
            y.domain([0, d3.max(data, function(d) {
                return d.number;
            })]);

            svg.append('g')
                .attr('class', 'x axis')
                .attr('transform', 'translate(0,' + height + ')');
            /* .call(xAxis); */

            svg.append('g')
                .attr('class', 'y axis')
                .call(yAxis)
                .append('text')
                .attr('transform', 'rotate(-90)')
                .attr('y', 6)
                .attr('dy', '.71em')
                .style('font-size', '10px')
                .style('text-anchor', 'end');
            /* .text('Frequency'); */

            svg.selectAll('.bar')
                .data(data)
                .enter().append('rect')
                .attr('class', 'bar')
                .attr('fill', function(d, i) {
                    return color(i);
                })
                .attr('x', function(d) {
                    return x(d.id);
                })
                .attr('width', x.rangeBand())
                .attr('y', function(d) {
                    return y(d.number);
                })
                .attr('height', function(d) {
                    return height - y(d.number);
                });
        }
    });

    return InfoView;

});