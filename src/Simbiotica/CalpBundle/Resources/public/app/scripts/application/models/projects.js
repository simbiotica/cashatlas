define(['dateutils'], function() {
    'use strict';

    var ProjectModel, ProjectFullModel, ProjectsCollection;

    ProjectModel = Backbone.Model.extend();

    ProjectFullModel = Backbone.Model.extend({
        url: Routing.generate('ajax_simbiotica_project_projects'),
        parse: function(data) {
            var project = {};

            function checkDate(date) {
                if (!date || !date.year) {
                    return 'unknown';
                }
                if (!date.month) {
                    return new Date(date.year, null).toFormat('YYYY');
                }
                return new Date(date.year, date.month - 1).toFormat('MM/YYYY');
            }

            function checkAmount(amount) {
                amount = parseInt(amount, 10);
                if (parseInt(amount, 10) === 0) {
                    return 'unknown';
                }
                return CURRENCYDATA[CURRENCY].symbol + ' ' + Math.round(amount).toString().dots();
            }

            function calculateByInitial(initial, amount) {
                amount = parseInt(amount, 10);
                initial = parseInt(initial, 10);
                if (amount > 0) {
                    return Math.round(amount).toString().dots();
                }
                if (initial) {
                    if (amount === 0 && initial === 0) {
                        return 'unknown';
                    }
                    if (initial > 0) {
                        amount = initial;
                        return amount;
                    }
                }
                if (!amount || amount === 0) {
                    return 'unknown';
                }
                return Math.round(amount).toString().dots();
            }

            function calculateAmount(initial, amount) {
                var result = calculateByInitial(initial, amount);
                if (parseInt(result, 0)) {
                    return CURRENCYDATA[CURRENCY].symbol + ' ' + result;
                }
                return result;
            }

            function getOrganization() {
                var org = _.find(CONSTANTS.organization, function(org) {
                    return org.id === data.projects[0].organizations[0].organization.id;
                });
                org.logo = CONSTANTS.media[org.id];
                return org;
            }

            function getCountries() {
                var countries, locations, temp;

                countries = {};
                locations = data.projects[0].locations;

                countries = _.map(locations, function(location) {
                    if (!temp || temp.id !== location.country.id) {
                        temp = location.country;
                        return location.country;
                    }
                });

                countries = _.compact(countries);

                return countries;
            }

            function getRegions() {
                var regions, locations;

                regions = {};
                locations = data.projects[0].locations;

                regions = _.map(locations, function(location) {
                    var region = location.region;
                    if (location.region && location.scale) {
                        region.beneficiaries = location.scale.toString().dots();
                    }
                    return region;
                });

                return regions;
            }

            function getFromConstants(cons, elements) {
                var constant;
                var result = _.map(elements, function(el) {
                    if (_.isObject(el)) {
                        constant = _.find(cons, function(c) {
                            if (el.organization) {
                                if (el.amount) {
                                    c.amount = el.amount;
                                }
                                return c.id === el.organization.id;
                            }
                            return c.id === el.id;
                        });
                        if (el.name) {
                            el.name = constant.name;
                        }
                        return constant;
                    }
                    return cons[el];
                });
                if (result.length === 0) {
                    return 'unknown';
                }
                return result;
            }

            function getModalities() {
                var result, modalities;
                modalities = _.map(data.projects[0].modalities, function(mod) {
                    result = {};
                    if (mod.modality) {
                        result.modality = CONSTANTS.modalities[mod.modality];
                    }
                    if (mod.modality_specify) {
                        result.modalitySpecify = mod.modality_specify;
                    }
                    if (mod.delivery_mechanism) {
                        result.deliveryMechanism = CONSTANTS.deliveryMechanism[mod.delivery_mechanism];
                    }
                    if (mod.delivery_agent) {
                        result.deliveryAgent = CONSTANTS.deliveryAgent[mod.delivery_agent];
                    }
                    if (mod.delivery_agent_specify) {
                        result.deliveryAgentSpecify = mod.delivery_agent_specify;
                    }
                    if (mod.scale) {
                        result.scale = mod.scale.toString().dots();
                    }
                    if (mod.amount) {
                        result.amount = mod.amount;
                    }
                    if (mod.transfers) {
                        result.transfers = mod.transfers;
                    }
                    return result;
                });
                if (modalities.length === 0) {
                    return 'unknown';
                }
                return modalities;
            }

            function getCoordinationMechanism() {
                var result = _.map(data.projects[0].coordination_mechanisms, function(c) {
                    if (c.specify) {
                        return c.coordination_mechanism.name + ' (' + c.specify + ')';
                    }
                    if (c.coordination_mechanism) {
                        return c.coordination_mechanism.name;
                    }
                    return 'unknown';
                });
                if (result.length === 0) {
                    return 'unknown';
                }
                return result;
            }

            function getMarketAssessment() {
                var result = '';
                if (!data.projects[0].market_assessment || data.projects[0].market_assessment.length === 0) {
                    return 'unknown';
                }
                if (data.projects[0].market_assessment.options) {
                    result += CONSTANTS.marketAssessment[data.projects[0].market_assessment.options];
                }
                if (data.projects[0].market_assessment.other) {
                    result += ' (' + data.projects[0].market_assessment.other + ')';
                }
                return result;
            }

            function getDocuments() {
                var media = _.filter(data.projects[0].gallery_has_medias, function(ghm) {
                    return ghm.media;
                });
                var result = _.map(media, function(doc) {
                    if (doc.media) {
                        return data.media[doc.media.id];
                    } else {
                        return null;
                    }
                });
                if (result.length === 0) {
                    return Translator.get('project_detail:nodocuments');
                }
                return result;
            }

            // TO-DO
            function getDonors(donors) {
                return _.map(donors, function(d) {
                    d.amount = checkAmount(d.amount);
                    return d;
                });
            }

            project = {
                id: data.projects[0].id,
                owner: data.acls[data.projects[0].id],

                organization: getOrganization(),
                affiliates: data.projects[0].affiliates,
                partners: data.projects[0].partners || 'N/A',
                overallAmount: calculateAmount(data.projects[0].initial_budget_overall, data.projects[0].budget_overall),
                allocatedAmount: checkAmount(data.projects[0].budget_allocated),
                beneficiaries: calculateByInitial(data.projects[0].initial_scale, data.projects[0].scale),
                startDate: checkDate(data.projects[0].starting_date),
                endDate: checkDate(data.projects[0].ending_date),
                firstCashDate: checkDate(data.projects[0].first_cash_date),
                duration: data.projects[0].duration,
                status: data.projects[0].status,

                countries: getCountries(),
                regions: getRegions(),
                cities: data.projects[0].city,

                specificObjective: data.projects[0].specific_objective,
                description: data.projects[0].description || 'N/A',

                modalities: getModalities(),
                sectors: getFromConstants(CONSTANTS.sector, data.projects[0].sectors),
                projectContext: getFromConstants(CONSTANTS.projectContext, data.projects[0].project_context),
                typeOfIntervention: CONSTANTS.typeOfIntervention[data.projects[0].type_of_intervention] || 'unknown',
                donors: getDonors(getFromConstants(CONSTANTS.organization, data.projects[0].donors)),
                marketAssessment: getMarketAssessment(),
                coordinationMechanism: getCoordinationMechanism(),
                documents: getDocuments(),

                contactPerson: {
                    name: data.projects[0].contact_person_name,
                    position: data.projects[0].contact_person_position,
                    email: data.projects[0].contact_person_email_one,
                    alternative_email: data.projects[0].contact_person_email_two
                },
                website: data.projects[0].contact_organization_url
            };

            return project;
        },
        getById: function(id, callback) {
            this.fetch({
                data: {
                    id: [id],
                    full: true,
                    currency: CURRENCYDATA[CURRENCY].name
                },
                success: function(model) {
                    if (callback && typeof callback === 'function') {
                        callback(model);
                    }
                },
                error: function(res, err) {
                    $.error(err.responseText);
                }
            });
        }
    });

    ProjectsCollection = Backbone.Collection.extend({
        model: ProjectModel,
        url: function() {
            var params = {};
            switch (app.filters.get('level')) {
            case 'areas':
                params = _.omit(app.filters.attributes, ['country', 'region']);
                break;
            case 'countries':
                params = _.omit(app.filters.attributes, ['continent', 'region']);
                break;
            case 'regions':
                params = _.omit(app.filters.attributes, ['continent', 'country']);
                break;
            default:
                params = app.filters.attributes;
            }
            return Routing.generate('ajax_simbiotica_project_projects', params);
        },
        parse: function(data) {
            var models = data.projects,
                countries = [],
                regions = [],
                tempCountry = 0,
                tempRegion = 0;

            function checkDate(date) {
                if (!date || !date.year) {
                    return null;
                }
                if (!date.month) {
                    return new Date(date.year, null).toFormat('YYYY');
                }
                return new Date(date.year, date.month - 1).toFormat('MM/YYYY');
            }

            models = _.map(models, function(m) {
                tempCountry = 0;
                tempRegion = 0;

                countries = _.filter(m.locations, function(d) {
                    if (tempCountry === d.country.id) {
                        return false;
                    }
                    tempCountry = d.country.id;
                    return d.country;
                });

                regions = _.filter(m.locations, function(d) {
                    return d.region;
                });

                m.countries = _.map(countries, function(d) {
                    if (d.country) {
                        return d.country;
                    }
                });

                m.regions = _.map(regions, function(d) {
                    if (d && d.region) {
                        return d.region;
                    }
                });

                m.organizations = _.map(m.organizations, function(d) {
                    d = d.organization;
                    if (d.id) {
                        var result = _.find(CONSTANTS.organization, function(s) {
                            return s.id === d.id;
                        });
                        if (result) {
                            d.name = result.name;
                        }
                    }
                    return d;
                });
                m.sectors = _.map(m.sectors, function(d) {
                    if (d.id) {
                        var result = _.find(CONSTANTS.sector, function(s) {
                            return s.id === d.id;
                        });
                        if (result) {
                            d.name = result.name;
                        }
                    }
                    return d;
                });

                function calculareOverallAmount() {
                    var budgetOverall = parseInt(m.budget_overall, 10),
                        initialBudgetOverall = parseInt(m.initial_budget_overall, 10);

                    if (initialBudgetOverall) {
                        if (budgetOverall === 0 && initialBudgetOverall === 0) {
                            return 'unknown';
                        }

                        if (initialBudgetOverall > 0) {
                            budgetOverall = initialBudgetOverall;
                        }
                    }

                    if (budgetOverall === 0) {
                        return 'unknown';
                    }

                    return CURRENCYDATA[CURRENCY].symbol + ' ' + Math.round(budgetOverall).toString().dots();
                }

                m.starting_date = checkDate(m.starting_date);
                m.ending_date = checkDate(m.ending_date);
                m.first_cash_date = checkDate(m.first_cash_date);
                m.budget_overall = calculareOverallAmount();

                return m;
            });
            return models;
        },
        getById: function() {
            // TO - DO
        },
        getAll: function(projects, callback) {
            if (projects.length > 0) {
                this.fetch({
                    url: Routing.generate('ajax_simbiotica_project_projects', {
                        id: projects,
                        currency: CURRENCYDATA[CURRENCY].name
                    }),
                    success: function(collection) {
                        if (callback && typeof callback === 'function') {
                            callback(collection);
                        }
                    },
                    error: function(res, err) {
                        $.error(err.responseText);
                    }
                });
            } else {
                callback([]);
            }
        },
        setFilter: function(params, callback) {
            this.fetch({
                success: function(collection) {
                    if (callback && typeof callback === 'function') {
                        callback(collection);
                    }
                },
                error: function(res, err) {
                    $.error(err.responseText);
                }
            });
        }
    });

    return {
        full: ProjectFullModel,
        collection: ProjectsCollection
    };

});