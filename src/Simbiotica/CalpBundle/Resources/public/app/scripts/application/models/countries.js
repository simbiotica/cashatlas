define(function() {
    'use strict';

    var Country = Backbone.Model.extend({
        defaults: {
            id: null,
            code: null,
            area_id: null,
            name: null,
            cash_transfers: null,
            scale: null,
            feature: null
        }
    });

    var CountriesCollection = Backbone.Collection.extend({
        model: Country,
        url: function() {
            return Routing.generate('ajax_simbiotica_map_countries', _.omit(app.filters.attributes, ['continent', 'country', 'region', 'active']));
        },
        parse: function(data) {
            var models = _.map(data, function(m) {
                return {
                    id: m.id,
                    cartodb_id: m.cartodb_id,
                    code: m.code,
                    area_id: m.continent.id,
                    name: m.name,
                    cash_transfers: m.project_count,
                    budget_allocated: m.budget_allocated,
                    feature: {
                        'type': 'Feature',
                        'properties': {
                            'id': m.id,
                            'area_id': m.continent.id,
                            'name': m.name,
                            'cash_transfers': m.project_count,
                            'budget_allocated': m.budget_allocated,
                            'scale': m.scale_sum,
                            'level': 2,
                            'current': 1
                        },
                        'geometry': $.parseJSON(m.centroid)
                    }
                };
            });
            return models;
        },
        getAll: function(callback) {
            if (this.length === 0 || this.length < 241) {
                this.fetch({
                    dataType: 'json',
                    success: function(models) {
                        if (callback && typeof callback === 'function') {
                            callback(models);
                        }
                    },
                    error: function(res, err) {
                        $.error(err.responseText);
                    }
                });
            } else {
                if (callback && typeof callback === 'function') {
                    callback(this);
                }
            }
        },
        getAllWithout: function(id, callback) {
            function processCollection(collection) {
                var model = collection.get(id),
                    othermodels = _.without(collection.models, model),
                    newcollection = new CountriesCollection(othermodels);

                if (callback && typeof callback === 'function') {
                    callback(newcollection);
                }
            }

            if (this.length === 0 || this.length < 241) {
                this.getAll(processCollection);
            } else {
                processCollection(this);
            }
        },
        getByAreaWithout: function(id, area_id, callback) {
            function processCollection(collection) {
                var model, models, othermodels, newmodels;

                model = collection.get(id);

                if (model) {
                    models = _.map(collection.models, function(m) {
                        return m.attributes;
                    });
                    othermodels = _.without(models, model.attributes);
                    newmodels = _.where(othermodels, {
                        area_id: area_id
                    });

                    return new CountriesCollection(newmodels);
                }

                return [];
            }

            var self = this,
                result = processCollection(this);

            if (result.length === 0) {
                $.ajax({
                    url: this.url(),
                    dataType: 'json',
                    success: function(data) {
                        var models = self.parse(data);
                        self.add(models);
                        result = processCollection(self);
                        if (callback && typeof callback === 'function') {
                            callback(result);
                        }
                    },
                    error: function(res, err) {
                        $.error(err.responseText);
                    }
                });
            } else {
                if (callback && typeof callback === 'function') {
                    callback(result);
                }
            }
        },
        getByArea: function(area_id, callback) {
            function processCollection(collection) {
                var models, newmodels;

                models = _.map(collection.models, function(m) {
                    return m.attributes;
                });
                newmodels = _.where(models, {
                    area_id: area_id
                });

                return new CountriesCollection(newmodels);
            }

            var self = this,
                result = processCollection(this);

            if (result.length === 0) {
                $.ajax({
                    url: this.url(),
                    dataType: 'json',
                    success: function(data) {
                        var models = self.parse(data);
                        self.add(models);
                        result = processCollection(self);
                        if (callback && typeof callback === 'function') {
                            callback(result);
                        }
                    },
                    error: function(res, err) {
                        $.error(err.responseText);
                    }
                });
            } else {
                if (callback && typeof callback === 'function') {
                    callback(result);
                }
            }
        },
        getById: function(id, callback) {
            var self = this,
                model = this.get(id);

            if (model) {
                if (callback && typeof callback === 'function') {
                    callback(model);
                }
            } else {
                $.ajax({
                    url: this.url(),
                    dataType: 'json',
                    success: function(data) {
                        var models, result;

                        models = self.parse(data);

                        self.add(models);

                        result = self.get(id);

                        if (callback && typeof callback === 'function') {
                            callback(result);
                        }
                    },
                    error: function(res, err) {
                        $.error(err.responseText);
                    }
                });
            }
        }
    });

    return CountriesCollection;

});