define([
    'jqueryui',
    'handlebars',
    'text!../../../../templates/breadcrumbs.handlebars'
], function($, Handlebars, tpl) {
    'use strict';

    var BreadcrumbsView = Backbone.View.extend({
        el: '#breadcrumbsMap',
        data: {
            areas: {
                current: null,
                others: null
            },
            countries: {
                current: null,
                others: null
            },
            regions: {
                current: null,
                others: null
            }
        },
        template: Handlebars.compile(tpl),
        initialize: function() {
            this.$projectToggleBtn = $('#projectToggleBtn');
            this.$scaleToggleBtn = $('#scaleToggleBtn');
            this.$amountToggleBtn = $('#amountToggleBtn');
            this.$resetFiltersBtn = $('#resetFilters');

            this.$breadcrumbs = $('#breadcrumbsBtn');

            this.$breadcrumbs.find('a').tooltip({
                position: {
                    my: 'center top+15',
                    at: 'center bottom',
                    collision: 'flipfit'
                }
            });

            Backbone.Events.on('breadcrumbs:update', this.controller, this);
            Backbone.Events.on('filter:change', this.checkReset, this);
            Backbone.Events.on('filter:reset', this.hideReset, this);
        },
        render: function(data) {
            var self = this,
            items = self.$breadcrumbs.find('li'),
            active;

            this.$breadcrumbs.find('a').off('click');

            function toggleBreadcrumbsBtns(e) {
                e.preventDefault();
                items.removeClass('active');
                $(e.currentTarget).parent().addClass('active');
            }

            this.$resetFiltersBtn.on('click', function(e) {
                e.preventDefault();
                Backbone.Events.trigger('filter:reset');
                self.hideReset().showWorld();
            });

            this.$projectToggleBtn.on('click', function(e) {
                toggleBreadcrumbsBtns(e);
                Backbone.Events.trigger('datatype:change', 'cash_transfers');
            });

            this.$scaleToggleBtn.on('click', function(e) {
                toggleBreadcrumbsBtns(e);
                Backbone.Events.trigger('datatype:change', 'scale');
            });

            this.$amountToggleBtn.on('click', function(e) {
                toggleBreadcrumbsBtns(e);
                Backbone.Events.trigger('datatype:change', 'budget_allocated');
            });

            active = {
                areas: false,
                countries: false,
                regions: false
            };

            if (!data) {
                self.$el.html(this.template({
                    data: null
                }));
                return self;
            }

            self.$el.html(this.template({
                data: data
            }));

            self.$el.find('a.worldBtn').off('click').on('click', function(e) {
                e.preventDefault();
                Backbone.Events.trigger('selection:world');
            });

            $('#areasBreadcrumbsList').on('mouseover', function() {
                var $self, scrollable;

                $self = $(this);
                scrollable = $self.find('.scrollable');

                if (scrollable.length > 0) {
                    if (!active.areas) {
                        scrollable.jScrollPane({
                            verticalGutter: 0
                        });

                        $self.find('a').on('click', function(e) {
                            e.preventDefault();
                            Backbone.Events.trigger('selection:area', $(this).data('area'));
                        });

                        active.areas = true;
                    }
                }
            });

            $('#countriesBreadcrumbsList').on('mouseover', function() {
                var $self, scrollable;

                $self = $(this);
                scrollable = $(this).find('.scrollable');

                if (scrollable.length > 0) {
                    if (!active.countries) {
                        scrollable.jScrollPane({
                            verticalGutter: 0
                        });

                        $self.find('a.countryBtn').off('click').on('click', function(e) {
                            e.preventDefault();
                            Backbone.Events.trigger('selection:country', $(this).data('country'));
                        });

                        active.countries = true;
                    }
                }
            });

            $('#regionsBreadcrumbsList').on('mouseover', function() {
                var $self, scrollable;

                $self = $(this);
                scrollable = $(this).find('.scrollable');

                if (scrollable.length > 0) {
                    if (!active.regions) {
                        scrollable.jScrollPane({
                            verticalGutter: 0
                        });

                        $self.find('a.regionBtn').off('click').on('click', function(e) {
                            e.preventDefault();
                            Backbone.Events.trigger('selection:region', $(this).data('region'));
                        });

                        active.regions = true;
                    }
                }
            });

            return this;
        },
        controller: function(currentView) {
            switch (currentView) {
            case 'world':
                this.showWorld();
                break;
            case 'areas':
                this.showArea(app.filters.get('continent')[0]);
                break;
            case 'countries':
                this.showCountry(app.filters.get('country')[0]);
                break;
            case 'regions':
                this.showRegion(app.filters.get('region')[0]);
                break;
            }
        },
        showWorld: function() {
            this.render();
        },
        showArea: function(area_id) {
            var self = this;

            this.data.countries = null;
            this.data.regions = null;

            app.areas.getById(area_id, function(area) {
                self.data.areas.current = area.attributes;

                app.areas.getAllWithout(area_id, function(collection) {
                    self.data.areas.others = _.map(collection.models, function(m) {
                        return m.attributes;
                    });

                    self.data.areas.others = _.filter(self.data.areas.others, function(m) {
                        if (parseInt(m.cash_transfers, 0) !== 0) {
                            return m;
                        }
                    });

                    self.render(self.data);
                });
            });

            this.checkReset();
        },
        showCountry: function(country_id) {
            var self = this;

            this.data.regions = null;
            this.data.countries = {};

            app.countries.getById([country_id], function(country) {
                var area_id = country.get('area_id');

                self.data.countries.current = country.attributes;

                app.areas.getById(area_id, function(area) {
                    self.data.areas.current = area.attributes;

                    app.areas.getAllWithout(area_id, function(areas) {
                        self.data.areas.others = _.map(areas.models, function(m) {
                            return m.attributes;
                        });

                        self.data.areas.others = _.filter(self.data.areas.others, function(m) {
                            if (parseInt(m.cash_transfers, 0) !== 0) {
                                return m;
                            }
                        });

                        app.countries.getByAreaWithout(country_id, area_id, function(countries) {
                            self.data.countries.others = _.map(countries.models, function(m) {
                                return m.attributes;
                            });

                            self.data.countries.others = _.filter(self.data.countries.others, function(m) {
                                if (parseInt(m.cash_transfers, 0) !== 0) {
                                    return m;
                                }
                            });

                            self.render(self.data);
                        });
                    });
                });
            });

            this.checkReset();
        },
        showRegion: function(region_id) {
            var self = this;

            this.data.regions = {};

            app.regions.getById(region_id, function(region) {
                self.data.regions.current = region.attributes;

                app.regions.getByCountryWithout(region_id, region.get('country_id'), function(collection) {
                    self.data.regions.others = _.map(collection.models, function(m) {
                        return m.attributes;
                    });

                    self.data.regions.others = _.filter(self.data.regions.others, function(m) {
                        if (parseInt(m.cash_transfers, 0) !== 0) {
                            return m;
                        }
                    });

                    self.render(self.data);
                });
            });

            this.checkReset();
        },
        checkReset: function() {
            //Show or hide reset button
            if (app.filters.get('active')) {
                this.$resetFiltersBtn.css('display', 'block');
            } else {
                this.$resetFiltersBtn.css('display', 'none');
            }
            return this;
        },
        hideReset: function() {
            this.$resetFiltersBtn.css('display', 'none');
            return this;
        }
    });

    return BreadcrumbsView;

});