define(function() {
    'use strict';

    var Area = Backbone.Model.extend({
        defaults: {
            id: null,
            code: null,
            name: null,
            cash_transfers: null,
            scale: null,
            feature: null
        }
    });

    var AreasCollection = Backbone.Collection.extend({
        model: Area,
        url: function() {
            return Routing.generate('ajax_simbiotica_map_continents', _.omit(app.filters.attributes, ['continent', 'country', 'region', 'active']));
        },
        parse: function(data) {
            var model = _.map(data, function(m) {
                return {
                    id: m.id,
                    cartodb_id: m.cartodb_id,
                    code: m.code,
                    name: m.name,
                    cash_transfers: m.project_count,
                    budget_allocated: m.budget_allocated,
                    feature: {
                        'type': 'Feature',
                        'properties': {
                            'id': m.id,
                            'code': m.code,
                            'name': m.name,
                            'cash_transfers': m.project_count,
                            'scale': m.scale_sum,
                            'budget_allocated': m.budget_allocated,
                            'level': 1,
                            'current': 0
                        },
                        'geometry': $.parseJSON(m.centroid)
                    }
                };
            });
            return model;
        },
        getAll: function(callback) {
            if (this.isEmpty()) {
                this.fetch({
                    dataType: 'json',
                    success: function(collection) {
                        if (callback && typeof callback === 'function') {
                            callback(collection);
                        }
                    },
                    error: function(res, err) {
                        $.error(err.responseText);
                    }
                });
            } else {
                if (callback && typeof callback === 'function') {
                    callback(this);
                }
            }
        },
        getAllWithout: function(id, callback) {
            function processCollection(collection) {
                var model, othermodels, result;

                model = collection.get(id);
                othermodels = _.without(collection.models, model);
                result = new AreasCollection(othermodels);

                if (callback && typeof callback === 'function') {
                    callback(result);
                }
            }

            if (this.isEmpty()) {
                this.getAll(processCollection);
            } else {
                processCollection(this);
            }
        },
        getById: function(id, callback) {
            function processCollection(collection) {
                var model = collection.get(id);

                if (callback && typeof callback === 'function') {
                    callback(model);
                }
            }

            if (this.isEmpty()) {
                this.getAll(processCollection);
            } else {
                processCollection(this);
            }
        }
    });

    return AreasCollection;
});