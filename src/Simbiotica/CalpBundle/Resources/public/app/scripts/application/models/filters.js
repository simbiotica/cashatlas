define(function() {
    'use strict';

    var FiltersModel = Backbone.Model.extend({
        defaults: {
            continent: [],
            country: [],
            region: [],
            donor: [],
            organization: [],
            sector: [],
            modality: [],
            deliveryMechanism: [],
            deliveryAgent: [],
            implementingPartners: [],
            scaleMin: '',
            scaleMax: '',
            status: [],
            years: [],
            duration: [],
            keyword: [],
            level: 'world',
            active: false
        },
        initialize: function() {
            Backbone.Events.on('selection:world', this.setWorld, this);
            Backbone.Events.on('selection:area', this.setArea, this);
            Backbone.Events.on('selection:country', this.setCountry, this);
            Backbone.Events.on('selection:region', this.setRegion, this);
            Backbone.Events.on('filter:change', this.setFilter, this);
            Backbone.Events.on('filter:reset', this.resetFilter, this);
        },
        setWorld: function() {
            this.set({
                continent: [],
                country: [],
                region: [],
                level: 'world'
            });

            app.map.currentView = 'world';

            Backbone.Events.trigger('filter:done');
        },
        setArea: function(id) {
            this.set({
                continent: [id],
                level: 'areas'
            });

            app.map.currentView = 'areas';

            Backbone.Events.trigger('filter:done');
        },
        setCountry: function(id) {
            this.set({
                country: [id],
                level: 'countries'
            });

            app.map.currentView = 'countries';

            Backbone.Events.trigger('filter:done');
        },
        setRegion: function(id) {
            this.set({
                region: [id],
                level: 'regions'
            });

            app.map.currentView = 'regions';

            Backbone.Events.trigger('filter:done');
        },
        setFilter: function(params) {
            var model = {},
                status = {};

            status = { active: true };
            model = _.extend({}, this.defaults, this.attributes, status, params);

            this.clear();
            this.set(model);

            Backbone.Events.trigger('filter:done');
        },
        resetFilter: function() {
            var model = {};

            model = _.extend({}, this.defaults, { continent: [], country: [], region: [] }, { level: 'world' }, { active: false });

            this.clear();
            this.set(model);

            Backbone.Events.trigger('filter:done');
        }
    });

    return FiltersModel;

});