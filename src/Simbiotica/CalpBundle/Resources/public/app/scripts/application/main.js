require.config({
    paths: {
        jqueryform: '../../../vendor/jquery-form/jquery.form',
        validate: '../../../vendor/jquery.validation/jquery.validate',
        mousewheel: '../../../vendor/jscrollpane/script/jquery.mousewheel',
        select2: '../../../vendor/select2/select2',
        jqueryui: '../../lib/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom',
        handlebars: '../../../vendor/handlebars/handlebars',
        d3: '../../../vendor/d3/d3',
        dateutils: '../../lib/date-utils',
        helpers: '../../lib/helpers',
        text: '../../../vendor/requirejs-text/text'
    },
    shim: {
        jqueryform: {
            exports: '$'
        },
        validate: {
            exports: '$'
        },
        select2: {
            exports: '$'
        },
        jqueryui: {
            exports: '$'
        },
        handlebars: {
            exports: 'Handlebars'
        },
        helpers: {
            deps: ['handlebars'],
            exports: 'Handlebars'
        },
        d3: {
            exports: 'd3'
        }
    }
});

require([
    'models/areas',
    'models/countries',
    'models/regions',
    'models/filters',
    'models/projects',
    'models/info',

    'views/general/overlay',
    'views/general/contact_overlay',

    'views/map/breadcrumbs',
    'views/map/spin',
    'views/map/map',

    'views/dashboard/dashboard',
    'views/dashboard/filters_form',
    'views/dashboard/filters_list',
    'views/dashboard/info',
    'views/dashboard/project_detail',
    'views/dashboard/projects_list',
    'views/dashboard/spin'
], function(
    AreasCollection,
    CountriesCollection,
    RegionsCollection,
    FiltersModel,
    Projects,
    InfoModel,

    OverlayView,
    ContactOverlayView,

    BreadcrumbsView,
    SpinMapView,
    MapView,

    DashboardView,
    FiltersFormView,
    FiltersListView,
    InfoView,
    ProjectDetailView,
    ProjectsListView,
    SpinView
) {
    'use strict';

    // Models - Map
    app.filters = new FiltersModel();
    app.info = new InfoModel();

    // Collections
    app.areas = new AreasCollection();
    app.countries = new CountriesCollection();
    app.regions = new RegionsCollection();

    // Views
    app.spinMap = new SpinMapView();
    app.map = new MapView();
    app.breadcrumbs = new BreadcrumbsView();
    app.filtersList = new FiltersListView();

    app.spin = new SpinView();
    app.dashboard = new DashboardView();
    app.infoView = new InfoView();

    // Only If user is not logued
    if (!AUTH) {
        // Views
        app.overlay = new OverlayView();
    }

    // Only If user is logued
    if (AUTH) {
        // Collections
        app.projectFull = new Projects.full();
        app.projects = new Projects.collection();

        // Views
        app.filtersForm = new FiltersFormView();
        app.projectsList = new ProjectsListView();
        app.projectDetail = new ProjectDetailView();
        app.contactOverlay = new ContactOverlayView();
    }

    Backbone.Events.trigger('selection:world'); //starting app
    
});