define([
    'handlebars',
    'text!../../../../templates/projects_list.handlebars'
], function(Handlebars, tpl) {
    'use strict';

    var ProjectsListView = Backbone.View.extend({
        el: '#projectsList',
        initialize: function() {
            Backbone.Events.on('info:done', this.start, this);
            Backbone.Events.on('info:update', this.clear, this);
        },
        template: Handlebars.compile(tpl),
        render: function(data) {
            var self = this,
                pag = 0,
                moreProjectsBtn;

            this.items = [];

            function onClickProject(e) {
                var $this, projectId;

                $this = $(e.currentTarget);

                if ($this.hasClass('active')) {
                    Backbone.Events.trigger('project_detail:hide');
                    $this.removeClass('active');
                    return $this;
                }

                projectId = $this.data('project');

                self.items.removeClass('active');
                $this.addClass('active');

                Backbone.Events.trigger('selection:project', projectId);
            }


            Backbone.Events.trigger('ajax:stop');

            pag = 0;

            self.$el.html(this.template({
                projects: data
            }));

            self.items = self.$el.find('.project_item');

            moreProjectsBtn = $('#moreProjectsBtn');

            function projectsListPaginator() {
                pag++;

                self.items.each(function(i, el) {
                    if (i < 20 * pag) {
                        $(el).css('display', 'block');
                    }
                });

                if (pag * 20 > self.items.length) {
                    moreProjectsBtn.hide();
                } else if (20 > self.items.length) {
                    moreProjectsBtn.hide();
                } else {
                    moreProjectsBtn.show();
                }
            }

            projectsListPaginator();

            self.items.on('click', onClickProject);

            moreProjectsBtn.on('click', function(e) {
                e.preventDefault();
                projectsListPaginator();
            });

            // Generating url to export
            $('#exportListToExcel').attr('href', Routing.generate('simbiotica_calp_export_excel', _.extend({}, app.filters.toJSON(), {
                format: 'xlsx'
            })));
            $('#exportListToCSV').attr('href', Routing.generate('simbiotica_calp_export_excel', _.extend({}, app.filters.toJSON(), {
                format: 'csv'
            })));

            Backbone.Events.trigger('projectsList:done');
        },
        start: function(projectsArr) {
            var self = this;

            if (!projectsArr) {
                return false;
            }

            if (!AUTH) {
                return false;
            }

            this.clear();

            Backbone.Events.trigger('ajax:start', this.el);

            app.projects.getAll(projectsArr, function(collection) {
                var models;
                if (collection.length > 0) {
                    models = _.map(collection.models, function(m) {
                        return m.attributes;
                    });
                } else {
                    models = null;
                }

                self.render(models);
            });
        },
        clear: function() {
            this.$el.empty();
        }
    });

    return ProjectsListView;

});