define(function() {
    'use strict';

    var MapView = Backbone.View.extend({
        id: 'mapVisualization',
        API: 'https://cashatlas.cartodb.com/api/v2/sql?format=GeoJSON&q=',
        tables: {
            areas: 'continent_sync',
            countries: 'country_sync',
            regions: 'region_sync',
            region: 'region_sync'
        },
        currentView: 'world',
        currentId: null,
        currentDataType: 'scale',
        layers: {
            info: [],
            current: null,
            nocurrent: null,
            region: null
        },
        // Geom Styles
        styles: {
            global: '#{{table_name}} [status=1]{ line-smooth: 0.2; polygon-fill: #fff; polygon-opacity: 0; line-width: 1; line-color: #b5d5df; line-opacity: 1; } [status=2]{ line-smooth: 0; polygon-fill: transparent; polygon-opacity: 0.65; line-width: 2; line-color: #40A6D2; line-opacity: 1; polygon-pattern-file:url(http://www.cash-atlas.org/bundles/simbioticacalp/dist/images/assets/bg/tile-blue.png);polygon-pattern-opacity: 1;} [status=3] { line-smooth: 0; polygon-fill: #0593D8; polygon-pattern-opacity: 0.4; polygon-opacity: 0; line-width: 2; line-color: #085685; line-opacity: 1;polygon-pattern-file:url(http://www.cash-atlas.org/bundles/simbioticacalp/dist/images/assets/bg/tile-blue.png)}',
        },
        initialize: function() {
            this.$body = $('body');
            this.wrap = $('#map');
            this.createMap();

            Backbone.Events.on('sidebar:toggle', this.toggleMap, this);
            Backbone.Events.on('filter:done', this.applyFilters, this);
            Backbone.Events.on('datatype:change', this.setDataType, this);
        },
        createMap: function() {
            var self, map, tiles, attribution;

            self = this;

            map = L.map(this.id, {
                center: [0, 0],
                zoom: 2,
                /*worldCopyJump: true,*/
                maxZoom: 10,
                minZoom: 2,
                maxBounds: new L.LatLngBounds(new L.LatLng(-100, -200), new L.LatLng(100, 200)),
                attributionControl: false
            });

            attribution = L.control.attribution({
                position: 'bottomleft',
                prefix: ''
            })
                .addTo(map);

            /*
            L.tileLayer('/bundles/simbioticacalp/img/assets/map-bg-tile.png', {
                attribution: 'Simbiótica'
            }).addTo(map); // Tile layer is a must (bug opacity on zoom)
            */
            //tiles = L.tileLayer('http://{s}.tiles.mapbox.com/v3/d4weed.map-ygw7ut6s/{z}/{x}/{y}.png', {
            tiles = L.tileLayer('http://{s}.tiles.mapbox.com/v3/simbiotica.map-16ay03ku/{z}/{x}/{y}.png', {
                noWrap: true,
                attribution: '<a href="http://www.mapbox.com/about/maps/" target="_blank">MapBox</a>'
            }).addTo(map);

            this.map = map;
        },
        toggleMap: function(active, sidebarWidth) {
            var self = this;

            if (active) {
                this.wrap.css('right', sidebarWidth + 'px');
            } else {
                this.wrap.css('right', '27px');
            }

            setTimeout(function() {
                self.map.invalidateSize(true);
            }, 310); // Animation in CSS
        },
        showWorld: function() {
            var self, geomQuery;

            self = this;
            geomQuery = 'SELECT cartodb_id AS id, name, the_geom_webmercator, \'area\' as type, 1 as status FROM ' + this.tables.areas;

            this.currentId = null;
            // Always first remove or hide layers
            this._removeLayers();

            this.currentView = 'world';

            app.areas.getAll(function(collection) {
                self._infoLayer(collection, 'nocurrent', function(model) {
                    Backbone.Events.trigger('selection:area', model.id);
                });
                Backbone.Events.trigger('breadcrumbs:update', self.currentView);
            });

            self._geomLayer('global', geomQuery, false);

            this.map.fitWorld();
        },
        showArea: function(area_id) {
            var self, area, queries;

            self = this;
            area = app.areas.get(area_id);
            queries = {
                geom: 'SELECT cartodb_id AS id, name, the_geom_webmercator, \'area\' as type, 1 as status FROM ' + this.tables.areas + ' WHERE cartodb_id != \'' + area.get('cartodb_id') + '\' UNION SELECT cartodb_id AS id, name, the_geom_webmercator, \'country\' as type, 2 as status FROM ' + this.tables.countries + ' WHERE continent_id = \'' + area.get('cartodb_id') + '\''
            };

            this.currentView = 'areas';
            this.currentId = area_id;
            // Always first remove or hide layers
            this._removeLayers();

            // Geometries
            self._geomLayer('global', queries.geom, false);

            // Continents
            app.areas.getAllWithout(area_id, function(collection) {
                self._infoLayer(collection, 'nocurrent', function(model) {
                    Backbone.Events.trigger('selection:area', model.id);
                });
            });

            // Countries
            app.countries.getByArea(area_id, function(collection) {
                self._infoLayer(collection, 'current', function(model) {
                    Backbone.Events.trigger('selection:country', model.id);
                }, function(layer) {
                    //Provisional hack to have custom bounds in East Asia
                    if (self.currentId !== 8) {
                        self.map.fitBounds(layer.getBounds());
                    } else {
                        self.map.fitBounds(new L.LatLngBounds(new L.LatLng(-18, 88), new L.LatLng(30, 155)));
                    }
                });
                Backbone.Events.trigger('breadcrumbs:update', self.currentView);
            });
        },
        showCountry: function(country_id) {
            var self, country, queries;

            self = this;
            country = app.countries.get(country_id);

            this.currentView = 'countries';
            this.currentId = country_id;
            // Always first remove or hide layers
            this._removeLayers();

            queries = {
                geom: 'SELECT cartodb_id AS id, name, the_geom_webmercator, \'country\' as type, 1 as status FROM ' + this.tables.countries + ' WHERE cartodb_id != \'' + country.get('cartodb_id') + '\' UNION SELECT cartodb_id AS id, name,  the_geom_webmercator, \'region\' as type, 2 as status FROM ' + this.tables.regions + ' WHERE country_id = \'' + country.get('cartodb_id') + '\''
            };

            // Geometries
            //this.map.off('moveend').on('moveend', function() {
            self._geomLayer('global', queries.geom, false);

            self.map.off('moveend');

            // Countries
            app.countries.getAllWithout(country_id, function(collection) {
                self._infoLayer(collection, 'nocurrent', function(model) {
                    Backbone.Events.trigger('selection:country', model.id);
                });
            });

            // Regions
            app.regions.getByCountry(country_id, function(collection) {
                self._infoLayer(collection, 'current', function(model) {
                    Backbone.Events.trigger('selection:region', model.id);
                }, function(layer) {
                    self.map.fitBounds(layer.getBounds());
                });
                Backbone.Events.trigger('breadcrumbs:update', self.currentView);
            });
        },
        showRegion: function(region_id) {
            var self, geoData, region, region_code, country_id;

            function createLayer(geojson) {
                var geoData = new L.GeoJSON(geojson, {
                    style: {
                        fillColor: '#0593D8',
                        color: '#085685',
                        opacity: 1,
                        fillOpacity: 0.65,
                        weight: 3
                    }
                });

                self.map.fitBounds(geoData.getBounds());

                self.layers.region = geoData;
            }

            self = this;
            geoData = null;

            // Always first remove or hide layers
            this._removeLayers();

            this.currentView = 'regions';
            this.currentId = region_id;

            if (this.layers.region) {
                this.map.removeLayer(this.layers.region);
            }

            //TODO change this to query and/or get id
            region = app.regions.get(region_id);
            region_code = region.get('cartodb_id');
            country_id = region.get('country_id');

            this.layers.nocurrent.setCartoCSS('#{{table_name}} [status=1]{ line-smooth: 0.2; polygon-fill: #fff; polygon-opacity: 0; line-width: 1; line-color: #b5d5df; line-opacity: 1; }  [ status=2] { line-smooth: 0; polygon-fill: transparent; polygon-pattern-opacity: 1; polygon-opacity: 0; line-width: 2; line-color: #40A6D2; line-opacity: 1;polygon-pattern-file:url(http://www.cash-atlas.org/bundles/simbioticacalp/dist/images/assets/bg/tile-blue.png)} [id=' + region_code + '] { line-smooth: 0; polygon-fill: #0593D8; polygon-opacity: 0.65; line-width: 3; line-color: #40A6D2; line-opacity: 1; }');

            // Regions
            app.regions.getByCountry(country_id, function(collection) {
                self._infoLayer(collection, 'current', function(model) {
                    if (self.currentId !== model.id) {
                        Backbone.Events.trigger('selection:region', model.id);
                    }
                });
            });

            app.regions.getById(region_id, function(model) {
                createLayer(model.get('geometry'));
                Backbone.Events.trigger('breadcrumbs:update', self.currentView);
            });
        },
        applyFilters: function() {
            var self = this;

            this._removeLayers();

            switch (app.filters.get('level')) {
            case 'world':
                app.areas.fetch({
                    success: function() {
                        self.showWorld();
                    }
                });
                break;
            case 'areas':
                app.areas.fetch();

                app.countries.fetch({
                    success: function() {
                        self.showArea(app.filters.get('continent')[0]);
                    }
                });
                break;
            case 'countries':
                app.areas.fetch();
                app.countries.fetch();
                app.regions.fetch({
                    success: function() {
                        self.showCountry(app.filters.get('country')[0]);
                    }
                });
                break;
            case 'regions':
                app.areas.fetch();
                app.countries.fetch();

                app.regions.fetch({
                    //url: Routing.generate('ajax_simbiotica_map_regions', $.extend({}, { region: [self.currentId] }, app.filters.attributes)),
                    success: function() {
                        self.showRegion(app.filters.get('region')[0]);
                    }
                });
                break;
            }
        },
        setDataType: function(datatype) {
            this.currentDataType = datatype;
            this._removeLayers();
            switch (app.filters.get('level')) {
            case 'world':
                this.showWorld();
                break;
            case 'areas':
                this.showArea(app.filters.get('continent')[0]);
                break;
            case 'countries':
                this.showCountry(app.filters.get('country')[0]);
                break;
            case 'regions':
                this.showRegion(app.filters.get('region')[0]);
                break;
            }
        },
        _geomLayer: function(style, query, current, callback) {
            var self, cartoLayer, currentInfowindow, activeInfowindow, currentZoom, timer, geom_style;

            function _onFeatureOver(e, latlng, pos, data) {
                currentZoom = self.map.getZoom();
                document.body.style.cursor = 'pointer';
                if (currentInfowindow === data.name && currentZoom >= 3) {
                    if (timer) {
                        clearTimeout(timer);
                    }
                    timer = setTimeout(function() {
                        if (!activeInfowindow) {
                            Backbone.Events.trigger('infowindow:show', self.currentView, {
                                pos: pos,
                                info: data
                            });
                        }
                        activeInfowindow = true;
                    }, 200);
                } else {
                    currentInfowindow = data.name;
                    activeInfowindow = false;
                }
            }

            function _onFeatureOut() {
                document.body.style.cursor = 'default';
                currentInfowindow = null;
                clearTimeout(timer);
                Backbone.Events.trigger('infowindow:hide');
            }

            function _showLayer() {
                if (current) {
                    self.layers.current.show();
                } else {
                    self.layers.nocurrent.show();
                }
            }

            self = this;
            cartoLayer = null;
            currentInfowindow = null;
            activeInfowindow = false;
            currentZoom = this.map.getZoom();
            timer = null;
            geom_style = this.styles[style];

            if (current) {
                cartoLayer = this.layers.current;
            } else {
                cartoLayer = this.layers.nocurrent;
            }

            if (!cartoLayer) {
                cartodb.createLayer(self.map, {
                    type: 'cartodb',
                    options: {
                        user_name: 'cashatlas',
                        table_name: 'bug',
                        query: query,
                        tile_style: geom_style,
                        interactivity: 'id, name'
                    }
                })
                    .on('done', function(layer) {
                        layer.on('loading', function() {
                            Backbone.Events.trigger('selection:start');
                        });

                        layer.on('load', function() {
                            Backbone.Events.trigger('selection:done');
                        });

                        if (!current) {
                            layer.on('featureOver', _onFeatureOver).on('featureOut', _onFeatureOut);
                        }

                        if (current) {
                            self.layers.current = layer;
                        } else {
                            self.layers.nocurrent = layer;
                        }

                        self.map.addLayer(layer);

                        _showLayer();

                        if (callback && typeof callback === 'function') {
                            callback(layer);
                        }
                    })
                    .on('error', function(errors) {
                        $.error(errors);
                    });
            } else {
                cartoLayer.setQuery(query);
                cartoLayer.setCartoCSS(geom_style);
                _showLayer();
                if (callback && typeof callback === 'function') {
                    callback(cartoLayer);
                }
            }
        },
        _onGeomAreaClick: function(e, latlng, pos, data) {
            Backbone.Events.trigger('selection:area', data.id);
        },
        _onGeomCountryClick: function(e, latlng, pos, data) {
            Backbone.Events.trigger('selection:country', data.id);
        },
        _hideGeomLayer: function() {
            if (this.layers.nocurrent) {
                this.layers.nocurrent.hide();
            }
            if (this.layers.current) {
                this.layers.current.hide();
            }
            if (this.layers.region) {
                this.map.removeLayer(this.layers.region);
            }
        },
        _infoLayer: function(collection, className, clickEvent, callback) {
            var self, features, geojson, geoData, models;
            var markerStyle, infoMarkerStyle, nameMarkerStyle, htmlIcon, name, icon, marker, scale, cash_transfers, budget_allocated;
            //var currentDataType = this.currentDataType;
            var options = {};

            function createLayers(feature, latlng) {
                markerStyle = self._calculateMarkerStyle(feature.properties.level, feature.properties[self.currentDataType], 3, feature.properties.name, 'cash-transfers');

                infoMarkerStyle = 'style="font-size: ' + markerStyle.fontSize + 'px; line-height: ' + markerStyle.iconSize + 'px; width:' + markerStyle.iconSize + 'px; height:' + markerStyle.iconSize + 'px;"';
                nameMarkerStyle = '';

                htmlIcon = '<div class="inner-marker">';
                name = feature.properties.name || '';
                scale = (feature.properties.scale) ? feature.properties.scale.toString().dots() : 'Unknown';
                cash_transfers = (feature.properties.cash_transfers) ? feature.properties.cash_transfers.toString().dots() : 'Unknown';
                budget_allocated = (feature.properties.budget_allocated) ? feature.properties.budget_allocated.toString().dots() : 'Unknown';

                if (feature.properties.scale > 0 && self.currentDataType === 'scale') {
                    htmlIcon += '<div class="info-marker" data-cash-transfers="' + feature.properties.cash_transfers + '" data-scale="' + feature.properties.scale + '" data-level="' + feature.properties.level + '" ' + infoMarkerStyle + '>' + scale + '</div>';
                    htmlIcon += '<div class="name-marker active" ' + nameMarkerStyle + '>' + name + '</div>';
                } else if (feature.properties.cash_transfers > 0 && self.currentDataType === 'cash_transfers') {
                    htmlIcon += '<div class="info-marker" data-cash-transfers="' + feature.properties.cash_transfers + '" data-scale="' + feature.properties.scale + '" data-level="' + feature.properties.level + '" ' + infoMarkerStyle + '>' + cash_transfers + '</div>';
                    htmlIcon += '<div class="name-marker active" ' + nameMarkerStyle + '>' + name + '</div>';
                } else if (feature.properties.budget_allocated > 0 && self.currentDataType === 'budget_allocated') {
                    htmlIcon += '<div class="info-marker" data-cash-transfers="' + feature.properties.budget_allocated + '" data-scale="' + feature.properties.budget_allocated + '" data-level="' + feature.properties.level + '" ' + infoMarkerStyle + '>' + CURRENCYDATA[CURRENCY].symbol + ' ' + budget_allocated + '</div>';
                    htmlIcon += '<div class="name-marker active" ' + nameMarkerStyle + '>' + name + '</div>';
                } else {
                    htmlIcon += '<div class="name-marker" ' + nameMarkerStyle + '>' + name + '</div>';
                }

                htmlIcon += '</div>';

                icon = L.divIcon({
                    className: (self.currentId === feature.properties.id) ? 'circle-marker current-region ' + className : 'circle-marker ' + className,
                    iconSize: new L.Point(markerStyle.iconSize, markerStyle.iconSize),
                    html: htmlIcon
                });

                marker = L.marker(latlng, {
                    icon: icon
                });

                return marker;
            }

            function setEvents(feature, layer) {
                if (feature.properties.cash_transfers > 0) {
                    layer.on('click', function() {
                        clickEvent.apply(this, [feature.properties]);
                    }).on('mouseover', function() {
                        $(this._icon).css('z-index', 10000);
                    }).on('mouseout', function() {
                        $(this._icon).css('z-index', 10);
                    });
                }
            }

            self = this;
            models = collection.models;
            options = {
                pointToLayer: createLayers,
                onEachFeature: setEvents
            };

            features = _.map(models, function(model) {
                return model.get('feature');
            });
            geojson = {
                type: 'FeatureCollection',
                features: features
            };
            geoData = L.geoJson(geojson, options);

            geoData.addTo(this.map);

            // Name marker position
            $('.name-marker').each(function(index, el) {
                var $el = $(el),
                    w = $el.width();
                $el.css('margin-left', ((-w - 12) / 2) + 'px');
            });

            this.layers.info.push(geoData);

            this.$body
                .removeClass('world')
                .removeClass('areas')
                .removeClass('countries')
                .removeClass('regions')
                .addClass(this.currentView);

            if (callback && typeof callback === 'function') {
                callback(geoData);
            }
        },
        _removeLayers: function() {
            var i, len;
            len = this.layers.info.length;
            if (len > 0) {
                for (i = 0; i < len; i++) {
                    this.map.removeLayer(this.layers.info[i]);
                }
                this.layers.info = [];
            }
        },
        _toggleMarkers: function(type) {
            var self = this;
            $('.info-marker').each(function() {
                var $this = $(this);
                $(this).text($(this).attr('data-' + type));
                var markerStyle = self._calculateMarkerStyle($(this).attr('data-level'), $(this).attr('data-' + type), 3, $this.next().text(), type);
                $this.css({
                    'top': markerStyle.textTop + 'px',
                    'margin-left': markerStyle.textPadding + 'px',
                    'font-size': markerStyle.fontSize + 'px'
                }).parent().css({
                    'width': markerStyle.iconSize + 'px',
                    'height': markerStyle.iconSize + 'px',
                    'margin-left': -markerStyle.iconSize / 2 + 'px',
                    'margin-top': -markerStyle.iconSize / 2 + 'px'
                }).next().css({
                    'top': markerStyle.nameTop + 'px',
                    'margin-left': markerStyle.namePadding + 'px'
                });
            });
        },
        _calculateMarkerStyle: function(level, amount, zoom, name) { // TO-DO
            var levelMultiplier, typeMultiplier, iconSizeAdjusted, charWidth, charHeight, fontSize, textSize, nameSize, textPadding, namePadding, textTop, nameTop, minBubbleSize, maxBubbleSize;
            amount = Number(amount);
            levelMultiplier = (level > 2) ? 3 : (level > 1) ? 0.5 : 0.3;
            typeMultiplier = (this.currentDataType === 'scale') ? 1 / 38000 : 2;
            minBubbleSize = (this.currentDataType === 'scale') ? 50 : 20;
            maxBubbleSize = (this.currentDataType === 'scale') ? 200 : 100;
            iconSizeAdjusted = (amount > 0) ? Math.min(Math.max(zoom * level * levelMultiplier * amount * typeMultiplier, minBubbleSize), maxBubbleSize) : 16;
            charWidth = iconSizeAdjusted / Math.max(amount.toString().dots().length, 3);
            //charHeight = iconSizeAdjusted / 2;
            fontSize = (amount > 0) ? Math.max(charWidth * 1.5, 11) : 16;
            if (iconSizeAdjusted === minBubbleSize) {
                fontSize = 11;
            }
            charHeight = fontSize;
            textSize = (amount) ? amount.toString().dots().length * charWidth : charWidth;
            nameSize = (name) ? name.toString().length * 8.5 : 0;
            textPadding = (iconSizeAdjusted < textSize) ? (iconSizeAdjusted - textSize) / 2 : 0;
            namePadding = (iconSizeAdjusted < nameSize) ? (iconSizeAdjusted - nameSize) / 2 : 0;
            textTop = (amount > 0) ? (iconSizeAdjusted / 2 - charHeight / 2) : 0;
            nameTop = (amount > 0) ? (iconSizeAdjusted / 2) + (charHeight / 1) : 0;

            return {
                'iconSize': iconSizeAdjusted,
                'textTop': textTop,
                'textPadding': textPadding,
                'fontSize': fontSize,
                'nameTop': nameTop,
                'namePadding': namePadding,
                'nameSize': nameSize
            };
        }
    });

    return MapView;

});