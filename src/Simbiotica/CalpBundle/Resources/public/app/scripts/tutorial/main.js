(function(Reveal) {

    Reveal.initialize({
        height: 674,
        width: 1099,
        margin: 0,
        rollingLinks: false
    });

} (Reveal));