require.config({
    paths: {
        jquery: '../../../vendor/jquery/jquery',
        validate: '../../../vendor/jquery.validation/jquery.validate',
        select2: '../../../vendor/select2/select2'
    },
    shim: {
        validate: {
            deps: ['jquery'],
            exports: 'jQuery.fn.validate'
        },
        select2: {
            deps: ['jquery'],
            exports: 'jQuery.fn.select2'
        }
    }
});

require(['validate', 'select2'], function() {
    'use strict';

    var fixFooter = {
        vars: {
            winHeight: 0,
            bodyHeight: 0
        },
        initialize: function() {
            var self = this;
            this.$body = $('body');
            this.$el = $('.main_footer');
            this.vars.winHeight = window.innerHeight;
            this.vars.bodyHeight = this.$body.height();
            this.checkWindowHeight();
            $(window).on('resize', function(e) {
                self.checkWindowHeight(e);
            });
        },
        checkWindowHeight: function(e) {
            if (e) {
                this.vars.winHeight = e.currentTarget.innerHeight;
            }
            if (this.vars.winHeight > this.vars.bodyHeight) {
                this.$el.addClass('fixed');
            } else {
                this.$el.removeClass('fixed');
            }
        }
    };

    var loginRegister = {

        initialize: function() {
            var self = this;

            $('form').validate();
            $('select').select2({
                width: 'element'
            });

            // Registration
            if ($('#regiterForm').length >= 1) {
                this.$regOrgSelect = $('#fos_user_registration_form_organizations_0_organization');
                this.$regOthers = $('#regOther');
                this.$regOtherInput = this.$regOthers.find('input');

                this.$regOrgSelect.on('change', function() {
                    self.hideOthers();
                });

                this.$regOrgSelect.select2('val', '');

                this.hideOthers();
            }
        },

        hideOthers: function() {
            if (this.$regOrgSelect.select2('val') !== '') {
                this.$regOthers.hide();
                this.$regOtherInput.val('');
            } else {
                this.$regOthers.show();
            }
        }

    };

    loginRegister.initialize();
    fixFooter.initialize();

});