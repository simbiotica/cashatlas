/*global $*/

(function (document) {
    'use strict';

    var $document = $(document),
        evCounter = 0;

    var actions =  {
        init: function() {
            this.selects();
            this.tooltip(); // Enable tooltip on info
            this.loading();
            this.showOtherField();
        },
        selects: function() {
            var $selects = $('select');
            
            if ($selects.length > 0) {
                $selects.select2({
                    width: 'resolve'
                });
            }
        },
        tooltip: function() {
            var $links = $('.navbar-text a');
            //var $helpers = $('.help-block');

            $links.tooltip({
                placement: 'bottom'
            });

            $('.btn-similar-project').popover();

            // $helpers.each(function(i, el) {
            //     // $(el).tooltip({
            //     //     placement: 'bottom',
            //     //     title: $(el).text()
            //     // }).addClass('icon-info-sign').text('');
            //     $(el).prepend('<span class="icon-info-sign"></span>');
            // });
        },
        loading: function(method) {
            var $els = $('a.btn.sonata-ba-action');

            if ($els && method === 'stop') {
                return $els.button('reset');
            }

            $els.die('click').live('click', function() {
                $(this).button('loading');
            });
        },
        showOtherField: function() {
            var valueMarkerAssessment;
            var markerAssessment = $('.marker_assessment');
            var otherMarkerAssessment = markerAssessment.find('.choice-plus-other-other');
            var inputOtherMarkerAssessment = otherMarkerAssessment.find('input');

            var valueSectors;
            var sectors = $('.sectors');
            var otherSectors = sectors.find('.choice-plus-other-other');
            var inputOtherSectors = otherSectors.find('input');

            var valueTypeIntervention;
            var typeIntervention = $('select.type_of_intervention');
            var inputOtherTypeIntervention = $('.type_of_intervention_specify');
            var otherTypeIntervention = inputOtherTypeIntervention.parent().parent();
            

            markerAssessment.find('select').on('change', function (e) {
                valueMarkerAssessment = $(e.currentTarget).val();
                if (parseInt(valueMarkerAssessment) === 3) {
                    otherMarkerAssessment.removeClass('hidden');
                } else {
                    otherMarkerAssessment.addClass('hidden');
                    inputOtherMarkerAssessment.val('');
                }
            });

            sectors.find('select').on('change', function (e) {
                var result = false;

                valueSectors = $(e.currentTarget).val();

                for (var i = 0, len = valueSectors.length; i < len; i++) {
                    if (parseInt(valueSectors[i]) === 1) {
                        result = true;
                    }
                }

                if (result) {
                    otherSectors.removeClass('hidden');
                } else {
                    otherSectors.addClass('hidden');
                    inputOtherSectors.val('');
                }
            });

            typeIntervention.on('change', function (e) {
                valueTypeIntervention = $(e.currentTarget).val();
                if (parseInt(valueTypeIntervention) === 2) {
                    otherTypeIntervention.removeClass('hidden');
                } else {
                    otherTypeIntervention.addClass('hidden');
                    inputOtherTypeIntervention.val('');
                }
            });

            valueTypeIntervention = typeIntervention.val();

            if (parseInt(valueTypeIntervention) === 2) {
                otherTypeIntervention.removeClass('hidden');
            } else {
                otherTypeIntervention.addClass('hidden');
                inputOtherTypeIntervention.val('');
            }
        }
    };

    var projectForm = {
        init: function() {
            var self = this;

            this.$el = $('#projectForm');

            if (this.$el.length === 0) {
                return false;
            }

            this.locationSelect();
            this.modalitiesSelect();
            this.amount();
        },
        locationSelect: function() {
            var self = this;
            var $locations = $('.locations');
            var rows;

            $locations = $($locations[$locations.length - 1]);
            rows = $locations.find('tbody tr');

            function eachLocations(i, el) {
                var $row = $(el);
                var $countrySelect = $row.find('select.country-select');
                var $regionSelect = $row.find('select.region-select');

                function appendRegions(data) {
                    var html = '';
                    var choice;
                    var value = $regionSelect.val();

                    for (var item in data.choices) {
                        choice = data.choices[item];
                        html += '<optgroup label="' + item + '">';
                        for (var element in choice) {
                            html += '<option value="' + element + '">' + choice[element] + '</option>';
                        }
                        html += '</optgroup>';
                    }

                    $regionSelect
                        .html(html).removeAttr('disabled')
                        .select2('enable').select2('val', value);
                }

                function onCountryChange() {
                    var value = $(this).val();

                    if (value) {
                        $regionSelect.select2('disable');
                        $.ajax({
                            url: Routing.generate('ajax_simbiotica_location_regions'),
                            dataType: 'json',
                            data: { countries: value },
                            success: appendRegions
                        });
                    } else {
                        $regionSelect.select2('val', '');
                    }
                }

                $countrySelect.on('change', onCountryChange);
            }

            $.each(rows, eachLocations);
        },
        modalitiesSelect: function() {
            var $modalities = $('.one_to_many.modalities');

            function getData(values, callback) {
                $.ajax({
                    url: Routing.generate('ajax_simbiotica_modalities'),
                    dataType: 'json',
                    type: 'POST',
                    data: {modality: values},
                    success: callback
                });
            }

            function addData(data, $target) {
                var htmlOptions = '<option value="">- no value -</option>';

                $target.select2('disable').attr('disabled', 'disabled');

                if (!data || data.length === 0) {
                    return $target.html(htmlOptions).select2('val', '');
                }

                for (var item in data) {
                    htmlOptions += '<option value="' + item + '">' + data[item] + '</option>';
                }

                $target.html(htmlOptions).select2('val', '').select2('enable').removeAttr('disabled');
            }

            function modalities() {
                var $this = $(this);
                var $selects = $this.find('select');
                var $modality = $this.find('select.modality_select');
                var $deliveryMechanism = $this.find('select.delivery_mechanism_select');
                var $deliveryAgent = $this.find('select.delivery_agent_select');

                $selects.select2({width: 'element'});

                $selects.each(function(i, el) {
                    var sel = $(el);
                    if (sel.select2('val') === '') {
                        sel.select2('val', '');
                    }
                })

                $modality.on('change', function(e) {
                    var values = [$(e.currentTarget).val()];
                    getData(values, function(data) {
                        addData(data, $deliveryMechanism);
                    });
                    $deliveryAgent.select2('disable').select2('val', '');
                });

                $deliveryMechanism.on('change', function(e) {
                    var values = [$modality.select2('val'), $(e.currentTarget).val()];
                    getData(values, function(data) {
                        addData(data, $deliveryAgent);
                    });
                });
            }

            function init() {
                $modalities.find('table').each(function(i, el) {
                    modalities.apply(el);
                });
            }

            $modalities.on('sonata.add_element', function() {
                init();
            });

            init();
        },
        amount: function () {
            var scaleSum = $('.scale-sum');
            var amountSum = $('.amount-sum');
            var inputScale = $('.scale');
            var inputAmount = $('.amount');
            var scaleTotal;
            var amountTotal;
            var val;

            function sumScale() {
                scaleTotal = 0;

                $.each(inputScale, function(i, el) {
                    val = parseInt(el.value) || 0;
                    scaleTotal = scaleTotal + val;
                });

                scaleSum.val(scaleTotal);
            }

            function sumAmount() {
                amountTotal = 0;

                $.each(inputAmount, function(i, el) {
                    val = parseInt(el.value) || 0;
                    amountTotal = amountTotal + val;
                });

                amountSum.val(amountTotal);
            }

            inputScale.on('change', sumScale);

            inputAmount.on('change', sumAmount);

            sumScale();
            sumAmount();
        }
    };

    function initialize() {
        var navbar = $('.navbar-fixed-top'),
            body = $('body');

        function calculateHeight() {
            if (navbar.height() > 66) {
                body.css('padding-top', '130px');
            } else {
                body.css('padding-top', '90px');
            }
        }

        calculateHeight();

        // Common actions
        actions.init();

        // Project form
        projectForm.init();

        $document.live('sonata.add_element', function(e) {
            evCounter = evCounter + 1;
            if (evCounter === 1) {
                actions.selects();
                actions.loading();
                projectForm.locationSelect();
            } else {
                evCounter = 0;
            }
            projectForm.amount();
        });

        $document.live("sonata.open_modal", function(e) {
            actions.loading('stop');
            actions.selects();
            $('ul.a2lix_translationsLocales').find('a').off('click').on('click', function(evt) {
                evt.preventDefault();
                $(this).tab('show');
            });
            $('ul.a2lix_translationsLocales a:first').tab('show');
        });

        $(window).on('resize', function() {
            calculateHeight();
        });
    }

    $document.on('ready', initialize);

}(document));