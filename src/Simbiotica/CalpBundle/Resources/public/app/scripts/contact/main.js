require.config({
    paths: {
        jquery: '../../../vendor/jquery/jquery',
        validate: '../../../vendor/jquery.validation/jquery.validate',
        select2: '../../../vendor/select2/select2'
    },
    shim: {
        validate: {
            deps: ['jquery'],
            exports: 'jQuery.fn.validate'
        },
        select2: {
            deps: ['jquery'],
            exports: 'jQuery.fn.select2'
        }
    }
});

require(['validate', 'select2'], function() {
    'use strict';

    var contactForm = {

        initialize: function() {
            var self = this;

            this.$el = $('#contactForm');
            this.$page = $('html, body');
            this.$typeSelect = this.$el.find('.type');
            this.$context = this.$el.find('.context');
            this.$selects = this.$el.find('select');

            this.$selects.select2({
                width: '240px',
                closeOnSelect: false,
                allowClear: true
            });

            this.$selects.each(function(i, sel) {
                if ($(sel).val() === '') {
                    $(sel).select2('val', '');
                }
            });

            this.$el.validate({
                debug: true,
                ignore: '.select2-input',
                submitHandler: function() {
                    self.submitForm();
                    return false;
                }
            });

            this.$typeSelect.on('change', function(e) {
                self.onTypeChange(e);
            });
        },

        swapForm: function(data) {
            this.$el.html(data);
            this.initialize();
        },

        submitForm: function() {
            var self = this;

            $.ajax({
                url: this.$el.attr('action'),
                dataType: 'html',
                type: 'POST',
                data: this.$el.serializeArray(),
                success: function(data) {
                    self.swapForm(data);
                }
            });
        },

        onTypeChange: function(e) {
            var self, value;

            self = this;
            value = $(e.currentTarget).val();

            $.ajax({
                url: Routing.generate('ajax_simbiotica_contact_form'),
                dataType: 'html',
                data: {
                    context: this.$context.val(),
                    provider: value,
                    showProvider: true
                },
                success: function(data) {
                    self.swapForm(data);
                }
            });
        }

    };

    contactForm.initialize();

});