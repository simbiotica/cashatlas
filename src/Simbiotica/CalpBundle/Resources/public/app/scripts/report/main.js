'use strict';

require.config({
    paths: {
        'ReportModel': 'models/report',
        'SpinView': 'views/spin',
        'FormView': 'views/form',
        'GraphicsView': 'views/graphics'
    }
});

require(['SpinView', 'FormView', 'GraphicsView'], function(SpinView, FormView, GraphicsView) {
    var report = {};
    report.spin = new SpinView();
    report.graphics = new GraphicsView();
    report.FormView = new FormView();
});