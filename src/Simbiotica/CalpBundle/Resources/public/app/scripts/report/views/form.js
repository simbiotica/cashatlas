'use strict';

define(['ReportModel'], function(ReportModel) {
    var FormView = Backbone.View.extend({
        el: '#reportForm',
        events: {
            'click #btnSubmitForm': 'submitForm',
            'click #btnResetForm': 'resetForm',
            'click #markBtn': 'checkAll',
            'click #unmarkBtn': 'uncheckAll',
            'click #simbiotica_form_report_selector_0': 'toggleSelect',
            'click #simbiotica_form_report_selector_1': 'toggleSelect'
        },
        initialize: function () {
            this.$page = $('html, body');
            this.$selects = $('select');
            this.$orgSelect = $('#organisationSelect').find('select');
            this.$donorSelect = $('#donorSelect').find('select');
            this.$checkboxes = $('.checkboxes input');

            this.$selects.select2({
                width: 'element',
                closeOnSelect: false,
                allowClear: true
            });

            if (this.getQueryVariable('donor%5B%5D')) {
                this.$donorSelect.select2('enable');
                this.$orgSelect.select2('disable');
            } else if (this.getQueryVariable('organizations%5B%5D')) {
                this.$donorSelect.select2('disable');
                this.$orgSelect.select2('enable');
            } else {
                this.$donorSelect.select2('disable');
            }

            this.$el.find('.btn').removeAttr('disabled'); // Activate buttons on load

            Backbone.Events.trigger('spinner:stop');
        },
        submitForm: function (e) {
            //var self = this;
            Backbone.Events.trigger('spinner:start');
            ReportModel.fetch({
                data: this.$el.serialize(),
                success: function(collection) {
                    Backbone.Events.trigger('graphics:start', collection.toJSON());
                    Backbone.Events.trigger('spinner:stop');
                }
            });
            // this.$el.ajaxSubmit({
            //     success: function (data) {
            //         graphics.render(data);
            //         Backbone.Events.trigger('spinner:stop');
            //         self.$page.animate({
            //             'scrollTop': 870
            //         }, 500);
            //     }
            // });
            e.preventDefault();
        },
        resetForm: function () {
            this.$selects.select2('val', '');
            return false;
        },
        checkAll: function () {
            this.$checkboxes.prop('checked', true);
            return false;
        },
        uncheckAll: function () {
            this.$checkboxes.prop('checked', false);
            return false;
        },
        toggleSelect: function (e) {
            var value = $(e.currentTarget).val();

            this.$orgSelect.select2('disable');
            this.$donorSelect.select2('disable');

            if (value === 'organization') {
                this.$orgSelect.select2('enable');
            } else if (value === 'donor') {
                this.$donorSelect.select2('enable');
            }
        },
        getQueryVariable: function (variable) {
            var query = window.location.search.substring(1),
                vars = query.split('&'),
                pair;
            for (var i = 0; i < vars.length; i++) {
                pair = vars[i].split('=');
                if (pair[0] === variable) {
                    return true;
                }
            }
            return (false);
        }
    });

    return FormView;
});