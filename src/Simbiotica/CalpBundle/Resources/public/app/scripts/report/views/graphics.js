/*global d3*/
'use strict';

define(function () {
    var GraphicsView = Backbone.View.extend({
        el: '#graphics',
        events: {
            'click #printBtn': 'print'
        },
        initialize: function() {
            Backbone.Events.on('graphics:start', this.render, this);
        },
        render: function (data) {
            var self, template,
                color = d3.scale.category20();

            self = this;

            function renderAction() {
                var params = $('#reportForm').serializeArray();

                self.$el.removeClass('hidden');

                data.reportTitle = {};

                _.each(data, function (values, key, list) {
                    if (key !== 'timeline' && key !== 'totals' && key !== 'error') {
                        list[key] = _.map(values, function (v) {
                            v.color = color(v.name);
                            return v;
                        });
                    }
                });

                if (data.totals) {
                    data.reportTitle.intro = 'This report shows <strong>' + data.totals.number + '</strong> projects with a total of <strong>' + data.totals.scale + ' beneficiaries</strong> and an overall amount of <strong>' + data.totals.amount + ' €</strong>.';
                }

                var organizations = _.filter(params, function (p) {
                    return p.name === 'simbiotica_form_report[organization][]';
                });

                data.reportTitle.organizations = _.map(organizations, function (p) {
                    return $('select[name="simbiotica_form_report[organization][]"]').find('option[value="' + p.value + '"]').text();
                });

                var donors = _.filter(params, function (p) {
                    return p.name === 'simbiotica_form_report[donor][]';
                });

                data.reportTitle.donors = _.map(donors, function (p) {
                    return $('select[name="simbiotica_form_report[donor][]"]').find('option[value="' + p.value + '"]').text();
                });

                var countries = _.filter(params, function (p) {
                    return p.name === 'simbiotica_form_report[country][]';
                });

                data.reportTitle.countries = _.map(countries, function (p) {
                    return $('select[name="simbiotica_form_report[country][]"]').find('option[value="' + p.value + '"]').text();
                });

                var fromYear = $('select[name="simbiotica_form_report[after][year]"]');
                var fromMonth = $('select[name="simbiotica_form_report[after][month]"]');

                data.reportTitle.fromYear = fromYear.find('option[value="' + fromYear.val() + '"]').text();
                data.reportTitle.fromMonth = fromMonth.find('option[value="' + fromMonth.val() + '"]').text();

                var toYear = $('select[name="simbiotica_form_report[before][year]"]');
                var toMonth = $('select[name="simbiotica_form_report[before][month]"]');

                data.reportTitle.toYear = toYear.find('option[value="' + toYear.val() + '"]').text();
                data.reportTitle.toMonth = toMonth.find('option[value="' + toMonth.val() + '"]').text();

                template = Handlebars.compile(self.template);
                self.$el.html(template(data));

                if (!data.error) {
                    if (data.donors) {
                        self.createPie('#donorBudget', data.donors, 'amount');
                        self.createBars('#donorNumber', data.donors, 'number');
                        self.createBars('#donorScale', data.donors, 'scale');
                    }
                    if (data.organizations) {
                        self.createPie('#organizationBudget', data.organizations, 'amount');
                        self.createPie('#organizationNumber', data.organizations, 'number');
                        self.createBars('#organizationScale', data.organizations, 'scale');
                    }

                    if (data.sectors) {
                        self.createBars('#sectorNumber', data.sectors, 'number');
                    }
                    if (data.modalities) {
                        self.createBars('#modalityNumber', data.modalities, 'number');
                    }
                    if (data.delivery_mechanisms) {
                        self.createBars('#deliveryMechanismNumber', data.delivery_mechanisms, 'number');
                    }
                    if (data.delivery_agents) {
                        self.createBars('#deliveryAgentNumber', data.delivery_agents, 'number');
                    }
                    if (data.conditionalities) {
                        self.createPie('#conditionalityNumber', data.conditionalities, 'number');
                    }
                    if (data.contexts) {
                        self.createBars('#contextNumber', data.contexts, 'number');
                    }
                    if (data.timeline) {
                        self.createSpark('#timelineNumber', data.timeline, 'number');
                    }

                }
            }

            if (typeof this.template === 'string') {
                renderAction();
            } else {
                this.template(renderAction);
            }

            return this;
        },
        template: function (callback) {
            var self = this;
            $.ajax({
                url: '/bundles/simbioticacalp/dist/templates/report.handlebars',
                dataType: 'html',
                success: function (layout) {
                    self.template = layout;
                    if (callback && typeof callback === 'function') {
                        callback();
                    }
                }
            });
        },
        print: function () {
            window.print();
        },
        createPie: function (selector, data, unit) {
            //var formatPercent = d3.format(".0%"); 
            function formatPercent(number) {
                return Math.round(number * 100) > 0 ? Math.round(number * 100).toString() + '%' : '';
            }
            var totalData = d3.sum(data, function (d) {
                return d[unit];
            });

            function prescale(number) {
                return (number / totalData);
            }
            //var color = d3.scale.category20();

            var pie = d3.layout.pie()
                .value(function (d) {
                    return d[unit];
                })
                .sort(null);

            var arc = d3.svg.arc()
                .innerRadius(30)
                .outerRadius(85);

            var svg = d3.select(selector).append('svg')
                .attr('width', 290)
                .attr('height', 200)
                .append('g')
                .attr('transform', 'translate(145,100)');

            var path = svg.selectAll('path')
                .data(pie(data))
                .enter().append('path')
                .attr('fill', function (d, i) {
                    return d.data.color;
                })
                .attr('d', arc);

            svg.selectAll('g').data(pie(data))
                .enter().append('text')
                .attr('transform', function (d) {
                    return 'translate(' + arc.centroid(d) + ')';
                })
                .attr('dy', '.35em')
                .style('text-anchor', 'middle').style('font-size', '10px').style('fill', '#fff').style('font-weight', 'bold')
                .text(function (d) {
                    return formatPercent(prescale(d.data[unit]));
                });
        },
        createBars: function (selector, data, unit) {
            var color = d3.scale.category20();
            var margin = {
                top: 20,
                right: 20,
                bottom: 30,
                left: 50
            },
                width = 300 - margin.left - margin.right,
                height = 200 - margin.top - margin.bottom;

            var x = d3.scale.ordinal()
                .rangeRoundBands([0, width], .1);

            var formatInt = d3.format('d');

            var y = d3.scale.linear()
                .range([height, 1]);

            var xAxis = d3.svg.axis()
                .scale(x)
                .orient('bottom');

            var yAxis = d3.svg.axis()
                .scale(y)
                .orient('left').tickFormat(formatInt);;

            var svg = d3.select(selector).append('svg')
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom)
                .append('g')
                .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

            x.domain(data.map(function (d) {
                return d.name;
            }));
            y.domain([0, d3.max(data, function (d) {
                return d[unit];
            })]);

            svg.append('g')
                .attr('class', 'x axis')
                .attr('transform', 'translate(0,' + height + ')');
            /* .call(xAxis); */

            svg.append('g')
                .attr('class', 'y axis')
                .call(yAxis)
                .append('text')
                .attr('transform', 'rotate(-90)')
                .attr('y', 6)
                .attr('dy', '.71em')
                .style('text-anchor', 'end');
            /* .text('Frequency'); */

            svg.selectAll('.bar')
                .data(data)
                .enter().append('rect')
                .attr('class', 'bar')
                .attr('fill', function (d, i) {
                    return d.color;
                })
                .attr('x', function (d) {
                    return x(d.name);
                })
                .attr('width', x.rangeBand())
                .attr('y', function (d) {
                    return y(d[unit]);
                })
                .attr('height', function (d) {
                    return height - y(d[unit]);
                });
        },
        createSpark: function (selector, data, unit) {
            var year;

            var margin = {
                top: 20,
                right: 20,
                bottom: 30,
                left: 50
            },
                width = 960 - margin.left - margin.right,
                height = 400 - margin.top - margin.bottom;

            var parseDate = d3.time.format('%d-%m-%Y').parse;

            var x = d3.time.scale()
                .range([0, width]);

            var y = d3.scale.linear()
                .range([height, 0]);

            var xAxis = d3.svg.axis()
                .scale(x)
                .orient('bottom');

            var yAxis = d3.svg.axis()
                .scale(y)
                .orient('left');

            var line = d3.svg.line()
                .x(function (d) {
                    return x(d.date);
                })
                .y(function (d) {
                    return y(d.number);
                });

            var svg = d3.select(selector).append('svg')
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom)
                .append('g')
                .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');
            var dataArray = [];
            var yearsArray = [];

            for (year in data) {
                yearsArray.push(Number(year));
            }

            for (var i = yearsArray[0]; i < yearsArray[yearsArray.length - 1] + 1; i++) {

                for (var j = 1; j < 13; j++) {
                    var look = data.hasOwnProperty(i) ? (data[i].hasOwnProperty(j) ? data[i][j][unit] : 0) : 0;
                    var date = {
                        date: parseDate('1-' + j + '-' + i),
                        number: look
                    };
                    dataArray.push(date);
                }
            }

            dataArray.forEach(function (d) {
                d.number = +d.number;
            });

            x.domain(d3.extent(dataArray, function (d) {
                return d.date;
            }));
            y.domain(d3.extent(dataArray, function (d) {
                return d.number;
            }));

            svg.append('g')
                .attr('class', 'x axis')
                .attr('transform', 'translate(0,' + height + ')')
                .call(xAxis);

            svg.append('g')
                .attr('class', 'y axis')
                .call(yAxis)
                .append('text')
                .attr('transform', 'rotate(-90)')
                .attr('y', 6)
                .attr('dy', '.71em')
                .style('text-anchor', 'end');
            /* .text('Price ($)'); */

            svg.append('path')
                .datum(dataArray)
                .attr('class', 'line')
                .attr('d', line);
        }
    });

    return GraphicsView;
});