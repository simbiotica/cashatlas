/*global Routing*/
'use strict';

define(function() {
    var ReportModel = Backbone.Model.extend({
        url: Routing.generate('ajax_simbiotica_report_generate'),
        parse: function(data) {
            return data;
        }
    });

    return new ReportModel();
});