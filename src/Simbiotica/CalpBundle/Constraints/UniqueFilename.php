<?php

namespace Simbiotica\CalpBundle\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueFilename extends Constraint
{
	public $message = 'Duplicate file name';
	
	public function validatedBy()
	{
		return 'unique_filename';
	}
	
	public function getTargets()
	{
		return self::CLASS_CONSTRAINT;
	}
}