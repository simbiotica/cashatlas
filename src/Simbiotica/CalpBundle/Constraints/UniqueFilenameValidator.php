<?php

namespace Simbiotica\CalpBundle\Constraints;

use Sonata\AdminBundle\Validator\ErrorElement;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @Annotation
 */
class UniqueFilenameValidator extends ConstraintValidator
{
	private $entityManager;

	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

	public function validate($value, Constraint $constraint)
	{
		if (in_array($value->getProviderName(), array('simbiotica.media.provider.youtube', 'simbiotica.media.provider.vimeo', 'simbiotica.media.provider.flash.video')))
			return;
		
		$filename = $value->getProviderReference();
		$id = $value->getId();
		
		if ($value->getFilename() != '')
		{
			if ($value->getBinaryContent() instanceof UploadedFile) {
				$path_info = pathinfo($value->getBinaryContent()->getClientOriginalName());
				$filename = $value->getFilename().'.'.$path_info['extension'];
			} elseif ($value->getBinaryContent() instanceof File) {
				$path_info = pathinfo($value->getBinaryContent()->getBasename());
				$filename = $value->getFilename().'.'.$path_info['extension'];
			} else {
				$filename = $value->getFilename();
			}
		}
		elseif ($value->getBinaryContent() instanceof UploadedFile) {
			$filename = $value->getBinaryContent()->getClientOriginalName();
		} elseif ($value->getBinaryContent() instanceof File) {
			$filename = $value->getBinaryContent()->getBasename();
		} else $filename = $value->getBinaryContent();
		 
		$query = $this->entityManager->createQuery('
				SELECT count(DISTINCT p.providerReference) 
				FROM Simbiotica\CalpBundle\Entity\Media p 
				WHERE p.providerReference LIKE :filename'.((isset($id)&&trim($id)!='')?(' AND p.id != '.$id):''))
		->setParameter('filename', $filename);

		$files = $query->getSingleScalarResult();

		if ($files)
		{
			$this->context
				->addViolation($constraint->message);
		}
	}
}