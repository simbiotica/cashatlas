<?php

namespace Simbiotica\CalpBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Simbiotica\CalpBundle\DependencyInjection\Compiler\AddContactProviderCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SimbioticaCalpBundle extends Bundle
{
    const CACHE_TTL_SMALL = 100;
    const CACHE_TTL_MEDIUM = 300;
    const CACHE_TTL_LARGE = 3600;
    
    const CONTACT_STATUS_OPEN = 0;
    const CONTACT_STATUS_CLOSED = 1;
    
    const VALIDATION_INCOMPLETE = 0;
    const VALIDATION_COMPLETE = 1;
    
    const PUBLISHED_UNPUBLISHED = 0;
    const PUBLISHED_REVISION = 2;
    const PUBLISHED_PUBLIC = 3;
    
    const LANGUAGE_EN = 'en_US';
    const LANGUAGE_FR = 'fr_FR';
    const LANGUAGE_ES = 'es_ES';
    
    const INTERVENTION_CASH_TRANS_ONLY = 1;
    const INTERVENTION_CASH_TRANS_KIND = 2;
    
    const INTENT_VIEW = 1;
    const INTENT_EDIT = 2;
    
    const CONTEXT_URBAN = 1;
    const CONTEXT_RURAL_SEDENTARY = 2;
    const CONTEXT_RURAL_PASTORALIST = 3;
    
    const MARKET_ASSESS_COMMON = 1;
    const MARKET_ASSESS_SPECIFIC = 2;
    const MARKET_ASSESS_NOT_ASSESSED = 0;
    const MARKET_ASSESS_OTHER = 3;
    
    const MODALITY_CONDITIONAL_GRANT = 1;
    const MODALITY_UNCONDITIONAL_GRANT = 2;
    const MODALITY_VOUCHERS = 3;
    const MODALITY_VOUCHERS_FOR_WORK = 4;
    const MODALITY_CASH_FOR_WORK = 5;
    const MODALITY_CASH_FOR_TRAINING = 29;
    const MODALITY_DIRECT_CASH = 6;
    const MODALITY_BANK_TRANSFER = 7;
    const MODALITY_CHEQUES = 8;
    const MODALITY_MOBILE_PHONE = 9;
    const MODALITY_SMART_CARD = 10;
    const MODALITY_PREPAID_CARD = 11;
    const MODALITY_DIGITAL_CASH = 12;
    const MODALITY_DIGITAL_COMMODITY = 13;
    const MODALITY_DIGITAL_SERVICE = 14;
    const MODALITY_PAPER_CASH = 15;
    const MODALITY_PAPER_COMMODITY = 16;
    const MODALITY_PAPER_SERVICE = 17;
    const MODALITY_AID_AGENCY_DIRECTLY = 18;
    const MODALITY_GOVERNMENT = 19;
    const MODALITY_BANK = 20;
    const MODALITY_POST_OFFICE = 21;
    const MODALITY_MICRO_FINANCE_INSTITUTION = 22;
    const MODALITY_REMITTANCE_COMPANY = 23;
    const MODALITY_SECURITY_COMPANY = 24;
    const MODALITY_TRADERS = 25;
    const MODALITY_TELECOM_COMPANY_MOBILE_MONEY = 26;
    const MODALITY_HAWALA = 27;
    const MODALITY_TELECOM_COMPANY_MOBILE_BANKING = 28;
    const MODALITY_OTHER = 0;
    
    const STATUS_UPCOMING = 1;
    const STATUS_ONGOING = 2;
    const STATUS_COMPLETED = 3;
    
    const COLUMN_ID = 0;
    const COLUMN_ORGANISATION = 1;
    const COLUMN_AFFILIATES = 2;
    const COLUMN_IMPLEMENTING_PARTNERS = 3;
    const COLUMN_COUNTRIES = 4;
    const COLUMN_REGIONS = 5;
    const COLUMN_SCALE_PER_REGION = 6;
    const COLUMN_NUM_CITIES_VILLAGES = 7;
    const COLUMN_CITIES_VILLAGES = 8;
    const COLUMN_SPECIFIC_OBJECTIVES = 9;
    const COLUMN_SECTORS = 10;
    const COLUMN_SPECIFY_IF_OTHER = 11;
    const COLUMN_DONORS = 12;
    const COLUMN_AMOUNT_PER_DONOR = 13;
    const COLUMN_CONTEXT = 14; // 17
    const COLUMN_START_DATE = 15;
    const COLUMN_FIRST_TRANSFER = 16;
    const COLUMN_END_DATE = 17;
    const COLUMN_MARKET_ASESSMENT_TOOL = 18;
    const COLUMN_MARKET_ASSESSMENT_TOOL_SPECIFY = 19;
    const COLUMN_COORDINATION_MECHANISM = 20;
    const COLUMN_COORDINATION_MECHANISM_SPECIFY = 21;
    const COLUMN_MODALITIES = 22;
    const COLUMN_MODALITIES_SPECIFY = 23;
    const COLUMN_DELIVERY_MECHANISM = 24;
    const COLUMN_DELIVERY_MECHANISM_SPECIFY = 25;
    const COLUMN_DELIVERY_AGENT = 26;
    const COLUMN_DELIVERY_AGENT_SPECIFY = 27;
    const COLUMN_TYPE_OF_INTERVENTION = 28; // 31
    const COLUMN_TYPE_OF_INTERVENTION_SPECIFY = 29;
    const COLUMN_NUMBER_OF_BENEFICIARIES = 30;
    const COLUMN_NUMBER_OF_TRANSFERS = 31;
    const COLUMN_AMOUNT_PER_TRANSFER = 32;
    const COLUMN_CONTACT_PERSON = 34;
    const COLUMN_POSITION = 35;
    const COLUMN_EMAIL = 36;
    const COLUMN_EMAIL2 = 37;
    const COLUMN_WEB = 38;
    const COLUMN_LANGUAGE = 39;
    const COLUMN_PROJECT_DESCRIPTION = 40;
    const COLUMN_PROJECT_DOCS = 41;
    const COLUMN_OWNER = 42;
    const COLUMN_CURRENCY = 43;
    
    const CURRENCY_EURO = 1;
    const CURRENCY_GBP = 2;
    const CURRENCY_DOLLAR = 3;
    
    const DURATION_1 = 1;
    const DURATION_2 = 2;
    const DURATION_3 = 3;
    const DURATION_4 = 4;
    const DURATION_5 = 5;
    
    const REPORT_FIELDS_DONORS = 1;
    const REPORT_FIELDS_ORGANIZATIONS = 2;
    const REPORT_FIELDS_SECTORS = 3;
    const REPORT_FIELDS_MODALITIES = 4;
    const REPORT_FIELDS_CONDITIONALITIES = 5;
    const REPORT_FIELDS_DELIVERY_MECHANISMS = 6;
    const REPORT_FIELDS_DELIVERY_AGENTS = 7;
    const REPORT_FIELDS_CONTEXTS = 8;
    
    
    static function getValidationStatus()
    {
        $fields = array(
            self::VALIDATION_COMPLETE => 'validation.complete',
            self::VALIDATION_INCOMPLETE => 'validation.incomplete',
        );
        
        return $fields;
    }
    
    static function getContactStatus()
    {
        $status = array(
            self::CONTACT_STATUS_OPEN => 'contact.status.open',
            self::CONTACT_STATUS_CLOSED => 'contact.status.close',
        );
        
        return $status;
    }
    
    static function getDurationRanges()
    {
        $fields = array(
            self::DURATION_1 => 'duration.one',
            self::DURATION_2 => 'duration.two',
            self::DURATION_3 => 'duration.three',
            self::DURATION_4 => 'duration.four',
            self::DURATION_5 => 'duration.five',
        );
        
        return $fields;
    }
    
    static function getIntents()
    {
        $fields = array(
            self::INTENT_VIEW => 'intent.view',
            self::INTENT_EDIT => 'intent.edit',
        );
        
        return $fields;
    }
    
    static function getReportFields()
    {
        $fields = array(
            self::REPORT_FIELDS_DONORS => 'report.fields.donors',
            self::REPORT_FIELDS_ORGANIZATIONS => 'report.fields.organizations',
            self::REPORT_FIELDS_SECTORS => 'report.fields.sectors',
            self::REPORT_FIELDS_MODALITIES => 'report.fields.modalities',
            self::REPORT_FIELDS_CONDITIONALITIES => 'report.fields.conditionalities',
            self::REPORT_FIELDS_DELIVERY_MECHANISMS => 'report.fields.delivery_mechanisms',
            self::REPORT_FIELDS_DELIVERY_AGENTS => 'report.fields.delivery_agents',
            self::REPORT_FIELDS_CONTEXTS => 'report.fields.contexts',
        );
        
        return $fields;
    }
    
    static function getYearRange()
    {
        $years = array();
        foreach (range(1990, 2020) as $year)
        {
            $years[$year] = $year;
        }
        
        return $years;
    }
    
    static function getMonths()
    {
        $months = array();
        
        $months[1] = 'month.jan';
        $months[2] = 'month.feb';
        $months[3] = 'month.mar';
        $months[4] = 'month.apr';
        $months[5] = 'month.may';
        $months[6] = 'month.jun';
        $months[7] = 'month.jul';
        $months[8] = 'month.aug';
        $months[9] = 'month.sep';
        $months[10] = 'month.oct';
        $months[11] = 'month.nov';
        $months[12] = 'month.dec';
        
        return $months;
    }
    
    static function getLanguages()
    {
        $languages = array(
                self::LANGUAGE_EN => 'en',
                self::LANGUAGE_FR => 'fr',
                self::LANGUAGE_ES => 'es',
        );
        
        return $languages;
    }
    
    static function getCurrencies()
    {
    	$languages = array(
    			self::CURRENCY_EURO => 'EUR',
    			self::CURRENCY_GBP => 'GBP',
    			self::CURRENCY_DOLLAR => 'USD',
    	);
    
    	return $languages;
    }
    
    public static function getPublishedStatus()
    {
        return array(
                self::PUBLISHED_PUBLIC => 'status.public',
                self::PUBLISHED_REVISION => 'status.revision',
                self::PUBLISHED_UNPUBLISHED => 'status.unpublished',
        );
    }

    static function getProjectStatus()
    {
        $status = array(
                self::STATUS_UPCOMING => 'status.upcoming',
                self::STATUS_ONGOING => 'status.ongoing',
                self::STATUS_COMPLETED => 'status.completed',
        );
        
        return $status;
    }
    
    public static function getMarketAsssessmentTypes()
    {
        $types = array(
                self::MARKET_ASSESS_COMMON => "market_assess.common",
                self::MARKET_ASSESS_SPECIFIC => "market_assess.specific",
                self::MARKET_ASSESS_NOT_ASSESSED => "market_assess.not_assessed",
                self::MARKET_ASSESS_OTHER => "Other", //added by DG
        );
        return $types;
    }
    
    public static function getInterventionTypes()
    {
        $types = array(
                self::INTERVENTION_CASH_TRANS_ONLY   => "intervention.cash_trans_only",
                self::INTERVENTION_CASH_TRANS_KIND => "intervention.cash_trans_kind",
        );
        return $types;
    }
    
    public static function getModalities(array $index = null, $level = null)
    {
        $modalities = array(
                self::MODALITY_CONDITIONAL_GRANT => array(
                        "label" => "modality.conditional_grant",
                        "values" => array(
                                self::MODALITY_DIRECT_CASH => array(
                                        "label" => "modality.direct_cash",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_MOBILE_PHONE => array(
                                        "label" => "modality.mobile_phone",
                                        "values" => array(
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_MONEY => array(
                                                        "label" => "modality.telecom_company_mobile_money",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_BANKING => array(
                                                        "label" => "modality.telecom_company_mobile_banking",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                                
                                        ),
                                ),
                                self::MODALITY_SMART_CARD => array(
                                        "label" => "modality.smart_card",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                                
                                        ),
                                ),
                                self::MODALITY_PREPAID_CARD => array(
                                        "label" => "modality.prepaid_card",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_BANK_TRANSFER => array(
                                        "label" => "modality.bank_transfer",
                                        "values" => array(
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_CHEQUES => array(
                                        "label" => "modality.cheques",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                        )
                ),
                self::MODALITY_UNCONDITIONAL_GRANT => array(
                        "label" => "modality.unconditional_grant",
                        "values" => array(
                                self::MODALITY_DIRECT_CASH => array(
                                        "label" => "modality.direct_cash",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_MOBILE_PHONE => array(
                                        "label" => "modality.mobile_phone",
                                        "values" => array(
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_MONEY => array(
                                                        "label" => "modality.telecom_company_mobile_money",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_BANKING => array(
                                                        "label" => "modality.telecom_company_mobile_banking",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                                
                                        ),
                                ),
                                self::MODALITY_SMART_CARD => array(
                                        "label" => "modality.smart_card",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_PREPAID_CARD => array(
                                        "label" => "modality.prepaid_card",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_BANK_TRANSFER => array(
                                        "label" => "modality.bank_transfer",
                                        "values" => array(
                                            self::MODALITY_BANK => array(
                                                "label" => "modality.bank",
                                                "values" => array(
                                                ),
                                            ),
                                        ),
                                ),
                                self::MODALITY_CHEQUES => array(
                                        "label" => "modality.cheques",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                        )
                ),
                self::MODALITY_VOUCHERS => array(
                        "label" => "modality.vouchers",
                        "values" => array(
                                self::MODALITY_DIGITAL_CASH => array(
                                        "label" => "modality.digital_cash",
                                        "values" => array(
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_MONEY => array(
                                                        "label" => "modality.telecom_company_mobile_money",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_BANKING => array(
                                                        "label" => "modality.telecom_company_mobile_banking",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_PAPER_CASH => array(
                                        "label" => "modality.paper_cash",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_DIGITAL_COMMODITY => array(
                                        "label" => "modality.digital_commodity",
                                        "values" => array(
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_MONEY => array(
                                                        "label" => "modality.telecom_company_mobile_money",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_BANKING => array(
                                                        "label" => "modality.telecom_company_mobile_banking",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_PAPER_COMMODITY => array(
                                        "label" => "modality.paper_commodity",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_DIGITAL_SERVICE => array(
                                        "label" => "modality.digital_service",
                                        "values" => array(
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_MONEY => array(
                                                        "label" => "modality.telecom_company_mobile_money",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_BANKING => array(
                                                        "label" => "modality.telecom_company_mobile_banking",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_PAPER_SERVICE => array(
                                        "label" => "modality.paper_service",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                ),
                self::MODALITY_CASH_FOR_WORK => array(
                        "label" => "modality.cash_for_work",
                        "values" => array(
                                self::MODALITY_DIRECT_CASH => array(
                                        "label" => "modality.direct_cash",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_MOBILE_PHONE => array(
                                        "label" => "modality.mobile_phone",
                                        "values" => array(
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_MONEY => array(
                                                        "label" => "modality.telecom_company_mobile_money",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_BANKING => array(
                                                        "label" => "modality.telecom_company_mobile_banking",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_SMART_CARD => array(
                                        "label" => "modality.smart_card",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_PREPAID_CARD => array(
                                        "label" => "modality.prepaid_card",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_BANK_TRANSFER => array(
                                        "label" => "modality.bank_transfer",
                                        "values" => array(
                                            self::MODALITY_BANK => array(
                                                "label" => "modality.bank",
                                                "values" => array(
                                                ),
                                            ),
                                        ),
                                ),
                                self::MODALITY_CHEQUES => array(
                                        "label" => "modality.cheques",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                ),
                self::MODALITY_VOUCHERS_FOR_WORK => array(
                        "label" => "modality.vouchers_for_work",
                        "values" => array(
                                self::MODALITY_DIGITAL_CASH => array(
                                        "label" => "modality.digital_cash",
                                        "values" => array(
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_MONEY => array(
                                                        "label" => "modality.telecom_company_mobile_money",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_BANKING => array(
                                                        "label" => "modality.telecom_company_mobile_banking",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_PAPER_CASH => array(
                                        "label" => "modality.paper_cash",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_DIGITAL_COMMODITY => array(
                                        "label" => "modality.digital_commodity",
                                        "values" => array(
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_MONEY => array(
                                                        "label" => "modality.telecom_company_mobile_money",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_BANKING => array(
                                                        "label" => "modality.telecom_company_mobile_banking",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_PAPER_COMMODITY => array(
                                        "label" => "modality.paper_commodity",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_DIGITAL_SERVICE => array(
                                        "label" => "modality.digital_service",
                                        "values" => array(
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_MONEY => array(
                                                        "label" => "modality.telecom_company_mobile_money",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_BANKING => array(
                                                        "label" => "modality.telecom_company_mobile_banking",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_PAPER_SERVICE => array(
                                        "label" => "modality.paper_service",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                ),
                self::MODALITY_CASH_FOR_TRAINING => array(
                        "label" => "modality.cash_for_training",
                        "values" => array(
                                self::MODALITY_DIRECT_CASH => array(
                                        "label" => "modality.direct_cash",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_MOBILE_PHONE => array(
                                        "label" => "modality.mobile_phone",
                                        "values" => array(
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_MONEY => array(
                                                        "label" => "modality.telecom_company_mobile_money",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TELECOM_COMPANY_MOBILE_BANKING => array(
                                                        "label" => "modality.telecom_company_mobile_banking",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                                
                                        ),
                                ),
                                self::MODALITY_SMART_CARD => array(
                                        "label" => "modality.smart_card",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                                
                                        ),
                                ),
                                self::MODALITY_PREPAID_CARD => array(
                                        "label" => "modality.prepaid_card",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_BANK_TRANSFER => array(
                                        "label" => "modality.bank_transfer",
                                        "values" => array(
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                                self::MODALITY_CHEQUES => array(
                                        "label" => "modality.cheques",
                                        "values" => array(
                                                self::MODALITY_AID_AGENCY_DIRECTLY => array(
                                                        "label" => "modality.aid_agency_directly",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_GOVERNMENT => array(
                                                        "label" => "modality.government",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_BANK => array(
                                                        "label" => "modality.bank",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_POST_OFFICE => array(
                                                        "label" => "modality.post_office",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_MICRO_FINANCE_INSTITUTION => array(
                                                        "label" => "modality.micro_finance_institution",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_REMITTANCE_COMPANY => array(
                                                        "label" => "modality.remittance_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_SECURITY_COMPANY => array(
                                                        "label" => "modality.security_company",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_TRADERS => array(
                                                        "label" => "modality.traders",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_HAWALA => array(
                                                        "label" => "modality.hawala",
                                                        "values" => array(
                                                        ),
                                                ),
                                                self::MODALITY_OTHER => array(
                                                        "label" => "modality.other",
                                                        "values" => array(
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                ),
        );
    
        return SimbioticaCalpBundle::parseDataArray($modalities, $index, $level);
    }
    
    public static function getContexts()
    {
        $contexts = array(
                self::CONTEXT_URBAN => "context.urban",
                self::CONTEXT_RURAL_SEDENTARY => "context.rural.sedentary",
                self::CONTEXT_RURAL_PASTORALIST => "context.rural.pastoralist",
        );
        
        return $contexts;
    }
    
    private static function parseDataArray(array $data, array $index = null, $level = null)
    {
        if (isset($index))
        {
            foreach($index as $key)
            {
                if ($key == null)
                    break;
                if (!array_key_exists($key, $data) || !array_key_exists('values', $data[$key]))
                    return array();
                $data = $data[$key]['values'];
            }
        }
        elseif(isset($level))
        {
            while ($level > 0)
            {
                $level--;
                $result = array();
                foreach ($data as $elem)
                {
                    $result += $elem['values'];
                }
                $data = $result;
            }
        }
        foreach($data as $key => $value)
        {
            if (array_key_exists('label', $value))
                $data[$key] = $value['label'];
        }
        
        return $data;
    }
    
    static public function slugify($text)
    {
        //replace acentuation
        $search = explode(",","ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,e,i,ø,u,ñ");
        $replace = explode(",","c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,e,i,o,u,n");
        $text = str_replace($search, $replace, $text);
        // replace all non letters or digits by -
        $text = preg_replace('/[^\w\/]+/', '-', $text);

        // trim and lowercase
        $text = strtolower(trim($text, '-'));

        return $text;
    }
    
    static public function unparse_url($parsed_url) {
        $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
        $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
        $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
        $user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
        $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
        $pass     = ($user || $pass) ? "$pass@" : '';
        $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
        $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
        $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
        return "$scheme$user$pass$host$port$path$query$fragment";
    }

    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new AddContactProviderCompilerPass());
    }

    public function getParent()
    {
        return 'SonataUserBundle';
    }
}
