<?php

namespace Simbiotica\CalpBundle\Command;

use Doctrine\Bundle\DoctrineBundle\Command\DoctrineCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use \Simbiotica\CalpBundle\Entity\Organization;

class MigrateDonorsCommand extends DoctrineCommand
{
    protected function configure()
    {
        $this
        ->setName('migrate:donors')
        ->setDescription('Move donor data into organizations')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $em->getFilters()->disable('softdeleteable');
        $donors = $em->getRepository('SimbioticaCalpBundle:Donor')->findAll();
        
        foreach($donors as $donor)
        {
            $organization = $em->getRepository('SimbioticaCalpBundle:Organization')->findOneByName($donor->getName());
            if ($organization)
            {
                if($organization->getDeletedAt() && !$donor->getDeletedAt())
                {
                    $organization->setDeletedAt(null);
                }
                $output->writeln('<comment>Organization '.$organization->getName().' exists</comment>');
            }
            else
            {
                $organization = new Organization();
                $organization->setName($donor->getName());
                $output->writeln('<info>Organization '.$organization->getName().' doesnt exist</info>');
                
                $repository = $em->getRepository('Gedmo\Translatable\Entity\Translation');
                $translations = $repository->findTranslations($organization);
                foreach($translations as $language => $translation)
                {
                    if (array_key_exists('name', $translation))
                        $repository->translate($organization, 'name', $language, $translation['name']);
                }
                
                $em->persist($organization);
            }
            
            $pds = $em->getRepository('SimbioticaCalpBundle:ProjectDonor')->findBy(array('donor' => $donor->getId()));
            foreach($pds as $pd)
            {
                $pd->setOrganization($organization);
            }
            $output->writeln('<info>Found '.count($pds).' PDs</info>');
            $em->flush();
        }
    }
}

?>