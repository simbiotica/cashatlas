<?php

namespace Simbiotica\CalpBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Simbiotica\CalpBundle\Entity\Project;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Sonata\NotificationBundle\Backend\BackendInterface;

class SimilarProjectGenerateCommand extends ContainerAwareCommand
{
    
    protected $notify;
    protected $output;
    protected $userId;
    protected $emailContent;
    
    public function __construct($name = null)
    {
        parent::__construct($name);
        
        $this->emailContent = '';
    }
    
    protected function configure()
    {
        $this
            ->setName('calp:similarProject:generate')
            ->setDescription('Generate similar projects')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $projects = $this->getContainer()->get('doctrine')->getRepository('SimbioticaCalpBundle:Project')->findAll();
        
        try
        {
            $backend = $this->getContainer()->get('sonata.notification.backend');
            foreach($projects as $project) {
                $backend->createAndPublish('comparison', array(
                    'projectId' => $project->getId(),
                ));
                $output->writeln('Project '.$project->getId().' sent for comparison');
            }
        } catch (\Exception $e) 
        {
            $message = \Swift_Message::newInstance()
                ->setSubject('RabbitMQ exception')
                ->setFrom($this->container->getParameter('mailer_from'))
                ->setTo($this->container->getParameter('notification_mail'))
                ->setBody($e->getMessage());
            
            $this->get('mailer')->send($message);

            $outpu->writeln('RabbitMQ Error!');
        }
    }
}
?>
