<?php

namespace Simbiotica\CalpBundle\Command;

use Doctrine\Bundle\DoctrineBundle\Command\DoctrineCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;

class RegenProjectsCommand extends DoctrineCommand
{
    protected function configure()
    {
        $this
        ->setName('regen:projects')
        ->setDescription('Updates projects autocalculated values')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $rep = $this->getContainer()->get('doctrine')->getRepository('SimbioticaCalpBundle:Project');
        $em = $this->getContainer()->get('doctrine')->getEntityManager();

        $projects = $rep->findAll();
        foreach ($projects as $project)
        {
            $validationList = $this->getContainer()->get('simbiotica.utils.project_validator')->validateProject($project);
            $project->setStatus(empty($validationList)?SimbioticaCalpBundle::VALIDATION_COMPLETE:SimbioticaCalpBundle::VALIDATION_INCOMPLETE);
        
            $em->persist($project);
            $em->flush();

            $output->writeln('<comment>Processes project '.$project->getId().'</comment>');
        }
    }
}

?>