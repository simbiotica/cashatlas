<?php

namespace Simbiotica\CalpBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Simbiotica\CalpBundle\Entity\Project;
use Simbiotica\CalpBundle\Entity\Organization;
use Simbiotica\CalpBundle\Entity\Modality;
use Simbiotica\CalpBundle\Entity\ProjectDonor;
use Simbiotica\CalpBundle\Entity\ProjectOrganization;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism;
use Simbiotica\CalpBundle\Entity\ProjectLocation;
use FOS\UserBundle\Model\User;

class ImportExcelCommand extends ContainerAwareCommand
{
    
    protected $notify;
    protected $output;
    protected $userId;
    protected $emailContent;
    
    public function __construct($name = null)
    {
        parent::__construct($name);
        
        $this->emailContent = '';
    }
    
    protected function configure()
    {
        $this
            ->setName('calp:excel:import')
            ->setDescription('Import Excel file(s) with projects')
            ->addOption(
                    'file',
                    null,
                    InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                    'Name of the files to be imported'
            )
            ->addOption(
                    'media',
                    null,
                    InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                    'Ids of the media to be imported'
            )
            ->addOption(
                    'dry-run',
                    null,
                    InputOption::VALUE_NONE,
                    'If given, no actual import is done'
            )
            ->addOption(
                    'notify',
                    null,
                    InputOption::VALUE_NONE,
                    'Send the user a report after the process is finished. If no user is specified, an email address must be provided.'
            )
            ->addOption(
                    'user',
                    null,
                    InputOption::VALUE_REQUIRED,
                    'User id who started the process'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $files = $input->getOption('file');
        $media = $input->getOption('media');
        $dryRun = $input->getOption('dry-run');
        $this->notify = $input->getOption('notify');
        $this->userId = $input->getOption('user')?:1;
        $this->output = $output;
        
        if (!$files && !$media) {
            $this->printError('You have to provide at least one file name or media id to be imported');
            exit(0);
        }
        
        $author = $this->getContainer()->get('doctrine')->getRepository('SimbioticaCalpBundle:User')->find($this->userId);

        if (is_bool($this->notify) && $this->notify) {
            if($input->getOption('user')) {
                $this->notify = $author->getEmail();
            } else {
                $output->writeln('<error>Notify option requires you to specify either a user id or an email address</error>');
                exit(0);
            }
        }

        $em = $this->getContainer()->get('doctrine')->getManager();
        $mediaRep = $this->getContainer()->get('doctrine')->getRepository('SimbioticaCalpBundle:Media');
        $pool = $this->getContainer()->get('sonata.media.pool');
        $projects = array();
        $errors = array();
        
        try {
            if ($dryRun) {
                $this->printInfo('Dry run - Printing summary');
                $this->printInfo('');
                foreach ($files as $file) {
                    $this->printInfo(''.$file.' to be imported');
                }
                $this->printInfo('');
                foreach ($media as $mediaId) {
                    $this->printInfo('Media with ID '.$mediaId.' to be imported');
                }
            } else {
                foreach($files as $file) {
                    $this->importFile(__DIR__.'/../Resources/excel/'.$file, $file, $projects, $errors, $output);
                }
                foreach($media as $key => $mediaId) {
                    $medium = $mediaRep->find($mediaId);
                    if (!$medium)
                    {
                        $this->printError('Media with id '.$mediaId.' does not exist');
                        exit(0);
                    }
                    if (strcmp($medium->getProviderName(), 'simbiotica.media.provider.excel') != 0 || strcmp($medium->getContext(), 'import') != 0)
                    {
                        $this->printError('Can only import "excel" media types of context "import"');
                        exit(0);
                    }

                    if(!$this->notify && $medium->getImportedAt() != null)
                    {
                        $dialog = $this->getHelperSet()->get('dialog');
                        if (!$dialog->askConfirmation($output, '<question>Media '.$mediaId.' has already been imported. Are you sure you want to import it again?</question>')) {
                            unset($media[$key]);
                            continue;
                        }
                    }

                    $provider = $pool->getProvider($medium->getProviderName());
                    $path = __DIR__.'/../../../../web'.$provider->generatePublicUrl($medium, 'reference');
                    $this->importFile($path, $medium->getId(), $projects, $errors, $output);
                }

                $this->printInfo('Done importing');

                if($files)
                {
                    $this->printInfo('');
                    $this->printInfo(count($files).' files imported sucessfully');
                    foreach($files as $file)
                    {
                        $this->printInfo($file);
                        $this->printInfo(count($projects[$file]).' projects imported sucessfully');
                        if($errors[$file])
                        {
                            $this->printError(count($errors[$file]).' projects with errors found:');
                            foreach($errors[$file] as $line => $lineErrors)
                            {
                                foreach($lineErrors as $lineError)
                                {
                                    $this->printError($line.': '.$lineError);
                                }
                            }
                        }
                    }
                }
                if($media)
                {
                    $this->printInfo('');
                    $this->printInfo(count($media).' media imported sucessfully');
                    foreach($media as $id)
                    {
                        //At this point, the GC has disposed of the existing media entity in memory
                        //so it need to be reloaded
                        $medium = $mediaRep->find($id);
                        $medium->setImportedAt(new \DateTime());
                        $em->persist($medium);

                        $this->printInfo('Media '.$id);
                        $this->printInfo(''.count($projects[$id]).' projects imported sucessfully');
                        if($errors[$id])
                        {
                            $this->printError(count($errors[$id]).' projects with errors found:');
                            foreach($errors[$id] as $line => $lineErrors)
                            {
                                foreach($lineErrors as $lineError)
                                {
                                    $this->printError($line.': '.$lineError);
                                }
                            }
                        }
                    }
                    $em->flush();
                }
            }
            if ($this->notify)
            {
                $message = \Swift_Message::newInstance()
                ->setSubject('Cash Atlas - Project Importation Report')
                ->setFrom($this->getContainer()->getParameter('mailer_from'))
                ->setTo($this->notify)
                ->setBody($this->getContainer()->get('templating')->render('SimbioticaCalpBundle:Emails:import_report.html.twig',
                                array('data' => $this->emailContent )), 'text/html');
                $this->getContainer()->get('mailer')->send($message);

                $mailer = $this->getContainer()->get('mailer');
                $spool = $mailer->getTransport()->getSpool();
                $transport = $this->getContainer()->get('swiftmailer.transport.real');
                $spool->flushQueue($transport);
            }
        } catch (\Exception $e) {
            if ($this->notify)
            {
                try {   
                    $message = \Swift_Message::newInstance()
                    ->setSubject('Cash Atlas - Project Importation Report')
                    ->setFrom($this->getContainer()->getParameter('mailer_from'))
                    ->setTo($this->notify)
                    ->setBody($this->getContainer()->get('templating')->render('SimbioticaCalpBundle:Emails:import_report.html.twig',
                                    array('data' => '<p style="color:red">An irrecoverable error occurred while importing your file. The system administrator has been notified, and the situation will be handled shortly.</p>' )), 'text/html');
                    $this->getContainer()->get('mailer')->send($message);
                } catch (Exception $e) {
                }

                try {
                    $message = \Swift_Message::newInstance()
                    ->setSubject('Cash Atlas - Project Importation Exception')
                    ->setFrom($this->getContainer()->getParameter('mailer_from'))
                    ->setTo($this->getContainer()->getParameter('mailer_to'))
                    ->setBody($this->getContainer()->get('templating')->render('SimbioticaCalpBundle:Emails:import_exception.html.twig', array(
                        'media' => (count($media)>=0?implode(', ', $media):'(none)'),
                        'files' => (count($files)>=0?implode(', ', $files):'(none)'),
                        'trace' => $e->getTrace(),
                        'message' => $e->getMessage(),
                        'notify' => $this->notify,
                    )), 'text/html');
                    $this->getContainer()->get('mailer')->send($message);
                } catch (Exception $e) {
                }

                $mailer = $this->getContainer()->get('mailer');
                $spool = $mailer->getTransport()->getSpool();
                $transport = $this->getContainer()->get('swiftmailer.transport.real');
                $spool->flushQueue($transport);
            } else {
                $this->printError($e->getMessage());
            }
        }
    }
    
    private function importFile($fileRoute, $name, &$projects, &$errors, $output)
    {
        $sheetName = "lookup";
        $projects[$name] = array();
        $errors[$name] = array();
        
        $objReader = \PHPExcel_IOFactory::createReaderForFile($fileRoute);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($fileRoute);
        
        $sheet = $objPHPExcel->getSheetByName($sheetName);
        $maxRow = $sheet->getHighestDataRow();

        $sheetData = $sheet->rangeToArray('A4:AR'.$maxRow);
        $sortedSheetData = array();
        
        foreach($sheetData as $row)
        {
            if ($row[0] != '0' && $row[0] != 'Example (information is not accurate, it is a random example)')
            {
                if (!array_key_exists(intval($row[0]), $sortedSheetData))
                    $sortedSheetData[intval($row[0])] = array();
                $sortedSheetData[intval($row[0])][] = $row;
            }
        }
        
        $output->writeln(count($sortedSheetData).' project found in file');
        
        foreach($sortedSheetData as $key => $data)
        {
            $output->writeln('<info>Importing project '.$key.'...');
            $result = $this->genProject($data);
            if (!is_array($result))
            {
                $output->writeln('Project imported sucessfully');
                $projects[$name][] =  $result;
            }
            else
            {
                $output->writeln('Errors found');
                foreach ($result as $line => $error)
                {
                    $output->writeln($line.': '.$error);
                }
                $errors[$name][$key] = $result;
            }
            $output->writeln('<info>');
        }
        
        $objPHPExcel->disconnectWorksheets();
        unset($objReader, $objPHPExcel);
    }

    private function genProject(array $input) {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $author = $this->getContainer()->get('doctrine')->getRepository('SimbioticaCalpBundle:User')->find($this->userId);

        $errors = array();
        $project = new Project();
        $project->setCreatedBy($author);
        $project->setUpdatedBy($author);
        $project->setPublished(SimbioticaCalpBundle::PUBLISHED_REVISION);

        foreach($input as $row)
        {
            foreach($row as $key => $data)
            {
                switch ($key) {
                    case SimbioticaCalpBundle::COLUMN_ID:
                        break;
                    case SimbioticaCalpBundle::COLUMN_ORGANISATION:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $organization = $this->getContainer()->get('doctrine')
                                ->getRepository('SimbioticaCalpBundle:Organization')->searchOrganization($data);
                            if (!$organization)
                            {
                                $organization = new Organization();
                                $organization->setName($data);
                                $organization->setCreatedBy($author);
                                $organization->setUpdatedBy($author);
                                
                                $em->persist($organization);
//                                $em->flush();
                            }
                            $organizationRelation = new ProjectOrganization();
                            $organizationRelation->setCreatedBy($author);
                            $organizationRelation->setUpdatedBy($author);
                            $project->addOrganization($organizationRelation);
                            $organization->addProject($organizationRelation);
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_AFFILIATES:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $project->setAffiliates($data);
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_IMPLEMENTING_PARTNERS:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $project->setImplementingPartners($data);
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_COUNTRIES:
                        if (!empty($data) && $data != '#N/A')
                        {
                             $country = $this->getContainer()->get('doctrine')
                                ->getRepository('SimbioticaCalpBundle:Country')->findOneByCode($data);
                            if ($country)
                            {
                                $location = new ProjectLocation();
                                $location->setCreatedBy($author);
                                $location->setUpdatedBy($author);
                                $country->addProject($location);
                                $project->addLocation($location);
                                if (!empty($data) && $data != '#N/A')
                                {
                                    $city = $location->getCity();

                                    if (empty($city)) {
                                        $location->setCity($row[SimbioticaCalpBundle::COLUMN_CITIES_VILLAGES]);
                                    }
                                    else {
                                        $location->setCity($city.', '.$row[SimbioticaCalpBundle::COLUMN_CITIES_VILLAGES]);
                                    }
                                }
                                if (!empty($row[SimbioticaCalpBundle::COLUMN_REGIONS]) && $row[SimbioticaCalpBundle::COLUMN_REGIONS] != '#N/A')
                                {
                                    $region = $this->getContainer()->get('doctrine')
                                        ->getRepository('SimbioticaCalpBundle:Region')->findOneByCode($row[SimbioticaCalpBundle::COLUMN_REGIONS]);
                                    
                                    $region->addProject($location);
                                    if (!empty($row[SimbioticaCalpBundle::COLUMN_SCALE_PER_REGION]) && $row[SimbioticaCalpBundle::COLUMN_SCALE_PER_REGION] != '#N/A')
                                    {
                                        if (is_numeric($row[SimbioticaCalpBundle::COLUMN_SCALE_PER_REGION]))
                                        {
                                            $location->setScale($row[SimbioticaCalpBundle::COLUMN_SCALE_PER_REGION]);
                                        }
                                        else
                                            $errors[] = ("Invalid scale per region value: ".$row[SimbioticaCalpBundle::COLUMN_SCALE_PER_REGION]);
                                    }
                                }
                            	$em->persist($location);
                            }
                            else
                                $errors[] = ("Invalid country code: ".$data);
                        }
                        break;
//                    case SimbioticaCalpBundle::COLUMN_REGIONS:
//                        if (!empty($data) && $data != '#N/A')
//                        {
//                            $region = $this->getContainer()->get('doctrine')
//                                ->getRepository('SimbioticaCalpBundle:Region')->findOneByCode($data);
//                            
//                            if ($region)
//                            {
//                                $regionRelation = new ProjectRegion();
//                                $regionRelation->setCreatedBy($author);
//                                $regionRelation->setUpdatedBy($author);
//                                $region->addProject($regionRelation);
//                                $project->addRegion($regionRelation);
//                                if (!empty($row[SimbioticaCalpBundle::COLUMN_SCALE_PER_REGION]) && $row[SimbioticaCalpBundle::COLUMN_SCALE_PER_REGION] != '#N/A')
//                                {
//                                    if (is_numeric($row[SimbioticaCalpBundle::COLUMN_SCALE_PER_REGION]))
//                                    {
//                                        $regionRelation->setScale($row[SimbioticaCalpBundle::COLUMN_SCALE_PER_REGION]);
//                                        $em->persist($regionRelation);
//                                    }
//                                    else
//                                        $errors[] = ("Invalid scale per region value: ".$row[SimbioticaCalpBundle::COLUMN_SCALE_PER_REGION]);
//                                }
//                            }
//                            else
//                                $errors[] = ("Invalid region code: ".$data);
//                        }
//                        break;
                    case SimbioticaCalpBundle::COLUMN_NUM_CITIES_VILLAGES:
                        break;
                    case SimbioticaCalpBundle::COLUMN_CITIES_VILLAGES:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $city = $project->getCity();

                            if (empty($city)) {
                                $project->setCity($data);
                            }
                            else {
                                $project->setCity($city.', '.$data);
                            }
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_SPECIFIC_OBJECTIVES:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $project->setSpecificObjective($data);
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_SECTORS:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $sector = $this->getContainer()->get('doctrine')
                                ->getRepository('SimbioticaCalpBundle:Sector')->find(intval($data));
                            if ($sector)
                                $project->addSector($sector);
                            else
                                $errors[] = ("Invalid sector id: ".$data);
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_SPECIFY_IF_OTHER:
                        if (!empty($data) && $data != '#N/A' && $row[SimbioticaCalpBundle::COLUMN_SECTORS] == 1)
                        {
                            $project->setOtherSectors($data);
                        }
                        break;
                    // case SimbioticaCalpBundle::COLUMN_SCALE:
                    //     if (!empty($data) && $data != '#N/A')
                    //     {
                    //         $project->setScale(intval(str_replace(' ', '', $data)));
                    //     }
                    //     break;
                    case SimbioticaCalpBundle::COLUMN_DONORS:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $donorOrganization = $this->getContainer()->get('doctrine')
                                ->getRepository('SimbioticaCalpBundle:Organization')->searchOrganization($data);
                            if (!$donorOrganization)
                            {
                                $donorOrganization = new Organization();
                                $donorOrganization->setCreatedBy($author);
                                $donorOrganization->setUpdatedBy($author);
                                $donorOrganization->setName($data);

                                $em->persist($donorOrganization);
                            }
                                
                            $donorRelation = new ProjectDonor();
                            $donorRelation->setCreatedBy($author);
                            $donorRelation->setUpdatedBy($author);
                            $donorOrganization->addDonatingProject($donorRelation);
                            $project->addDonor($donorRelation);
                            if (!empty($row[SimbioticaCalpBundle::COLUMN_AMOUNT_PER_DONOR]) && $row[SimbioticaCalpBundle::COLUMN_AMOUNT_PER_DONOR] != '#N/A')
                            {
                                if (is_numeric($row[SimbioticaCalpBundle::COLUMN_AMOUNT_PER_DONOR]))
                                {
                                    $donorRelation->setAmount($row[SimbioticaCalpBundle::COLUMN_AMOUNT_PER_DONOR]);
                                    $em->persist($donorRelation);
                                }
                                else
                                    $errors[] = ("Invalid amount per donor value: ".$row[SimbioticaCalpBundle::COLUMN_AMOUNT_PER_DONOR]);
                            }
                        }
                        break;
                    // case SimbioticaCalpBundle::COLUMN_OVERALL_BUDGET:
                    //     if (!empty($data) && $data != '#N/A')
                    //     {
                    //         $project->setBudgetOverall($data);
                    //         $project->setBudgetOverallCurrency(1);
                    //     }
                    //     break;
                    // case SimbioticaCalpBundle::COLUMN_CASH_TRANSFERED:
                    //     if (!empty($data) && $data != '#N/A')
                    //     {
                    //         $project->setBudgetAllocated($data);
                    //     }
                    //     break;
                    case SimbioticaCalpBundle::COLUMN_CONTEXT:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $contexts = $project->getProjectContext();
                            if (false === array_search($data, $contexts))
                            {
                                $contexts[] = $data;
                            }
                            $project->setProjectContext($contexts);
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_START_DATE:
                        if (!empty($data) && $data != '#N/A' && $data != '01/1904' && $data != '#VALUE!')
                        {
                            if (preg_match("/^(0?[1-9]|1[012])[\/\-]\d{4}$/", $data))
                            {
                                $temp = str_replace('-', '/', $data);
                                $date = explode('/', $temp);
                                $date[0] = str_replace('0', '', $date[0]);
                                $project->setStartingDate(array(
                                    'month' => $date[0], 
                                    'year' => $date[1]
                                ));
                            }
                            else
                            {
                                $errors[] = ("Invalid start date format: ".$data);
                            }
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_FIRST_TRANSFER:
                        if (!empty($data) && $data != '#N/A' && $data != '01/1904' && $data != '#VALUE!')
                        {
                            if (preg_match("/^(0?[1-9]|1[012])[\/\-]\d{4}$/", $data))
                            {
                                $temp = str_replace('-', '/', $data);
                                $date = explode('/', $temp);
                                $date[0] = str_replace('0', '', $date[0]);
                                $project->setFirstCashDate(array(
                                    'month' => $date[0], 
                                    'year' => $date[1]
                                ));
                            }
                            else
                            {
                                $errors[] = ("Invalid first transfer date format: ".$data);
                            }
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_END_DATE:
                        if (!empty($data) && $data != '#N/A' && $data != '01/1904' && $data != '#VALUE!')
                        {
                            if (preg_match("/^(0?[1-9]|1[012])[\/\-]\d{4}$/", $data))
                            {
                                $temp = str_replace('-', '/', $data);
                                $date = explode('/', $temp);
                                $date[0] = str_replace('0', '', $date[0]);
                                $project->setEndingDate(array(
                                    'month' => $date[0], 
                                    'year' => $date[1]
                                ));
                            }
                            else
                            {
                                $errors[] = ("Invalid end date format: ".$data);
                            }
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_MARKET_ASESSMENT_TOOL:
                        if (!empty($data) && $data != '#N/A')
                        {
                            if (intval($data) != 0 && !empty($row[SimbioticaCalpBundle::COLUMN_MARKET_ASSESSMENT_TOOL_SPECIFY]) && $row[SimbioticaCalpBundle::COLUMN_MARKET_ASSESSMENT_TOOL_SPECIFY] != '#N/A')
                                $project->setMarketAssessment(array('options' => intval($data), 'other' => $row[SimbioticaCalpBundle::COLUMN_MARKET_ASSESSMENT_TOOL_SPECIFY]));
                            else
                                $project->setMarketAssessment(array('options' => intval($data), 'other' => null));
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_COORDINATION_MECHANISM:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $coordinationMechanism = $this->getContainer()->get('doctrine')
                                ->getRepository('SimbioticaCalpBundle:CoordinationMechanism')->find(intval($data));
                            
                            if($coordinationMechanism)
                            {
                                $projectCoordinationMechanism = new ProjectCoordinationMechanism();
                                $projectCoordinationMechanism->setCreatedBy($author);
                                $projectCoordinationMechanism->setUpdatedBy($author);
                                $projectCoordinationMechanism->setCoordinationMechanism($coordinationMechanism);
                                $projectCoordinationMechanism->setProject($project);
                                if (!empty($row[SimbioticaCalpBundle::COLUMN_COORDINATION_MECHANISM_SPECIFY]) && $row[SimbioticaCalpBundle::COLUMN_COORDINATION_MECHANISM_SPECIFY] != '#N/A')
                                    $projectCoordinationMechanism->setSpecify($row[SimbioticaCalpBundle::COLUMN_COORDINATION_MECHANISM_SPECIFY]);

                                $em->persist($projectCoordinationMechanism);
                            }
                            else
                            {
                                $errors[] = ("Invalid coordination mechanism with id: ".$data);
                            }
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_MODALITIES:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $modality = new Modality();
                            $modality->setCreatedBy($author);
                            $modality->setUpdatedBy($author);
                            $modality->setProject($project);
                            $modality->setModality(intval($data));
                            $modality->setModalitySpecify($row[SimbioticaCalpBundle::COLUMN_MODALITIES_SPECIFY]);
                            if(!empty($row[SimbioticaCalpBundle::COLUMN_DELIVERY_MECHANISM]) && $row[SimbioticaCalpBundle::COLUMN_DELIVERY_MECHANISM] != '#N/A' ) {
                                $modality->setDeliveryMechanism(intval($row[SimbioticaCalpBundle::COLUMN_DELIVERY_MECHANISM]));
                            }
                            $modality->setDeliveryMechanismSpecify($row[SimbioticaCalpBundle::COLUMN_MARKET_ASSESSMENT_TOOL_SPECIFY]);
                            if(!empty($row[SimbioticaCalpBundle::COLUMN_DELIVERY_AGENT]) && $row[SimbioticaCalpBundle::COLUMN_DELIVERY_AGENT] != '#N/A' ) {
                                $modality->setDeliveryAgent(intval($row[SimbioticaCalpBundle::COLUMN_DELIVERY_AGENT]));
                            }
                            $modality->setDeliveryAgentSpecify($row[SimbioticaCalpBundle::COLUMN_DELIVERY_AGENT_SPECIFY]);
                            $modality->setScale($row[SimbioticaCalpBundle::COLUMN_NUMBER_OF_BENEFICIARIES]);
                            $modality->setAmount($row[SimbioticaCalpBundle::COLUMN_AMOUNT_PER_TRANSFER]);
                            $modality->setTransfers($row[SimbioticaCalpBundle::COLUMN_NUMBER_OF_TRANSFERS]);


                            /*$modalitySpecify = $row[SimbioticaCalpBundle::COLUMN_MODALITIES_SPECIFY];
                            if (!empty($modalitySpecify) && $modalitySpecify != '#N/A')
                            {
                                $modality->setModalitySpecify($modalitySpecify);
                            }*/

                            /*$deliveryMechanism = $row[SimbioticaCalpBundle::COLUMN_DELIVERY_MECHANISM];
                            if (!empty($deliveryMechanism) && $deliveryMechanism != '#N/A')
                            {
                                $modality->setDeliveryMechanism(intval($deliveryMechanism));
                                $deliveryMechanismSpecify = $row[SimbioticaCalpBundle::COLUMN_DELIVERY_MECHANISM_SPECIFY];
                                if (!empty($deliveryMechanismSpecify) && $deliveryMechanismSpecify != '#N/A')
                                {
                                    $modality->setDeliveryMechanismSpecify($deliveryMechanismSpecify);
                                }

                                $deliveryAgent = $row[SimbioticaCalpBundle::COLUMN_DELIVERY_AGENT];
                                if (!empty($deliveryAgent) && $deliveryAgent != '#N/A')
                                {
                                    $modality->setDeliveryAgent(intval($deliveryAgent));
                                    $deliveryAgentSpecify = $row[SimbioticaCalpBundle::COLUMN_DELIVERY_AGENT_SPECIFY];
                                    if (!empty($deliveryAgentSpecify) && $deliveryAgentSpecify != '#N/A')
                                    {
                                        $modality->setDeliveryAgentSpecify($deliveryAgentSpecify);
                                    }
                                }
                                
                            }*/

                            $em->persist($modality);
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_TYPE_OF_INTERVENTION:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $project->setTypeOfIntervention(intval($data));
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_TYPE_OF_INTERVENTION_SPECIFY:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $project->setTypeOfInterventionSpecify($data);
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_CONTACT_PERSON:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $project->setContactPersonName($data);
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_POSITION:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $project->setContactPersonPosition($data);
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_EMAIL:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $project->setContactPersonEmailOne($data);
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_EMAIL2:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $project->setContactPersonEmailTwo($data);
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_WEB:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $project->setContactOrganizationUrl($data);
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_LANGUAGE:
                        if (!empty($data) && $data != '#N/A')
                        {
                            switch (trim(strtoupper($data))) {
                                case 'EN':
                                    $project->setLanguage(SimbioticaCalpBundle::LANGUAGE_EN);
                                    break;
                                case 'FR':
                                    $project->setLanguage(SimbioticaCalpBundle::LANGUAGE_FR);
                                    break;
                                case 'ES':
                                    $project->setLanguage(SimbioticaCalpBundle::LANGUAGE_ES);
                                    break;
                                default:
                                    $errors[] = ("Invalid or empty language: ".$data);
                                    break;
                            }
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_PROJECT_DESCRIPTION:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $project->setDescription($data);
                        }
                        break;
                    case SimbioticaCalpBundle::COLUMN_PROJECT_DOCS:
                        break;
                    case SimbioticaCalpBundle::COLUMN_OWNER:
                        if (!empty($data) && $data != '#N/A')
                        {
                            $user = $this->getContainer()->get('fos_user.user_manager')->findUserByUsernameOrEmail($data);
                            if($user instanceof User && !in_array($user, $project->getOwners()->toArray()))
                                $project->addOwner($user);
                            if((!$user || $user->getId() != $author->getId()) && !in_array($author, $project->getOwners()->toArray()))
                                $project->addOwner($author);
                        }
                        break;

                    case SimbioticaCalpBundle::COLUMN_CURRENCY:
                        if (!empty($data) && $data != '#N/A') {
                            $project->setCurrency($data);
                        }
                        break;
                }
            }
        }
        
        if (empty($errors))
        {
            $em->persist($project);
            $em->flush();
            $result = $project->getId();
            $em->clear();
            return $result;
        }
        else
            return $errors;
    }
            
    
    private function printInfo($string) {
        if ($this->notify)
            $this->emailContent .= '<p style="color:green">'.$string.'</p>';
        else
            $this->output->writeln('<info>'.$string.'</info>');
    }
    
    private function printError($string) {
        if ($this->notify)
            $this->emailContent .= '<p style="color:red">'.$string.'</p>';
        else
            $this->output->writeln('<error>'.$string.'</error>');
    }
}
?>
