<?php

namespace Simbiotica\CalpBundle\Command;

use Doctrine\Bundle\DoctrineBundle\Command\DoctrineCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SearchProjectDatesCommand extends DoctrineCommand
{
    protected function configure()
    {
        $this
        ->setName('projects:dates:search')
        ->setDescription('Search project with near important dates')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $month = date('n');
        $year = date('Y');
        
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $projectsIds = $em->getRepository('SimbioticaCalpBundle:Project')->searchByDates($month, $year, $month, $year);

        if($projectsIds)
        {
            $backend = $this->getContainer()->get('sonata.notification.backend');
            $projects = array();
            $start = array();
            $end = array();
            
            foreach ($projectsIds['start'] as $key => $value){
               $projects[] = $value['id'];
               $start[] = $value['id'];
            }            

            foreach ($projectsIds['end'] as $key => $value){
               $projects[] = $value['id'];
               $end[] = $value['id'];
            }

            $output->writeln('<comment>Notifying project '.implode($projects, ', ').'</comment>');
            
            try 
            {
                $backend->createAndPublish('nearDates', array(
                'ids' => $start,
                'type' => 'startDate'
                ));


                $backend->createAndPublish('nearDates', array(
                    'ids' => $end,
                    'type' => 'endDate'
                ));       
            } catch (\Exception $e) 
            {
                $message = \Swift_Message::newInstance()
                    ->setSubject('RabbitMQ exception')
                    ->setFrom($this->container->getParameter('mailer_from'))
                    ->setTo($this->container->getParameter('notification_mail'))
                    ->setBody("SearchProjectsDatesCommand::execute error: <br/>".$e->getMessage());
                
                $this->get('mailer')->send($message);

                $output->writeln('RabbitMQ Error');
            }
        }
        else
        {
            $output->writeln('<comment>No matches found</comment>');
        }
    }
}

?>