<?php

namespace Simbiotica\CalpBundle\Utils;

use Simbiotica\CalpBundle\Entity\Project;

class ProjectValidator {
    
    public function __construct() {
    }
    
    public function validateProject(Project $project) {
        $validations = array();
        
        if(count($project->getOrganizations()) == 0)
            $validations[] = 'validation.project.no_organization';
        else {
            $hasOrg = true;
            foreach($project->getOrganizations() as $po)
            {
                if (!$po->getOrganization())
                {
                    $hasOrg = false;
                    break;
                }
            }
            if(!$hasOrg)
                $validations[] = 'validation.project.no_organization';
        }

        if(count($project->getDonors()) == 0)
            $validations[] = 'validation.project.no_donors';
        else {
            $hasDonor = true;
            foreach($project->getDonors() as $pd)
            {
                if (!$pd->getOrganization() || !$pd->getAmount())
                {
                    $hasDonor = false;
                    break;
                }
            }
            if(!$hasDonor)
                $validations[] = 'validation.project.no_donors';
        }

        if(count($project->getLocations()) == 0)
            $validations[] = 'validation.project.no_locations';
        else {
            $hasLocation = true;
            foreach($project->getLocations() as $pl)
            {
                if (!$pl->getCountry() || !$pl->getRegion() || !$pl->getScale())
                {
                    $hasLocation = false;
                    break;
                }
            }
            if(!$hasLocation)
                $validations[] = 'validation.project.no_locations';
        }

        if(count($project->getSectors()) == 0)
            $validations[] = 'validation.project.no_sectors';
        
        if(count($project->getModalities()) == 0)
            $validations[] = 'validation.project.no_modalities';
        else {
            $hasMod = true;
            $hasModValues = true;
            foreach($project->getModalities() as $mod)
            {
                if ($mod->getModality() === null || $mod->getDeliveryMechanism() === null || $mod->getDeliveryAgent() === null)
                {
                    $hasMod = false;
                }
                if ($mod->getScale() === null || $mod->getAmount() === null || $mod->getTransfers() === null)
                {
                    $hasModValues = false;
                }
                if(!$hasMod && !$hasModValues)
                    break;
            }
            if(!$hasMod)
                $validations[] = 'validation.project.no_modalities';
            if(!$hasModValues)
                $validations[] = 'validation.project.no_modalities_values';
        }

        if((int)$project->getBudgetOverall() < $project->getBudgetAllocated())
            $validations[] = 'validation.project.project_overall_exceded';
        if(!$project->getDuration() > 0)
            $validations[] = 'validation.project.no_duration';

        if(count($project->getStartingDate()) == 0)
            $validations[] = 'validation.project.no_starting_date';
        if(count($project->getEndingDate()) == 0)
            $validations[] = 'validation.project.no_ending_date';
        if(count($project->getFirstCashDate()) == 0)
            $validations[] = 'validation.project.no_first_cash_date';
        if(!trim($project->getSpecificObjective()))
            $validations[] = 'validation.project.no_specific_objective';

        return $validations;
    }
}
