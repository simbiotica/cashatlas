<?php

namespace Simbiotica\CalpBundle\Utils;

use Symfony\Component\Translation\TranslatorInterface;

class ArrayTranslator {
    
    protected $translator;
    
    public function __construct(TranslatorInterface $translator) {
        $this->translator = $translator;
    }
    
    public function translateArray($inputArray, $domain) {
        $translator = $this->translator;
        return array_map(function($item) use ($translator, $domain) {
            /** @Ignore */
            return $translator->trans($item, array(), $domain);
        }, $inputArray);
    }
}
