<?php

namespace Simbiotica\CalpBundle\Utils;

use Simbiotica\CalpBundle\Entity\Project;
use Doctrine\ORM\EntityManager;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use Elastica\Query\Bool;
use Elastica\Query\Terms;
use Elastica\Query\Text;
use Simbiotica\CalpBundle\Entity\SimilarProject;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;

class ProjectComparator {
    
    private $elasticConnection;
    private $entityManager;
    
    public function __construct(TransformedFinder $elasticConnection, EntityManager $entityManager) {
        $this->elasticConnection = $elasticConnection;
        $this->entityManager = $entityManager;
    }
    
    public function compareProject(Project $project) {
        if($project->getPublished() != SimbioticaCalpBundle::PUBLISHED_PUBLIC)
            return;
        
        $boolQuery = new Bool();

        $fieldQuery = new Text();
        $fieldQuery->setFieldQuery('language', 'en_US');
        $boolQuery->addShould($fieldQuery);

        $orgs = array();
        foreach($project->getOrganizations() as $po)
        {
            if ($po->getOrganization())
                $orgs[] = $po->getOrganization()->getId();
        }
        
        if(count($orgs))
        {
            $organizationQuery = new Terms();
            $organizationQuery->setTerms('organizations.organization.id', $orgs);
            $boolQuery->addShould($organizationQuery);
        }
        
        $donors = array();
        foreach($project->getDonors() as $pd)
        {
            if ($pd->getOrganization())
                $donors[] = $pd->getOrganization()->getId();
        }
        
        if(count($donors))
        {
            $donorQuery = new Terms();
            $donorQuery->setTerms('donors.donor.id', $donors);
            $boolQuery->addShould($donorQuery);
        }

        $similarProjects = $this->elasticConnection->find($boolQuery, 1000);
        
        if (count($similarProjects))
        {
            $rep = $this->entityManager->getRepository('SimbioticaCalpBundle:SimilarProject');
            
            if(!$project->getId())
            {
                foreach($similarProjects as $match)
                {
                    $similarProject = new SimilarProject();
                    $similarProject->setReference($project);
                    $similarProject->setTarget($match);

                    $this->entityManager->persist($similarProject);
                }
            }
            else
            {
                $knownMatches = $rep->getNewMatches($project->getId());
                foreach($similarProjects as $match)
                {
                    if(!in_array($match->getId(), $knownMatches))
                    {
                        $similarProject = new SimilarProject();
                        $similarProject->setReference($project);
                        $similarProject->setTarget($match);

                        $this->entityManager->persist($similarProject);
                    }
                }
            }
           
            $this->entityManager->flush();
        }
    }
}
