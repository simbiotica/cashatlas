<?php

namespace Simbiotica\CalpBundle\Utils;

use Doctrine\ORM\EntityManager;

class CurrencyExchange 
{

	private $entityManager;

	public function __construct(EntityManager $em) {
		$this->entityManager = $em;
	}

	public function exchange($data, $currency)
	{
		$result = array('budgetOverall' => 0, 'budgetAllocated' => 0);
		if(is_array($data))
		{
			$rate = $this->entityManager->getRepository('SimbioticaCalpBundle:CurrencyExchange')->getLast();
			if(isset($data[0]) && is_array($data[0]))
			{
				if($data[0]['currency'] != $currency)
				{
					if($currency == 'EUR')
					{
						$data[0]['budgetOverallSum'] /= (float) $rate;
						$data[0]['budgetAllocatedSum'] /= (float) $rate;	
					} else if ($currency == 'USD') {
						$data[0]['budgetOverallSum'] *= (float) $rate;
						$data[0]['budgetAllocatedSum'] *= (float) $rate;
					}
				}
				$result['budgetOverall'] += round((float) $data[0]['budgetOverallSum'], 2);
				$result['budgetAllocated'] += round((float) $data[0]['budgetAllocatedSum'], 2);
			}
			if(isset($data[1]) && is_array($data[1]))
			{
				if($data[1]['currency'] != $currency)
				{
					if($currency == 'EUR')
					{
						$data[1]['budgetOverallSum'] /= (float) $rate;
						$data[1]['budgetAllocatedSum'] /= (float) $rate;	
					} else if ($currency == 'USD') {
						$data[1]['budgetOverallSum'] *= (float) $rate;
						$data[1]['budgetAllocatedSum'] *= (float) $rate;
					}
				}
				$result['budgetOverall'] += round((float) $data[1]['budgetOverallSum'], 2);
				$result['budgetAllocated'] += round((float) $data[1]['budgetAllocatedSum'], 2);				
			}
			return $result;
		} else {
			return null;
		}
	}

	public function exchangeData($amount, $currency)
	{
		$rate = $this->entityManager->getRepository('SimbioticaCalpBundle:CurrencyExchange')->getLast();
		if($currency === 'EUR')
		{
			return round($amount / $rate, 2);
		} else {
			return round($amount * $rate, 2);
		}
	}

}

