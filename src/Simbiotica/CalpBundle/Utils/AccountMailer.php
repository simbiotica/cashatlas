<?php

namespace Simbiotica\CalpBundle\Utils;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Sonata\NotificationBundle\Backend\BackendInterface;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Simbiotica\CalpBundle\Entity\EmailLog;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AccountMailer
{
    protected $backend;
    protected $templating;
    protected $translator;
    protected $fromAddress;
    protected $toAddress;
    protected $container;
    
    public function __construct(BackendInterface $backend, EngineInterface $templating, Translator $translator, ContainerInterface $container, $fromAddress, $toAddress)
    {
        $this->backend = $backend;
        $this->templating = $templating;
        $this->translator = $translator;
        $this->fromAddress = $fromAddress;
        $this->toAddress = $toAddress;
        $this->container = $container;
    }

    public function sendWelcomeEmailMessage(UserInterface $user)
    {
        $locale = $user->getLocale();
        if(!in_array($locale, SimbioticaCalpBundle::getLanguages()))
            $locale = 'en';
        
        $body = $this->templating->render('SimbioticaCalpBundle:Emails:welcome.outbound.'.$locale.'.html.twig', 
                            array('user' => $user, 'locale' => $locale)
            );
        $subject = $this->translator->trans('welcome.email.outbound.title', array(), 'SimbioticaFront');
        
        $this->sendEmailMessage($body, $this->fromAddress, $user->getEmail(), $subject);
    }
        
    public function sendProjectNotificationEmailMessage(UserInterface $user, Array $projects)
    {
        $locale = $user->getLocale();
        if(!in_array($locale, SimbioticaCalpBundle::getLanguages()))
            $locale = 'en';
        
        $body = $this->templating->render('SimbioticaCalpBundle:Emails:notification.project.outbound.'.$locale.'.html.twig', 
                            array('user' => $user, 'locale' => $locale, 'projects' => $projects)
            );
        $subject = $this->translator->trans('notification.project.email.outbound.title', array(), 'SimbioticaFront');
        
        $this->sendEmailMessage($body, $this->fromAddress, $user->getEmail(), $subject);
    }

    public function sendProjectNotificationDateEmailMessage(UserInterface $user, Array $projects, $type)
    {
        $locale = $user->getLocale();
        if(!in_array($locale, SimbioticaCalpBundle::getLanguages()))
            $locale = 'en';
        switch($type)
        {
            case 'startDate':
                $body = $this->templating->render('SimbioticaCalpBundle:Emails:notification.project.start.'.$locale.'.html.twig', 
                            array('user' => $user, 'locale' => $locale, 'projects' => $projects)
                    );
                $subject = $this->translator->trans('notification.project.email.start.title', array(), 'SimbioticaFront');
                break;
            case 'endDate':
                $body = $this->templating->render('SimbioticaCalpBundle:Emails:notification.project.end.'.$locale.'.html.twig', 
                            array('user' => $user, 'locale' => $locale, 'projects' => $projects)
                    );
                $subject = $this->translator->trans('notification.project.email.end.title', array(), 'SimbioticaFront');
                break;
            default: 
                break;    
        }
        
        $this->sendEmailMessage($body, $this->fromAddress, $user->getEmail(), $subject);
    }
        
    public function sendRegistrationEmailMessage(UserInterface $user)
    {
        $locale = $user->getLocale();
        if(!in_array($locale, SimbioticaCalpBundle::getLanguages()))
            $locale = 'en';
        
        if(is_array($this->toAddress))
            $orgAdminEmails = $this->toAddress;
        else
            $orgAdminEmails = array($this->toAddress);
        
//        foreach ($user->getOrganizations() as  $oou)
//        {
//            if ($oou->getOrganization())
//            {
//                foreach ($oou->getOrganization()->getUsers() as $ou)
//                {
//                    if($ou->getAdmin() && $ou->getUser() && $ou->getEnabled())
//                    {
//                        $orgAdminEmails[] = $ou->getUser()->getEmail();
//                    }
//                }
//            }
//        }
        
        //Inboud email
        $inBody = $this->templating->render('SimbioticaCalpBundle:Emails:registration.inbound.'.$locale.'.html.twig', 
                            array('user' => $user, 'intents' => SimbioticaCalpBundle::getIntents())
            );
        $inSubject = $this->translator->trans('registration.email.inbound.title', array(), 'SimbioticaFront');
        $this->sendEmailMessage($inBody, $this->fromAddress, $orgAdminEmails, $inSubject);
        
        //Outbound email
        $outBody = $this->templating->render('SimbioticaCalpBundle:Emails:registration.outbound.'.$locale.'.html.twig', 
                            array('user' => $user, 'intents' => SimbioticaCalpBundle::getIntents())
            );
        $outSubject = $this->translator->trans('registration.email.outbound.title', array(), 'SimbioticaFront');
        $this->sendEmailMessage($outBody, $this->fromAddress, $user->getEmail(), $outSubject);
    }
    
    protected function sendEmailMessage($body, $fromEmail, $toEmail, $subject)
    {
        try
        {
            $this->backend->createAndPublish('mailer', array(
                'subject' => $subject,
                'from' => array(
                    'email' => $fromEmail,
                    'name' => "Cash Atlas",
                ),
                'to' => $toEmail,
                'message' => array(
                    'html' => $body,
                )
            ));
        } catch (\Exception $e) 
        {
            $message = \Swift_Message::newInstance()
                ->setSubject('RabbitMQ exception')
                ->setFrom($this->container->getParameter('mailer_from'))
                ->setTo($this->container->getParameter('notification_mail'))
                ->setBody("Send email message error: <br/>".$e->getMessage());
            
            $this->get('mailer')->send($message);

        }
        $mailLog = new EmailLog();
        if(is_array($toEmail))
            $mailLog->setReceiver($toEmail);
        else
            $mailLog->setReceiver(array($toEmail));
        
        $realReceiver = $this->container->getParameter('swiftmailer.mailer.default.single_address')?:$toEmail;
        if(is_array($realReceiver))
            $mailLog->setRealReceiver($realReceiver);
        else
            $mailLog->setRealReceiver(array($realReceiver));
        $mailLog->setSendAt(new \Datetime());
        $mailLog->setSubject($subject);
        $mailLog->setBody($body);
        $mailLog->setSender($fromEmail);
        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($mailLog);
        $em->flush();   
    }
}

?>
