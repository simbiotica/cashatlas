<?php

namespace Simbiotica\CalpBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class BrowserDetectionListener
{
    protected $container;
    protected $request;
    protected $route;
    protected $event;
    
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
        
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }
        
        $this->request = $event->getRequest();
        if ($this->request->get('_route') === 'simbiotica_calp_unsupported_browser')
            return;
        
        $this->event = $event;
        
        $browser = get_browser(null, true);
        
        if ($browser['browser'] === 'IE' && intval($browser['majorver']) <= 8)
        {
            $event->setResponse( new RedirectResponse($this->container->get('router')->generate('simbiotica_calp_unsupported_browser')));
        }
    }
}