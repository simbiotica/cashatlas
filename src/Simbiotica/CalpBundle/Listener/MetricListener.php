<?php

namespace Simbiotica\CalpBundle\Listener;

use Simbiotica\CalpBundle\Entity\Project;
use Simbiotica\CalpBundle\Entity\User;
use Simbiotica\CalpBundle\Entity\ProjectOrganization;
use Simbiotica\CalpBundle\Entity\Organization;
use Simbiotica\CalpBundle\Entity\Metric;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;

class MetricListener
{
    protected $args;
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
    public function preSoftDelete(LifecycleEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();
        $entity = $eventArgs->getEntity();
        
        if ($entity instanceof Project)
        {
            $countArray = $em->getRepository('SimbioticaCalpBundle:Project')->getPublishedCount();
            $count = $countArray[0]['number']-1;

            $metric = new Metric();
            $metric->setName('publishedProjectsCount');
            $metric->setValue($count);
            $em->persist($metric);

            $uow->computeChangeSet($em->getClassMetadata(get_class($metric)), $metric);
        }
        elseif ($entity instanceof User)
        {
            $countArray = $em->getRepository('SimbioticaCalpBundle:User')->getEnabledCount();
            $count = $countArray[0]['number']-1;

            $metric = new Metric();
            $metric->setName('enabledUsers');
            $metric->setValue($count);
            $em->persist($metric);

            $uow->computeChangeSet($em->getClassMetadata(get_class($metric)), $metric);
        }
        elseif ($entity instanceof ProjectOrganization)
        {
            $countArray = $em->getRepository('SimbioticaCalpBundle:Organization')->getCountWithProject();
            $count = $countArray[0]['number']-1;

            $metric = new Metric();
            $metric->setName('organizationsWithProjects');
            $metric->setValue($count);
            $em->persist($metric);

            $uow->computeChangeSet($em->getClassMetadata(get_class($metric)), $metric);
        }
        elseif ($entity instanceof Organization)
        {
            $countArray = $em->getRepository('SimbioticaCalpBundle:Organization')->getCount();
            $count = $countArray[0]['number']-1;

            $metric = new Metric();
            $metric->setName('organizations');
            $metric->setValue($count);
            $em->persist($metric);

            $uow->computeChangeSet($em->getClassMetadata(get_class($metric)), $metric);
        }
    }
    
    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        //on update
        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            $changeset = $uow->getEntityChangeSet($entity);
            $this->args = new PreUpdateEventArgs($entity, $eventArgs->getEntityManager(), $changeset);
            
        
            if ($entity instanceof Project)
            {
                if($this->hasChangedField('published'))
                {
                    $countArray = $em->getRepository('SimbioticaCalpBundle:Project')->getPublishedCount();
                    $count = $countArray[0]['number'];
                    
                    if($this->args->getNewValue('published') == SimbioticaCalpBundle::PUBLISHED_PUBLIC)
                        $count++;
                    elseif($this->args->getOldValue('published') == SimbioticaCalpBundle::PUBLISHED_PUBLIC)
                        $count--;
                    
                    $metric = new Metric();
                    $metric->setName('publishedProjectsCount');
                    $metric->setValue($count);
                    $em->persist($metric);
                    
                    $uow->computeChangeSet($em->getClassMetadata(get_class($metric)), $metric);
                }
            }
            elseif ($entity instanceof User)
            {
                if($this->hasChangedField('enabled'))
                {
                    $countArray = $em->getRepository('SimbioticaCalpBundle:User')->getEnabledCount();
                    $count = $countArray[0]['number'];
                    
                    if($this->args->getNewValue('enabled') == true)
                        $count++;
                    else
                        $count--;
                    
                    $metric = new Metric();
                    $metric->setName('enabledUsers');
                    $metric->setValue($count);
                    $em->persist($metric);
                    
                    $uow->computeChangeSet($em->getClassMetadata(get_class($metric)), $metric);
                }
                if($this->hasChangedField('lastLogin'))
                {
                    $metric = new Metric();
                    $metric->setName('login');
                    $metric->setValue($entity->getId());
                    $em->persist($metric);
                    
                    $uow->computeChangeSet($em->getClassMetadata(get_class($metric)), $metric);
                }
            }
        }
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof Project)
            {
                if($entity->getPublished() == SimbioticaCalpBundle::PUBLISHED_PUBLIC)
                {
                    $countArray = $em->getRepository('SimbioticaCalpBundle:Project')->getPublishedCount();
                    $count = $countArray[0]['number']+1;
                    
                    $metric = new Metric();
                    $metric->setName('publishedProjectsCount');
                    $metric->setValue($count);
                    $em->persist($metric);
                    
                    $uow->computeChangeSet($em->getClassMetadata(get_class($metric)), $metric);
                }
            }
            elseif ($entity instanceof User)
            {
                if($entity->isEnabled() == true)
                {
                    $countArray = $em->getRepository('SimbioticaCalpBundle:User')->getEnabledCount();
                    $count = $countArray[0]['number']+1;
                    
                    $metric = new Metric();
                    $metric->setName('enabledUsers');
                    $metric->setValue($count);
                    $em->persist($metric);
                    
                    $uow->computeChangeSet($em->getClassMetadata(get_class($metric)), $metric);
                }
            }
        }
    }
    
    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof ProjectOrganization)
        {
            $countArray = $em->getRepository('SimbioticaCalpBundle:Organization')->getCountWithProject();
            $count = $countArray[0]['number'];

            $metric = new Metric();
            $metric->setName('organizationsWithProjects');
            $metric->setValue($count);
            $em->persist($metric);
            $em->flush();
        }
    }
    
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof ProjectOrganization)
        {
            $countArray = $em->getRepository('SimbioticaCalpBundle:Organization')->getCountWithProject();
            $count = $countArray[0]['number'];

            $metric = new Metric();
            $metric->setName('organizationsWithProjects');
            $metric->setValue($count);
            $em->persist($metric);
            $em->flush();
        }
        if ($entity instanceof Organization)
        {
            $countArray = $em->getRepository('SimbioticaCalpBundle:Organization')->getCount();
            $count = $countArray[0]['number'];

            $metric = new Metric();
            $metric->setName('organizations');
            $metric->setValue($count);
            $em->persist($metric);
            $em->flush();
        }
    }
    

    private function hasChangedField($name = null)
    {
        if ($name != null && $this->args->hasChangedField($name))
            return ($this->args->getNewValue($name) !== $this->args->getOldValue($name));
        else
            return false;
    }
}
