<?php

namespace Simbiotica\CalpBundle\Listener;


use Doctrine\ORM\Event\OnFlushEventArgs;
use Simbiotica\CalpBundle\Entity\Project;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProjectListener
{
    protected $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if (!$entity instanceof Project) {
                continue;
            }

            $entity = $this->processProject($entity);

            $uow = $em->getUnitOfWork();
            $uow->recomputeSingleEntityChangeSet(
                $em->getClassMetadata(get_class($entity)),
                $entity
            );
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if (!$entity instanceof Project) {
                continue;
            }

            $entity = $this->processProject($entity);

            $uow = $em->getUnitOfWork();
            $uow->recomputeSingleEntityChangeSet(
                $em->getClassMetadata(get_class($entity)),
                $entity
            );
        }
    }

    /**
     * @param $entity
     * @return array
     */
    public function processProject($entity)
    {
        $scale = 0;
        foreach ($entity->getLocations() as $location) {
            $scale += $location->getScale();
        }
        $entity->setScale($scale);

        $budgetOverall = 0;
        foreach ($entity->getDonors() as $donor) {
            $budgetOverall += $donor->getAmount();
        }
        $entity->setBudgetOverall($budgetOverall);

        $budgetAllocated = 0;
        foreach ($entity->getModalities() as $modality) {
            $budgetAllocated += ($modality->getAmount() * $modality->getScale() * $modality->getTransfers());
        }
        $entity->setBudgetAllocated($budgetAllocated);

        $startingDate = $entity->getStartingDate();
        $endingDate = $entity->getEndingDate();
        if ($startingDate && $endingDate
            && array_key_exists('year', $startingDate) && array_key_exists('month', $startingDate)
            && array_key_exists('year', $endingDate) && array_key_exists('month', $endingDate)
            && in_array($endingDate['year'], SimbioticaCalpBundle::getYearRange()) && in_array($startingDate['year'], SimbioticaCalpBundle::getYearRange())
            && in_array($endingDate['month'], array_keys(SimbioticaCalpBundle::getMonths())) && in_array($startingDate['month'], array_keys(SimbioticaCalpBundle::getMonths()))
        ) {
            $monthCount = ($endingDate['year'] - $startingDate['year']) * 12;
            $monthCount += ($endingDate['month'] - $startingDate['month']);

            $entity->setDuration(max($monthCount, 0));
        }

        $validationList = $this->container->get('simbiotica.utils.project_validator')->validateProject($entity);

        $entity->setStatus(
            empty($validationList) ? SimbioticaCalpBundle::VALIDATION_COMPLETE : SimbioticaCalpBundle::VALIDATION_INCOMPLETE
        );

        if (!empty($validationList) && $entity->getPublished() == SimbioticaCalpBundle::PUBLISHED_PUBLIC) {
            $entity->setPublished(SimbioticaCalpBundle::PUBLISHED_REVISION);
        }

        try {
            $this->container->get('sonata.notification.backend')->createAndPublish('comparison', array(
                'projectId' => $entity->getId(),
            ));
        } catch (\Exception $e) {
            $message = \Swift_Message::newInstance()
                ->setSubject('RabbitMQ exception')
                ->setFrom($this->container->getParameter('mailer_from'))
                ->setTo($this->container->getParameter('notification_mail'))
                ->setBody("Project manager create error: <br/>".$e->getMessage());

            $this->container->get('mailer')->send($message);
        }

        return $entity;
    }
}