<?php

namespace Simbiotica\CalpBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Simbiotica\CalpBundle\Entity\Media;
use Simbiotica\CalpBundle\Entity\Metric;

use Gedmo\Tool\Wrapper\AbstractWrapper;
use Gedmo\Loggable\Entity\LogEntry;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

class ACLQueueFiller {

    protected $container;
    protected $pendingObjects = array();
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }
    
    protected function enqueue($classname, $objectId, $changeset)
    {
        $this->container->get('simbiotica.calp.acl.queue')->enqueue($classname, $objectId, $changeset);
    }
    
    private function handleChanges($entity, $changeset, $em)
    {
        $wrapped = AbstractWrapper::wrap($entity, $em);
        $meta = $wrapped->getMetadata();
        $objectId = $wrapped->getIdentifier();
        if (!$objectId) {
            $this->pendingObjects[spl_object_hash($entity)] = $changeset;
        }
        else {
            $this->enqueue($meta->name, $objectId, $changeset);
        }
    }
    
    //This needs to be reviewed
    public function preSoftDelete(LifecycleEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();
        $entity = $eventArgs->getEntity();
        $changeset = $uow->getEntityChangeSet($entity);
        
        if (!$entity instanceof LogEntry && !$entity instanceof AbstractPersonalTranslation && !$entity instanceof Metric)
            $this->handleChanges($entity, $changeset, $em);
    }
    
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        $oid = spl_object_hash($entity);
        if ($this->pendingObjects && array_key_exists($oid, $this->pendingObjects)) {
            $wrapped = AbstractWrapper::wrap($entity, $em);
            $meta = $wrapped->getMetadata();
            $objectId = $wrapped->getIdentifier();
            $changeset = $this->pendingObjects[$oid];
            $changeset['id'] = array(1 => $objectId);

            $this->enqueue($meta->name, $objectId, $changeset);
        }
    }
    
    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();
        
        foreach ($uow->getScheduledCollectionDeletions() AS $col) {
            foreach($col->getSnapshot() as $entity)
            {
                $changeset = $uow->getEntityChangeSet($entity);
                if ($col->isDirty() && !$entity instanceof LogEntry && !$entity instanceof AbstractPersonalTranslation && !$entity instanceof Metric)
                    $this->handleChanges($entity, $changeset, $em);
            }
        }

        foreach ($uow->getScheduledCollectionUpdates() AS $col) {
            foreach($col->getSnapshot() as $entity)
            {
                $changeset = $uow->getEntityChangeSet($entity);
                if ($col->isDirty() && !$entity instanceof LogEntry && !$entity instanceof AbstractPersonalTranslation && !$entity instanceof Metric)
                    $this->handleChanges($entity, $changeset, $em);
            }
        }
        
        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            $changeset = $uow->getEntityChangeSet($entity);
            if (!$entity instanceof LogEntry && !$entity instanceof AbstractPersonalTranslation && !$entity instanceof Metric)
                $this->handleChanges($entity, $changeset, $em);
        }
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            $changeset = $uow->getEntityChangeSet($entity);
            if (!$entity instanceof LogEntry && !$entity instanceof AbstractPersonalTranslation && !$entity instanceof Metric)
                $this->handleChanges($entity, $changeset, $em);
        }
        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            $changeset = $uow->getEntityChangeSet($entity);
            if (!$entity instanceof LogEntry && !$entity instanceof AbstractPersonalTranslation && !$entity instanceof Metric)
                $this->handleChanges($entity, $changeset, $em);
        }
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        
        if ($entity instanceof Media)
        {
            if ($entity->getContext() == 'project')
            {
                $projectList = array_map(function($ghm) {
                                    return $ghm->getGallery();
                                }, $entity->getGalleryHasMedias()->toArray()
                );

                if(!empty($projectList))
                {
                    $owners = array();
                    foreach($entity->getUsers() as $user)
                    {
                        $owners[$user->getId()] = $user;
                    }
                    foreach($projectList as $project)
                    {
                        foreach($project->getUsers() as $user)
                        {
                            $owners[$user->getId()] = $user;
                        }
                    }

                    $entity->setUsers($owners);
                }
            }
            
            $token = $this->container->get('security.context')->getToken();

            if (count($entity->getUsers()) && $token)
            {
                $entity->addUser($token->getUser());
            }
        }
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs) {
        $entity = $eventArgs->getEntity();
        $em = $eventArgs->getEntityManager();
        
        if ($entity instanceof Media)
        {
            if ($entity->getContext() == 'project')
            {
                $projectList = array_map(function($ghm) {
                                    return $ghm->getGallery();
                                }, $entity->getGalleryHasMedias()->toArray()
                );

                if(!empty($projectList))
                {
                    $owners = array();
                    foreach($entity->getUsers() as $user)
                    {
                        $owners[$user->getId()] = $user;
                    }
                    foreach($projectList as $project)
                    {
                        foreach($project->getUsers() as $user)
                        {
                            $owners[$user->getId()] = $user;
                        }
                    }

                    $entity->setUsers($owners);
                }
            }
            
            $token = $this->container->get('security.context')->getToken();

            if (count($entity->getUsers()) && $token)
            {
                $entity->addUser($token->getUser());
            }
        }

        $uow = $em->getUnitOfWork();
        $uow->recomputeSingleEntityChangeSet(
                $em->getClassMetadata(get_class($entity)), $eventArgs->getEntity()
        );
    }
}

?>
