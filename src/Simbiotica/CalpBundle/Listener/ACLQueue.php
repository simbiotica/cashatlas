<?php

namespace Simbiotica\CalpBundle\Listener;

use Doctrine\Orm\Proxy\Proxy;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Sonata\NotificationBundle\Backend\BackendInterface;
use Simbiotica\CalpBundle\Entity\Project;
use Simbiotica\CalpBundle\Entity\User;
use Simbiotica\CalpBundle\Entity\Contact;
use Simbiotica\CalpBundle\Entity\OrganizationUser;
use Simbiotica\CalpBundle\Entity\Organization;
use Simbiotica\CalpBundle\Entity\ProjectOrganization;
use Simbiotica\CalpBundle\Entity\ProjectLocation;
use Simbiotica\CalpBundle\Entity\ProjectDonor;
use Simbiotica\CalpBundle\Entity\ProjectCoordinationMechanism;
use Simbiotica\CalpBundle\Entity\ProjectField;
use Simbiotica\CalpBundle\Entity\Modality;
use Simbiotica\CalpBundle\Entity\ProjectMedia;


class ACLQueue 
{
    private $entityManager;
    private $backend;
    
    private $rawQueue = array();
    private $processedQueue = array();
    private $processed = false;
    
    public function __construct(EntityManager $entityManager, BackendInterface $backend) {
        $this->entityManager = $entityManager;
        $this->backend = $backend;
    }
    
    public function onKernelTerminate(PostResponseEvent $event) {
        if(count($this->rawQueue))
        {
            $this->backend->createAndPublish('acl', array(
                'payload' => $this->rawQueue
            ));
        }
    }
    
    public function enqueue($classname, $objectId, $changeset)
    {
        if (in_array($classname, $this->rawQueue)) {
            $this->rawQueue[$classname] = array($objectId => $changeset);
        } else {
            $this->rawQueue[$classname][$objectId] = $changeset;
        }
    }
    
    private function addToProcessedQueue($entity) {
        if (!is_object($entity) || !method_exists($entity, 'getId')) {
            return;
        }

        if ($entity instanceof Proxy) {
            $class = get_parent_class($entity);
        } else {
            $class = get_class($entity);
        }
        
        if (!array_key_exists($class, $this->processedQueue))
        {
            $this->processedQueue[$class] = array($entity->getId() => $entity->getId());
        }
        elseif(!array_key_exists($entity->getId(), $this->processedQueue[$class]))
        {
            $this->processedQueue[$class][$entity->getId()] = $entity->getId();
        }
    }
    
    private function handleChanges($entity, $changeset)
    {
        $preUpdateEventArgs = new PreUpdateEventArgs($entity, $this->entityManager, $changeset);
        /**
         * If is about to be deleted, crawl and queue
         */
        if($preUpdateEventArgs->hasChangedField('deletedAt') && $preUpdateEventArgs->getNewValue('deletedAt'))
        {
            $this->crawlObject($entity);
        }
        /**
         * else, let's go type by type
         */
        elseif($entity instanceof Contact)
        {
            $this->crawlObject($entity);
        }
        elseif($entity instanceof OrganizationUser)
        {
            if (!$preUpdateEventArgs->hasChangedField('id') || $entity->getEnabled())
            {
                if(array_key_exists('organization', $changeset))
                {
                    foreach($changeset['organization'] as $organization)
                    {
                        if($organization && $organization instanceof Organization)
                        {
                            foreach($organization->getUsers() as $ou)
                            {
                                $this->crawlObject($ou->getUser());
                            }
                            foreach($organization->getProjects() as $po)
                            {
                                $project = $po->getProject();
                                $this->crawlObject($project);
                                foreach($project->getGalleryHasMedias() as $ghm)
                                {
                                    $this->crawlObject($ghm->getMedia());
                                }
                            }
                        }
                    }
                }
                $this->crawlObject($entity);
            }
        }
        elseif($entity instanceof ProjectOrganization)
        {
            if( $preUpdateEventArgs->hasChangedField('organization') ||
                    $preUpdateEventArgs->hasChangedField('project') ||
                    $preUpdateEventArgs->hasChangedField('enabled')
            )
            {
                if(array_key_exists('project', $changeset))
                {
                    foreach($changeset['project'] as $project)
                    {
                        if($project)
                        {
                            $this->crawlObject($project);
                            foreach($project->getGalleryHasMedias() as $ghm)
                            {
                                $this->crawlObject($ghm->getMedia());
                            }
                        }
                    }
                }
                $this->crawlObject($entity);
            }
        }
        elseif($entity instanceof ProjectLocation ||
                $entity instanceof ProjectCoordinationMechanism ||
                $entity instanceof ProjectDonor ||
                $entity instanceof Modality
                )
        {
            if($preUpdateEventArgs->hasChangedField('project'))
            {
                if(array_key_exists('project', $changeset))
                {
                    foreach($changeset['project'] as $project)
                    {
                        if($project)
                        {
                            $this->crawlObject($project);
                            foreach($project->getGalleryHasMedias() as $ghm)
                            {
                                $this->crawlObject($ghm->getMedia());
                            }
                        }
                    }
                }
                $this->crawlObject($entity);
            }
        }
        elseif($entity instanceof ProjectField)
        {
            if( $preUpdateEventArgs->hasChangedField('project') ||
                    $preUpdateEventArgs->hasChangedField('field')
            )
            {
                if(array_key_exists('project', $changeset))
                {
                    foreach($changeset['project'] as $project)
                    {
                        if($project)
                        {
                            $this->crawlObject($project);
                            foreach($project->getGalleryHasMedias() as $ghm)
                            {
                                $this->crawlObject($ghm->getMedia());
                            }
                        }
                    }
                }
                $this->crawlObject($entity);
            }
        }
        elseif($entity instanceof Field)
        {
            if($preUpdateEventArgs->hasChangedField('user'))
            {
                if(array_key_exists('project', $changeset))
                {
                    foreach($changeset['project'] as $project)
                    {
                        if($project)
                        {
                            $this->crawlObject($project);
                            foreach($project->getGalleryHasMedias() as $ghm)
                            {
                                $this->crawlObject($ghm->getMedia());
                            }
                        }
                    }
                }
                $this->crawlObject($entity);
            }
        }
        elseif($entity instanceof ProjectMedia)
        {
            if( $preUpdateEventArgs->hasChangedField('project') ||
                    $preUpdateEventArgs->hasChangedField('media')
            )
            {
                if(array_key_exists('project', $changeset))
                {
                    foreach($changeset['project'] as $project)
                    {
                        if($project)
                        {
                            $this->crawlObject($project);
                            foreach($project->getGalleryHasMedias() as $ghm)
                            {
                                $this->crawlObject($ghm->getMedia());
                            }
                        }
                    }
                }
                if(array_key_exists('media', $changeset))
                {
                    foreach($changeset['media'] as $media)
                    {
                        if($media)
                        {
                            $this->crawlObject($media);
                        }
                    }
                }
                $this->crawlObject($entity);
            }
        }
        elseif ($entity instanceof User)
        {
            if( $preUpdateEventArgs->hasChangedField('ownedProjects') ||
                    $preUpdateEventArgs->hasChangedField('editableProjects') ||
                    $preUpdateEventArgs->hasChangedField('organizations')
            )
            {
                if(array_key_exists('ownedProjects', $changeset))
                {
                    foreach($changeset['ownedProjects'] as $projects)
                    {
                        if ($projects)
                        {
                            foreach($projects as $project)
                            {
                                $this->crawlObject($project);
                                foreach($project->getGalleryHasMedias() as $ghm)
                                {
                                    $this->crawlObject($ghm->getMedia());
                                }
                            }
                        }
                    }
                }
                if(array_key_exists('editableProjects', $changeset))
                {
                    foreach($changeset['editableProjects'] as $projects)
                    {
                        if ($projects)
                        {
                            foreach($projects as $project)
                            {
                                $this->crawlObject($project);
                                foreach($project->getGalleryHasMedias() as $ghm)
                                {
                                    $this->crawlObject($ghm->getMedia());
                                }
                            }
                        }
                    }
                }
                if(array_key_exists('organizations', $changeset))
                {
                    foreach($changeset['organizations'] as $organizations)
                    {
                        if ($organizations)
                        {
                            foreach ($organizations as $uo)
                            {
                                if ($uo->getOrganization() && ($uo->getId() || $uo->getEnabled()))
                                {
                                    foreach($uo->getOrganization()->getProjects() as $po)
                                    {
                                        $project = $po->getProject();
                                        $this->crawlObject($project);
                                        foreach($project->getGalleryHasMedias() as $ghm)
                                        {
                                            $this->crawlObject($ghm->getMedia());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $this->crawlObject($entity);
            }
        }
        elseif ($entity instanceof Project)
        {
            if( $preUpdateEventArgs->hasChangedField('owners') ||
                    $preUpdateEventArgs->hasChangedField('editors') ||
                    $preUpdateEventArgs->hasChangedField('locations') ||
                    $preUpdateEventArgs->hasChangedField('donors') ||
                    $preUpdateEventArgs->hasChangedField('coordinationMechanisms') ||
                    $preUpdateEventArgs->hasChangedField('fields') ||
                    $preUpdateEventArgs->hasChangedField('modalities') ||
                    $preUpdateEventArgs->hasChangedField('galleryHasMedias') ||
                    $preUpdateEventArgs->hasChangedField('sectors') ||
                    $preUpdateEventArgs->hasChangedField('organizations') 
            )
            {
                $this->crawlObject($entity);
               // foreach($project->getGalleryHasMedias() as $ghm)
                foreach($entity->getGalleryHasMedias() as $ghm)
                {
                    $this->crawlObject($ghm->getMedia());
                }
            }
        }
        else
        {
            //OrganizationUser, User, Group, Project
            $this->crawlObject($entity);
        }
    }
    
    public function crawlObject($entity) {
        if($entity instanceof Contact)
        {
            $this->addToProcessedQueue($entity);
        }
        elseif($entity instanceof OrganizationUser)
        {
            if($entity->getOrganization())
            {
                foreach($entity->getOrganization()->getUsers() as $ou)
                {
                    $this->addToProcessedQueue($ou->getUser());
                }
                foreach($entity->getOrganization()->getProjects() as $po)
                {
                    if ($entity->getOrganization())
                    {
                        $project = $po->getProject();
                        $this->addToProcessedQueue($project);
                        foreach($project->getGalleryHasMedias() as $ghm)
                        {
                            $this->addToProcessedQueue($ghm->getMedia());
                        }
                    }
                }
            }
        }
        elseif(
                $entity instanceof ProjectOrganization ||
                $entity instanceof ProjectLocation ||
                $entity instanceof ProjectDonor ||
                $entity instanceof ProjectCoordinationMechanism ||
                $entity instanceof ProjectField ||
                $entity instanceof Field ||
                $entity instanceof Modality ||
                $entity instanceof ProjectMedia
        )
        {
            if($entity->getProject())
            {
                $project = $entity->getProject();
                $this->addToProcessedQueue($project);
                foreach($project->getGalleryHasMedias() as $ghm)
                {
                    $this->addToProcessedQueue($ghm->getMedia());
                }
            }
        }
        elseif ($entity instanceof User)
        {
            foreach($entity->getOwnedProjects() as $project)
            {
                $this->addToProcessedQueue($project);
                foreach($project->getGalleryHasMedias() as $ghm)
                {
                    $this->addToProcessedQueue($ghm->getMedia());
                }
            }
            foreach($entity->getEditableProjects() as $project)
            {
                $this->addToProcessedQueue($project);
                foreach($project->getGalleryHasMedias() as $ghm)
                {
                    $this->addToProcessedQueue($ghm->getMedia());
                }
            }
            foreach($entity->getOrganizations() as $uo)
            {
                if ($uo->getOrganization())
                {
                    foreach($uo->getOrganization()->getProjects() as $po)
                    {
                        $project = $po->getProject();
                        $this->addToProcessedQueue($project);
                        foreach($project->getGalleryHasMedias() as $ghm)
                        {
                            $this->addToProcessedQueue($ghm->getMedia());
                        }
                    }
                }
                
            }
        }
        elseif ($entity instanceof Project)
        {
            $this->addToProcessedQueue($entity);
            foreach($entity->getGalleryHasMedias() as $ghm)
            {
                $this->addToProcessedQueue($ghm->getMedia());
            }
        }
        elseif (!$entity instanceof LogEntry && !$entity instanceof AbstractPersonalTranslation)
        {
            $this->addToProcessedQueue($entity);
        }
    }
    
    private function process()
    {
        foreach($this->rawQueue as $classname => $changesets)
        {
            foreach($changesets as $id => $changeset)
            {
                $entity = $this->entityManager->getRepository($classname)->find($id);
                $this->handleChanges($entity, $changeset);
            }
        }
    }
    
    public function getProcessedQueue()
    {
        if(!$this->processed)
        {
            $this->process();
        }
        
        return $this->processedQueue;
    }
    
    public function getRawQueue()
    {
        return $this->rawQueue;
    }
    
    public function setRawQueue($queue)
    {
        $this->rawQueue = $queue;
        $this->processed = false;
    }
}
