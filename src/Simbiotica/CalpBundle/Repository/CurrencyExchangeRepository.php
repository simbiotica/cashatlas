<?php

namespace Simbiotica\CalpBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;

class CurrencyExchangeRepository extends EntityRepository
{
	public function getLast()
	{
        $query = $this->createQueryBuilder('ce')
        		->orderBy('ce.date', 'desc')
        		->setMaxResults(1)
        		->getQuery();
        try {
            $result = $query->getArrayResult();
            return $result[0]['rate'];
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
	}
}