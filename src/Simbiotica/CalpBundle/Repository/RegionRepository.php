<?php

namespace Simbiotica\CalpBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;

class RegionRepository extends EntityRepository
{
    public function getList($locale, $asArray = true)
    {
        $query = $this->getEntityManager()->createQuery('SELECT '.($asArray?'r.id, r.name':'r').' FROM SimbioticaCalpBundle:Region r ORDER BY r.name ASC');
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_FALLBACK, 1)
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_LARGE, __METHOD__. $locale. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            return $query->getResult($asArray?Query::HYDRATE_ARRAY:Query::HYDRATE_OBJECT);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getListByCountry($locale, $country, $asArray = true)
    {
        $query = $this->getEntityManager()->createQuery(
                'SELECT '.($asArray?'r.id, r.name':'r').' 
                FROM SimbioticaCalpBundle:Region r 
                LEFT JOIN r.country c
                WHERE c.id = :id
                ORDER BY r.name ASC')->setParameter('id', $country);
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_FALLBACK, 1)
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_LARGE, __METHOD__. $locale. serialize($query->getParameters()));
        $query->useQueryCache(true);

        try {
            return $query->getResult($asArray?Query::HYDRATE_ARRAY:Query::HYDRATE_OBJECT);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getListWithProjects($locale, $published = SimbioticaCalpBundle::PUBLISHED_PUBLIC, $asArray = true)
    {
        $query = $this->getEntityManager()->createQuery('
            SELECT distinct r.id, r.name FROM SimbioticaCalpBundle:Region r 
            LEFT JOIN r.projects loc LEFT JOIN loc.project p 
            WHERE p.id is not null AND p.published >= :published 
            ORDER BY r.name asc');
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_FALLBACK, 1)
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
                ->setParameter('published', $published);
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_SMALL, __METHOD__. $locale. serialize($query->getParameters()));
        $query->useQueryCache(true);

        try {
            return $query->getResult($asArray?Query::HYDRATE_ARRAY:Query::HYDRATE_OBJECT);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getByCountry($countryIds, $filtered = false)
    {
        $qb = $this->createQueryBuilder('r')
                ->leftJoin('r.country', 'c')
                ->where('c.id in (:ids)')
                ->setParameter('ids', $countryIds);
        if($filtered)
        {
            $qb->leftJoin('r.projects', 'pr')
                    ->andWhere('pr.id is not null');
        }
        $query = $qb->getQuery();
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_LARGE, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            $result = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        return $result;
    }
}
?>
