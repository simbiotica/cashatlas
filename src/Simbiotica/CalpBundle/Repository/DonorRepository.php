<?php

namespace Simbiotica\CalpBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;

class DonorRepository extends EntityRepository
{
    public function getList($locale, $asArray = true)
    {
        $query = $this->getEntityManager()->createQuery('SELECT '.($asArray?'d.id, d.name':'d').' FROM SimbioticaCalpBundle:Donor d ORDER BY d.name ASC');
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_FALLBACK, 1)
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);

        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_MEDIUM, __METHOD__. $locale. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            return $query->getResult($asArray?Query::HYDRATE_ARRAY:Query::HYDRATE_OBJECT);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getListWithProjects($locale, $published = SimbioticaCalpBundle::PUBLISHED_PUBLIC, $asArray = true)
    {
        $query = $this->getEntityManager()->createQuery('
            SELECT distinct d.id, d.name FROM SimbioticaCalpBundle:Donor d 
            LEFT JOIN d.projects pd LEFT JOIN pd.project p 
            WHERE p.id is not null AND p.published >= :published 
            ORDER BY d.name asc');
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_FALLBACK, 1)
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
                ->setParameter('published', $published);

        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_SMALL, __METHOD__. $locale. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            return $query->getResult($asArray?Query::HYDRATE_ARRAY:Query::HYDRATE_OBJECT);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function searchDonor($keyword)
    {
        $query = $this->createQueryBuilder('d')
            ->where('d.name LIKE :keyword')
            ->setParameter('keyword', $keyword)
        ->getQuery();
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_MEDIUM, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            $result = $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        } catch (\Doctrine\ORM\NonUniqueResultException $e) {
            $tempResult = $query->getResult();
            $result = array_shift($tempResult);
        }
        

        return $result;
    }
}
