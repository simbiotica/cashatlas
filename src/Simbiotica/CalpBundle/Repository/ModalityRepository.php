<?php

namespace Simbiotica\CalpBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;

class ModalityRepository extends EntityRepository
{
    public function getSums($projectIds, $published = SimbioticaCalpBundle::PUBLISHED_PUBLIC)
    {
        if (!is_array($projectIds) || empty($projectIds))
            return null;

        $query = $this->createQueryBuilder('m')
                ->select("m.modality as id, count(distinct p.id ) as number")
                ->leftJoin('m.project', 'p')
                ->where('p.id in (:ids)')
                ->andWhere('p.published >= :published')
                ->groupBy('m.modality')
                ->orderBy('number', 'desc')
                ->setParameter('ids', $projectIds)
                ->setParameter('published', $published)
        ->getQuery();

        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_SMALL, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            $result = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        return $result;
    }
}
