<?php

namespace Simbiotica\CalpBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class UserRepository extends EntityRepository
{
    public function getEnabledCount()
    {
        $query = $this->getEntityManager()->createQuery(
                'SELECT count(distinct u.id ) as number
                FROM SimbioticaCalpBundle:User u 
                WHERE u.enabled = :enabled'
            )
            ->setParameter('enabled', true);
        
        try {
            return $query->getResult(Query::HYDRATE_ARRAY);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
