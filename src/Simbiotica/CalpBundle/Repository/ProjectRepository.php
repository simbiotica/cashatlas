<?php

namespace Simbiotica\CalpBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\ResultSetMapping;

use Simbiotica\CalpBundle\Utils\CurrencyExchange;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;

class ProjectRepository extends EntityRepository
{
    public function getList($locale, $asArray = true)
    {
        $query = $this->getEntityManager()->createQuery('SELECT '.($asArray?'p.id, p.specificObjective':'p').' FROM SimbioticaCalpBundle:Project p ORDER BY p.id ASC');
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_MEDIUM, __METHOD__. $locale. serialize($query->getParameters()));
        $query->useQueryCache(true);

        try {
            return $query->getResult($asArray?Query::HYDRATE_ARRAY:Query::HYDRATE_OBJECT);
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function findByRevisors(array $users) {
        $query = $this->getEntityManager()->createQuery(
                'SELECT p 
                FROM SimbioticaCalpBundle:Project p 
                LEFT JOIN p.revisors r 
                WHERE r.id IN (:users)'
            )
            ->setParameter('users', array_map(function ($item) {return $item->getId();}, $users));
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_SMALL, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getMaxScale()
    {
        $query = $this->getEntityManager()->createQuery(
                'SELECT max(p.scale) 
                FROM SimbioticaCalpBundle:Project p 
                WHERE p.published >= :visibility'
            )
            ->setParameter('visibility', SimbioticaCalpBundle::PUBLISHED_PUBLIC);
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_SMALL, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            return $query->getSingleScalarResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getSumScales($ids, $visibility = SimbioticaCalpBundle::PUBLISHED_PUBLIC)
    {
        $query = $this->getEntityManager()->createQuery(
                    'SELECT sum(p.scale)
                    FROM SimbioticaCalpBundle:Project p 
                    WHERE p.id in (:id) AND p.published >= :visibility'
                )->setParameter('id', $ids)
                ->setParameter('visibility', $visibility);
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_SMALL, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            return $query->getSingleScalarResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    // SUM(p.budgetAllocated) AS budgetAllocatedSum, 
    public function getSumCash($ids, $currency, $visibility = SimbioticaCalpBundle::PUBLISHED_PUBLIC)
    {
        $query = $this->getEntityManager()->createQuery(
                    'SELECT sum(p.budgetOverall) as budgetOverallSum, 
                     sum(coalesce(CASE WHEN p.budgetAllocated=0 THEN p.initialBudgetAllocated ELSE p.budgetAllocated END, 0)) as budgetAllocatedSum,
                     p.currency AS currency
                    FROM SimbioticaCalpBundle:Project p 
                    WHERE p.id in (:id) AND p.published >= :visibility GROUP BY p.currency'
                )->setParameter('id', $ids)
                ->setParameter('visibility', $visibility);
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_SMALL, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            $queryResult = $query->getResult();
            $currencyExchange = new CurrencyExchange($this->getEntityManager());

            $result = $currencyExchange->exchange($queryResult, $currency);
            return $result;
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getPublishedCount()
    {
        $query = $this->getEntityManager()->createQuery(
                'SELECT count(distinct p.id ) as number
                FROM SimbioticaCalpBundle:Project p 
                WHERE p.published >= :visibility'
            )
            ->setParameter('visibility', SimbioticaCalpBundle::PUBLISHED_PUBLIC);
        
        try {
            return $query->getResult(Query::HYDRATE_ARRAY);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getYearRange()
    {
        $query = $this->getEntityManager()->createQuery(
                'SELECT 
                    max(SUBSTRING(p.firstCashDate, -6, 4)) as first_cash_max, 
                    min(SUBSTRING(p.firstCashDate, -6, 4)) as first_cash_min, 
                    max(SUBSTRING(p.endingDate, -6, 4)) as end_max, 
                    min(SUBSTRING(p.endingDate, -6, 4)) as end_min 
                    FROM SimbioticaCalpBundle:Project p 
                    WHERE LENGTH(p.firstCashDate) >= 35 
                    AND LENGTH(p.endingDate) >= 35 
                    AND p.startingDate not like \'%:"year";s:4:"%\'
                    AND p.published >= :visibility')
            ->setParameter('visibility', SimbioticaCalpBundle::PUBLISHED_PUBLIC);
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_SMALL, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            return $query->getResult(Query::HYDRATE_ARRAY);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getAgregatedValues($projectIds)
    {
        $query = $this->getEntityManager()->createQuery(
                'SELECT sum(coalesce(CASE WHEN p.scale=0 THEN p.initialScale ELSE p.scale END, 0)) as scale, 
                sum(coalesce(CASE WHEN p.budgetAllocated=0 THEN p.initialBudgetAllocated ELSE p.budgetAllocated END, 0)) as budgetAllocated,
                count(DISTINCT p.id) as projectCount
                FROM SimbioticaCalpBundle:Project p 
                WHERE p.id IN (:ids) AND p.published >= :visibility')
            ->setParameter('visibility', SimbioticaCalpBundle::PUBLISHED_PUBLIC)
            ->setParameter('ids', $projectIds);
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_SMALL, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            $result = $query->getResult(Query::HYDRATE_ARRAY);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        return $result;
    }

    public function getAgregatedValuesWithExchange($projectIds, $currency='EUR')
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('Project', 'p');
        $rsm->addScalarResult('scale', 'scale');
        $rsm->addScalarResult('budget_allocated', 'budgetAllocated');
        $rsm->addScalarResult('projectCount', 'projectCount');

        $query = $this->getEntityManager()->createNativeQuery(
                'SELECT sum(coalesce(CASE WHEN p.scale=0 THEN p.initial_scale ELSE p.scale END, 0)) as scale, 
                    sum(coalesce(
                        CASE WHEN p.currency<>? THEN 
                            CASE WHEN p.currency="USD" THEN 
                                CASE WHEN p.budget_allocated = 0 THEN 
                                    p.initial_budget_allocated / rate ELSE p.budget_allocated / rate 
                                END 
                            ELSE CASE WHEN p.budget_allocated = 0 THEN 
                                 p.initial_budget_allocated * rate ELSE p.budget_allocated * rate 
                                 END 
                            END 
                        ELSE 
                            CASE WHEN p.budget_allocated = 0 THEN 
                                p.initial_budget_allocated ELSE p.budget_allocated 
                            END 
                        END, 0)) AS budget_allocated,
                count(DISTINCT p.id) as projectCount FROM project AS p, currency_exchange AS c 
                WHERE p.id IN (?) AND c.id = (SELECT id FROM currency_exchange ORDER BY date DESC LIMIT 1)
                AND p.published >= ?', $rsm)
            ->setParameter(1, $currency)
            ->setParameter(2, $projectIds)
            ->setParameter(3, SimbioticaCalpBundle::PUBLISHED_PUBLIC);
        
        try {
            $result = $query->getResult(Query::HYDRATE_ARRAY);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        return $result;
    }
    
    public function searchProject($continent = null, $country = null, $region = null, 
            $organization = null, $donor = null, $implementingPartners = null, 
            $sector = null, $modality = null, $deliveryMechanism = null, 
            $deliveryAgent= null, $status = null, 
            $years = null, array $before = null, array $after = null,
            $scaleMin = null, $scaleMax = null, $duration = null, 
            $keyword = null, $securityContext = false, $locale = null, 
            $visibility = SimbioticaCalpBundle::PUBLISHED_PUBLIC)
    {
        $from = array('SimbioticaCalpBundle:Project p');
        $where = array('p.published >= :visibility');
        $parameters = array('visibility' => $visibility);
        
        if ($country)
        {
            $from[] = 'p.locations loc';
            $from[] = 'loc.country ctr';
            if (is_array($country))
                $where[] = 'ctr.id IN (:country)';
            else 
                $where[] = 'ctr.id = :country';
            $parameters['country'] = $country;
        }
        
        if($continent)
        {
            if (!$country)
            {
                $from[] = 'p.locations loc';
                $from[] = 'loc.country ctr';
            }
            $select[] = 'ctn';
            $from[] = 'ctr.continent ctn';
            if (is_array($continent))
                $where[] = 'ctn.id IN (:continent)';
            else
                $where[] = 'ctn.id = :continent';
            $parameters['continent'] = $continent;
        }
        
        if($region)
        {
            if (!$country && !$continent)
            {
                $from[] = 'p.locations loc';
            }
            $select[] = 'rgn';
            $from[] = 'loc.region rgn';
            if (is_array($region))
                $where[] = 'rgn.id IN (:region)';
            else
                $where[] = 'rgn.id = :region';
            $parameters['region'] = $region;
        }
        
        if($organization || $keyword)
        {
            $from[] = 'p.organizations po';
            $from[] = 'po.organization org';
            if($organization)
            {
                if (is_array($organization))
                    $where[] = 'org.id IN (:organization)';
                else
                    $where[] = 'org.id = :organization';
                $parameters['organization'] = $organization;
            }
        }
        
        if($donor || $keyword)
        {
            $from[] = 'p.donors dnr';
            $from[] = 'dnr.organization o';
            if($donor)
            {
                if (is_array($donor))
                    $where[] = 'o.id IN (:donor)';
                else
                    $where[] = 'o.id = :donor';
                $parameters['donor'] = $donor;
            }
        }
        
        if($implementingPartners)
        {
            $where[] =  'p.implementingPartners LIKE :implementingPartners';
            $parameters['implementingPartners'] = '%'.$implementingPartners.'%';
        }
        if($scaleMin)
        {
            $where[] =  'p.scale >= :scaleMin';
            $parameters['scaleMin'] = $scaleMin;
        }
        if($scaleMax)
        {
            $where[] =  'p.scale <= :scaleMax';
            $parameters['scaleMax'] = $scaleMax;
        }
        if($duration)
        {
            if(is_int($duration))
                $duration = array($duration);
            if (is_array($duration) && count($duration) > 0)
            {
                $durationWhereArray = array();
                foreach ($duration as $durationValue)
                {
                    switch ($durationValue) {
                    case 1:
                        $durationWhereArray[] =  'p.duration < 4';
                        break;
                    case 2:
                        $durationWhereArray[] =  'p.duration >= 4 AND p.duration < 6';
                        break;
                    case 3:
                        $durationWhereArray[] =  'p.duration >= 6 AND p.duration < 12';
                        break;
                    case 4:
                        $durationWhereArray[] =  'p.duration >= 12 AND p.duration < 18';
                        break;
                    case 5:
                        $durationWhereArray[] =  'p.duration >= 18';
                        break;
                    }
                }
                $where[] = sprintf('(%s)', implode(') OR (', $durationWhereArray));
            }
        }
        
        if($sector || $keyword)
        {
            $from[] = 'p.sectors sct';
            if ($sector)
            {
                if (is_array($sector))
                    $where[] = 'sct.id IN (:sector)';
                else
                    $where[] = 'sct.id = :sector';
                $parameters['sector'] = $sector;
            }
        }
        
        if ($modality || $deliveryMechanism || $deliveryAgent)
        {
            $from[] = 'p.modalities mdl';
            if($modality)
            {
                if (is_array($modality))
                    $where[] = 'mdl.modality IN (:modality)';
                else
                    $where[] = 'mdl.modality = :modality';
                $parameters['modality'] = $modality;
            }
            if($deliveryMechanism)
            {
                if (is_array($deliveryMechanism))
                    $where[] = 'mdl.deliveryMechanism IN (:deliveryMechanism)';
                else
                    $where[] = 'mdl.deliveryMechanism = :deliveryMechanism';
                $parameters['deliveryMechanism'] = $deliveryMechanism;
            }
            if($deliveryAgent)
            {
                if (is_array($deliveryAgent))
                    $where[] = 'mdl.deliveryAgent IN (:deliveryAgent)';
                else
                    $where[] = 'mdl.deliveryAgent = :deliveryAgent';
                $parameters['deliveryAgent'] = $deliveryAgent;
            }
        }
        
        if($keyword)
        {
            $where[] = 
                'p.implementingPartners LIKE :keyword OR 
                p.specificObjective LIKE :keyword OR 
                p.projectContext LIKE :keyword OR 
                p.marketAssessment LIKE :keyword OR 
                p.typeOfInterventionSpecify LIKE :keyword OR 
                p.contactPersonName LIKE :keyword OR 
                p.contactPersonPosition LIKE :keyword OR 
                p.contactPersonEmailOne LIKE :keyword OR 
                p.contactPersonEmailTwo LIKE :keyword OR 
                p.contactOrganizationUrl LIKE :keyword OR 
                p.description LIKE :keyword OR 
                p.city LIKE :keyword OR 
                p.affiliates LIKE :keyword OR 
                p.otherSectors LIKE :keyword OR 
                org.name LIKE :keyword OR 
                d.name LIKE :keyword OR 
                sct.name LIKE :keyword'
            ;
            $parameters['keyword'] = '%'.$keyword.'%';
        }
        
        $select = array('distinct(p.id)');
        if($status || $years && is_array($years) || 
                $after && is_array($after) && array_key_exists('year', $after) && array_key_exists('month', $after) ||
                $before && is_array($before) && array_key_exists('year', $before) && array_key_exists('month', $before) )
        {
            $select[] = 'p.firstCashDate';
            $select[] = 'p.endingDate';
        }
        
        $dql = sprintf('SELECT %s FROM %s WHERE (%s)', implode(', ', $select), implode(' LEFT JOIN ', $from), implode(') AND (', $where));
        
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_FALLBACK, 1)
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        foreach($parameters as $name => $value)
            $query->setParameter ($name, $value);
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_SMALL, __METHOD__. $locale. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            $result = $query->getResult(Query::HYDRATE_ARRAY);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
        

        if($status)
        {
            $curYear = (string)date('Y'); 
            $curMonth = (string)date('n'); 
            $newResult = array();
            
            foreach($result as $project)
            {
                $projectFirstCashDate = $project['firstCashDate'];
                $projectEndingDate = $project['endingDate'];
                
                if(!is_array($projectFirstCashDate) || !is_array($projectEndingDate) || count($projectFirstCashDate) != 2 || count($projectEndingDate) != 2)
                    continue;
                
                if (!is_array($status))
                {
                    $status = array($status);
                }
                foreach($status as $statusLine)
                {
                    switch ($statusLine) {
                        case SimbioticaCalpBundle::STATUS_UPCOMING:
                            if ( ($projectFirstCashDate['month'] >= $curMonth && $projectFirstCashDate['year'] == $curYear) || $projectFirstCashDate['year'] > $curYear)
                                $newResult[$project['id']] = $project;
                            break;
                        case SimbioticaCalpBundle::STATUS_ONGOING:
                            if (( ($projectFirstCashDate['month'] <= $curMonth && $projectFirstCashDate['year'] == $curYear) || $projectFirstCashDate['year'] < $curYear) &&
                                   ( ($projectEndingDate['month'] >= $curMonth && $projectEndingDate['year'] == $curYear) || $projectEndingDate['year'] > $curYear) )
                                $newResult[$project['id']] = $project;
                            break;
                        case SimbioticaCalpBundle::STATUS_COMPLETED:
                            if ( ($projectEndingDate['month'] < $curMonth && $projectEndingDate['year'] == $curYear) || $projectEndingDate['year'] < $curYear)
                                $newResult[$project['id']] = $project;
                            break;
                    }
                }
            }
            $result = $newResult;
        }
        
        if($years && is_array($years))
        {
            $newResult = array();
            
            foreach($result as $project)
            {
                $projectFirstCashDate = $project['firstCashDate'];
                
                if(!is_array($projectFirstCashDate) || count($projectFirstCashDate) != 2)
                    continue;
                
                if (in_array($projectFirstCashDate['year'], $years))
                    $newResult[$project['id']] = $project;
            }
            $result = $newResult;
        }
        
        if($after && is_array($after) && array_key_exists('year', $after) && array_key_exists('month', $after) )
        {
            //we assume a project is after a date if it didn't end before that date
            $newResult = array();
            
            foreach($result as $project)
            {
                $projectEndingDate = $project['endingDate'];
                
                if(!is_array($projectEndingDate) || count($projectEndingDate) != 2)
                    continue;
                
                if ($projectEndingDate['year'] > $after['year'] || ($projectEndingDate['year'] == $after['year'] && $projectEndingDate['month'] >= $after['month']) )
                    $newResult[$project['id']] = $project;
            }
            $result = $newResult;
        }
        
        if($before && is_array($before) && array_key_exists('year', $before) && array_key_exists('month', $before) )
        {
            //we assume a project is before a date if it didn't start before that date
            $newResult = array();
            
            foreach($result as $project)
            {
                $projectFirstCashDate = $project['firstCashDate'];
                
                if(!is_array($projectFirstCashDate) || count($projectFirstCashDate) != 2)
                    continue;
                
                if ($projectFirstCashDate['year'] < $before['year'] || ($projectFirstCashDate['year'] == $before['year'] && $projectFirstCashDate['month'] <= $before['month']) )
                    $newResult[$project['id']] = $project;
            }
            $result = $newResult;
        }
        
        if($securityContext)
        {
            $newResult = array();
            
            foreach($result as $project)
            {
                if($securityContext->isGranted('EDIT', $this->find($project['id'])))
                    $newResult[$project['id']] = $project;
            }
            $result = $newResult;
        }

        return array_map(function ($project) {return $project['id'];}, $result);
    }

    public function searchByDates($startMonth = null, $startYear = null, $endMonth = null, $endYear = null) 
    {
        $project = array();
        if (!($startMonth || $startYear || $endMonth || $endYear))
            return array();
        
        if ($startMonth || $startYear)
        {
            $startDate = '';
            $startDate .= $startMonth?sprintf('%%s:5:"month";i:%d;', $startMonth):'';
            $startDate .= $startYear?sprintf('s:4:"year";i:%d;%%', $startYear):'';
        }
        else
            $startDate = null;
        
        if ($endMonth || $endYear)
        {
            $endDate = '';
            $endDate .= $endMonth?sprintf('%%s:5:"month";i:%d;', (int)$endMonth):'';
            $endDate .= $endYear?sprintf('s:4:"year";i:%d;%%', (int)$endYear):'';
        }
        else
            $endDate = null;
        
        if($startDate)
        {
            $qbStart = $this->getEntityManager()->createQueryBuilder()
                ->select('p.id')
                ->from('SimbioticaCalpBundle:Project', 'p');

            $qbStart->andWhere('p.startingDate LIKE :start_date')
                    ->setParameter('start_date', $startDate);
            
            $query = $qbStart->getQuery();

            $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_LARGE, __METHOD__. serialize($query->getParameters()));
            $query->useQueryCache(true);  

            $projects['start'] = $query->getArrayResult();  
        }
        if($endDate)
        {
            $qbEnd = $this->getEntityManager()->createQueryBuilder()
                ->select('p.id')
                ->from('SimbioticaCalpBundle:Project', 'p');

            $qbEnd->andWhere('p.endingDate LIKE :ending_date')
                    ->setParameter('ending_date', $endDate);

            $query = $qbEnd->getQuery();

            $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_LARGE, __METHOD__. serialize($query->getParameters()));
            $query->useQueryCache(true);  

            $projects['end'] = $query->getArrayResult();  
        }
        
        return $projects;
    }
}
