<?php

namespace Simbiotica\CalpBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class SimilarProjectRepository extends EntityRepository
{
    public function getNewMatches($reference) {
        $query = $this->getEntityManager()->createQuery('
            SELECT IDENTITY(sp.target)
            FROM SimbioticaCalpBundle:SimilarProject sp 
            WHERE IDENTITY(sp.reference) = :reference')
        ->setParameter('reference', $reference);

        try {
            $result = $query->getResult(Query::HYDRATE_ARRAY);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        return $result;
    }

    public function numberSimilarProjects($reference) {
        $query = $this->getEntityManager()->createQuery('
            SELECT COUNT(sp.id) 
            FROM SimbioticaCalpBundle:SimilarProject sp 
            WHERE sp.reference = :reference')
        ->setParameter('reference', $reference);

        try {
            $result = $query->getResult(Query::HYDRATE_SINGLE_SCALAR);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        return $result;
    }
}
