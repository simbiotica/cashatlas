<?php

namespace Simbiotica\CalpBundle\Repository;

use Doctrine\ORM\EntityRepository;;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;

class ContactRepository extends EntityRepository
{
    public function findByPendingAssignee($user)
    {
        $query = $this->getEntityManager()
        ->createQuery('SELECT c FROM SimbioticaCalpBundle:Contact c
            LEFT JOIN c.assignedTo users
            WHERE c.status = 0
            AND users.id = :user')
            ->setParameter('user', $user->getId())
            ->setMaxResults(10);

        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_SMALL, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
