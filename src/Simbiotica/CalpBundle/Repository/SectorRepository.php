<?php

namespace Simbiotica\CalpBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;

class SectorRepository extends EntityRepository
{
    public function getList($locale, $asArray = true)
    {
        $query = $this->getEntityManager()->createQuery('SELECT '.($asArray?'s.id, s.name':'s').' FROM SimbioticaCalpBundle:Sector s ORDER BY s.name ASC');
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_FALLBACK, 1)
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);

        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_LARGE, __METHOD__. $locale. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            return $query->getResult($asArray?Query::HYDRATE_ARRAY:Query::HYDRATE_OBJECT);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getSums($projectIds, $published = SimbioticaCalpBundle::PUBLISHED_PUBLIC)
    {
        if (!is_array($projectIds) || empty($projectIds))
            return null;

        $query = $this->createQueryBuilder('s')
                ->select("s.id, count(distinct p.id ) as number")
                ->leftJoin('s.projects', 'p')
                ->where('p.id in (:ids)')
                ->andWhere('p.published >= :published')
                ->groupBy('s.id')
                ->orderBy('number', 'desc')
                ->setParameter('ids', $projectIds)
                ->setParameter('published', $published)
        ->getQuery();

        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_SMALL, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);

        try {
            $result = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        return $result;
    }
}
