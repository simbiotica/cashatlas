<?php

namespace Simbiotica\CalpBundle\Repository;

use Gedmo\Sortable\Entity\Repository\SortableRepository;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;

class ContentRepository extends SortableRepository
{
    public function findTranslations($id, $visibility = 3)
    {
        $query = $this->getEntityManager()
        ->createQuery('SELECT c FROM SimbioticaCalpBundle:Content c
            WHERE c.tid = :id
            AND c.published >= :visibility')
            ->setParameter('id', $this->getEntityManager()->find('SimbioticaCalpBundle:Content', $id)->getTid())
            ->setParameter('visibility', $visibility);

        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_MEDIUM, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getMenuContents($language, $visibility = 3)
    {
        $query = $this->getEntityManager()
        ->createQuery('SELECT c FROM SimbioticaCalpBundle:Content c
            WHERE c.language = :language
            AND c.published >= :visibility
            ORDER BY c.position ASC')
            ->setParameter('language', $language)
            ->setParameter('visibility', $visibility);

        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_MEDIUM, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
