<?php

namespace Simbiotica\CalpBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;

class OrganizationRepository extends EntityRepository
{
    public function getCountWithProject()
    {
        $query = $this->getEntityManager()->createQuery(
                'SELECT count(distinct o.id ) as number
                FROM SimbioticaCalpBundle:Organization o
                LEFT JOIN o.projects po
                WHERE po.enabled = :enabled'
            )
            ->setParameter('enabled', true);
        
        try {
            return $query->getResult(Query::HYDRATE_ARRAY);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getCount()
    {
        $query = $this->getEntityManager()->createQuery(
                'SELECT count(distinct o.id ) as number
                FROM SimbioticaCalpBundle:Organization o'
            );
        
        try {
            return $query->getResult(Query::HYDRATE_ARRAY);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getList($locale, $asArray = true)
    {
        $query = $this->getEntityManager()->createQuery('SELECT '.($asArray?'o.id, o.name':'o').' FROM SimbioticaCalpBundle:Organization o ORDER BY o.name ASC');
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_FALLBACK, 1)
            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_MEDIUM, __METHOD__. $locale. serialize($query->getParameters()));
        $query->useQueryCache(true);

        try {
            return $query->getResult($asArray?Query::HYDRATE_ARRAY:Query::HYDRATE_OBJECT);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getListWithProjects($locale, $published = SimbioticaCalpBundle::PUBLISHED_PUBLIC, $asArray = true)
    {
        $query = $this->getEntityManager()->createQuery('
            SELECT distinct o.id, o.name FROM SimbioticaCalpBundle:Organization o 
            LEFT JOIN o.projects op LEFT JOIN op.project p1 
            LEFT JOIN o.donatingProjects pd LEFT JOIN pd.project p2 
            WHERE (p1.id is not null AND p1.published >= :published) 
            OR (p2.id is not null AND p2.published >= :published) 
            ORDER BY o.name ASC');
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_FALLBACK, 1)
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
                ->setParameter('published', $published);
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_MEDIUM, __METHOD__. $locale. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            return $query->getResult($asArray?Query::HYDRATE_ARRAY:Query::HYDRATE_OBJECT);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getListWithProjectsAsOrganization($locale, $published = SimbioticaCalpBundle::PUBLISHED_PUBLIC, $asArray = true)
    {
        $query = $this->getEntityManager()->createQuery('
            SELECT distinct o.id, o.name FROM SimbioticaCalpBundle:Organization o 
            LEFT JOIN o.projects op LEFT JOIN op.project p 
            WHERE p.id is not null AND p.published >= :published 
            ORDER BY o.name ASC');
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_FALLBACK, 1)
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
                ->setParameter('published', $published);
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_MEDIUM, __METHOD__. $locale. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            return $query->getResult($asArray?Query::HYDRATE_ARRAY:Query::HYDRATE_OBJECT);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getListWithProjectsAsDonor($locale, $published = SimbioticaCalpBundle::PUBLISHED_PUBLIC, $asArray = true)
    {
        $query = $this->getEntityManager()->createQuery('
            SELECT distinct o.id, o.name FROM SimbioticaCalpBundle:Organization o 
            LEFT JOIN o.donatingProjects pd LEFT JOIN pd.project p 
            WHERE p.id is not null AND p.published >= :published 
            ORDER BY o.name ASC');
        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_FALLBACK, 1)
                ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale)
                ->setParameter('published', $published);
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_MEDIUM, __METHOD__. $locale. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            return $query->getResult($asArray?Query::HYDRATE_ARRAY:Query::HYDRATE_OBJECT);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function searchOrganization($keyword)
    {
        $query = $this->createQueryBuilder('o')
            ->where('o.name LIKE :keyword')
            ->setParameter('keyword', $keyword)
        ->getQuery();
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_MEDIUM, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            $result = $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        return $result;
    }
    
    public function getSums($projectIds, $orderBy = 'scale', $published = SimbioticaCalpBundle::PUBLISHED_PUBLIC)
    {
        if (!is_array($projectIds) || empty($projectIds))
            return array();
        
        $query = $this->createQueryBuilder('o')
                ->select("o.id, sum(coalesce(CASE WHEN p.scale=0 THEN p.initialScale ELSE p.scale END, 0)) as scale, 
                    sum(coalesce(CASE WHEN p.budgetAllocated=0 THEN p.initialBudgetAllocated ELSE p.budgetAllocated END, 0)) as amount, 
                    count(distinct p.id ) as number")
                ->leftJoin('o.projects', 'po')
                ->leftJoin('po.project', 'p')
                ->where('p.id in (:ids)')
                ->andWhere('p.published >= :published')
                ->groupBy('o.id')
                ->orderBy($orderBy, 'desc')
                ->setParameter('ids', $projectIds)
                ->setParameter('published', $published)
        ->getQuery();
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_SMALL, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            $result = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        return $result;
    }
    
    public function getSumsAsDonor($projectIds, $orderBy = 'amount', $published = SimbioticaCalpBundle::PUBLISHED_PUBLIC)
    {
        if (!is_array($projectIds) || empty($projectIds))
            return array();
        
        $query = $this->createQueryBuilder('o')
                ->select("o.id, coalesce(sum(p.scale), 0) as scale, coalesce(sum(pd.amount), 0) as amount, coalesce(count(distinct p.id), 0) as number")
                ->leftJoin('o.donatingProjects', 'pd')
                ->leftJoin('pd.project', 'p')
                ->where('p.id in (:ids)')
                ->andWhere('p.published >= :published')
                ->groupBy('o.id, o.name')
                ->orderBy($orderBy, 'desc')
                ->setParameter('ids', $projectIds)
                ->setParameter('published', $published)
        ->getQuery();
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_SMALL, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            $result = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        return $result;
    }
    
    public function getMedia($organizations)
    {
        if (!is_array($organizations) || empty($organizations))
            return array();
        
        $query = $this->createQueryBuilder('o')
                ->select('m, o')
                ->leftJoin('o.media', 'm')
                ->where('m.context = \'organization\'')
                ->andWhere('o.id in (:ids)')
                ->setParameter('ids', $organizations)
            ->getQuery();
        
        $query->useResultCache(true, SimbioticaCalpBundle::CACHE_TTL_MEDIUM, __METHOD__. serialize($query->getParameters()));
        $query->useQueryCache(true);
        
        try {
            $result = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return array();
        }

        return $result;
    }
}
