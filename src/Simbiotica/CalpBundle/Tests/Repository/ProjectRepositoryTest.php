<?php 

namespace Simbiotica\CalpBundle\Tests\Repository;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProjectRepositoryTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
    }

    public function testSearchProject()
    {
        $products = $this->em
            ->getRepository('SimbioticaCalpBundle:Project')
            ->searchProject()
        ;
        
        $allProducts = $this->em
            ->getRepository('SimbioticaCalpBundle:Project')
            ->findAll();
        ;

        $this->assertCount(count($allProducts), $products, 'No arguments loads all projects');
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
    }
}

?>