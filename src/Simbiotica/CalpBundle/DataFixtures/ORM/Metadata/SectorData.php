<?php

namespace Simbiotica\CalpBundle\DataFixtures\ORM\Metadata;

use Simbiotica\CalpBundle\Entity\Sector;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Simbiotica\CalpBundle\Entity\Translations\SectorTranslation;

class SectorData extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {

        $sector = new Sector('Other');
        $manager->persist($sector);

        $sector = new Sector('Food security and livelihood');
        $manager->persist($sector);

        $sector = new Sector('Nutrition');
        $manager->persist($sector);

        $sector = new Sector('Shelter, settlement');
        $manager->persist($sector);

        $sector = new Sector('NFI');
        $manager->persist($sector);

        $sector = new Sector('Water supply, sanitation and hygiene promotion');
        $manager->persist($sector);

        $sector = new Sector('Education');
        $manager->persist($sector);

        $sector = new Sector('Public health');
        $manager->persist($sector);

        $sector = new Sector('Protection');
        $manager->persist($sector);

        $sector = new Sector('Market based intervention');
        $manager->persist($sector);

        $manager->flush();
    }
    
    function getOrder() {
        return 1; // the order in which fixtures will be loaded
    }

}
