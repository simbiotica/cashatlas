<?php

namespace Simbiotica\CalpBundle\DataFixtures\ORM\Metadata;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Simbiotica\CalpBundle\Entity\CoordinationMechanism;
use Simbiotica\CalpBundle\Entity\Translations\CoordinationMechanismTranslation;

class CoordinationMechanismData extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {

        $coordinationMechanism = new CoordinationMechanism('Technical Working Group');
        $manager->persist($coordinationMechanism);

        $coordinationMechanism = new CoordinationMechanism('Cluster');
        $manager->persist($coordinationMechanism);

        $coordinationMechanism = new CoordinationMechanism('Other');
        $manager->persist($coordinationMechanism);

        $coordinationMechanism = new CoordinationMechanism('None');
        $manager->persist($coordinationMechanism);

        $manager->flush();
    }
    
    function getOrder() {
        return 2; // the order in which fixtures will be loaded
    }

}
