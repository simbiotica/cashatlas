<?php

namespace Simbiotica\CalpBundle\DataFixtures\ORM\User;

use Simbiotica\CalpBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface {

    private static $container;

    public function setContainer(ContainerInterface $container = null) {
        self::$container = $container;
    }

    public function load(ObjectManager $manager) {
        $adminUser = new User();
        $adminUser->setUsername('admin-user@simbiotica.es');
        $adminUser->setUsernameCanonical('admin-user@simbiotica.es');
        $adminUser->setFirstname('Admin');
        $adminUser->setLastname('User');
        $adminUser->setEmail('admin-user@simbiotica.es');
        $adminUser->setEmailCanonical('admin-user@simbiotica.es');
        $adminUser->setEnabled(1);
        $adminUser->setPlainPassword('1234567890');
        $adminUser->setLocked(0);
        $adminUser->setExpired(0);
        $adminUser->setCredentialsExpired(0);
        $adminUser->addGroup($manager->merge($this->getReference('super-admin-group')));
        $manager->persist($adminUser);
        $manager->flush();
        $this->addReference('user-admin', $adminUser);

        $one = new User();
        $one->setUsername('one@simbiotica.es');
        $one->setUsernameCanonical('one@simbiotica.es');
        $one->setFirstname('One');
        $one->setLastname('Stub');
        $one->setEmail('one@simbiotica.es');
        $one->setEmailCanonical('one@simbiotica.es');
        $one->setEnabled(1);
        $one->setPlainPassword('1234567890');
        $one->setLocked(0);
        $one->setExpired(0);
        $one->setCredentialsExpired(0);
        $one->addRole('ROLE_EDITOR');
        $manager->persist($one);
        $manager->flush();
        $this->addReference('user-one', $one);
        
        $two = new User();
        $two->setUsername('two@simbiotica.es');
        $two->setUsernameCanonical('two@simbiotica.es');
        $two->setFirstname('One');
        $two->setLastname('Stub');
        $two->setEmail('two@simbiotica.es');
        $two->setEmailCanonical('two@simbiotica.es');
        $two->setEnabled(1);
        $two->setPlainPassword('1234567890');
        $two->setLocked(0);
        $two->setExpired(0);
        $two->setCredentialsExpired(0);
        $two->addRole('ROLE_EDITOR');
        $manager->persist($two);
        $manager->flush();
        $this->addReference('user-two', $two);
        
        $three = new User();
        $three->setUsername('three@simbiotica.es');
        $three->setUsernameCanonical('three@simbiotica.es');
        $three->setFirstname('One');
        $three->setLastname('Stub');
        $three->setEmail('three@simbiotica.es');
        $three->setEmailCanonical('three@simbiotica.es');
        $three->setEnabled(1);
        $three->setPlainPassword('1234567890');
        $three->setLocked(0);
        $three->setExpired(0);
        $three->setCredentialsExpired(0);
        $three->addRole('ROLE_EDITOR');
        $manager->persist($three);
        $manager->flush();
        $this->addReference('user-three', $three);
        
        $four = new User();
        $four->setUsername('four@simbiotica.es');
        $four->setUsernameCanonical('four@simbiotica.es');
        $four->setFirstname('One');
        $four->setLastname('Stub');
        $four->setEmail('four@simbiotica.es');
        $four->setEmailCanonical('four@simbiotica.es');
        $four->setEnabled(1);
        $four->setPlainPassword('1234567890');
        $four->setLocked(0);
        $four->setExpired(0);
        $four->setCredentialsExpired(0);
        $four->addRole('ROLE_EDITOR');
        $manager->persist($four);
        $manager->flush();
        $this->addReference('user-four', $four);
    }

    function getOrder() {
        return 2; // the order in which fixtures will be loaded
    }

}
