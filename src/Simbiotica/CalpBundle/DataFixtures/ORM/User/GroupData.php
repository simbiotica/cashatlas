<?php

namespace Simbiotica\CalpBundle\DataFixtures\ORM\User;

use Doctrine\Common\Persistence\ObjectManager;
use Simbiotica\CalpBundle\Entity\Group;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class GroupData extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {
        $super_admin_group = new Group();
        $super_admin_group->setName('System administrators');
        $super_admin_group->addRole('ROLE_SYSTEM_ADMIN');
        $manager->persist($super_admin_group);
        $manager->flush();
        $this->addReference('super-admin-group', $super_admin_group);
        
        $admin_group = new Group();
        $admin_group->setName('Site administrators');
        $admin_group->addRole('ROLE_SITE_ADMIN');
        $manager->persist($admin_group);
        $manager->flush();
        $this->addReference('admin-group', $admin_group);
    }

    function getOrder() {
        return 1; // the order in which fixtures will be loaded
    }

}

?>