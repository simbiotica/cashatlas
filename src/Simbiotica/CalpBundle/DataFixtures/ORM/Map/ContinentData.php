<?php

namespace Simbiotica\CalpBundle\DataFixtures\ORM\Map;

use Simbiotica\CalpBundle\Entity\Region;
use Simbiotica\CalpBundle\Entity\Country;
use Simbiotica\CalpBundle\Entity\Continent;
use Simbiotica\CalpBundle\Entity\Translations\CountryTranslation;
use Simbiotica\CalpBundle\Entity\Translations\RegionTranslation;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ContinentData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    private static $container;

    public function setContainer(ContainerInterface $container = null)
    {
        self::$container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $target = self::$container->get('simbiotica.cartodb_connection.cashatlas');
        $target->truncateTable('continent_sync');
        $target->truncateTable('country_sync');
        $target->truncateTable('region_sync');

        $provider = self::$container->get('simbiotica.cartodb_connection.cashatlas');

        $continents = $provider
            ->getRowsForColumns('areas_reference', array('cartodb_id', 'area', 'centroid', 'area_id as code', 'ST_AsGeoJSON(the_geom,20,4) as the_geom'), null, array('order' => array('cartodb_id' => 'asc')))
//     	    ->getAllRowsForColumns('areas_reference', array('cartodb_id', 'area_id as code', 'area', 'ST_AsGeoJSON(st_centroid(the_geom)) as centroid'), array('order' => array('cartodb_id' => 'asc')))
        ->getData();

        foreach($continents as $continentData)
        {
            $continent = new Continent();
            $continent->setName($continentData->area);
            $continent->setCartodbId($continentData->cartodb_id);
            $continent->setCentroid($continentData->centroid);
            $continent->setGeom($continentData->the_geom);
            $continent->setCode($continentData->code);

            $countries = $provider
//            ->getRowsForColumns('countries_reference', array('cartodb_id', 'name', 'area_id as code', 'adm0_a3 as code', 'ST_AsGeoJSON(st_centroid(the_geom)) as centroid'), array('area_id' => $continentData->code), array('order' => array('cartodb_id' => 'asc')))
            ->getRowsForColumns('countries_reference', array('cartodb_id', 'name', 'centroid', 'area_id as code', 'adm0_a3 as code', 'ST_AsGeoJSON(the_geom,20,4) as the_geom'), array('area_id' => $continentData->code), array('order' => array('cartodb_id' => 'asc')))
            ->getData();

            foreach($countries as $countryData)
            {
                $country = new Country();
                $country->setName($countryData->name);
                $country->setCartodbId($countryData->cartodb_id);
                $country->setCentroid($countryData->centroid);
                $country->setCode($countryData->code);
                $country->setContinent($continent);
                $country->setGeom($countryData->the_geom);

                $regions = $provider
//                ->getRowsForColumns('provinces_reference', array('cartodb_id', 'name', 'ST_AsGeoJSON(st_centroid(the_geom)) as centroid', 'sr_adm0_a3 as country_id', 'adm1_code as code'), array('sr_adm0_a3' => $countryData->code), array('order' => array('cartodb_id' => 'asc')))
                ->getRowsForColumns('provinces_reference', array('cartodb_id', 'name', 'centroid', 'sr_adm0_a3 as country_id', 'adm1_code as code', 'ST_AsGeoJSON(the_geom,20,4) as the_geom'), array('sr_adm0_a3' => $countryData->code), array('order' => array('cartodb_id' => 'asc')))
                ->getData();

                foreach($regions as $regionData)
                {
                    $region = new Region();
                    $region->setName($regionData->name);
                    $region->setCartodbId($regionData->cartodb_id);
                    $region->setCentroid($regionData->centroid);
                    $region->setCode($regionData->code);
                    $region->setCountry($country);
                    $region->setGeom($regionData->the_geom);

                    $manager->persist($region);
                    $manager->flush();
                }

                $manager->persist($country);
                $manager->flush();
            }

            $manager->persist($continent);
            $manager->flush();
        }
    }
    
    function getOrder()
    {
        return 4; // the order in which fixtures will be loaded
    }
}
