<?php

namespace Simbiotica\CalpBundle\DataFixtures\ORM;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Simbiotica\CalpBundle\Entity\Organization;
use Symfony\Component\Finder\Finder;

class OrganizationData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $files = Finder::create()
            ->name('*')
            ->in(__DIR__.'/../../../Resources/content/media');
        $mediaManager = $this->getMediaManager();
        
        foreach ($files as $file) {
            $organization = new Organization();
            $media = $mediaManager->create();
            $media->setBinaryContent($file);
            $media->setEnabled(true);
            $media->addUser($this->container->get('doctrine')->getManager()->getRepository('SimbioticaCalpBundle:User')->find(1));
            $organization->setMedia($media);
            
            switch ($file->getRelativePathname()) {
                case "acf.jpeg":
                    $organization->setName("ACF");
                    break;
                case "oxfam.jpeg":
                    $organization->setName("OXFAM");
                    break;
                case "si.jpeg":
                    $organization->setName("Solidarités International");
                    break;
                case "stc.jpeg":
                    $organization->setName("Save the Children");
                    break;
                case "unicef.png":
                    $organization->setName("UNICEF");
                    break;
                case "wfp.gif":
                    $organization->setName("WFP");
                    break;
                
                default:
                    continue;
                    break;
            }
            
            $mediaManager->save($media, 'organization', 'sonata.media.provider.image');
            $manager->persist($organization);
            $manager->flush();
        }
    }
    
    /**
     * @return \Sonata\MediaBundle\Model\MediaManagerInterface
     */
    public function getMediaManager()
    {
        return $this->container->get('sonata.media.manager.media');
    }

    function getOrder()
    {
            return 8; // the order in which fixtures will be loaded
    }
}
