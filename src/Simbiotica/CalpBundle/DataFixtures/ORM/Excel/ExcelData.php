<?php

namespace Simbiotica\CalpBundle\DataFixtures\ORM;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Finder\Finder;

class ExcelData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $files = Finder::create()
            ->name('*')
            ->in(__DIR__.'/../../../Resources/excel');
        $mediaManager = $this->getMediaManager();
        
        foreach ($files as $file) {
            $media = $mediaManager->create();
            $media->setBinaryContent($file);
            $media->setEnabled(true);
            
            $mediaManager->save($media, 'import', 'simbiotica.media.provider.excel');
            $manager->flush();
        }
    }
    
    /**
     * @return \Sonata\MediaBundle\Model\MediaManagerInterface
     */
    public function getMediaManager()
    {
        return $this->container->get('sonata.media.manager.media');
    }

    function getOrder()
    {
            return 1; // the order in which fixtures will be loaded
    }
}
