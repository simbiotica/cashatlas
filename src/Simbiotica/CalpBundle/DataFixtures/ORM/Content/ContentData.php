<?php

namespace Simbiotica\CalpBundle\DataFixtures\ORM;

use Simbiotica\CalpBundle\Entity\Content;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ContentData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /*
         * About the Cash Atlas
         */
        $cash_atlas_en = new Content();
        $cash_atlas_en->setName("The Cash Atlas");
        $cash_atlas_en->setTitle("About Cash Atlas");
        $cash_atlas_en->setContent(file_get_contents(__DIR__.'/../../../Resources/content/en/cashatlas.html'));
        $cash_atlas_en->setUri("cash-atlas");
        $cash_atlas_en->setDescription("info about the cash atlas");
        $cash_atlas_en->setKeywords("about cash atlas");
        $cash_atlas_en->setPublished(3);
        $cash_atlas_en->setLanguage("en_US");
        $manager->persist($cash_atlas_en);
        
        $cash_atlas_es = new Content();
        $cash_atlas_es->setName("The Cash Atlas");
        $cash_atlas_es->setTitle("About Cash Atlas");
        $cash_atlas_es->setContent(file_get_contents(__DIR__.'/../../../Resources/content/es/cashatlas.html'));
        $cash_atlas_es->setUri("sobre");
        $cash_atlas_es->setDescription("sobre");
        $cash_atlas_es->setKeywords("sobre");
        $cash_atlas_es->setPublished(2);
        $cash_atlas_es->setLanguage("es_ES");
        $cash_atlas_es->setTid($cash_atlas_en);
        $manager->persist($cash_atlas_es);
        
        $cash_atlas_fr = new Content();
        $cash_atlas_fr->setName("The Cash Atlas");
        $cash_atlas_fr->setTitle("About Cash Atlas");
        $cash_atlas_fr->setContent(file_get_contents(__DIR__.'/../../../Resources/content/fr/cashatlas.html'));
        $cash_atlas_fr->setUri("sobre");
        $cash_atlas_fr->setDescription("sobre");
        $cash_atlas_fr->setKeywords("sobre");
        $cash_atlas_fr->setPublished(2);
        $cash_atlas_fr->setLanguage("fr_FR");
        $cash_atlas_fr->setTid($cash_atlas_en);
        $manager->persist($cash_atlas_fr);
        
        
        /*
         * The data
         */
        $data_en = new Content();
        $data_en->setName("The Data");
        $data_en->setTitle("About the Data");
        $data_en->setContent(file_get_contents(__DIR__.'/../../../Resources/content/en/data.html'));
        $data_en->setUri("data");
        $data_en->setDescription("info about the data");
        $data_en->setKeywords("about data");
        $data_en->setPublished(3);
        $data_en->setLanguage("en_US");
        $manager->persist($data_en);
        
        
        /*
         * CaLP
         */
        $calp_en = new Content();
        $calp_en->setName("CaLP");
        $calp_en->setTitle("What is CaLP?");
        $calp_en->setContent(file_get_contents(__DIR__.'/../../../Resources/content/en/calp.html'));
        $calp_en->setUri("calp");
        $calp_en->setDescription("info about calp");
        $calp_en->setKeywords("about calp");
        $calp_en->setPublished(3);
        $calp_en->setLanguage("en_US");
        $manager->persist($calp_en);
        
        
        /*
         * Donors
         */
        $donors_en = new Content();
        $donors_en->setName("Donors");
        $donors_en->setTitle("Our Donors");
        $donors_en->setContent(file_get_contents(__DIR__.'/../../../Resources/content/en/donors.html'));
        $donors_en->setUri("donors");
        $donors_en->setDescription("info about donors");
        $donors_en->setKeywords("about donors");
        $donors_en->setPublished(3);
        $donors_en->setLanguage("en_US");
        $manager->persist($donors_en);
        
        
        /*
         * Contact
         */
        $contact_en = new Content();
        $contact_en->setName("Contact");
        $contact_en->setTitle("Contact us");
        $contact_en->setContent(file_get_contents(__DIR__.'/../../../Resources/content/en/contact.html'));
        $contact_en->setUri("contact");
        $contact_en->setDescription("contact");
        $contact_en->setKeywords("contact");
        $contact_en->setPublished(3);
        $contact_en->setLanguage("en_US");
        $manager->persist($contact_en);
        
        $manager->flush();
    }

    function getOrder()
    {
        return 7; // the order in which fixtures will be loaded
    }
}
