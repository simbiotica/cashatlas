<?php

namespace Simbiotica\CalpBundle\ContactProvider;

use Simbiotica\CalpBundle\Entity\Contact;
use Doctrine\ORM\EntityManager;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Request;

class GeneralContactProvider implements ContactProviderInterface
{
    protected $name;
    protected $entityManager;
    protected $securityContext;
    
    public function __construct($name, EntityManager $entityManager, SecurityContext $securityContext) {
        $this->name = $name;
        $this->entityManager = $entityManager;
        $this->securityContext = $securityContext;
    }
    
    public function setDefaults(Contact &$contact, Request $request) {
        $contact->addAssignedTo($this->entityManager->getRepository('SimbioticaCalpBundle:User')->find(11));
        $contact->setStatus(SimbioticaCalpBundle::CONTACT_STATUS_OPEN);
        $contact->setContactType($this->name);
        if($this->securityContext->getToken()->getUser() instanceof UserInterface)
        {
            $user = $this->securityContext->getToken()->getUser();
            $contact->setFirstname($user->getFirstname());
            $contact->setLastname($user->getLastname());
            $contact->setEmail($user->getEmail());
            $contact->setRequestedBy($user);
        }
    }
    
    public function getForm(FormBuilderInterface &$builder) {
        $builder
                ->add('firstname', null, array(
                    'label' => 'form.contact.firstname',
                    'required' => true,
                    'attr' => array('class' => 'required')
                ))
                ->add('lastname', null, array(
                    'label' => 'form.contact.lastname',
                    'required' => true,
                    'attr' => array('class' => 'required')
                ))
                ->add('email', null, array(
                    'label' => 'form.contact.email',
                    'required' => true,
                    'attr' => array('class' => 'required')
                ))
                ->add('content', null, array(
                    'label' => 'form.contact.content',
                    'required' => true,
                    'attr' => array('class' => 'required', 'rows' => 5),
                ));
    }
    
    public function handleSubmission(Contact &$contact) {
        
    }
    
    public function getTemplates() {
        return array();
    }
}
?>
