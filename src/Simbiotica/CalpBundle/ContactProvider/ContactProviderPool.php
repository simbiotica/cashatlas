<?php

namespace Simbiotica\CalpBundle\ContactProvider;

class ContactProviderPool {
    
    protected $providers = array();
    protected $contexts = array();
    
    public function addProvider($name, ContactProviderInterface $instance, $contexts = null)
    {
        $this->providers[$name] = $instance;
        if (is_array($contexts) && count($contexts)) {
            foreach($contexts as $context)
            {
                if (!array_key_exists($context, $this->contexts))
                    $this->contexts[$context] = array();
                $this->contexts[$context][$name] = $instance;
            }
        }
        elseif(is_string($contexts) && !empty ($contexts))
        {
            if (!array_key_exists($contexts, $this->contexts))
                $this->contexts[$contexts] = array();
            $this->contexts[$contexts][$name] = $instance;
        }
    }
    
    public function setProviders($providers)
    {
        $this->providers = $providers;
    }
    
    public function getProvider($name)
    {
        if(array_key_exists($name, $this->providers))
            return $this->providers[$name];
        else
            return null;
    }

    public function getProviders($context = null)
    {
        if (!context)
            return $this->providers;
        else
            return $this->contexts[$context];
    }
    
    public function getProviderNames($context = null)
    {
        if ($context && array_key_exists($context, $this->contexts))
            return array_keys($this->contexts[$context]);
        else
            return array_keys($this->providers);
    }
}

?>
