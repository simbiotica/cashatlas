<?php

namespace Simbiotica\CalpBundle\ContactProvider;

use Symfony\Component\Form\FormBuilderInterface;
use Simbiotica\CalpBundle\Entity\Contact;
use Symfony\Component\HttpFoundation\Request;

interface ContactProviderInterface {
    
    public function setDefaults(Contact &$contact, Request $request);
    
    public function handleSubmission(Contact &$contact);

    public function getForm(FormBuilderInterface &$builder);
    
    public function getTemplates();
}
?>
