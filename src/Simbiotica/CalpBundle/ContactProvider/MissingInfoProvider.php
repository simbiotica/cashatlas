<?php

namespace Simbiotica\CalpBundle\ContactProvider;

use Simbiotica\CalpBundle\Entity\Contact;
use Doctrine\ORM\EntityManager;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Request;

class MissingInfoProvider implements ContactProviderInterface
{
    protected $name;
    protected $projectManager;
    protected $securityContext;
    
    public function __construct($name, EntityManager $projectManager, SecurityContext $securityContext) {
        $this->name = $name;
        $this->entityManager = $projectManager;
        $this->securityContext = $securityContext;
    }
    
    public function setDefaults(Contact &$contact, Request $request) {
        $formData = $request->request->get('form', array());
        $projectId = $request->get('projectId', array_key_exists('project', $formData)?$formData['project']:null);
        
        if($projectId)
        {
            $project = $this->entityManager->getRepository('SimbioticaCalpBundle:Project')->find($projectId);

            $contact->setProject($project);

            $assignees = array();

            foreach($project->getOwners() as $user)
            {
                $assignees[$user->getId()] = $user;
            }
            foreach($project->getEditors() as $user)
            {
                $assignees[$user->getId()] = $user;
            }

            foreach ($project->getOrganizations() as $pos) 
            {
                if($pos->getOrganization())
                {
                    foreach ($pos->getOrganization()->getUsers() as $ou)
                    {
                        if ($ou->getEnabled() && $ou->getUser())
                        {
                            $assignees[$user->getId()] = $user;
                        }
                    }
                }
            }
            $contact->setAssignedTo($assignees);
        }
        
        $contact->setStatus(SimbioticaCalpBundle::CONTACT_STATUS_OPEN);
        $contact->setContactType($this->name);
        if($this->securityContext->getToken()->getUser() instanceof UserInterface)
        {
            $user = $this->securityContext->getToken()->getUser();
            $contact->setFirstname($user->getFirstname());
            $contact->setLastname($user->getLastname());
            $contact->setEmail($user->getEmail());
            $contact->setRequestedBy($user);
        }
    }
    
    public function getForm(FormBuilderInterface &$builder) {
        $builder
                ->add('firstname', null, array(
                    'label' => 'form.contact.firstname',
                    'required' => true,
                    'attr' => array('class' => 'required')
                ))
                ->add('lastname', null, array(
                    'label' => 'form.contact.lastname',
                    'required' => true,
                    'attr' => array('class' => 'required')
                ))
                ->add('email', null, array(
                    'label' => 'form.contact.email',
                    'required' => true,
                    'attr' => array('class' => 'required')
                ))
                ->add('content', null, array(
                    'label' => 'form.contact.content',
                    'required' => true,
                    'attr' => array('class' => 'required', 'rows' => 5),
                ))
                ->add('project', 'entity_hidden', array(
                    'class' => 'Simbiotica\CalpBundle\Entity\Project',
                ))
                ;
    }
    
    public function handleSubmission(Contact &$contact) {
        
    }
    
    public function getTemplates() {
        return array('success' => 'SimbioticaCalpBundle:Contact:missing_info_success.html.twig',);
    }
}
?>
