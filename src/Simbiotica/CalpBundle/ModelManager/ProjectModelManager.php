<?php

namespace Simbiotica\CalpBundle\ModelManager;

use Symfony\Component\HttpFoundation\Request;
use Sonata\DoctrineORMAdminBundle\Model\ModelManager;
use Simbiotica\CalpBundle\SimbioticaCalpBundle;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Sonata\NotificationBundle\Backend\BackendInterface;

class ProjectModelManager extends ModelManager {
    
    protected $request;
    protected $securityContext;
    protected $projectValidator;

    function __construct(
        RegistryInterface $registry,
        $securityContext, $projectValidator
    ) {
        parent::__construct($registry);
        $this->securityContext = $securityContext;
        $this->projectValidator = $projectValidator;
    }
    
    public function setRequest(Request $request = null) {
        $this->request = $request;
    }
    
    public function projectList() {
        $indexes = array(
                'continent',
                'country',
                'region',
                'organization',
                'donor',
                'implementingPartners',
                'sector',
                'modality',
                'deliveryMechanism',
                'deliveryAgent',
                'status',
                'years',
                'before',
                'after',
                'scaleMin',
                'scaleMax',
                'duration',
                'keyword',
                'owner',
        );
        
        $validParameters = array_intersect_key($this->request->query->all(), array_flip($indexes));

        $continent = array_key_exists('continent', $validParameters)?$validParameters['continent']:null;
        $country = array_key_exists('country', $validParameters)?$validParameters['country']:null;
        $region = array_key_exists('region', $validParameters)?$validParameters['region']:null;
        $organization = array_key_exists('organization', $validParameters)?$validParameters['organization']:null;
        $donor = array_key_exists('donor', $validParameters)?$validParameters['donor']:null;
        $implementingPartners = array_key_exists('implementingPartners', $validParameters)?$validParameters['implementingPartners']:null;
        $sector = array_key_exists('sector', $validParameters)?$validParameters['sector']:null;
        $modality = array_key_exists('modality', $validParameters)?$validParameters['modality']:null;
        $deliveryMechanism = array_key_exists('deliveryMechanism', $validParameters)?$validParameters['deliveryMechanism']:null;
        $deliveryAgent = array_key_exists('deliveryAgent', $validParameters)?$validParameters['deliveryAgent']:null;
        $status = array_key_exists('status', $validParameters)?$validParameters['status']:null;
        $years = array_key_exists('years', $validParameters)?$validParameters['years']:null;
        $scaleMin = array_key_exists('scaleMin', $validParameters)?$validParameters['scaleMin']:null;
        $scaleMax = array_key_exists('scaleMax', $validParameters)?$validParameters['scaleMax']:null;
        $duration = array_key_exists('duration', $validParameters)?$validParameters['duration']:null;
        $keyword = array_key_exists('keyword', $validParameters)?$validParameters['keyword']:null;
        $securityContext = (array_key_exists('owner', $validParameters) && $validParameters['owner'] == "true")?$this->securityContext:null;

        if(array_key_exists('before', $validParameters) && array_key_exists('year', $validParameters['before']) && array_key_exists('month', $validParameters['before']) && strcmp(implode('', $validParameters['before']), '') != 0)
            $before = $validParameters['before'];
        else
            $before = null;
        
        if(array_key_exists('after', $validParameters) && array_key_exists('year', $validParameters['after']) && array_key_exists('month', $validParameters['after']) && strcmp(implode('', $validParameters['after']), '') != 0)
            $after = $validParameters['after'];
        else
            $after = null;

        return $this->getEntityManager('SimbioticaCalpBundle:Project')->getRepository('SimbioticaCalpBundle:Project')
            ->searchProject($continent, $country, $region, $organization, $donor, $implementingPartners, 
                    $sector, $modality, $deliveryMechanism, $deliveryAgent, $status, 
                    $years, $before, $after, $scaleMin, $scaleMax, $duration, 
                    $keyword, $securityContext, $this->request->getLocale());
    }

}

?>
