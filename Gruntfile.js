'use strict';

module.exports = function (grunt) {

    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        app: {
            dir: 'src/Simbiotica/CalpBundle/Resources/public/'
        },
        jshint: {
            files: [
                'Gruntfile.js',
                '<%= app.dir %>app/scripts/login_register/*.js',
                '<%= app.dir %>app/scripts/application/{,*/}*{,*/}*.js',
                '<%= app.dir %>app/scripts/contact/*.js'
            ],
            options: {
                jshintrc: '.jshintrc'
            }
        },
        clean: {
            app: {
                files: [{
                    dot: true,
                    src: ['<%= app.dir %>.tmp']
                }]
            },
            dist: {
                files: [{
                    dot: true,
                    src: ['<%= app.dir %>.tmp', '<%= app.dir %>dist']
                }]
            }
        },
        requirejs: {
            options: {
                name: 'main',
                optimize: 'uglify',
                preserveLicenseComments: false,
                useStrict: true,
                wrap: true
            },
            login_register: {
                options: {
                    baseUrl: '<%= app.dir %>app/scripts/login_register',
                    mainConfigFile: '<%= app.dir %>app/scripts/login_register/main.js',
                    out: '<%= app.dir %>dist/scripts/login_register/main.js'
                }
            },
            application: {
                options: {
                    baseUrl: '<%= app.dir %>app/scripts/application',
                    mainConfigFile: '<%= app.dir %>app/scripts/application/main.js',
                    out: '<%= app.dir %>dist/scripts/application/main.js',
                    wrap: false
                }
            },
            contact: {
                options: {
                    baseUrl: '<%= app.dir %>app/scripts/contact',
                    mainConfigFile: '<%= app.dir %>app/scripts/contact/main.js',
                    out: '<%= app.dir %>dist/scripts/contact/main.js'
                }
            }
        },
        uglify: {
            options: {
                mangle: false
            },
            dist: {
                files: {
                    '<%= app.dir %>dist/vendor/cartodb.js': ['<%= app.dir %>vendor/cartodb.js/dist/cartodb.uncompressed.js'],
                    '<%= app.dir %>dist/vendor/require.js': ['<%= app.dir %>vendor/requirejs/require.js']
                }
            }
        },
        compass: {
            options: {
                sassDir: '<%= app.dir %>app/styles',
                cssDir: '<%= app.dir %>.tmp/styles',
                imagesDir: '<%= app.dir %>app/images/assets',
                generatedImagesDir: '<%= app.dir %>.tmp/images/assets',
                fontsDir: '<%= app.dir %>app/fonts',
                relativeAssets: true
            },
            app: {
                options: {
                    debugInfo: true,
                    environment: 'development'
                }
            },
            dist: {
                options: {
                    fontsDir: '<%= app.dir %>dist/fonts',
                    httpGeneratedImagesPath: '<%= app.dir %>dist/images/assets',
                    environment: 'production'
                }
            }
        },
        copy: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= app.dir %>app',
                    dest: '<%= app.dir %>dist',
                    src: [
                        'fonts/{,*/}*',
                        'lib/{,*/}*{,*/}*',
                        'templates/{,*/}*'
                    ]
                }]
            }
        },
        cssmin: {
            options: {
                report: 'min'
            },
            dist: {
                files: {
                    // Application
                    '<%= app.dir %>dist/styles/application/main.css': [
                        '<%= app.dir %>vendor/normalize-css/normalize.css',
                        '<%= app.dir %>vendor/cartodb.js/themes/css/cartodb.css',
                        '<%= app.dir %>vendor/jscrollpane/style/jquery.jscrollpane.css',
                        '<%= app.dir %>vendor/select2/select2.css',
                        '<%= app.dir %>app/lib/jquery-ui-1.10.3.custom/css/no-theme/jquery-ui-1.10.3.custom.css',
                        '<%= app.dir %>.tmp/styles/application/main.css'
                    ],
                    // Content
                    '<%= app.dir %>dist/styles/content/main.css': [
                        '<%= app.dir %>vendor/normalize-css/normalize.css',
                        '<%= app.dir %>vendor/select2/select2.css',
                        '<%= app.dir %>.tmp/styles/content/main.css'
                    ],
                    // Login register
                    '<%= app.dir %>dist/styles/login_register/main.css': [
                        '<%= app.dir %>vendor/normalize-css/normalize.css',
                        '<%= app.dir %>vendor/select2/select2.css',
                        '<%= app.dir %>.tmp/styles/login_register/main.css'
                    ],
                    // Export
                    '<%= app.dir %>dist/styles/export/main.css': ['<%= app.dir %>.tmp/styles/export/main.css'],
                    // Report
                    '<%= app.dir %>dist/styles/report/main.css': [
                        '<%= app.dir %>vendor/normalize-css/normalize.css',
                        '<%= app.dir %>vendor/select2/select2.css',
                        '<%= app.dir %>.tmp/styles/report/main.css'
                    ],
                    // Admin
                    '<%= app.dir %>dist/styles/admin/main.css': ['<%= app.dir %>.tmp/styles/admin/main.css']
                }
            }
        },
        imagemin: {
            options: {
                optimizationLevel: 3
            },
            images: {
                files: [{
                    expand: true,
                    cwd: '<%= app.dir %>app/images',
                    src: '{,*/}*{,*/}*.{png,jpg,jpeg}',
                    dest: '<%= app.dir %>dist/images'
                }]
            },
            sprites: {
                files: [{
                    expand: true,
                    cwd: '<%= app.dir %>.tmp/images/assets/sprites',
                    src: '{,*/}*.{png,jpg,jpeg}',
                    dest: '<%= app.dir %>dist/images/assets/sprites'
                }]
            }
        },
        watch: {
            compass: {
                files: ['<%= app.dir %>app/styles/{,*/}*.{scss,sass}'],
                tasks: ['compass:app']
            }
        }
    });

    grunt.registerTask('default', ['compass:app']);

    grunt.registerTask('test', [
        'jshint'
    ]);

    grunt.registerTask('build', [
        'jshint',
        'clean:dist',
        'copy:dist',
        'compass:dist',
        'requirejs',
        'uglify',
        'cssmin',
        'imagemin'
    ]);

};