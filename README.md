# Cash Atlas

## Requirements

* Php 5.4+
* Ruby 1.9.3+
* Node 0.10+

Checking available versions

	 php -v
	 ruby -v
	 node -v
 
Install globally libraries
 
    npm install -g grunt-cli bower

### Install browscap.ini php extension (Max OSX)

First, download extension from [http://browscap.org/stream?q=PHP_BrowsCapINI](http://browscap.org/stream?q=PHP_BrowsCapINI)

	sudo cp [route where extension has been downloaded]/php_browscap.ini /etc/apache2/extra/browscap.ini
	
	sudo echo "[browscap] \n browscap = /etc/php5/apache2/extra/browscap.ini" > /etc/php.ini


## To install 
In project folder


    curl -sS https://getcomposer.org/installer | php
    
    php composer.phar install
    
    npm install && bower install
    
    bundle install
    
   
## Before run

**First duplicate and rename `web/.htaccess.dist` to `web/.htaccess`.**

Now you can open your browser.

## On development

Development (web/app_dev.php)
    
    grunt

Production (web/app.php)
    
    grunt build

Testing

    grunt test

Watching

    grunt watch

# Deploy

    bundle exec cap deploy

# Dependencies

    ElasticSearch
    RabbitMQ with the following queues:
        - acl
        - comparison
        - mailer
        - nearDates
        - notification
        - sonata.media.create_thumbnail
    Supervisor with handlers for
        - acl
        - comparison
        - notification
        - thumbnails
