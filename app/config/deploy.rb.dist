set :application, "cashatlas"
set :domain,      ""
set :deploy_to,   "/var/www/cashatlas"

set :user, "root"
set :use_sudo, false

set :repository,  "git@bitbucket.org:simbiotica/cashatlas.git"
set :scm,         :git
ssh_options[:forward_agent] = false
set :deploy_via, :remote_cache
set  :keep_releases, 5

set :use_composer, true
set :update_vendors, false
set :vendors_mode, "install"
set :composer_options,  "--no-dev --verbose --prefer-dist --optimize-autoloader"

set :model_manager, "doctrine"
set :shared_files,      ["app/config/parameters.yml", "web/.htaccess"]
set :shared_children,   [app_path + "/logs", "vendor", app_path + "/sessions", "node_modules"]

role :web,        domain                         # Your HTTP server, Apache/etc
role :app,        domain, :primary => true       # This may be the same as your `Web` server
role :db,         domain, :primary => true


set :dump_assetic_assets, true
set :writable_dirs,       [app_path + "/cache", app_path + "/logs", app_path + "/sessions", "web/uploads", "src/Simbiotica/CalpBundle/Resources/translations"]
set :webserver_user,      "www-data"
set :permission_method,   :acl
set :use_set_permissions, true
set :assets_relative, true
set :copy_vendors, true
set :backup_path,  "sql"

before 'deploy:update_code', 'move:server_media'
before 'deploy:update_code', 'move:server_translations'
namespace :move do
  task :server_media do
    if Capistrano::CLI.ui.agree("Do you want to copy media from server to git ?: (y/N)")
        capifony_pretty_print "--> Move server media"
        find_servers_for_task(current_task).each do |current_server|
            run_locally "rsync -avz --delete #{user}@#{current_server.host}:#{deploy_to}/current/#{web_path}/uploads/ #{web_path}/uploads"
            run_locally "git reset"
            run_locally "git add #{web_path}/uploads/"
            run_locally "git commit -m 'capifony: server media' ; true"
            run_locally "git push -q"
        end
    end
    capifony_puts_ok
  end

  task :server_translations do
    if Capistrano::CLI.ui.agree("Do you want to copy translations from server to git ?: (y/N)")
        capifony_pretty_print "--> Move server translations"
        find_servers_for_task(current_task).each do |current_server|
            run_locally "rsync -avz --delete #{user}@#{current_server.host}:#{deploy_to}/current/src/Simbiotica/CalpBundle/Resources/translations/ src/Simbiotica/CalpBundle/Resources/translations"
            run_locally "git reset"
            run_locally "git add src/Simbiotica/CalpBundle/Resources/translations/"
            run_locally "git commit -m 'capifony: server translations' ; true"
            run_locally "git push -q"
        end
    end
    capifony_puts_ok
  end
end

after 'symfony:composer:install', 'npm:install'
namespace :npm do
  task :install do
    capifony_pretty_print "--> Installing NPM dependencies"
    run "#{try_sudo} sh -c 'cd #{latest_release} && npm install --loglevel error'"
    capifony_puts_ok
  end
end

after 'npm:install', 'bower:install'
namespace :bower do
  task :install do
    capifony_pretty_print "--> Installing JS libs with Bower"
    run "#{try_sudo} sh -c 'cd #{latest_release} && bower install --production --allow-root'"
    capifony_puts_ok
  end
end

after 'bower:install', 'grunt:build'
namespace :grunt do
  task :build do
    capifony_pretty_print "--> Compiling JS and CSS"
    run "#{try_sudo} sh -c 'cd #{latest_release} && grunt build'"
    capifony_puts_ok
  end
end

after "deploy:create_symlink", "symfony:clear_apc"
namespace :symfony do
  desc "Clear apc cache"
  task :clear_apc do
    capifony_pretty_print "--> Clear apc cache"
    run "#{try_sudo} sh -c 'cd #{latest_release} && #{php_bin} #{symfony_console} apc:clear --env=#{symfony_env_prod}'"
    capifony_puts_ok
  end
end

after "deploy:create_symlink", "supervisor:restart"
namespace :supervisor do
  desc "Restart Supervisor tasks"
  task :restart do
    capifony_pretty_print "--> Restart Supervisor tasks"
    # YOU MIGHT NEED TO ADD '-p <password>' HERE
    run "supervisorctl -u cashatlas restart calp_comparison"
    run "supervisorctl -u cashatlas restart calp_mailer"
    run "supervisorctl -u cashatlas restart calp_acl"
    run "supervisorctl -u cashatlas restart calp_thumbnails"
    run "supervisorctl -u cashatlas restart calp_notification"
    capifony_puts_ok
  end
end

after "deploy", "deploy:cleanup"

# Be more verbose by uncommenting the following line
# logger.level = Logger::MAX_LEVEL