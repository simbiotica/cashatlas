<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20130628170441 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE fos_user_user ADD created_by INT DEFAULT NULL, ADD updated_by INT DEFAULT NULL, ADD deleted_at DATETIME DEFAULT NULL");
        $this->addSql("ALTER TABLE fos_user_user ADD CONSTRAINT FK_C560D761DE12AB56 FOREIGN KEY (created_by) REFERENCES fos_user_user (id)");
        $this->addSql("ALTER TABLE fos_user_user ADD CONSTRAINT FK_C560D76116FE72E1 FOREIGN KEY (updated_by) REFERENCES fos_user_user (id)");
        $this->addSql("CREATE INDEX IDX_C560D761DE12AB56 ON fos_user_user (created_by)");
        $this->addSql("CREATE INDEX IDX_C560D76116FE72E1 ON fos_user_user (updated_by)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE fos_user_user DROP FOREIGN KEY FK_C560D761DE12AB56");
        $this->addSql("ALTER TABLE fos_user_user DROP FOREIGN KEY FK_C560D76116FE72E1");
        $this->addSql("DROP INDEX IDX_C560D761DE12AB56 ON fos_user_user");
        $this->addSql("DROP INDEX IDX_C560D76116FE72E1 ON fos_user_user");
        $this->addSql("ALTER TABLE fos_user_user DROP created_by, DROP updated_by, DROP deleted_at");
    }
}
