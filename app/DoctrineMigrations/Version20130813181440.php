<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20130813181440 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, requested_by INT DEFAULT NULL, assigned_to INT DEFAULT NULL, project_id INT DEFAULT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, language VARCHAR(6) NOT NULL, contact_type VARCHAR(255) DEFAULT NULL, firstname VARCHAR(50) DEFAULT NULL, lastname VARCHAR(50) DEFAULT NULL, email VARCHAR(50) DEFAULT NULL, data LONGTEXT NOT NULL COMMENT '(DC2Type:array)', content LONGTEXT DEFAULT NULL, status SMALLINT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_4C62E63818C491A5 (requested_by), INDEX IDX_4C62E63889EEAF91 (assigned_to), INDEX IDX_4C62E638166D1F9C (project_id), INDEX IDX_4C62E638DE12AB56 (created_by), INDEX IDX_4C62E63816FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("ALTER TABLE contact ADD CONSTRAINT FK_4C62E63818C491A5 FOREIGN KEY (requested_by) REFERENCES fos_user_user (id)");
        $this->addSql("ALTER TABLE contact ADD CONSTRAINT FK_4C62E63889EEAF91 FOREIGN KEY (assigned_to) REFERENCES fos_user_user (id)");
        $this->addSql("ALTER TABLE contact ADD CONSTRAINT FK_4C62E638166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)");
        $this->addSql("ALTER TABLE contact ADD CONSTRAINT FK_4C62E638DE12AB56 FOREIGN KEY (created_by) REFERENCES fos_user_user (id)");
        $this->addSql("ALTER TABLE contact ADD CONSTRAINT FK_4C62E63816FE72E1 FOREIGN KEY (updated_by) REFERENCES fos_user_user (id)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("DROP TABLE contact");
    }
}
