<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20130821115257 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE email_log CHANGE receiver receiver LONGTEXT NOT NULL COMMENT '(DC2Type:array)', CHANGE bcc bcc LONGTEXT DEFAULT NULL COMMENT '(DC2Type:array)', CHANGE cc cc LONGTEXT DEFAULT NULL COMMENT '(DC2Type:array)', CHANGE real_receiver real_receiver LONGTEXT NOT NULL COMMENT '(DC2Type:array)'");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE email_log CHANGE receiver receiver VARCHAR(255) NOT NULL, CHANGE bcc bcc VARCHAR(255) DEFAULT NULL, CHANGE cc cc VARCHAR(255) DEFAULT NULL, CHANGE real_receiver real_receiver VARCHAR(255) NOT NULL");
    }
}
