<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20130823161642 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE donor_translation DROP FOREIGN KEY FK_4004F5B9232D562B");
        $this->addSql("ALTER TABLE project_donor DROP FOREIGN KEY FK_C4A749093DD7B7A7");
        $this->addSql("DROP TABLE donor");
        $this->addSql("DROP TABLE donor_log");
        $this->addSql("DROP TABLE donor_translation");
        $this->addSql("DROP INDEX IDX_C4A749093DD7B7A7 ON project_donor");
        $this->addSql("ALTER TABLE project_donor DROP donor_id");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("CREATE TABLE donor (id INT AUTO_INCREMENT NOT NULL, updated_by INT DEFAULT NULL, created_by INT DEFAULT NULL, name VARCHAR(128) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_D7F24097DE12AB56 (created_by), INDEX IDX_D7F2409716FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE donor_log (id INT AUTO_INCREMENT NOT NULL, action VARCHAR(8) NOT NULL, logged_at DATETIME NOT NULL, object_id VARCHAR(64) DEFAULT NULL, object_class VARCHAR(255) NOT NULL, version INT NOT NULL, data LONGTEXT DEFAULT NULL COMMENT '(DC2Type:array)', username VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE donor_translation (id INT AUTO_INCREMENT NOT NULL, object_id INT DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, UNIQUE INDEX lookup_unique_idx (locale, object_id, field), INDEX IDX_4004F5B9232D562B (object_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("ALTER TABLE donor ADD CONSTRAINT FK_D7F2409716FE72E1 FOREIGN KEY (updated_by) REFERENCES fos_user_user (id)");
        $this->addSql("ALTER TABLE donor ADD CONSTRAINT FK_D7F24097DE12AB56 FOREIGN KEY (created_by) REFERENCES fos_user_user (id)");
        $this->addSql("ALTER TABLE donor_translation ADD CONSTRAINT FK_4004F5B9232D562B FOREIGN KEY (object_id) REFERENCES donor (id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE project_donor ADD donor_id INT DEFAULT NULL");
        $this->addSql("ALTER TABLE project_donor ADD CONSTRAINT FK_C4A749093DD7B7A7 FOREIGN KEY (donor_id) REFERENCES donor (id)");
        $this->addSql("CREATE INDEX IDX_C4A749093DD7B7A7 ON project_donor (donor_id)");
    }
}
