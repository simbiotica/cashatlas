<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20130729165919 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("CREATE TABLE similar_project (id INT AUTO_INCREMENT NOT NULL, similar_project_reference INT DEFAULT NULL, similar_project_target INT DEFAULT NULL, marked_by INT DEFAULT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, marked TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_9A3A41FA720D85DD (similar_project_reference), INDEX IDX_9A3A41FA823AD5C6 (similar_project_target), INDEX IDX_9A3A41FAB2C4DDBA (marked_by), INDEX IDX_9A3A41FADE12AB56 (created_by), INDEX IDX_9A3A41FA16FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE similar_project_log (id INT AUTO_INCREMENT NOT NULL, action VARCHAR(8) NOT NULL, logged_at DATETIME NOT NULL, object_id VARCHAR(64) DEFAULT NULL, object_class VARCHAR(255) NOT NULL, version INT NOT NULL, data LONGTEXT DEFAULT NULL COMMENT '(DC2Type:array)', username VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("ALTER TABLE similar_project ADD CONSTRAINT FK_9A3A41FA720D85DD FOREIGN KEY (similar_project_reference) REFERENCES project (id)");
        $this->addSql("ALTER TABLE similar_project ADD CONSTRAINT FK_9A3A41FA823AD5C6 FOREIGN KEY (similar_project_target) REFERENCES project (id)");
        $this->addSql("ALTER TABLE similar_project ADD CONSTRAINT FK_9A3A41FAB2C4DDBA FOREIGN KEY (marked_by) REFERENCES fos_user_user (id)");
        $this->addSql("ALTER TABLE similar_project ADD CONSTRAINT FK_9A3A41FADE12AB56 FOREIGN KEY (created_by) REFERENCES fos_user_user (id)");
        $this->addSql("ALTER TABLE similar_project ADD CONSTRAINT FK_9A3A41FA16FE72E1 FOREIGN KEY (updated_by) REFERENCES fos_user_user (id)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("DROP TABLE similar_project");
        $this->addSql("DROP TABLE similar_project_log");
    }
}
