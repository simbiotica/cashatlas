<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20130823120505 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE project_donor ADD organization_id INT DEFAULT NULL");
        $this->addSql("ALTER TABLE project_donor ADD CONSTRAINT FK_C4A7490932C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id)");
        $this->addSql("CREATE INDEX IDX_C4A7490932C8A3DE ON project_donor (organization_id)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE project_donor DROP FOREIGN KEY FK_C4A7490932C8A3DE");
        $this->addSql("DROP INDEX IDX_C4A7490932C8A3DE ON project_donor");
        $this->addSql("ALTER TABLE project_donor DROP organization_id");
    }
}
