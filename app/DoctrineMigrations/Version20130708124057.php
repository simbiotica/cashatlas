<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20130708124057 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE project DROP budget_overall_amount, DROP budget_overall_currency, DROP budget_allocated_amount, DROP budget_allocated_currency");
        $this->addSql("ALTER TABLE project_field ADD public TINYINT(1) NOT NULL");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE project ADD budget_overall_amount NUMERIC(10, 0) DEFAULT NULL, ADD budget_overall_currency VARCHAR(255) DEFAULT NULL, ADD budget_allocated_amount NUMERIC(10, 0) DEFAULT NULL, ADD budget_allocated_currency VARCHAR(255) DEFAULT NULL");
        $this->addSql("ALTER TABLE project_field DROP public");
    }
}
