<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20130729121921 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("CREATE TABLE project_user_revisable (project_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_6B68F7AC166D1F9C (project_id), INDEX IDX_6B68F7ACA76ED395 (user_id), PRIMARY KEY(project_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("ALTER TABLE project_user_revisable ADD CONSTRAINT FK_6B68F7AC166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)");
        $this->addSql("ALTER TABLE project_user_revisable ADD CONSTRAINT FK_6B68F7ACA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user_user (id)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("DROP TABLE project_user_revisable");
    }
}
