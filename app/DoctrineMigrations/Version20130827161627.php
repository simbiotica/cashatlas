<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20130827161627 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("CREATE TABLE contact_user (contact_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_A56C54B6E7A1254A (contact_id), INDEX IDX_A56C54B6A76ED395 (user_id), PRIMARY KEY(contact_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("ALTER TABLE contact_user ADD CONSTRAINT FK_A56C54B6E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE contact_user ADD CONSTRAINT FK_A56C54B6A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user_user (id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE contact DROP FOREIGN KEY FK_4C62E63889EEAF91");
        $this->addSql("DROP INDEX IDX_4C62E63889EEAF91 ON contact");
        $this->addSql("ALTER TABLE contact DROP assigned_to");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("DROP TABLE contact_user");
        $this->addSql("ALTER TABLE contact ADD assigned_to INT DEFAULT NULL");
        $this->addSql("ALTER TABLE contact ADD CONSTRAINT FK_4C62E63889EEAF91 FOREIGN KEY (assigned_to) REFERENCES fos_user_user (id)");
        $this->addSql("CREATE INDEX IDX_4C62E63889EEAF91 ON contact (assigned_to)");
    }
}
