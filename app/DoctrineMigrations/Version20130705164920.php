<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20130705164920 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("DROP TABLE exchange_rate");
        $this->addSql("DROP TABLE exchange_rate_log");
        $this->addSql("ALTER TABLE project ADD initial_scale INT DEFAULT NULL, CHANGE budget_overall_amount_calculated budget_overall VARCHAR(255) DEFAULT NULL, ADD initial_budget_overall VARCHAR(255) DEFAULT NULL, CHANGE budget_allocated_amount_calculated budget_allocated VARCHAR(255) DEFAULT NULL");
        $this->addSql("UPDATE project SET initial_scale = scale, initial_budget_overall = budget_overall");
        $this->addSql("ALTER TABLE project_location ADD location_city LONGTEXT DEFAULT NULL");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("CREATE TABLE exchange_rate (id INT AUTO_INCREMENT NOT NULL, updated_by INT DEFAULT NULL, created_by INT DEFAULT NULL, currency VARCHAR(255) NOT NULL, rate VARCHAR(255) NOT NULL, date DATE NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_E9521FABDE12AB56 (created_by), INDEX IDX_E9521FAB16FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE exchange_rate_log (id INT AUTO_INCREMENT NOT NULL, action VARCHAR(8) NOT NULL, logged_at DATETIME NOT NULL, object_id VARCHAR(64) DEFAULT NULL, object_class VARCHAR(255) NOT NULL, version INT NOT NULL, data LONGTEXT DEFAULT NULL COMMENT '(DC2Type:array)', username VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("ALTER TABLE exchange_rate ADD CONSTRAINT FK_E9521FAB16FE72E1 FOREIGN KEY (updated_by) REFERENCES fos_user_user (id)");
        $this->addSql("ALTER TABLE exchange_rate ADD CONSTRAINT FK_E9521FABDE12AB56 FOREIGN KEY (created_by) REFERENCES fos_user_user (id)");
        $this->addSql("ALTER TABLE project ADD budget_overall_amount NUMERIC(10, 0) DEFAULT NULL, ADD budget_overall_currency VARCHAR(255) DEFAULT NULL, ADD budget_overall_amount_calculated VARCHAR(255) DEFAULT NULL, ADD budget_allocated_amount NUMERIC(10, 0) DEFAULT NULL, ADD budget_allocated_currency VARCHAR(255) DEFAULT NULL, ADD budget_allocated_amount_calculated VARCHAR(255) DEFAULT NULL, DROP initial_scale, DROP budget_overall, DROP initial_budget_overall, DROP budget_allocated");
        $this->addSql("ALTER TABLE project_location DROP location_city");
    }
}
