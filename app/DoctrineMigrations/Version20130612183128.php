<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130612183128 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE project_location ADD country_id INT DEFAULT NULL");
        $this->addSql("ALTER TABLE project_location ADD CONSTRAINT FK_63D00210F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)");
        $this->addSql("CREATE INDEX IDX_63D00210F92F3E70 ON project_location (country_id)");
        $this->addSql("UPDATE project_location LEFT JOIN region ON project_location.region_id = region.id SET project_location.country_id = region.country_id");
        $this->addSql("INSERT INTO project_location (project_id, country_id) SELECT project_id, country_id FROM project_country");
        $this->addSql("DROP TABLE project_country");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("CREATE TABLE project_country (project_id INT NOT NULL, country_id INT NOT NULL, INDEX IDX_C56DC503166D1F9C (project_id), INDEX IDX_C56DC503F92F3E70 (country_id), PRIMARY KEY(project_id, country_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("ALTER TABLE project_country ADD CONSTRAINT FK_C56DC503166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE project_country ADD CONSTRAINT FK_C56DC503F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE project_location DROP FOREIGN KEY FK_63D00210F92F3E70");
        $this->addSql("DROP INDEX IDX_63D00210F92F3E70 ON project_location");
        $this->addSql("ALTER TABLE project_location DROP country_id");
    }
}
