<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20130820153810 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE email_log ADD bcc VARCHAR(255) DEFAULT NULL, ADD cc VARCHAR(255) DEFAULT NULL, ADD sender VARCHAR(255) NOT NULL, ADD real_receiver VARCHAR(255) NOT NULL, DROP realReceiver, CHANGE receiver receiver VARCHAR(255) NOT NULL");
        $this->addSql("ALTER TABLE media CHANGE context context VARCHAR(64) DEFAULT NULL");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE email_log ADD realReceiver VARCHAR(100) NOT NULL, DROP bcc, DROP cc, DROP sender, DROP real_receiver, CHANGE receiver receiver VARCHAR(100) NOT NULL");
        $this->addSql("ALTER TABLE media CHANGE context context VARCHAR(16) DEFAULT NULL");
    }
}
